#ifndef ZYXEL_UMAC_DATA_HOOK_HEADER_H
#define ZYXEL_UMAC_DATA_HOOK_HEADER_H

#define DHCP_SERVER_PORT   67
#define DHCP_CLIENT_PORT   68

#define DHCP_CHADDR_LENGTH           16
#define DHCP_SNAME_LENGTH            64
#define DHCP_FILE_LENGTH             128
#define DHCP_OPTIONS_LENGTH          312
#define DHCP_MAGIC_LENGTH            4

#define DHCPDISCOVER    1
#define DHCPOFFER       2
#define DHCPREQUEST     3
#define DHCPDECLINE     4
#define DHCPACK         5
#define DHCPNACK        6
#define DHCPRELEASE     7

#define DHCP_OPTION_MESSAGE_TYPE        53
#define DHCP_OPTION_HOST_NAME           12
#define DHCP_OPTION_BROADCAST_ADDRESS   28
#define DHCP_OPTION_REQUESTED_ADDRESS   50
#define DHCP_OPTION_LEASE_TIME          51
#define DHCP_OPTION_RENEWAL_TIME        58
#define DHCP_OPTION_REBINDING_TIME      59

#ifndef _NET_IF_UPPERPROTO_H_
#ifndef _NET_ETHERNET_H_
struct ether_header {
	u_int8_t    ether_dhost[6];
	u_int8_t    ether_shost[6];
	u_int16_t   ether_type;
} __packed;

struct ethervlan_header {
	u_int8_t    ether_dhost[6];
	u_int8_t    ether_shost[6];
	u_int16_t   vlan_TCI;
	u_int16_t   vlan_encapsulated_proto;
	u_int16_t   ether_type;
} __packed;
#endif
#endif

typedef struct dhcp_packet {
	u_int8_t op;		/* packet type */
	u_int8_t htype;		/* type of hardware address for this machine (Ethernet, etc) */
	u_int8_t hlen;		/* length of hardware address (of this machine) */
	u_int8_t hops;		/* hops */
	u_int32_t xid;		/* random transaction id number - chosen by this machine */
	u_int16_t secs;		/* seconds used in timing */
	u_int16_t flags;	/* flags */
	struct in_addr ciaddr;	/* IP address of this machine (if we already have one) */
	struct in_addr yiaddr;	/* IP address of this machine (offered by the DHCP server) */
	struct in_addr siaddr;	/* IP address of DHCP server */
	struct in_addr giaddr;	/* IP address of DHCP relay */
	unsigned char chaddr[DHCP_CHADDR_LENGTH];	/* hardware address of this machine */
	char sname[DHCP_SNAME_LENGTH];	/* name of DHCP server */
	char file[DHCP_FILE_LENGTH];	/* boot file name (used for diskless booting?) */
	char magic_cookie[DHCP_MAGIC_LENGTH];
	char options[0];
} dhcp_packet_t;

int zyUMAC_WAL_wdev_hook_rx_data_inspec(zyUMAC_wal_wdev_t wdev,
				   struct sk_buff *skb);

#endif
