#ifndef ZYXEL_UMAC_PVT_HEADER_H
#define ZYXEL_UMAC_PVT_HEADER_H

#include "zyUMAC_types.h"
#include "zyUMAC_wal_types.h"
#include "zyUMAC_stats.h"
#include "zyUMAC_node_survey.h"

#define FAIL_STATUS_DROP    0
#define FAIL_STATUS_REPORT  1
#define MAX_AC_SEND_MIN 10
#define MAX_VAP_ACL_LEN 128
#define ZY_SENDER_TYPE     0xffee

//#ifdef ZY_DOT11K_SUPPORT
#define MAX_A_BAND_CHANNEL_NUM 64
#define MAX_G_BAND_CHANNEL_NUM 14

#define MAX_RECORD_STA_PER_VAP 128

#define MAX_LEFT_STA_NUM 16

#define zyUMAC_IS_CHAN_2GHZ(_c) \
	((_c) >= 1 && (_c) <= 14)
#define zyUMAC_IS_CHAN_5GHZ(_c) \
	((_c) >= 36 && (_c) <= 165)

#ifndef MACSTR
#define MACSTR      "%02X:%02X:%02X:%02X:%02X:%02X"
#define MAC2STR(a)  (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#endif

enum {
	BAND_IDX_2G,	/*take care of swap*/
	BAND_IDX_5G,	/*TODO: tri-band*/
	BAND_IDX_MAX
};

/*
 * Reason/Status Codes not defined in IEEE Std 802.11
 */
//TODO: move SKS Reason/Status Codes also here, since should not be used directly in wlan driver layer
enum {
	WLAN_REASON_OTHER = 4500,
	WLAN_REASON_RSSI_KICKOUT_PROBE_COUNT = 4501,
	WLAN_REASON_RSSI_KICKOUT_NO_ACK = 4502,
	WLAN_REASON_ASSOCIATION_INACTIVE_TIMEOUT = 4503,
	WLAN_REASON_AGING_TIMER_TIMEOUT = 4504,
	WLAN_REASON_MANUAL_KICK_OUT = 4505,
	WLAN_REASON_OPEN_AUTHENTICATION_TIMEOUT = 4506,
	WLAN_REASON_1X_AUTHENTICATION_TIMEOUT = 4507,
	WLAN_REASON_1X_RADIUS_SERVER_REJECT = 4508,
	WLAN_REASON_RATE_KICKOUT_PROBE_COUNT = 4509,
	WLAN_REASON_RATE_KICKOUT_NO_ACK = 4510,
	WLAN_REASON_WATCH_DOG = 4511,
	WLAN_REASON_AP_RESTART = 4512,
	WLAN_REASON_DFS = 4513,
	WLAN_REASON_CLIENT = 4514,
	WLAN_REASON_ASSO_TABLE_OVER = 4515,
	WLAN_REASON_USBREBOOT = 4516,
};

typedef struct sta_leave_rpt_info {
	int reason;
	int onlinetime;
	u_int16_t sender;
	u_int16_t is_passive;
	char reserve[16];
} sta_leave_rpt_info_t;

typedef struct sta_leave_entry {
	int32_t reason;
	int32_t online_time;	//min
	u_int16_t sender;
	u_int16_t frame_type;
	u_int16_t report_flag;
	struct timeval record_time;
	uint8_t mac[6];
} sta_leave_entry_t;

typedef struct sta_leave_args {
	int32_t reason;
	u_int16_t sender;
	u_int16_t frame_type;
	u_int8_t is_passive;
	int32_t online_time;
	u_int8_t event_type;
} sta_leave_args_t;

typedef struct sta_leave_entry_mgr {
	spinlock_t leave_lock;
	short int size;
	short int next_entry_num;
	sta_leave_entry_t entry[MAX_RECORD_STA_PER_VAP];
} sta_leave_entry_mgr_t;

typedef struct zyUMAC_ieee80211_bl {
	osdev_t bl_osdev;
	spinlock_t bl_lock;
	int bl_policy;
	int bl_quickage;
	int bl_age_time;	//in seconds
	u_int32_t bl_ticks;
	u_int32_t bl_entries;
	//TAILQ_HEAD(, ieee80211_bl_entry)    bl_list; /* list of all acl_entries */
	//ATH_LIST_HEAD(, ieee80211_bl_entry) bl_hash[BL_HASHSIZE];
} zyUMAC_ieee80211_bl_t;

typedef struct zyUMAC_txrx_fw_stats {
	u_int32_t ic_tx_duration;
	u_int32_t ic_rx_duration;
	u_int32_t ic_rx_in_bss_duration;
	u_int32_t ic_rx_ou_bss_duration;
} zyUMAC_txrx_fw_stats_t;

typedef struct zyUMAC_left_node_stats {
	u_int8_t mac[MAC_ADDR_LEN];
	zyUMAC_wal_station_txrx_stats_t stats;
} zyUMAC_left_node_stats_t;

typedef struct zyUMAC_left_node_table {
	u_int16_t idx;
	zyUMAC_left_node_stats_t node[MAX_LEFT_STA_NUM];
} zyUMAC_left_node_table_t;

struct __zyUMAC_scn_priv {
	struct zyUMAC_wal_priv scn_wal_pri;
	zyUMAC_wal_wphy_t scn_dev;
	struct proc_dir_entry *scn_proc;
	u_int32_t scn_anti_attack_threshold;
	u_int32_t scn_anti_attack_interval;
	u_int8_t scn_anti_attack_enable;
	u_int8_t scn_anti_attack_dbglist;

	/*switch of black-list function, 0-disable, 1-enable */
	u_int8_t scn_bl_enable;
	zyUMAC_ieee80211_bl_t scn_bl;
	zyUMAC_scn_priv_radio_stats_t scn_radio_stats;

	u_int8_t ic_assoc_min_rssi_access_ctl;	/*assoc min rssi access contrl for AP */
	u_int8_t ic_min_rssi_check_enable;	/*DUMMY in orig WAVE2 low rssi check enable */
	u_int16_t ic_assoc_min_rssi;	/*assoc min rssi for AP */
	u_int16_t ic_assoc_min_rssi_freq;	/*DUMMY in orig WAVE2 assoc min rssi freq for AP */
	u_int16_t ic_assoc_min_rssi_times;	/*DUMMY in orig WAVE2 assoc min rssi times for AP */

	u_int8_t ic_assnoresp;	/*assoc no resp at max_assoc reached */
	u_int8_t current_channel;  /* current channel */
	u_int32_t ic_frgblackto;	/*declared black-list timeout */
	u_int32_t ic_thrpktlen;	/*min packet length of throughput */
	u_int16_t ic_probreq_rpt_rssi;

	zyUMAC_txrx_fw_stats_t txrx_fw_stats;
	u_int32_t bdst_client_count;
	u_int8_t node_survey_en;
	zyUMAC_survey_db_t note_survey_db;
};

typedef struct {
	unsigned char num_channel_rep_a;
	unsigned char channel_rep_a[MAX_A_BAND_CHANNEL_NUM];
	unsigned char change_flag_a;
	unsigned char num_channel_rep_g;
	unsigned char channel_rep_g[MAX_G_BAND_CHANNEL_NUM];
	unsigned char change_flag_g;

	unsigned char num_beacon_rep_a;
	unsigned char beacon_rep_a[MAX_A_BAND_CHANNEL_NUM];
	unsigned char num_beacon_rep_g;
	unsigned char beacon_rep_g[MAX_G_BAND_CHANNEL_NUM];

	unsigned char beacon_rep_mode;
	unsigned char beacon_rep_interval;

	unsigned char dot11k_eb;
	unsigned char dot11v_transition_eb;
	unsigned char roam_assist_eb;
	unsigned char base_snr;
} v_wlan_dot11k_t;
//#endif

typedef struct sender_info {
	u_int16_t type;
	u_int16_t sender;
} sender_info_t;

typedef struct kick_sta_info {
	unsigned short reason;
	unsigned char mac[6];
} kick_sta_info_t;

//#if ZY_BDST_SUPPORT
struct vap_config {
	int band_steering;
	int band_steering_mode;
	int band_steering_time;
	int band_steering_count;
	int band_steering_hash;
	int band_steering_debug;
};
//#endif

typedef struct _vap_zytats_netlink_event {
	u_int64_t rx_fcserr;
	u_int64_t rx_micerr;
	zyUMAC_vap_priv_stats_t stats;
} zyUMAC_stats_vap_netlink_event;

struct __zyUMAC_vap_priv {
	struct zyUMAC_wal_vap_priv wal_vap_priv;

	sta_leave_entry_mgr_t *iv_sta_leave_reason_tab;
	sta_leave_entry_mgr_t *iv_sta_fail_status_tab;

	char vap_acl_file[MAX_VAP_ACL_LEN];

	os_timer_t loadbalance_timer;

	zyUMAC_vap_priv_stats_t is_stats;
//#ifdef ZY_BDST_SUPPORT
	struct vap_config app_config;
//#endif
//#ifdef CONFIG_DP_SUPPORT
	u_int8_t disable_special_ssid;
	u_int8_t tunnel;
	u_int8_t local_dhcp;
	u_int8_t local_assoc;
	u_int8_t enable_web_auth;
	u_int16_t vlan;
//#endif
//#ifdef ZY_DOT11K_SUPPORT
	v_wlan_dot11k_t dot11k;
//#endif

	u_int8_t prbreqlimit;
	u_int8_t assreqlimit;
	u_int8_t authreqlim;
	u_int8_t authignore;
	u_int8_t maxclientlimit;
	u_int8_t frgkickoutpower;
	u_int16_t frgrate;
	u_int8_t frgrcnt;
	u_int8_t anyprbreq;
	u_int8_t frgblackto;
	u_int8_t txpower;	/*txpower for JAPAN !W52 channel in 0.5dBm */
	u_int8_t txw52power;	/*txpower for JAPAN W52 channel in 0.5dBm */

	zyUMAC_stats_vap_netlink_event  vap_netlink_event;
	zyUMAC_left_node_table_t left_node_tbl;
};

typedef struct _sta_ZY_stats_netlink_event {
	u_int8_t mac[IEEE80211_ADDR_LEN];
	u_int8_t reason;
	u_int8_t type;
	u_int32_t radio_type;
	u_int16_t assoc_id;
	zyUMAC_node_priv_stats_t stats;
} zyUMAC_stats_node_netlink_event;

struct __zyUMAC_node_priv {
	struct zyUMAC_wal_wnode_priv wal_wnode_priv;
	zyUMAC_node_priv_stats_t ns_stats;
	int skleave_report;
	u_int16_t skleave_reason;
	sta_leave_rpt_info_t leave_info;
	zyUMAC_stats_node_netlink_event node_netlink_event;
};

#endif
