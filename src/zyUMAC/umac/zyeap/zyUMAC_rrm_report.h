#ifndef __ZYXEL_UMAC_RRM_REPORT_HEAD__
#define __ZYXEL_UMAC_RRM_REPORT_HEAD__

#ifdef ZY_DOT11K_SUPPORT
void zyUMAC_report_dot11k_beacon_table(wlan_if_t vap);
int zyUMAC_rrm_channel_update(wlan_if_t vap,
			     ieee80211_rrm_beaconreq_info_t * bcnrpt);

#define MAX_DOT11K_TABLE_SIZE 256
typedef struct {
	unsigned char bss_mac[6];
	unsigned char rcpi;
	unsigned char rsni;
	unsigned char channel;
	unsigned char reg_class;
	unsigned char phy_type;
	unsigned char reserve[5];
} __packed dot11k_element_t;

typedef struct {
	unsigned short count;
	dot11k_element_t dot11k_info[MAX_DOT11K_TABLE_SIZE];
} __packed dot11k_msg_info;

typedef struct {
	unsigned char ifname[16];
	unsigned char vap_mac[6];
	unsigned char sta_mac[6];
	/* take from RRM Enabled Capabilities.
	 * bit0: Beacon Passive Measurement capability enabled(IEEE 802.11 is bit4);
	 * bit1: Beacon Active Measurement capability enabled(IEEE 802.11 is bit5);
	 * bit2: Beacon Table Measurement capability enabled(IEEE 802.11 is bit6);
	 */
	unsigned char rrm_en_cap;
	/* take from Extended Capabilities.
	 * bit0: BSS Transition(IEEE 802.11 is bit19)
	 */
	unsigned char extend_cap;
	//unsigned int  timestamp;
	unsigned short count;
} __packed dot11k_msg_head;
#endif /* ZY_DOT11K_SUPPORT */

#endif
