#include "zyUMAC.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_common_var.h"
#include "zyUMAC_genl.h"

#ifdef ZY_LOAD_BALANCE_SUPPORT

extern void ieee80211_update_sta_inout_statics(struct ieee80211com *ic,
					       u_int64_t * sta_sum_rx_bytes,
					       u_int64_t * sta_sum_tx_bytes);

static int assemble_traffic_report(unsigned char *report, uint32_t if_index,
				   uint32_t max_speed, uint32_t in_rate,
				   uint32_t out_rate)
{
	uint32_t temp = 0, offset = 0;

	//if_index
	temp = htonl(if_index + 1);	//'+1' because china mobile use radio 1~31. 
	memcpy(report, &temp, sizeof(temp));
	offset += sizeof(temp);

	//speed
	temp = htonl(max_speed);
	memcpy(report + offset, &temp, sizeof(temp));
	offset += sizeof(temp);

	//in_rate
	temp = htonl(in_rate);
	memcpy(report + offset, &temp, sizeof(temp));
	offset += sizeof(temp);

	//out_rate
	temp = htonl(out_rate);
	memcpy(report + offset, &temp, sizeof(temp));
	offset += sizeof(temp);

	return 0;
}

void ieee80211_update_sta_inout_statics(struct ieee80211com *ic,
					u_int64_t * sta_sum_rx_bytes,
					u_int64_t * sta_sum_tx_bytes)
{
	struct ieee80211_node_table *nt = &ic->ic_sta;
	struct ieee80211_node *ni = NULL;
	rwlock_state_t lock_state;

#ifdef UMAC_SUPPORT_CFG80211
	struct ieee80211_nodestats node_stats_user = { 0 };
#endif

	OS_BEACON_DECLARE_AND_RESET_VAR(flags);

	OS_BEACON_WRITE_LOCK(&nt->nt_nodelock, &lock_state, flags);
	TAILQ_FOREACH(ni, &nt->nt_node, ni_list) {
		if (ni == ni->ni_vap->iv_bss) {
			continue;
		}
//TODO: needs to check current implementation
#ifdef UMAC_SUPPORT_CFG80211

		if (wlan_get_peer_dp_stats(ic,
					   ni->peer_obj,
					   &node_stats_user) != 0) {
			OS_BEACON_WRITE_UNLOCK(&nt->nt_nodelock, &lock_state,
					       flags);
			return;
		}
		*sta_sum_rx_bytes = node_stats_user.ns_rx_bytes;
		*sta_sum_tx_bytes = node_stats_user.ns_tx_bytes;
#else
#if 0
		*sta_sum_rx_bytes +=
		    ni->ni_stats.ns_rx_bytes - ni->ni_stats.ns_rx_bytes;
		*sta_sum_tx_bytes +=
		    ni->ni_stats.ns_tx_bytes - ni->ni_stats.ns_tx_bytes;

		/* update save statics */
		ni->ni_stats.ns_rx_bytes = ni->ni_stats.ns_rx_bytes;
		ni->ni_stats.ns_tx_bytes = ni->ni_stats.ns_tx_bytes;
#endif
		*sta_sum_rx_bytes = ni->ni_stats.ns_rx_bytes;
		*sta_sum_tx_bytes = ni->ni_stats.ns_tx_bytes;
#endif
	}

	OS_BEACON_WRITE_UNLOCK(&nt->nt_nodelock, &lock_state, flags);
	return;
}

void zyUMAC_loadbalance_report_handle(struct ieee80211vap *vap)
{
	uint32_t in_data_rate, out_data_rate;	//data_rate, in kbps.
	uint32_t phy_max_rate = 300;	//max phy rate, in Mbps.
	unsigned char report[16];	//report len is 16 bytes.
	u_int64_t tx_temp = 0, rx_temp = 0;
	uint32_t rem;

	wlan_dev_t ic = wlan_vap_get_devhandle(vap);
	if (!ic) {
		qdf_print("Invalid ic ptr");
		return;
	}

	/* get interface phy bitrate */
	if (vap && vap->iv_bss) {
		phy_max_rate = ic->ic_get_maxphyrate(ic, vap->iv_bss);
		phy_max_rate = phy_max_rate / 1000;
		if (0 == phy_max_rate) {
			phy_max_rate = 300;
		}
	}

	ieee80211_update_sta_inout_statics(ic, &rx_temp, &tx_temp);

	//do the devision: rate = ic_rx_bytes *8 /1024 / time.      
	rem =
	    do_div(rx_temp,
		   (uint32_t) (128 * TIMEOUT_LOAD_BALANCE_REPORT_DEFAULT));
	in_data_rate = (uint32_t) rx_temp;

	if (rem >= 128 * TIMEOUT_LOAD_BALANCE_REPORT_DEFAULT / 2)	//if the remainder is biger than 0.5, round up.
	{
		in_data_rate += 1;
	}

	rem =
	    do_div(tx_temp,
		   (uint32_t) (128 * TIMEOUT_LOAD_BALANCE_REPORT_DEFAULT));
	out_data_rate = (uint32_t) tx_temp;

	if (rem >= 128 * TIMEOUT_LOAD_BALANCE_REPORT_DEFAULT / 2)	//if the remainder is biger than 0.5, round up.
	{
		out_data_rate += 1;
	}

	assemble_traffic_report(report, ic->interface_id, phy_max_rate,
				in_data_rate, out_data_rate);

	zyUMAC_genl_event_report((void *)report, sizeof(report),
				zyUMAC_MCGRP_STA_INFO, zyUMAC_E_TRAFFIC_REPORT,
				zyUMAC_A_TRAFFIC_REPORT);
	return;
}

static OS_TIMER_FUNC(zyUMAC_loadbalance_timer)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	struct ieee80211vap *vap;

	OS_GET_TIMER_ARG(vap, struct ieee80211vap *);
	ASSERT(vap != NULL);

	zyUMAC_vap_priv = vap->iv_zyUMAC_vap_priv;

	/* load_balance report radio static handle function */
	zyUMAC_loadbalance_report_handle(vap);

	if (zyUMAC_vap_priv) {
		OS_SET_TIMER(&zyUMAC_vap_priv->loadbalance_timer,
			     TIMEOUT_LOAD_BALANCE_REPORT_DEFAULT * 1000);
	}
}

void zyUMAC_loadbalance_attach(zyUMAC_wal_wdev_t vap)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(vap);
	ASSERT(zyUMAC_vap_priv != NULL);

	OS_INIT_TIMER(vap->iv_ic->ic_osdev,
		      &(zyUMAC_vap_priv->loadbalance_timer),
		      zyUMAC_loadbalance_timer, (void *)vap,
		      QDF_TIMER_TYPE_WAKE_APPS);

	OS_SET_TIMER(&(zyUMAC_vap_priv->loadbalance_timer),
		     TIMEOUT_LOAD_BALANCE_REPORT_DEFAULT * 1000);
}

void zyUMAC_loadbalance_detach(zyUMAC_wal_wdev_t vap)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(vap);
	ASSERT(zyUMAC_vap_priv != NULL);

	OS_FREE_TIMER(&(zyUMAC_vap_priv->loadbalance_timer));
}

#else

void zyUMAC_loadbalance_attach(zyUMAC_wal_wdev_t vap)
{
}

void zyUMAC_loadbalance_detach(zyUMAC_wal_wdev_t vap)
{
}

#endif
