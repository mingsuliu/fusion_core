/*
 * Zyxel  Micro UMAC module (zyUMAC).
 */

//#ifndef EXPORT_SYMTAB
//#define EXPORT_SYMTAB
//#endif

//#include <linux/version.h>
#include <linux/module.h>
#include <linux/netdevice.h>

#include "zyUMAC.h"
#include "zyUMAC_vap.h"
#include "zyUMAC_stats.h"
#include "zyUMAC_genl.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_event.h"
#include "zyUMAC_band_steering.h"
#include "zyUMAC_node_survey.h"

/*
 * Replace all necessary ic function pointer here, call from __ol_ath_attach of qca_ol module
 */
int zyUMAC_umac_attach(zyUMAC_wal_wphy_t wphy)
{
	struct net_device *netdev;
	zyUMAC_scn_priv_t *scn_priv;

	scn_priv = zyUMAC_wal_wphy_getdata(wphy);
	if (scn_priv == NULL) {
		scn_priv = kmalloc(sizeof(zyUMAC_scn_priv_t), GFP_KERNEL);
	}

	if (scn_priv == NULL) {
		zyUMAC_LOG(LOG_ERR, "scn_priv is null");
		return -1;
	}

	memset(scn_priv, 0, sizeof(zyUMAC_scn_priv_t));
	scn_priv->scn_dev = wphy;
	zyUMAC_wal_wphy_setdata(wphy, scn_priv);

	netdev = zyUMAC_wal_wphy_to_netdev(wphy);
	if (netdev)
		zyUMAC_LOG(LOG_INFO, "%s attach", netdev->name);
	else
		zyUMAC_LOG(LOG_ERR, "can't find wphy's netdev");

	zyUMAC_phy_ioctl_attach(wphy);
	zyUMAC_vap_init(wphy);
	zyUMAC_proc_radio_attach(wphy);
	zyUMAC_node_survey_init(wphy);


	//initialize rssi related parameters
	scn_priv->ic_assoc_min_rssi = 20;
	scn_priv->ic_assoc_min_rssi_freq = 5;
	scn_priv->ic_assoc_min_rssi_times = 3;
	scn_priv->ic_thrpktlen = 512;

	return 0;
}

EXPORT_SYMBOL(zyUMAC_umac_attach);

/*
 * Release alloc memory
 */
void zyUMAC_umac_detach(zyUMAC_wal_wphy_t wphy)
{
	struct net_device *netdev;
	zyUMAC_scn_priv_t *scn_priv;

	netdev = zyUMAC_wal_wphy_to_netdev(wphy);
	if (netdev)
		zyUMAC_LOG(LOG_INFO, "%s detach", netdev->name);
	else
		zyUMAC_LOG(LOG_ERR, "can't find wphy's netdev");

	zyUMAC_node_survey_deinit(wphy);
	zyUMAC_vap_ioctl_detach();
	zyUMAC_proc_radio_detach(wphy);
	zyUMAC_phy_ioctl_detach(wphy);

	scn_priv = zyUMAC_wal_wphy_getdata(wphy);
	kfree(scn_priv);
	zyUMAC_wal_wphy_setdata(wphy, NULL);
}

EXPORT_SYMBOL(zyUMAC_umac_detach);

int zyUMAC_umac_init(void)
{
	zyUMAC_LOG(LOG_INFO, "Init zyUMAC");
	zyUMAC_bdst_init();
	zyUMAC_genl_init();
	zyUMAC_umac_event_handler_attach();
	return 0;
}

EXPORT_SYMBOL(zyUMAC_umac_init);

void zyUMAC_umac_exit(void)
{
	zyUMAC_LOG(LOG_INFO, "Exit zyUMAC");
	zyUMAC_umac_event_handler_detach();
	zyUMAC_genl_exit();
	zyUMAC_bdst_exit();
}

EXPORT_SYMBOL(zyUMAC_umac_exit);
