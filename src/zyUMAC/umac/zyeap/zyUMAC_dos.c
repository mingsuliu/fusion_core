#include "zyUMAC.h"
#include "zyUMAC_common_var.h"
#include "zyUMAC_events.h"
#include "zyUMAC_genl.h"
#include "zyUMAC_wal_api.h"

int zyUMAC_dos_is_enabled(zyUMAC_wal_wdev_t vap)
{
	zyUMAC_wal_wphy_t scn = zyUMAC_wal_wdev_get_wphy(vap);
	zyUMAC_scn_priv_t *zyUMAC_scn_priv =
	    (zyUMAC_scn_priv_t *) zyUMAC_wal_wphy_getdata(scn);

	return zyUMAC_scn_priv->scn_anti_attack_enable;
}

int zyUMAC_dos_handler(zyUMAC_wal_wdev_t vap, int frametype,
		      u_int8_t *stamac, int rssi)
{
	zyUMAC_nl_event_dos_t event;

	event.frametype = frametype;
	memcpy(event.stamac, stamac, IEEE80211_ADDR_LEN);
	zyUMAC_genl_event_report(&event, sizeof(event), zyUMAC_MCGRP_MLME,
				zyUMAC_E_DOS, zyUMAC_A_DOS);
	return 0;
}

void zyUMAC_dos_process(zyUMAC_wal_wnode_t ni, zyUMAC_ieee80211_header_t *wh,
		       int subtype, int rssi)
{
	zyUMAC_wal_wdev_t vap = zyUMAC_wal_wnode_get_wdev(ni);

	if (subtype == IEEE80211_FC0_SUBTYPE_PROBE_RESP ||
	    subtype == IEEE80211_FC0_SUBTYPE_BEACON ||
	    subtype == IEEE80211_FC0_SUBTYPE_PROBE_REQ)
		return;

	if (zyUMAC_dos_is_enabled(vap)  &&
	    IEEE80211_ADDR_EQ(
		    zyUMAC_wal_wdev_get_macaddr(zyUMAC_wal_wnode_get_wdev(ni)),
		    zyUMAC_wal_wnode_get_macaddr(ni))) {
		zyUMAC_dos_handler(vap, subtype,
				  zyUMAC_wal_ieee80211_header_get_addr2(wh),
				  rssi);
	}

}
