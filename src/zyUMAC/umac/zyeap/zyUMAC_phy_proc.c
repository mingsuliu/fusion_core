#include "zyUMAC_proc.h"
#include <linux/kernel.h>
#include <linux/proc_fs.h>

#include "zyUMAC_wal_api.h"
#include "zyUMAC_umac.h"
#include "zyUMAC_stats.h"

static int proc_anti_attack_enable_show(struct seq_file *m, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) m->private;
	seq_printf(m, "%d\n", scn_priv->scn_anti_attack_enable);
	return 0;
}

ssize_t proc_anti_attack_enable_write(struct file * file, const char *buffer,
				      size_t count, loff_t * ppos)
{
	int rc;
	zyUMAC_scn_priv_t *scn_priv =
	    (zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));

	rc = kstrtouint_from_user(buffer, count, 0,
				  (unsigned int *)&scn_priv->
				  scn_anti_attack_enable);
	if (rc == 0) {
		return count;
	} else {
		return 0;
	}
}

static int proc_anti_attack_threshold_show(struct seq_file *m, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) m->private;
	seq_printf(m, "%d\n", scn_priv->scn_anti_attack_threshold);
	return 0;
}

ssize_t proc_anti_attack_threshold_write(struct file * file, const char *buffer,
					 size_t count, loff_t * ppos)
{
	int rc;
	zyUMAC_scn_priv_t *scn_priv =
	    (zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));

	rc = kstrtouint_from_user(buffer, count, 0,
				  (unsigned int *)&scn_priv->
				  scn_anti_attack_threshold);
	if (rc == 0) {
		return count;
	} else {
		return 0;
	}
}

static int proc_anti_attack_interval_show(struct seq_file *m, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) m->private;
	seq_printf(m, "%d\n", scn_priv->scn_anti_attack_interval);
	return 0;
}

ssize_t proc_anti_attack_interval_write(struct file * file, const char *buffer,
					size_t count, loff_t * ppos)
{
	int rc;
	zyUMAC_scn_priv_t *scn_priv =
	    (zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));

	rc = kstrtouint_from_user(buffer, count, 0,
				  (unsigned int *)&scn_priv->
				  scn_anti_attack_interval);
	if (rc == 0) {
		return count;
	} else {
		return 0;
	}
}

static int proc_bl_age_time_show(struct seq_file *m, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) m->private;
	seq_printf(m, "%d\n", scn_priv->scn_bl.bl_age_time);
	return 0;
}

ssize_t proc_bl_age_time_write(struct file * file, const char *buffer,
			       size_t count, loff_t * ppos)
{
	int rc;
	zyUMAC_scn_priv_t *scn_priv =
	    (zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));

	rc = kstrtouint_from_user(buffer, count, 0,
				  (unsigned int *)&scn_priv->scn_bl.
				  bl_age_time);
	if (rc == 0) {
		return count;
	} else {
		return 0;
	}
}

static int proc_bl_policy_show(struct seq_file *m, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) m->private;
	seq_printf(m, "%d\n", scn_priv->scn_bl.bl_policy);
	return 0;
}

ssize_t proc_bl_policy_write(struct file * file, const char *buffer,
			     size_t count, loff_t * ppos)
{
	int rc;
	zyUMAC_scn_priv_t *scn_priv =
	    (zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));

	rc = kstrtouint_from_user(buffer, count, 0,
				  (unsigned int *)&scn_priv->scn_bl.bl_policy);
	if (rc == 0) {
		return count;
	} else {
		return 0;
	}
}

static int proc_bl_enable_show(struct seq_file *m, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) m->private;
	seq_printf(m, "%d\n", scn_priv->scn_bl_enable);
	return 0;
}

ssize_t proc_bl_enable_write(struct file * file, const char *buffer,
			     size_t count, loff_t * ppos)
{
	int rc;
	zyUMAC_scn_priv_t *scn_priv =
	    (zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));

	rc = kstrtouint_from_user(buffer, count, 0,
				  (unsigned int *)&scn_priv->scn_bl_enable);
	if (rc == 0) {
		return count;
	} else {
		return 0;
	}
}

static int proc_dos_mac_show(struct seq_file *m, void *v)
{
#if 0
	struct ieee80211com *ic = (struct ieee80211com *)m->private;
	int i = 0;
	ieee80211_bl_entry_t *entry = NULL;

	seq_printf(m, "Policy: %d, Count: %u\n", ic->ic_bl->bl_policy,
		   ic->ic_bl->bl_entries);
	for (; i < BL_HASHSIZE; i++) {
		LIST_FOREACH(entry, &ic->ic_bl->bl_hash[i], be_hash) {
			seq_printf(m, "%02x:%02x:%02x:%02x:%02x:%02x\n",
				   entry->be_macaddr[0], entry->be_macaddr[1],
				   entry->be_macaddr[2], entry->be_macaddr[3],
				   entry->be_macaddr[4], entry->be_macaddr[5]);
		}
	}
#endif
	return 0;
}

typedef struct {
	u16 dos_type;
	u8 dos_mac[6];
} dos_t;

typedef struct dos_type {
	int dos_type;
	int ieee_type;
} dos_type_t;

int anti_cmcc_to_attack_type_ieee80211(int dos_type)
{
#if 1
	return 0;
#else
	dos_type_t cmcc_to_iee80211[10] = {
		{IEEE80211_DOS_ATTACK_FLOOD_ASSOC,
		 IEEE80211_FC0_SUBTYPE_ASSOC_REQ},
		{IEEE80211_DOS_ATTACK_FLOOD_REASSOC,
		 IEEE80211_FC0_SUBTYPE_REASSOC_REQ},
		{IEEE80211_DOS_ATTACK_FLOOD_DISASSOC,
		 IEEE80211_FC0_SUBTYPE_DISASSOC},
		{IEEE80211_DOS_ATTACK_FLOOD_PROBREQ,
		 IEEE80211_FC0_SUBTYPE_PROBE_REQ},
		{IEEE80211_DOS_ATTACK_FLOOD_ACTION,
		 IEEE80211_FC0_SUBTYPE_ACTION},
		{IEEE80211_DOS_ATTACK_FLOOD_AUTH, IEEE80211_FC0_SUBTYPE_AUTH},
		{IEEE80211_DOS_ATTACK_FLOOD_DEAUTH,
		 IEEE80211_FC0_SUBTYPE_DEAUTH},
		{IEEE80211_DOS_ATTACK_FLOOD_NULLDATA,
		 IEEE80211_FC0_SUBTYPE_QOS_NULL},
		{IEEE80211_DOS_ATTACK_FLOOD_ATIM, IEEE80211_FC0_SUBTYPE_ATIM},
	};
	if (dos_type > IEEE80211_DOS_ATTACK_FLOOD_ATIM) {
		return -1;
	} else {
		return cmcc_to_iee80211[dos_type].ieee_type;
	}
#endif
}

ssize_t proc_dos_mac_write(struct file * file, const char *buffer, size_t count,
			   loff_t * ppos)
{
#if 0
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,19,0)
	struct ieee80211com *ic =
	    (struct ieee80211com *)PDE_DATA(file->f_dentry->d_inode);
#else
	struct ieee80211com *ic =
	    (struct ieee80211com *)PDE_DATA(file->f_path.dentry->d_inode);
#endif
	dos_t *dos = (dos_t *) buffer;
	int i = 0;
	int re;
	for (i = 0; i < count / sizeof(dos_t); i++) {
		if (ic->ic_anti_attack_enable) {
			re = anti_cmcc_to_attack_type_ieee80211(dos[i].
								dos_type);
			if (re >= 0) {
				ieee80211_bl_add(ic, ic->ic_myaddr,
						 dos[i].dos_mac,
						 BE_AT_DOS_ATTAKC, re, 0);
			}
		}
	}
#endif
	return count;
}

static int proc_anti_attack_count_show(struct seq_file *m, void *v)
{
#if 0
	seq_printf(m, "attack_sta_time_count: %d\n", attack_sta_time_count);
	seq_printf(m, "attack_list_count: %d\n", attack_list_count);
	seq_printf(m, "attack_acl_list_count: %d\n", attack_acl_list_count);
#endif
	return 0;
}

ssize_t proc_anti_attack_count_write(struct file * file, const char *buffer,
				     size_t count, loff_t * ppos)
{
	return -1;
}

ssize_t proc_radio_basic_statistics_write(struct file * file,
					  const char *buffer, size_t count,
					  loff_t * ppos)
{
	return -1;
}

int proc_radio_basic_statistics_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_vap_priv_stats_t *vapstats;
	zyUMAC_node_priv_stats_t *node_stats;
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_wal_wdev_t wdev = NULL;
	zyUMAC_wal_wnode_t wnode = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	int frame_dup_cnt = 0;
	int rx_bcast_frame_cnt = 0, tx_bcast_frame_cnt = 0;
	u_int64_t rx_bcast_frame_bytes = 0, tx_bcast_frame_bytes = 0;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	wdev = zyUMAC_wal_wphy_get_first_wdev(wphy);
	while (wdev) {
		zyUMAC_vap_priv = wdev->iv_zyUMAC_vap_priv;
		if (zyUMAC_vap_priv) {
			vapstats = &zyUMAC_vap_priv->is_stats;
			zyUMAC_wal_wdev_update_stats(wdev, vapstats);

			rx_bcast_frame_cnt += vapstats->is_rx_bcast;
			rx_bcast_frame_bytes += vapstats->is_rx_bcast_bytes;
			tx_bcast_frame_cnt += vapstats->is_tx_bcast;
			tx_bcast_frame_bytes += vapstats->is_tx_bcast_bytes;
		}
		wdev = zyUMAC_wal_wphy_get_next_wdev(wphy, wdev);
	}

	wnode = zyUMAC_wal_wphy_get_first_wnode(wphy);
	while (wnode) {
		zyUMAC_node_priv = zyUMAC_wal_wnode_getdata(wnode);
		if (zyUMAC_node_priv) {
			node_stats = &zyUMAC_node_priv->ns_stats;
			zyUMAC_wal_wnode_update_stats(wnode, node_stats);
			frame_dup_cnt += node_stats->wal_stats.ns_rx_dup;
		}
		wnode = zyUMAC_wal_wphy_get_next_wnode(wphy, wnode);
	}

	seq_puts(p, "TxFragCnt\t");
	seq_puts(p, "TxMcastCnt\t");
	seq_puts(p, "FailCnt\t");
	seq_puts(p, "RtryCnt\t");
	seq_puts(p, "MultiRtryCnt\t");
	seq_puts(p, "FrameDupCnt\t");
	seq_puts(p, "RtsSucessCnt\t");
	seq_puts(p, "RtsFailCnt\t");
	seq_puts(p, "AckFailCnt\t");
	seq_puts(p, "RxFragCnt\t");
	seq_puts(p, "RxMultiCnt\t");
	seq_puts(p, "FcsErrCnt\t");
	seq_puts(p, "TxFrameCnt\t");
	seq_puts(p, "DcrytErrCnt\t");
	seq_puts(p, "ResouUseRecord\t");
	seq_puts(p, "ResouUseRecordTime\t");
	seq_puts(p, "OldResouUseRecord\t");

	seq_puts(p, "OldResouUseRecordTime\t");
	seq_puts(p, "RxFrameCnt\t");
	seq_puts(p, "RxUcastFrameCnt\t");
	seq_puts(p, "RxBcastFrameCnt\t");
	seq_puts(p, "RxFrameBytes\t");
	seq_puts(p, "RxUcastFrameBytes\t");
	seq_puts(p, "RxBcastFrameBytes\t");
	seq_puts(p, "PhyErrCnt\t");

	seq_puts(p, "MicErrCnt\t");
	seq_puts(p, "RxDataFrameCnt\t");
	seq_puts(p, "RxDataFrameBytes\t");
	seq_puts(p, "RxDiscardFrameCnt\t");
	seq_puts(p, "RxDiscardFrameBytes\t");
	seq_puts(p, "TxUcastFrameCnt\t");
	seq_puts(p, "TxBcastFrameCnt\t");

	seq_puts(p, "TxFrameBytes\t");
	seq_puts(p, "TxUcastFrameBytes\t");
	seq_puts(p, "TxBcastFrameBytes\t");
	seq_puts(p, "TxDataFrameCnt\t");
	seq_puts(p, "TxDataFrameBytes\t");
	seq_puts(p, "Last1TxFailCnt\t");
	seq_puts(p, "Last1TxCnt\n");

	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", stats->tx_mcast);
	seq_printf(p, "%d\t",
		   stats->wal_stats.rx_badcrypt + stats->wal_stats.rx_badmic);
	seq_printf(p, "%d\t", stats->wal_stats.tx_xretry);
	seq_printf(p, "%d\t", stats->wal_stats.pdev_cont_xretry);

	seq_printf(p, "%d\t", frame_dup_cnt);

	seq_printf(p, "%d\t", stats->wal_stats.rtsGood);
	seq_printf(p, "%d\t", stats->wal_stats.rtsBad);
	seq_printf(p, "%d\t", stats->wal_stats.ackRcvBad);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%d\t", stats->wal_stats.fcsBad);
	seq_printf(p, "%d\t", stats->wal_stats.tx_packets);
	seq_printf(p, "%d\t", stats->wal_stats.rx_badcrypt);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", 0LLU);

	seq_printf(p, "%d\t", stats->wal_stats.rx_packets);
	seq_printf(p, "%llu\t", stats->rx_ucast);
	seq_printf(p, "%d\t", rx_bcast_frame_cnt);

	seq_printf(p, "%llu\t", stats->wal_stats.rx_bytes);
	seq_printf(p, "%llu\t", 0LLU);

	seq_printf(p, "%llu\t", rx_bcast_frame_bytes);

	seq_printf(p, "%d\t", stats->wal_stats.rx_phyerr);
	seq_printf(p, "%d\t", stats->wal_stats.rx_badmic);

	seq_printf(p, "%d\t", stats->wal_stats.rx_num_data);
	seq_printf(p, "%d\t", stats->wal_stats.rx_data_bytes);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", 0LLU);

	seq_printf(p, "%llu\t", stats->tx_ucast);
	seq_printf(p, "%d\t", tx_bcast_frame_cnt);

	seq_printf(p, "%llu\t", stats->wal_stats.tx_bytes);
	seq_printf(p, "%llu\t", stats->tx_ucast_bytes);
	seq_printf(p, "%llu\t", tx_bcast_frame_bytes);

	seq_printf(p, "%d\t", stats->wal_stats.tx_num_data);
	seq_printf(p, "%llu\t", stats->tx_data_bytes);
	seq_printf(p, "%u\t", stats->Last1TxFailCnt);
	seq_printf(p, "%u\n", stats->Last1TxCnt);

	return 0;
}

ssize_t proc_radio_signal_sample_write(struct file * file, const char *buffer,
				       size_t count, loff_t * ppos)
{
	return -1;
}

int proc_radio_signal_sample_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;
	int max_rssi, min_rssi;
	u_int64_t sum_pkts;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	max_rssi = (stats->rx_rssi_highest > 80) ? 80 : stats->rx_rssi_highest;
	min_rssi = stats->rx_rssi_lowest;
	sum_pkts = stats->wal_stats.tx_num_data + stats->wal_stats.rx_packets;

	seq_puts(p, "MaxRssi\tMinRssi\tSumRssi\tSumPkt\tnoise\n");

	seq_printf(p, "%d\t", max_rssi);
	seq_printf(p, "%d\t", min_rssi);
	seq_printf(p, "%d\t", max_rssi + min_rssi);
	seq_printf(p, "%llu\t", sum_pkts);

	seq_printf(p, "%d\t\n", stats->noise_floor);

	return 0;
}

ssize_t proc_radio_mpdu_statistics_write(struct file * file, const char *buffer,
					 size_t count, loff_t * ppos)
{
	return -1;
}

int proc_radio_mpdu_statistics_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_vap_priv_stats_t *vapstats;
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_wal_wdev_t wdev = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	RADIO_MPDU_STATS_T mpdu;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	memset(&mpdu, 0, sizeof(RADIO_MPDU_STATS_T));
	mpdu.txMpduCnt = stats->wal_stats.tx_packets;
	mpdu.txPayloadBytes = stats->wal_stats.tx_bytes;
	mpdu.rxPayloadBytes = stats->wal_stats.rx_bytes;
	mpdu.txRetryMpduCnt = stats->wal_stats.tx_xretry;

	wdev = zyUMAC_wal_wphy_get_first_wdev(wphy);
	while (wdev) {
		zyUMAC_vap_priv = wdev->iv_zyUMAC_vap_priv;
		if (zyUMAC_vap_priv) {
			vapstats = &zyUMAC_vap_priv->is_stats;
			zyUMAC_wal_wdev_update_stats(wdev, vapstats);

			mpdu.txDiscardMpduCnt +=
				vapstats->wal_stats.unicast_tx_discard;
			mpdu.txDiscardMpduCnt +=
				vapstats->wal_stats.multicast_tx_discard;
		}
		wdev = zyUMAC_wal_wphy_get_next_wdev(wphy, wdev);
	}

	seq_puts(p,
		   "TxMPDUCnt\tTxDiscardMPDUCnt\tTxRetryMPDUCnt\tTxPayloadBytes\tRxPayloadBytes\n");

	seq_printf(p, "%llu\t", mpdu.txMpduCnt);
	seq_printf(p, "%llu\t", mpdu.txDiscardMpduCnt);
	seq_printf(p, "%lld\t", mpdu.txRetryMpduCnt);
	seq_printf(p, "%llu\t", mpdu.txPayloadBytes);
	seq_printf(p, "%llu\n", mpdu.rxPayloadBytes);

	return 0;
}

ssize_t proc_radio_rxtx_statistics_write(struct file * file, const char *buffer,
					 size_t count, loff_t * ppos)
{
	return -1;
}

int proc_radio_rxtx_statistics_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;
	int i;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	seq_puts(p, "TxMgtFrameCnt\t");
	seq_puts(p, "TxCtrlFrameCnt\t");
	seq_puts(p, "AssReqFrameCnt\t");
	seq_puts(p, "AssRespFrameCnt\t");
	seq_puts(p, "ReaReqFrameCnt\t");
	seq_puts(p, "ReaRespFrameCnt\t");
	seq_puts(p, "ProReqFrameCnt\t");
	seq_puts(p, "ProRespFrameCnt\t");
	seq_puts(p, "BeaconFrameCnt\t");
	seq_puts(p, "ATIMFrameCnt\t");
	seq_puts(p, "DisassMFrameCnt\t");
	seq_puts(p, "AuthFrameCnt\t");
	seq_puts(p, "DeauthFrameCnt\t");
	seq_puts(p, "ActionFrameCnt\t");
	seq_puts(p, "TxUcastDataFrameCnt\t");
	seq_puts(p, "RxUcastDataFrameCnt\t");

	seq_puts(p, "TxMPDUCntBySizeLvL0l\t");
	seq_puts(p, "RxMPDUCntBySizeLvL0l\t");
	seq_puts(p, "TxMPDUCntBySizeLvL02\t");
	seq_puts(p, "RxMPDUCntBySizeLvL02\t");
	seq_puts(p, "TxMPDUCntBySizeLvL03\t");
	seq_puts(p, "RxMPDUCntBySizeLvL03\t");
	seq_puts(p, "TxMPDUCntBySizeLvL04\t");
	seq_puts(p, "RxMPDUCntBySizeLvL04\t");

	for (i = 1; i <= 12; i++) {
		seq_printf(p, "TxMPDUCntByRateLvL%02d\t", i);
		seq_printf(p, "RxMPDUCntByRateLvL%02d\t", i);
	}

	for (i = 1; i <= 46; i++) {
		seq_printf(p, "TxMPDUCntByMcsRateLvL%02d\t", i);
		seq_printf(p, "RxMPDUCntByMcsRateLvL%02d\t", i);
	}
	seq_puts(p, "\n");

	seq_printf(p, "%llu\t", stats->tx_mgmt);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", stats->rx_assreq);
	seq_printf(p, "%llu\t", stats->tx_assocresp);
	seq_printf(p, "%llu\t", stats->rx_reassocreq);
	seq_printf(p, "%llu\t", stats->tx_reassocresp);
	seq_printf(p, "%llu\t", stats->rx_probreq);
	seq_printf(p, "%llu\t", stats->tx_probresp);
	seq_printf(p, "%llu\t", stats->wal_stats.tx_beacon);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", stats->rx_disassoc);
	seq_printf(p, "%llu\t", stats->rx_auth);
	seq_printf(p, "%llu\t", stats->rx_deauth);
	seq_printf(p, "%llu\t", 0LLU);
	seq_printf(p, "%llu\t", stats->tx_ucast);
	seq_printf(p, "%llu\t", stats->rx_ucast);

	seq_printf(p, "%llu\t", stats->txmsdu0to128);
	seq_printf(p, "%llu\t", stats->rxmsdu0to128);
	seq_printf(p, "%llu\t", stats->txmsdu128to512);
	seq_printf(p, "%llu\t", stats->rxmsdu128to512);
	seq_printf(p, "%llu\t", stats->txmsdu512to1024);
	seq_printf(p, "%llu\t", stats->rxmsdu512to1024);
	seq_printf(p, "%llu\t", stats->txmsduthan1024);
	seq_printf(p, "%llu\t", stats->rxmsduthan1024);

	for (i = 0; i < 12; i++) {
		seq_printf(p, "%llu\t", (u_int64_t) 0);
		seq_printf(p, "%llu\t", (u_int64_t) 0);
	}

	for (i = 0; i < 46; i++) {
		seq_printf(p, "%llu\t", (u_int64_t) 0);
		seq_printf(p, "%llu\t", (u_int64_t) 0);
	}

	return 0;
}

ssize_t proc_bss_statistics_write(struct file * file, const char *buffer,
				  size_t count, loff_t * ppos)
{
	return -1;
}

int proc_bss_statistics_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_vap_priv_stats_t *vapstats;
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_wal_wdev_t wdev = NULL;
	BSS_STATS_T bss;

	wphy = scn_priv->scn_dev;

	seq_puts(p, "WlanID\t");
	seq_puts(p, "DropFrameCnt\t");
	seq_puts(p, "DropFrameBytes\t");
	seq_puts(p, "TxFrameCnt\t");
	seq_puts(p, "TxUcastFrameCnt\t");
	seq_puts(p, "TxBcastFrameCnt\t");
	seq_puts(p, "TxAccBcastFrameCnt\t");
	seq_puts(p, "TxFrameBytes\t");
	seq_puts(p, "TxUcastFrameBytes\t");
	seq_puts(p, "TxBcastFrameByte\t");
	seq_puts(p, "RxFrameCnt\t");
	seq_puts(p, "RxUcastFrameCnt\t");
	seq_puts(p, "RxBcastFrameCnt\t");
	seq_puts(p, "RxFrameBytes\t");
	seq_puts(p, "RxUcastFrameBytes\t");
	seq_puts(p, "RxBcastFrameBytes\t");
	seq_puts(p, "RxTotalFrameCnt\t");

	seq_puts(p, "RxTotalFrameBytes\t");
	seq_puts(p, "TxTotalFrameCnt\t");
	seq_puts(p, "TxTotalFrameBytes\t");

	seq_puts(p, "RxMgmtFrameCnt\t");
	seq_puts(p, "TxMgmtFrameCnt\t");
	seq_puts(p, "RxMgmtFrameBytes\t");
	seq_puts(p, "TxMgmtFrameBytes\t");

	seq_puts(p, "TxAssocRespCnt\t");
	seq_puts(p, "RxAssocReqCnt\t");

	seq_puts(p, "\n");

	wdev = zyUMAC_wal_wphy_get_first_wdev(wphy);
	while (wdev) {
		zyUMAC_vap_priv = wdev->iv_zyUMAC_vap_priv;
		if (zyUMAC_vap_priv) {
			vapstats = &zyUMAC_vap_priv->is_stats;
			zyUMAC_wal_wdev_update_stats(wdev, vapstats);

			memset(&bss, 0, sizeof(BSS_STATS_T));
			snprintf(bss.WlanID, sizeof(bss.WlanID), "%s",
				zyUMAC_wal_wdev_to_netdev(wdev)->name);

			bss.DropFrameCnt = vapstats->is_tx_drop +
					vapstats->is_rx_drop;
			bss.DropFrameBytes = vapstats->is_tx_drop_bytes +
					vapstats->is_rx_drop_bytes;
			bss.TxFrameCnt = vapstats->is_tx_data;
			bss.TxFrameBytes = vapstats->is_tx_data_bytes;
			bss.TxUcastFrameCnt = vapstats->is_tx_unicast;
			bss.TxBcastFrameCnt = vapstats->is_tx_bcast;
			bss.TxAccBcastFrameCnt = 0;
			bss.TxUcastFrameBytes = vapstats->is_tx_unicast_bytes;
			bss.TxBcastFrameByte = vapstats->is_tx_bcast_bytes;
			bss.RxFrameCnt = vapstats->is_rx_data;
			bss.RxUcastFrameCnt = vapstats->is_rx_unicast;
			bss.RxBcastFrameCnt = vapstats->is_rx_bcast;
			bss.RxFrameBytes = vapstats->is_rx_data_bytes;
			bss.RxUcastFrameBytes = vapstats->is_rx_unicast_bytes;
			bss.RxBcastFrameBytes = vapstats->is_rx_bcast_bytes;
			bss.RxTotalFrameCnt =  vapstats->is_rx_packets;
			bss.RxTotalFrameBytes = vapstats->is_rx_bytes;
			bss.TxTotalFrameCnt =  vapstats->is_tx_packets;
			bss.TxTotalFrameBytes = vapstats->is_tx_bytes;

			seq_printf(p, "%s\t", bss.WlanID);
			seq_printf(p, "%llu\t", bss.DropFrameCnt);
			seq_printf(p, "%llu\t", bss.DropFrameBytes);
			seq_printf(p, "%llu\t", bss.TxFrameCnt);
			seq_printf(p, "%llu\t", bss.TxUcastFrameCnt);
			seq_printf(p, "%llu\t", bss.TxBcastFrameCnt);
			seq_printf(p, "%llu\t", bss.TxAccBcastFrameCnt);
			seq_printf(p, "%llu\t", bss.TxFrameBytes);
			seq_printf(p, "%llu\t", bss.TxUcastFrameBytes);
			seq_printf(p, "%llu\t", bss.TxBcastFrameByte);
			seq_printf(p, "%llu\t", bss.RxFrameCnt);
			seq_printf(p, "%llu\t", bss.RxUcastFrameCnt);
			seq_printf(p, "%llu\t", bss.RxBcastFrameCnt);
			seq_printf(p, "%llu\t", bss.RxFrameBytes);
			seq_printf(p, "%llu\t", bss.RxUcastFrameBytes);
			seq_printf(p, "%llu\t", bss.RxBcastFrameBytes);
			seq_printf(p, "%llu\t", bss.RxTotalFrameCnt);

			seq_printf(p, "%llu\t", bss.RxTotalFrameBytes);
			seq_printf(p, "%llu\t", bss.TxTotalFrameCnt);
			seq_printf(p, "%llu\t", bss.TxTotalFrameBytes);

			seq_printf(p, "%u\t", vapstats->is_rx_mgmt);
			seq_printf(p, "%u\t", vapstats->is_tx_mgmt);
			seq_printf(p, "%llu\t", vapstats->is_rx_mgmt_bytes);
			seq_printf(p, "%llu\t", vapstats->is_tx_mgmt_bytes);

			seq_printf(p, "%u\t", vapstats->is_tx_assoc_resp);
			seq_printf(p, "%u\t", vapstats->is_rx_assoc);

			seq_puts(p, "\n");
		}
		wdev = zyUMAC_wal_wphy_get_next_wdev(wphy, wdev);
	}

	return 0;
}

ssize_t proc_station_statistics_write(struct file * file, const char *buffer,
				      size_t count, loff_t * ppos)
{
	return -1;
}

static void zyUMAC_show_sta_cb(zyUMAC_wal_wnode_t wnode, void *arg)
{
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_node_priv_stats_t *node_stats;
	STATION_STATS_T sta;
	int i;
	struct seq_file *p;

	p = (struct seq_file *)arg;

	zyUMAC_node_priv = zyUMAC_wal_wnode_getdata(wnode);
	if (zyUMAC_node_priv) {
		node_stats = &zyUMAC_node_priv->ns_stats;
		zyUMAC_wal_wnode_update_stats(wnode, node_stats);
		snprintf(sta.IfName, sizeof(sta.IfName), "%s",
			zyUMAC_wal_wdev_to_netdev(
			    zyUMAC_wal_wnode_get_wdev(wnode))->name);
		memcpy(sta.MacAddr,
		zyUMAC_wal_wnode_get_macaddr(wnode), MAC_ADDR_LEN);
		for (i = 0; i < 4; i++) {
			sta.RxFrameCnt[i] =
				node_stats->an_framereceive[i];
			sta.TxFrameCnt[i] =
				node_stats->an_frametransmit[i];
			sta.DropFrameCnt[i] =
				node_stats->an_framediscard[i];
			sta.RxFrameBytes[i] =
				node_stats->an_bytesreceive[i];
			sta.TxFrameBytes[i] =
				node_stats->an_bytestransmit[i];
			sta.DropFrameBytes[i] =
				node_stats->an_bytesdiscard[i];
		}
		/* stats at ol_tx_send */
		sta.TxFrameCnt[0] =
			node_stats->tx_good_pkts;
		sta.TxFrameBytes[0] =
			node_stats->tx_good_bytes;
		sta.DropFrameCnt[0] =
			node_stats->tx_bad_pkts;
		sta.DropFrameBytes[0] =
			node_stats->tx_bad_bytes;
		sta.Rssi = zyUMAC_wal_wnode_get_rssi(wnode);
		sta.Snr = sta.Rssi - DEFAULT_CHAN_REF_NOISE_FLOOR;
		sta.RxRate = node_stats->wal_stats.ns_last_rx_rate;
		sta.TxRate = node_stats->wal_stats.ns_last_tx_rate;
		sta.DataRate = zyUMAC_wal_wnode_get_maxrate(wnode);
		sta.PER = node_stats->Last1SecPER;
		/* TODO: unified the phymode */
		sta.wireless_mode = zyUMAC_wal_wnode_get_phymode(wnode);

		seq_printf(p, "%s\t", sta.IfName);
		seq_printf(p, "%02X-%02X-%02X-%02X-%02X-%02X\t",
			sta.MacAddr[0], sta.MacAddr[1],
			sta.MacAddr[2], sta.MacAddr[3],
			sta.MacAddr[4], sta.MacAddr[5]);
		for (i = 0; i < 4; i++)
			seq_printf(p, "%llu\t", sta.RxFrameCnt[i]);
		for (i = 0; i < 4; i++)
			seq_printf(p, "%llu\t", sta.TxFrameCnt[i]);
		for (i = 0; i < 4; i++)
			seq_printf(p, "%llu\t", sta.DropFrameCnt[i]);
		for (i = 0; i < 4; i++)
			seq_printf(p, "%llu\t", sta.RxFrameBytes[i]);
		for (i = 0; i < 4; i++)
			seq_printf(p, "%llu\t", sta.TxFrameBytes[i]);
		for (i = 0; i < 4; i++)
			seq_printf(p, "%llu\t", sta.DropFrameBytes[i]);
		seq_printf(p, "%llu\t", sta.Snr);
		seq_printf(p, "%d\t", sta.Rssi);
		seq_printf(p, "%llu\t", sta.RxRate);
		seq_printf(p, "%llu\t", sta.TxRate);
		seq_printf(p, "%llu\t", sta.DataRate);
		seq_printf(p, "%u\t", sta.PER);
		seq_printf(p, "%u\t", sta.wireless_mode);

		seq_printf(p, "%lu\t", node_stats->auth_uptime);
		seq_printf(p, "%lu\t", node_stats->assoc_req_uptime);
		seq_printf(p, "%lu\t", node_stats->assoc_resp_uptime);
		seq_printf(p, "%u\t", zyUMAC_wal_wnode_get_associd(wnode));

		seq_puts(p, "\n");
	}
}

int proc_station_statistics_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_wal_wphy_t wphy;

	wphy = scn_priv->scn_dev;

	seq_puts(p, "IfName\t");
	seq_puts(p, "MAC_Address\t");

	seq_puts(p, "RxFrameCnt1\t");
	seq_puts(p, "RxFrameCnt2\t");
	seq_puts(p, "RxFrameCnt3\t");
	seq_puts(p, "RxFrameCnt4\t");

	seq_puts(p, "TxFrameCnt1\t");
	seq_puts(p, "TxFrameCnt2\t");
	seq_puts(p, "TxFrameCnt3\t");
	seq_puts(p, "TxFrameCnt4\t");

	seq_puts(p, "DropFrameCnt1\t");
	seq_puts(p, "DropFrameCnt2\t");
	seq_puts(p, "DropFrameCnt3\t");
	seq_puts(p, "DropFrameCnt4\t");

	seq_puts(p, "RxFrameBytes1\t");
	seq_puts(p, "RxFrameBytes2\t");
	seq_puts(p, "RxFrameBytes3\t");
	seq_puts(p, "RxFrameBytes4\t");

	seq_puts(p, "TxFrameBytes1\t");
	seq_puts(p, "TxFrameBytes2\t");
	seq_puts(p, "TxFrameBytes3\t");
	seq_puts(p, "TxFrameBytes4\t");

	seq_puts(p, "DropFrameBytes1\t");
	seq_puts(p, "DropFrameBytes2\t");
	seq_puts(p, "DropFrameBytes3\t");
	seq_puts(p, "DropFrameBytes4\t");
	seq_puts(p, "Snr\t");
	seq_puts(p, "Rssi\t");
	seq_puts(p, "RxRate\t");
	seq_puts(p, "TxRate\t");
	seq_puts(p, "DataRate\t");
	seq_puts(p, "PER (pcnt)\t");
	seq_puts(p, "WirelessMode\t");

	seq_puts(p, "AuthTime\t");
	seq_puts(p, "AssocReqTime\t");
	seq_puts(p, "AssocRespTime\t");
	seq_puts(p, "AssocID\t");

	seq_puts(p, "\n");

	zyUMAC_wal_wphy_iterate_wnodes(wphy, zyUMAC_show_sta_cb, p);

	return 0;
}

ssize_t proc_radio_extern_statistics_write(struct file * file,
					   const char *buffer, size_t count,
					   loff_t * ppos)
{
	return -1;
}

int proc_radio_extern_statistics_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	seq_puts(p, "RxErrorCnt\t");
	seq_puts(p, "TxErrorCnt\t");
	seq_puts(p, "RxMcastFrameCnt\t");
	seq_puts(p, "RxMcastFrameBytes\t");
	seq_puts(p, "TxMcastFrameCnt\t");
	seq_puts(p, "TxMcastFrameBytes\t\n");

	seq_printf(p, "%d\t\t",
		   stats->wal_stats.rx_phyerr + stats->wal_stats.rx_badcrypt +
		   stats->wal_stats.rx_badmic + stats->wal_stats.rx_crcerr);
	seq_printf(p, "%d\t\t", stats->wal_stats.tx_bawadv);

	seq_printf(p, "%llu\t\t", 0LLU);
	seq_printf(p, "%llu\t\t", 0LLU);
	seq_printf(p, "%llu\t\t", stats->tx_mcast);
	seq_printf(p, "%llu\t\n", stats->tx_mcast_bytes);

	return 0;
}

ssize_t proc_radio_ratepkt_statistics_write(struct file * file,
					    const char *buffer, size_t count,
					    loff_t * ppos)
{
	return -1;
}

int proc_radio_ratepkt_statistics_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_wal_wdev_t wdev = NULL;
	int i;
	int channelUtil = 0;
	int ratecnt = 0;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	wdev = zyUMAC_wal_wphy_get_first_wdev(wphy);
	while (wdev) {
		zyUMAC_vap_priv = wdev->iv_zyUMAC_vap_priv;
		if (zyUMAC_vap_priv) {
			/* FIXME: show which channelUtil? */
			channelUtil = zyUMAC_wal_wdev_get_chanutil(wdev);
		}
		wdev = zyUMAC_wal_wphy_get_next_wdev(wphy, wdev);
	}

	seq_printf(p, "channelUtil      = %u\n", channelUtil);
	seq_printf(p, "Total PER        = %u\n", stats->Last1SecPER);

	ratecnt = 0;
	seq_printf(p, "<txRate:txPkts>\n");
	for (i = 0; i < OL_RATE_TABLE_SIZE; i++) {
		if (!stats->txratecnt[i].rateval) {
			continue;
		}
		seq_printf(p, "<%u:%llu:%llu>",
			   stats->txratecnt[i].rateval,
			   stats->txratecnt[i].framecnt,
			   stats->txratecnt[i].bytecnt);
		ratecnt++;
	}
	seq_printf(p, "\n\n");
	seq_printf(p, "txRateCount      = %u\n", ratecnt);

	seq_printf(p, "\n\n");

	ratecnt = 0;
	seq_printf(p, "<rxRate:rxPkts>\n");
	for (i = 0; i < OL_RATE_TABLE_SIZE; i++) {
		if (!stats->rxratecnt[i].rateval) {
			continue;
		}
		seq_printf(p, "<%u:%llu:%llu>",
			   stats->rxratecnt[i].rateval,
			   stats->rxratecnt[i].framecnt,
			   stats->rxratecnt[i].bytecnt);
		ratecnt++;
	}
	seq_printf(p, "\n\n");
	seq_printf(p, "rxRateCount      = %u\n", ratecnt);

	return 0;
}

ssize_t proc_assoc_statistics_write(struct file * file, const char *buffer,
				    size_t count, loff_t * ppos)
{
	return -1;
}

int proc_assoc_statistics_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_vap_priv_stats_t *vapstats;
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_wal_wdev_t wdev = NULL;
	int rx_assoc_cnt = 0;
	int rx_reassoc_cnt = 0;
	int rx_assoc_fail_cnt = 0;
	int rx_reassoc_succ_cnt = 0;

	wphy = scn_priv->scn_dev;
	wdev = zyUMAC_wal_wphy_get_first_wdev(wphy);
	while (wdev) {
		zyUMAC_vap_priv = wdev->iv_zyUMAC_vap_priv;
		if (zyUMAC_vap_priv) {
			vapstats = &zyUMAC_vap_priv->is_stats;
			zyUMAC_wal_wdev_update_stats(wdev, vapstats);

			rx_assoc_cnt += vapstats->is_rx_assoc;
			rx_reassoc_cnt += vapstats->is_rx_reassoc;
			rx_assoc_fail_cnt += vapstats->is_rx_assoc_fail;
			rx_reassoc_succ_cnt += vapstats->is_rx_reassoc_ok;
		}
		wdev = zyUMAC_wal_wphy_get_next_wdev(wphy, wdev);
	}

	seq_puts(p, "AssocTimes\t");
	seq_puts(p, "AssocFailTimes\t");
	seq_puts(p, "ReassocTimes\t");
	seq_puts(p, "ReassocSuccTimes\t\n");

	seq_printf(p, "%u\t\t", rx_assoc_cnt);
	seq_printf(p, "%u\t\t", rx_assoc_fail_cnt);
	seq_printf(p, "%u\t\t", rx_reassoc_cnt);
	seq_printf(p, "%u\t\n", rx_reassoc_succ_cnt);

	return 0;
}

ssize_t proc_total_cycle_write(struct file * file, const char *buffer,
			       size_t count, loff_t * ppos)
{
	return -1;
}

int proc_total_cycle_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	/*seq_printf(p, "total_cycle\t\n");*/
	seq_printf(p, "%llu\n", stats->total_cycle);

	return 0;
}

ssize_t proc_busy_cycle_write(struct file * file, const char *buffer,
			      size_t count, loff_t * ppos)
{
	return -1;
}

//enabled by DCS
int proc_busy_cycle_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	/*seq_printf(p, "busy_cycle\t\n");*/
	seq_printf(p, "%llu\n", stats->busy_cycle);

	return 0;
}

ssize_t proc_rx_probreq_write(struct file * file, const char *buffer,
			      size_t count, loff_t * ppos)
{
	zyUMAC_scn_priv_t *scn_priv =
		(zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;
	int rc;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	rc = kstrtouint_from_user(buffer, count, 0,
				  (unsigned int *)&stats->rx_probreq);
	if (rc == 0)
		return count;
	else
		return 0;
}

int proc_rx_probreq_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	seq_printf(p, "%llu\n", stats->rx_probreq);

	return 0;
}

ssize_t proc_tx_probresp_write(struct file * file, const char *buffer,
			       size_t count, loff_t * ppos)
{
	zyUMAC_scn_priv_t *scn_priv =
		(zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;
	int rc;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	rc = kstrtouint_from_user(buffer, count, 0,
				  (unsigned int *)&stats->tx_probresp);
	if (rc == 0)
		return count;
	else
		return 0;
}

int proc_tx_probresp_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_radio_stats(wphy, stats);

	seq_printf(p, "%llu\n", stats->tx_probresp);

	return 0;
}

int proc_channel_utilization_show(struct seq_file *p, void *v)
{
	zyUMAC_scn_priv_t *scn_priv = (zyUMAC_scn_priv_t *) p->private;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_wal_wphy_t wphy;
	unsigned int enable = 0;

	wphy = scn_priv->scn_dev;
	stats = &scn_priv->scn_radio_stats;

	zyUMAC_wal_wphy_update_channel_util(wphy, stats, enable);

	seq_puts(p, "Channel Utilization:\n");
	seq_printf(p, "Duration time: %d\n", stats->channel_stats.duration);
	seq_puts(p, "busy\t");
	seq_puts(p, "tx\t");
	seq_puts(p, "rx\t");
	seq_puts(p, "obss\t");
	seq_puts(p, "edcca\t\n");
	seq_printf(p, "%u\t", stats->channel_stats.busy_time);
	seq_printf(p, "%u\t", stats->channel_stats.tx_time);
	seq_printf(p, "%u\t", stats->channel_stats.rx_time);
	seq_printf(p, "%u\t", stats->channel_stats.obss_time);
	seq_printf(p, "%u\t\n", stats->channel_stats.edcca_time);
	return 0;
}

ssize_t proc_channel_utilization_write(struct file *file, const char *buffer,
				       size_t count, loff_t *ppos)
{
	zyUMAC_scn_priv_t *scn_priv =
		(zyUMAC_scn_priv_t *) PDE_DATA(file_inode(file));
	zyUMAC_wal_wphy_t wphy;
	unsigned int enable = 0;

	wphy = scn_priv->scn_dev;
	kstrtouint_from_user(buffer, count, 0, &enable);

	zyUMAC_wal_wphy_update_channel_util(wphy, NULL, enable);

	return count;
}

struct osif_proc_info {
	const char *name;
	struct file_operations *ops;
	int extra;
};

#define PROC_FOO(func_base, extra) { #func_base, &proc_ieee80211_##func_base##_ops, extra }

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
#define GENERATE_PROC_STRUCTS(func_base)                \
    static int proc_##func_base##_open(struct inode *inode, \
                       struct file *file)        \
    {                                \
        struct proc_dir_entry *dp = PDE(inode);            \
        return single_open(file, proc_##func_base##_show, dp->data); \
    }                                \
                                    \
    static struct file_operations proc_ieee80211_##func_base##_ops = { \
        .open        = proc_##func_base##_open,    \
        .read        = seq_read,                \
        .llseek        = seq_lseek,                \
        .release    = single_release,            \
        .write      = proc_##func_base##_write,     \
    };
#else
#define GENERATE_PROC_STRUCTS(func_base)                \
    static int proc_##func_base##_open(struct inode *inode, \
                       struct file *file)        \
    {                                \
        return single_open(file, proc_##func_base##_show, PDE_DATA(inode)); \
    } \
                                    \
                                    \
    static struct file_operations proc_ieee80211_##func_base##_ops = { \
        .open        = proc_##func_base##_open,    \
        .read        = seq_read,                \
        .llseek        = seq_lseek,                \
        .release    = single_release,            \
        .write      = proc_##func_base##_write, \
    };
#endif

GENERATE_PROC_STRUCTS(anti_attack_enable);
GENERATE_PROC_STRUCTS(anti_attack_threshold);
GENERATE_PROC_STRUCTS(anti_attack_interval);
GENERATE_PROC_STRUCTS(bl_enable);
GENERATE_PROC_STRUCTS(bl_age_time);
GENERATE_PROC_STRUCTS(bl_policy);
GENERATE_PROC_STRUCTS(dos_mac);
GENERATE_PROC_STRUCTS(anti_attack_count);

GENERATE_PROC_STRUCTS(radio_basic_statistics);
GENERATE_PROC_STRUCTS(radio_signal_sample);
GENERATE_PROC_STRUCTS(radio_mpdu_statistics);
GENERATE_PROC_STRUCTS(radio_rxtx_statistics);
GENERATE_PROC_STRUCTS(bss_statistics);
GENERATE_PROC_STRUCTS(station_statistics);

GENERATE_PROC_STRUCTS(radio_extern_statistics);
GENERATE_PROC_STRUCTS(radio_ratepkt_statistics);
GENERATE_PROC_STRUCTS(assoc_statistics);

GENERATE_PROC_STRUCTS(total_cycle);
GENERATE_PROC_STRUCTS(busy_cycle);
GENERATE_PROC_STRUCTS(rx_probreq);
GENERATE_PROC_STRUCTS(tx_probresp);

GENERATE_PROC_STRUCTS(channel_utilization);

const struct osif_proc_info zyUMAC_proc_radio_infos[] = {
	PROC_FOO(anti_attack_enable, 0),
	PROC_FOO(anti_attack_threshold, 0),
	PROC_FOO(anti_attack_interval, 0),
	PROC_FOO(bl_enable, 0),
	PROC_FOO(bl_age_time, 0),
	PROC_FOO(bl_policy, 0),
	PROC_FOO(dos_mac, 0),
	PROC_FOO(anti_attack_count, 0),

	PROC_FOO(radio_basic_statistics, 0),
	PROC_FOO(radio_signal_sample, 0),
	PROC_FOO(radio_mpdu_statistics, 0),
	PROC_FOO(radio_rxtx_statistics, 0),
	PROC_FOO(bss_statistics, 0),
	PROC_FOO(station_statistics, 0),

	PROC_FOO(radio_extern_statistics, 0),
	PROC_FOO(radio_ratepkt_statistics, 0),
	PROC_FOO(assoc_statistics, 0),

	PROC_FOO(total_cycle, 0),
	PROC_FOO(busy_cycle, 0),
	PROC_FOO(rx_probreq, 0),
	PROC_FOO(tx_probresp, 0),

	PROC_FOO(channel_utilization, 0),
};

#define NUM_PROC_RADIO_INFOS (sizeof(zyUMAC_proc_radio_infos) / sizeof(zyUMAC_proc_radio_infos[0]))

void zyUMAC_proc_radio_attach(zyUMAC_wal_wphy_t wphy)
{
	zyUMAC_scn_priv_t *scn_priv;
	int i;

	scn_priv = zyUMAC_wal_wphy_getdata(wphy);

	if (scn_priv->scn_proc == NULL) {
		scn_priv->scn_proc = zyUMAC_wal_wphy_get_proc_dir(wphy);
	}

	for (i = 0; i < NUM_PROC_RADIO_INFOS; ++i) {
		struct proc_dir_entry *entry;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
		entry = proc_create_data(zyUMAC_proc_radio_infos[i].name,
					 0644, scn_priv->scn_proc,
					 (const struct file_operations *)
					 zyUMAC_proc_radio_infos[i].ops,
					 (void *)scn_priv);
#else
		entry =
		    create_proc_entry(zyUMAC_proc_radio_infos[i].name, 0644,
				      scn_priv->scn_proc);
#endif
		if (entry == NULL) {
			zyUMAC_LOG(LOG_ERR, "Proc Entry creation failed!");
			return;
		}
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
		entry->data = (void *)scn_priv;
		entry->proc_fops = zyUMAC_proc_radio_infos[i].ops;
#endif
	}
}

void zyUMAC_proc_radio_detach(zyUMAC_wal_wphy_t wphy)
{
	int i;
	zyUMAC_scn_priv_t *scn_priv;

	scn_priv = zyUMAC_wal_wphy_getdata(wphy);

	if (scn_priv->scn_proc == NULL) {
		zyUMAC_LOG(LOG_WARN, "scn_proc is null");
		return;
	}

	for (i = 0; i < NUM_PROC_RADIO_INFOS; ++i) {
		remove_proc_entry(zyUMAC_proc_radio_infos[i].name,
				  scn_priv->scn_proc);
	}
#ifndef UMAC_SUPPORT_CFG80211
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
	remove_proc_entry(scn_priv->scn_proc->name, scn_priv->scn_proc->parent);
#else
	proc_remove(scn_priv->scn_proc);
#endif
	scn_priv->scn_proc = NULL;
#endif
}
