#ifndef __ZYXEL_UMAC_MODULE_HEAD__
#define __ZYXEL_UMAC_MODULE_HEAD__

#include <linux/time.h>
#include <linux/rtc.h>

#include "zyUMAC_wal_types.h"
#include "zyUMAC_log.h"

typedef void (*zyUMAC_wal_event_handler_t) (int, void *);

int zyUMAC_phy_ioctl_attach(zyUMAC_wal_wphy_t wphy);
void zyUMAC_phy_ioctl_detach(zyUMAC_wal_wphy_t wphy);

void zyUMAC_vap_ioctl_attach(zyUMAC_wal_wdev_t vap);
void zyUMAC_vap_ioctl_detach(void);

int zyUMAC_acl_attach(wlan_if_t vap);
int zyUMAC_acl_detach(wlan_if_t vap);
int zyUMAC_acl_setpolicy_from_file(wlan_if_t vap, int policy,
				  u_int8_t acl_list_id);

void zyUMAC_proc_radio_attach(zyUMAC_wal_wphy_t scn);
void zyUMAC_proc_radio_detach(zyUMAC_wal_wphy_t scn);

int zyUMAC_dos_is_enabled(zyUMAC_wal_wdev_t vap);
int zyUMAC_dos_handler(zyUMAC_wal_wdev_t vap, int frametype,
		      u_int8_t *stamac, int rssi);
void zyUMAC_dos_process(zyUMAC_wal_wnode_t ni, zyUMAC_ieee80211_header_t *wh,
		       int subtype, int rssi);

int zyUMAC_umac_event_handler_attach(void);
void zyUMAC_umac_event_handler_detach(void);

void zyUMAC_mlme_deauth_complete_handler(int type, void *event);
void zyUMAC_mlme_disassoc_complete_handler(int type, void *event);
void zyUMAC_mlme_assoc_ind_handler(int type, void *event);
void zyUMAC_auth_indication_handler(int type, void *event);
void zyUMAC_deauth_indication_handler(int type, void *event);
void zyUMAC_disassoc_indication_handler(int type, void *event);

#endif
