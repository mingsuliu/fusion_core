#ifndef __ZYXEL_UMAC_BAND_STEERING_HEAD__
#define __ZYXEL_UMAC_BAND_STEERING_HEAD__

#ifdef ZY_BDST_SUPPORT

#define BS_NETLINK_REPORT_ADD_5G_STA            101	/* band steering add new 5g station */
#define BS_NETLINK_REPORT_QUERY_5G_STA          102
#define LB_NETLINK_REPORT_DENY_STA              103	/* load balance refuse station access */
#define BS_NETLINK_REPORT_DENY_STA              104	/* band steering refuse station access */
#define BS_NETLINK_REPORT_FAIL_STA              105
#define BS_NETLINK_REPORT_QUERY_DENY_STA        106

#define IEEE80211_LOCAL_ADDRESS           0x2

#define BAND_STA_HASH_SIZE  64
#define BAND_STA_MAX_CLIENTS    4096
#define BAND_STA_CLI_TIMEOUT    1800	/* half hour */

#define BAND_STA_CLIENT_RATIO_CHECK_TIME   2	/* check every 2 seconds */
#define BAND_STA_BAND_PREFERENCE_RATIO   4	/* for every 4 clients on 'a', send one to 'g' */

#define BANDSTEERING_GROUP 2
#define ZY_MSG_TYPE_BAND_STEER_INIT_REQUEST       16
#define ZY_BAND_STEERING_FAIL_COUNT_AGETIME       86400 * 7


typedef struct band_sta_info {
	struct band_sta_info *next;
	unsigned char mac[6];
	unsigned char g_burst_count;
	unsigned char a_burst_count;
	unsigned long last_pkt_time;
	unsigned long g_burst_start_time;
	unsigned long a_burst_start_time;
	unsigned long g_last_pkt_time;
	unsigned long a_last_pkt_time;
	u_int16_t g_seqno;
	u_int16_t a_seqno;
	int8_t g_rssi;
	int8_t a_rssi;
	int8_t unlocal;
	/* new bandsteering alg */
	unsigned char is_support_5G;
	unsigned int lb_reject_count;
	unsigned int reject_count;
	unsigned int band_steering_fail_count;
} band_sta_info_t;

#define NLA_HEADER_LEN 0x6
struct redis_message {
	unsigned int mes_type;
	unsigned char sta_mac[zyUMAC_IEEE80211_ADDR_LEN];
	unsigned char is_support_5G;
	unsigned int lb_reject_count;
	unsigned int reject_count;
	unsigned int band_steering_fail_count;
} __packed;

void zyUMAC_band_steering_debug_dump(void);
int zyUMAC_band_steering_handle_auth_req(zyUMAC_wal_wdev_t vap,
		unsigned char *mac, u_int16_t seqno, int rssi, int is2G);
void zyUMAC_band_steering_handle_probe_req(zyUMAC_wal_wdev_t vap,
		unsigned char *mac, u_int16_t seqno, int rssi);

#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
typedef int (*redis_msg_fun)(unsigned char *buf,
		unsigned char msg_type, int msg_len);
extern int sk_redis_client_nl_report(void *msg, unsigned short len,
		unsigned short type);
extern int register_redis_client_msg(redis_msg_fun fn);
extern void unregister_redis_client_msg(void);
#endif

#endif /* ZY_BDST_SUPPORT */

void zyUMAC_bdst_init(void);
void zyUMAC_bdst_exit(void);

#endif
