#include <linux/string.h>

#include "zyUMAC_wal_api.h"
#include "zyUMAC_mlme_hook.h"
#include "zyUMAC_events.h"
#include "zyUMAC_rrm_report.h"
#include "zyUMAC_wal_event.h"

#include "zyUMAC_api.h"
#include "zyUMAC_umac.h"
#include "zyUMAC_ieee80211.h"

#define zyUMAC_GetSender(x) ((x>>16) & 0xFFFF)
#define zyUMAC_GetReason(x) (x & 0xFFFF)

void zyUMAC_auth_indication_handler(int type, void *event)
{
#define REASON_STR_LEN 64
	zyUMAC_wal_event_mlme_auth_ind_t *fuEvent =
	    (zyUMAC_wal_event_mlme_auth_ind_t *) event;
	u_int8_t *macaddr = fuEvent->macaddr;
	u_int16_t status = fuEvent->status;
	zyUMAC_wal_wdev_t vap = fuEvent->wdev;
	zyUMAC_wal_wnode_t ni = NULL;
	char reason_str[REASON_STR_LEN] = "unspecified";
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	struct net_device *netdev = NULL;

	if (!vap) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	netdev = zyUMAC_wal_wdev_to_netdev(vap);
	if (!netdev) {
		zyUMAC_LOG(LOG_ERR, "netdev is null");
		return;
	}

	ni = zyUMAC_wal_wnode_get(vap, macaddr);
	if (!ni) {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
		return;
	}

	if (status != IEEE80211_STATUS_SUCCESS) {
		switch (status) {
		case IEEE80211_STATUS_ALG:
			strncpy(reason_str, "unsupported auth algorithm",
				REASON_STR_LEN);
			break;
		case IEEE80211_STATUS_SEQUENCE:
			strncpy(reason_str, "validate auth algorithm failure",
				REASON_STR_LEN);
			break;
		case IEEE80211_STATUS_TOOMANY:
			strncpy(reason_str, "too many stations for radio",
				REASON_STR_LEN);
			break;
		case IEEE80211_STATUS_REFUSED:
			strncpy(reason_str, "association refused",
				REASON_STR_LEN);
			break;
		}
#ifdef ZY_FM_SUPPORT
		notify_auth_fail(netdev->name,
				 zyUMAC_wal_wnode_get_macaddr(ni),
				 reason_str);
#endif
	} else {
		zyUMAC_node_priv = ni->ni_zyUMAC_node_priv;

		if (zyUMAC_node_priv) {
			zyUMAC_node_priv->ns_stats.auth_uptime =
			    OS_GET_UPTIME();
		} else {
			zyUMAC_LOG(LOG_ERR, "zyUMAC_node_priv is null");
		}
	}

	if (ni) {
		zyUMAC_wal_wnode_put(ni);
	}
	return;
#undef REASON_STR_LEN
}

void zyUMAC_deauth_indication_handler(int type, void *event)
{
#if ZY_LEAVE_REPORT_SUPPORT
	zyUMAC_wal_event_mlme_deauth_ind_t *fuEvent =
	    (zyUMAC_wal_event_mlme_deauth_ind_t *) event;
	zyUMAC_wal_wdev_t vap = fuEvent->wdev;
	u_int8_t *mac_addr = fuEvent->macaddr;
	//u_int16_t associd = fuEvent->associd;
	u_int16_t reason = fuEvent->reason;

	//TODO: changing SENDER for special case, depends on its reason
	// here is dumb handler, since arg is only 16bit
	u_int16_t sender = zyUMAC_STA_PROTO;
	u_int16_t newreason = zyUMAC_GetReason(reason);
	u_int8_t is_passive = RECORD_REASON;
	u_int16_t frame_type = zyUMAC_RECORD_TYPE_DEAUTH;

	switch (newreason) {
	case IEEE80211_REASON_KICK_OUT:
		sender = zyUMAC_AP_KICK_OUT;
		break;
	case IEEE80211_REASON_AUTH_EXPIRE:
		sender = zyUMAC_AP_TIMEOUT;
		break;

	case IEEE80211_STATUS_UNSPECIFIED:
		newreason = zyUMAC_AP_WEAK_RSSI;	//is this suitable?
	case IEEE80211_REASON_QOS:
	case IEEE80211_STATUS_NOT_ASSOCED:
	case IEEE80211_STATUS_INVALID_ELEMENT:
		frame_type = zyUMAC_RECORD_TYPE_DROP;
		is_passive = RECORD_STATUS;
	case IEEE80211_REASON_NOT_AUTHED:
	case IEEE80211_REASON_AUTH_LEAVE:
		sender = zyUMAC_AP_PROTO;
		break;
	}
	send_sta_leave_args(vap, mac_addr, newreason, sender, frame_type,
			    is_passive);
#endif
	return;
}

void zyUMAC_mlme_deauth_complete_handler(int type, void *event)
{
	zyUMAC_wal_event_mlme_deauth_complete_t *fuEvent =
	    (zyUMAC_wal_event_mlme_deauth_complete_t *) event;
	zyUMAC_wal_wdev_t vap = fuEvent->wdev;
	u_int8_t *macaddr = fuEvent->macaddr;
	int status = fuEvent->status;
	u_int16_t reason = zyUMAC_GetReason(status);
	u_int16_t sender = zyUMAC_GetSender(status);

	send_sta_leave_args(vap, macaddr, reason, sender,
			    zyUMAC_RECORD_TYPE_DEAUTH, RECORD_STATUS);
	return;
}

void zyUMAC_mlme_assoc_ind_handler(int type, void *event)
{
	zyUMAC_wal_event_mlme_assoc_ind_t *fuEvent =
	    (zyUMAC_wal_event_mlme_assoc_ind_t *) event;
	u_int8_t *macaddr = fuEvent->macaddr;
	u_int16_t result = fuEvent->result;
	u_int16_t sender = zyUMAC_AP_PROTO;
	u_int16_t newreason = zyUMAC_GetReason(result);
	zyUMAC_wal_wdev_t vap = fuEvent->wdev;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv;
	zyUMAC_wal_wnode_t ni = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;

	if (!vap || !vap->iv_zyUMAC_vap_priv) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	zyUMAC_vap_priv = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;

	ni = zyUMAC_wal_wnode_get(vap, macaddr);
	if (ni) {
		zyUMAC_node_priv = ni->ni_zyUMAC_node_priv;
		if (!zyUMAC_node_priv)
			zyUMAC_LOG(LOG_ERR, "zyUMAC_node_priv is null");

	}
	/*
	 * don't care
	else {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
	}
	*/

	if (newreason == IEEE80211_STATUS_SUCCESS) {
		//no need to store Success code
#ifdef CONFIG_DP_SUPPORT
		zyUMAC_wal_ssid_t ssid;

		zyUMAC_wal_wdev_get_ssid(vap, &ssid);
		if (ni)
			notify_dp_add_sta(zyUMAC_wal_wnode_get_macaddr(ni),
				zyUMAC_wal_wnode_get_bssid(ni),
				zyUMAC_wal_ssid_get_data(ssid),
				zyUMAC_wal_ssid_get_len(ssid),
				zyUMAC_node_priv ?
				zyUMAC_node_priv->ns_stats.auth_uptime : 0,
				zyUMAC_wal_wnode_get_assocuptime(ni));
#endif
		zyUMAC_vap_priv->is_stats.is_rx_reassoc_ok++;
	} else {
		zyUMAC_vap_priv->is_stats.is_rx_assoc_fail++;

		switch (newreason) {
		case IEEE80211_STATUS_TOO_MANY_STATIONS:
			zyUMAC_vap_priv->is_stats.
				is_tx_assoc_resp_no_resource++;
			break;
		case IEEE80211_STATUS_RATES:
			zyUMAC_vap_priv->is_stats.
				is_tx_assoc_resp_wrong_rate++;
			break;
		case IEEE80211_STATUS_UNSPECIFIED:
			zyUMAC_vap_priv->is_stats.
				is_tx_assoc_resp_unspecified++;
			break;
		}

		send_sta_leave_args(vap, macaddr, newreason, sender,
				    zyUMAC_RECORD_TYPE_ASSOC_RESP,
				    RECORD_STATUS);
	}

	if (ni) {
		zyUMAC_wal_wnode_put(ni);
	}

	return;
}

void
zyUMAC_disassoc_indication_handler(int type, void *event)
{
	zyUMAC_wal_event_mlme_disassoc_ind_t *fuEvent =
	    (zyUMAC_wal_event_mlme_disassoc_ind_t *) event;
	zyUMAC_wal_wdev_t vap = fuEvent->wdev;
	u_int8_t *mac_addr = fuEvent->macaddr;
	zyUMAC_wal_ssid_t ssid;
	zyUMAC_wal_wnode_t ni = NULL;
#ifdef CONFIG_DP_SUPPORT
	int ret = -1;
#endif
#if ZY_LEAVE_REPORT_SUPPORT
	//u_int16_t associd = fuEvent->associd;
	u_int32_t reason = fuEvent->reason;
	u_int16_t sender = zyUMAC_GetSender(reason);
	u_int16_t newreason = zyUMAC_GetReason(reason);

	send_sta_leave_args(vap, mac_addr, newreason, sender,
			    zyUMAC_RECORD_TYPE_DISASSOC, RECORD_REASON);
#endif
	ni = zyUMAC_wal_wnode_get(vap, mac_addr);
	if (!ni) {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
		return;
	}

	memset(&ssid, 0, sizeof(zyUMAC_wal_ssid_t));
	zyUMAC_wal_wdev_get_ssid(vap, &ssid);

#ifdef CONFIG_DP_SUPPORT
	ret = notify_dp_del_sta(zyUMAC_wal_wnode_get_macaddr(ni),
			zyUMAC_wal_wnode_get_bssid(ni),
			0,
			zyUMAC_wal_ssid_get_data(ssid),
			zyUMAC_wal_ssid_get_len(ssid));

	if (ret != NOTIFY_OK) {
		zyUMAC_LOG(LOG_ERR, "failed to notity dp");
	}
#endif

	zyUMAC_wal_wnode_put(ni);

	return;
}

void zyUMAC_mlme_disassoc_complete_handler(int type, void *event)
{
#define REASON_STR_LEN 64
	zyUMAC_wal_event_mlme_disassoc_complete_t *fuEvent =
	    (zyUMAC_wal_event_mlme_disassoc_complete_t *) event;
	u_int8_t *macaddr = fuEvent->macaddr;
	u_int32_t reason = fuEvent->reason;
	//IEEE80211_STATUS status = fuEvent->status;
	zyUMAC_wal_wdev_t vap = fuEvent->wdev;
	zyUMAC_wal_wnode_t ni = NULL;
	char reason_str[REASON_STR_LEN] = "unspecified";
	struct net_device *netdev = NULL;

#ifdef ZY_LEAVE_REPORT_SUPPORT
	u_int16_t sender = zyUMAC_GetSender(reason);
	send_sta_leave_args(vap, macaddr, zyUMAC_GetReason(reason), sender,
			    zyUMAC_RECORD_TYPE_DISASSOC, RECORD_REASON);
#endif

	if (!vap) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	netdev = zyUMAC_wal_wdev_to_netdev(vap);
	if (!netdev) {
		zyUMAC_LOG(LOG_ERR, "netdev is null");
		return;
	}

	ni = zyUMAC_wal_wnode_get(vap, macaddr);
	if (ni == NULL) {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
		return;
	}

	switch (zyUMAC_GetReason(reason)) {
	case IEEE80211_REASON_AUTH_EXPIRE:
		strncpy(reason_str, "wrong password", REASON_STR_LEN);
		break;
	case IEEE80211_STATUS_TOOMANY:
		strncpy(reason_str, "too many stations", REASON_STR_LEN);
		break;
	}

#ifdef ZY_FM_SUPPORT
	notify_assoc_fail(netdev->name,
			  zyUMAC_wal_wnode_get_macaddr(ni),
			  reason_str);
#endif
	if (ni) {
		zyUMAC_wal_wnode_put(ni);
	}
	return;
#undef REASON_STR_LEN
}

//static void zyUMAC_node_authorized_indication_ap(os_handle_t osif, u_int8_t *mac_addr)
//{
//    return;
//}

#ifdef ZY_LEAVE_REPORT_SUPPORT
static int sk_record_sta_fail_status(wlan_if_t vap, u_int8_t * macaddr,
				     sta_leave_args_t * arg)
{
	struct timeval tv;
	sta_leave_entry_t *p_entry = NULL;
	sta_leave_entry_mgr_t *p_leave_table = NULL;
	int ret = FAIL_STATUS_REPORT;
	int i, j;
	extern struct timezone sys_tz;
	zyUMAC_vap_priv_t *tmpPtr = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	if (!vap || !tmpPtr) {
		zyUMAC_LOG(LOG_ERR, "wdev or private data is null");
		return FAIL_STATUS_DROP;
	}
	p_leave_table = tmpPtr->iv_sta_fail_status_tab;
	if (!p_leave_table) {
		zyUMAC_LOG(LOG_ERR, "Invalid arguments");
		return -1;
	}
	do_gettimeofday(&tv);
	tv.tv_sec = tv.tv_sec - sys_tz.tz_minuteswest * 60;
	arg->online_time = 0;

	p_leave_table->next_entry_num %= MAX_RECORD_STA_PER_VAP;
	/*record fail status to fail table */
	spin_lock(&(p_leave_table->leave_lock));
	p_entry = &p_leave_table->entry[p_leave_table->next_entry_num];
	p_entry->frame_type = arg->frame_type;
	p_entry->online_time = arg->online_time;
	p_entry->reason = arg->reason;
	p_entry->sender = arg->sender;
	memcpy(&p_entry->record_time, &tv, sizeof(tv));
	memcpy(&p_entry->mac, macaddr, sizeof(p_entry->mac));

	/*if this is first record, need to report, else need to check if there are same sta records before */
	if (p_leave_table->size == 0)
		ret = FAIL_STATUS_REPORT;
	else {
		for (j = 0, i = p_leave_table->next_entry_num - 1;
		     j < p_leave_table->size; j++, i--) {
			if (i < 0) {
				i = MAX_RECORD_STA_PER_VAP - 1;
			}

			if (IEEE80211_ADDR_EQ
			    (macaddr, p_leave_table->entry[i].mac)
			    && p_leave_table->entry[i].report_flag ==
			    FAIL_STATUS_REPORT) {
				if (tv.tv_sec -
				    p_leave_table->entry[i].record_time.tv_sec <
				    MAX_AC_SEND_MIN) {
					ret = FAIL_STATUS_DROP;
				}
				break;
			}
		}
	}
	p_entry->report_flag = (unsigned short)ret;
	p_leave_table->next_entry_num++;
	if (p_leave_table->size < MAX_RECORD_STA_PER_VAP) {
		p_leave_table->size++;
	}
	spin_unlock(&(p_leave_table->leave_lock));

	return ret;
}

static int sk_record_sta_leave_reason(wlan_if_t vap, u_int8_t * macaddr,
				      sta_leave_args_t * arg)
{
	struct timeval tv;
	zyUMAC_wal_wnode_t ni;
	sta_leave_entry_t *p_entry = NULL;
	sta_leave_entry_mgr_t *p_leave_table = NULL;
	extern struct timezone sys_tz;
	u_int32_t jiffies_now = 0, jiffies_delta = 0, jiffies_assoc = 0;
	zyUMAC_vap_priv_t *tmpPtr = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	if (!vap || !tmpPtr) {
		zyUMAC_LOG(LOG_ERR, "Invalid arguments");
		return -1;
	}

	p_leave_table = tmpPtr->iv_sta_leave_reason_tab;
	if (!p_leave_table) {
		zyUMAC_LOG(LOG_ERR, "Invalid arguments");
		return -1;
	}
	do_gettimeofday(&tv);
	tv.tv_sec = tv.tv_sec - sys_tz.tz_minuteswest * 60;

	ni = zyUMAC_wal_wnode_get(vap, macaddr);

	if (ni == NULL || !zyUMAC_wal_wnode_is_authed(ni)) {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
		arg->online_time = 0;
	} else {
		/* bug26155 */
//add for difference assignments of ni->ni_assocuptime
#ifdef UMAC_SUPPORT_CFG80211
		jiffies_now = OS_GET_TICKS();
#else
		jiffies_now = OS_GET_UPTIME();
#endif
		jiffies_assoc = zyUMAC_wal_wnode_get_assocuptime(ni);
		jiffies_delta = jiffies_now - jiffies_assoc;
		arg->online_time = (int)(jiffies_delta / HZ);
	}

	spin_lock(&(p_leave_table->leave_lock));
	p_leave_table->next_entry_num %= MAX_RECORD_STA_PER_VAP;
	p_entry = &p_leave_table->entry[p_leave_table->next_entry_num];
	p_entry->frame_type = arg->frame_type;
	p_entry->online_time = arg->online_time;
	p_entry->reason = arg->reason;
	p_entry->sender = arg->sender;
	memcpy(p_entry->mac, macaddr, sizeof(p_entry->mac));
	memcpy(&p_entry->record_time, &tv, sizeof(tv));

	p_leave_table->next_entry_num++;
	if (p_leave_table->size < MAX_RECORD_STA_PER_VAP) {
		p_leave_table->size++;
	}
	spin_unlock(&(p_leave_table->leave_lock));

	if (ni) {
		zyUMAC_wal_wnode_put(ni);
	}
	return 0;
}

void sk_osif_sta_leave_indication_ap(zyUMAC_wal_wdev_t vap, u_int8_t * macaddr,
				     void *args)
{
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_stats_node_netlink_event *p_sta;
	sta_leave_rpt_info_t *p_rpt_info;
	sta_leave_args_t *p_leave_args = NULL;
	char *data = NULL;
	int data_len;
	int report = FAIL_STATUS_REPORT;
	zyUMAC_wal_wnode_t ni;
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_node_priv_stats_t *node_stats;

	if (!vap || !macaddr || !args) {
		zyUMAC_LOG(LOG_ERR, "Invalid arguments");
		return;
	}

	wphy = zyUMAC_wal_wdev_get_wphy(vap);
	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	ni = zyUMAC_wal_wnode_get(vap, macaddr);
	if (!ni) {
		zyUMAC_LOG(LOG_INFO, "wnode is null");
	}

	p_leave_args = (sta_leave_args_t *) args;

	if (p_leave_args->is_passive)
		report = sk_record_sta_fail_status(vap, macaddr, p_leave_args);
	else
		sk_record_sta_leave_reason(vap, macaddr, p_leave_args);

	data_len = sizeof(sta_leave_rpt_info_t) +
		   sizeof(zyUMAC_stats_node_netlink_event);

	data = kmalloc(data_len, GFP_KERNEL);
	if (!data) {
		if (ni)
			zyUMAC_wal_wnode_put(ni);

		zyUMAC_LOG(LOG_ERR, "failed to allocate data");
		return;
	}
	memset(data, 0, data_len);

	p_rpt_info = (sta_leave_rpt_info_t *) data;
	p_sta = (zyUMAC_stats_node_netlink_event *)
			(data + sizeof(sta_leave_rpt_info_t));

	p_rpt_info->is_passive = p_leave_args->is_passive;
	p_rpt_info->reason = p_leave_args->reason;
	p_rpt_info->sender = p_leave_args->sender;
	p_rpt_info->onlinetime = p_leave_args->online_time;

	p_sta->type = p_leave_args->event_type;
	p_sta->reason = p_leave_args->reason;
	p_sta->radio_type = zyUMAC_wal_wphy_get_id(wphy);
	memcpy(p_sta->mac, macaddr, IEEE80211_ADDR_LEN);
	if (ni) {
		zyUMAC_node_priv = ni->ni_zyUMAC_node_priv;
		p_sta->assoc_id = zyUMAC_wal_wnode_get_associd(ni);
		if (zyUMAC_node_priv) {
			node_stats = &zyUMAC_node_priv->ns_stats;
			zyUMAC_wal_wnode_update_stats(ni, node_stats);
	
			memcpy((char *)&p_sta->stats,
					(char *)node_stats,
					sizeof(zyUMAC_node_priv_stats_t));
			memcpy((char *)&(zyUMAC_node_priv->leave_info),
			       data,
			       sizeof(sta_leave_rpt_info_t));
			memcpy((char *)&(zyUMAC_node_priv->node_netlink_event),
			       data + sizeof(sta_leave_rpt_info_t),
			       sizeof(zyUMAC_stats_node_netlink_event));
			zyUMAC_node_priv->skleave_report = report;

			ath_sk_leave_netlink_bsend(vap,
					ZY_NETLINK_EVENT_STA,
					data,
					data_len,
					zyUMAC_wal_wnode_get_macaddr(ni),
					zyUMAC_node_priv->skleave_report);
		} else {
			zyUMAC_LOG(LOG_ERR, "zyUMAC_node_priv is null");
		}
		zyUMAC_wal_wnode_put(ni);
	} else
		p_sta->assoc_id = 0;

	kfree(data);
}
#endif

void send_sta_leave_args(zyUMAC_wal_wdev_t vap, u_int8_t * mac_addr,
			 int32_t reason, u_int16_t sender, u_int16_t frame_type,
			 u_int8_t is_passive)
{
#ifdef ZY_LEAVE_REPORT_SUPPORT
	sta_leave_args_t leave_arg = { 0 };
	leave_arg.reason = reason;
	leave_arg.sender = sender;
//    leave_arg.event_type = ATH_EVENT_NODE_LEAVE;
	leave_arg.frame_type = frame_type;
	leave_arg.is_passive = is_passive;
	//TODO send to WTP and write to proc
	sk_osif_sta_leave_indication_ap(vap, mac_addr, &leave_arg);
#endif
	return;
}
