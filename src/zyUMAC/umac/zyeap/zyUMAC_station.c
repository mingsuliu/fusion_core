#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_vap.h"
#include "zyUMAC_wal_api.h"

int zyUMAC_find_left_node(zyUMAC_wal_wdev_t vap, const u_int8_t *mac)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = vap->iv_zyUMAC_vap_priv;
	int i;

	for (i = 0; i < MAX_LEFT_STA_NUM; i++) {
		zyUMAC_left_node_stats_t *left_node =
		    &zyUMAC_vap_priv->left_node_tbl.node[i];

		if (memcmp(left_node->mac, mac, zyUMAC_IEEE80211_ADDR_LEN) == 0)
			break;
	}

	return i;
}

void zyUMAC_update_left_node(zyUMAC_wal_wdev_t vap, zyUMAC_wal_wnode_t ni)
{
	int idx;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv;
	zyUMAC_left_node_stats_t *left_node;
	zyUMAC_node_priv_t *zyumac_node_priv;

	if (vap == NULL)
		return;

	zyUMAC_vap_priv = vap->iv_zyUMAC_vap_priv;
	zyumac_node_priv = ni->ni_zyUMAC_node_priv;

	idx = zyUMAC_find_left_node(vap, zyUMAC_wal_wnode_get_macaddr(ni));

	/* If we find the entry in the node table, just use this entry */
	if (idx != MAX_LEFT_STA_NUM) {
		left_node = &zyUMAC_vap_priv->left_node_tbl.node[idx];
	} else {
		left_node =
		    &zyUMAC_vap_priv->left_node_tbl.node[zyUMAC_vap_priv->left_node_tbl.idx];
		zyUMAC_vap_priv->left_node_tbl.idx++;
		zyUMAC_vap_priv->left_node_tbl.idx &= (MAX_LEFT_STA_NUM - 1);
	}

	memcpy(left_node->mac, zyUMAC_wal_wnode_get_macaddr(ni), MAC_ADDR_LEN);

	zyUMAC_wal_wnode_update_stats(ni, &zyumac_node_priv->ns_stats);

	left_node->stats.rx_bytes =
	    zyumac_node_priv->ns_stats.wal_stats.ns_rx_success_bytes;
	left_node->stats.tx_bytes =
	    zyumac_node_priv->ns_stats.wal_stats.ns_tx_success_bytes;
	left_node->stats.rx_packets =
	    zyumac_node_priv->ns_stats.wal_stats.ns_rx_success_num;
	left_node->stats.tx_packets =
	    zyumac_node_priv->ns_stats.wal_stats.ns_tx_success_num;
}

int zyUWAL_wdev_hook_get_left_station_stats(zyUMAC_wal_wdev_t vap,
					   zyUMAC_wal_station_txrx_stats_t *stats, const u_int8_t *mac)
{
	int idx;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = vap->iv_zyUMAC_vap_priv;

	if (vap == NULL || stats == NULL || mac == NULL)
		return -1;

	/* If we can't find ni, search the cached leaving station table in VAP */
	idx = zyUMAC_find_left_node(vap, mac);
	if (idx != MAX_LEFT_STA_NUM) {
		zyUMAC_left_node_stats_t *left_node =
		    &zyUMAC_vap_priv->left_node_tbl.node[idx];

		stats->rx_bytes = left_node->stats.rx_bytes;
		stats->tx_bytes = left_node->stats.tx_bytes;
		stats->rx_packets = left_node->stats.rx_packets;
		stats->tx_packets = left_node->stats.tx_packets;
	} else {
		return -1;
	}

	return 0;
}
