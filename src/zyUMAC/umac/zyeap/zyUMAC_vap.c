#include "zyUMAC_wal_api.h"
#include "zyUMAC_common_var.h"
#include "zyUMAC_vap.h"
#include "zyUMAC_traffic_report.h"
#include "zyUMAC_mgmt_hook.h"
#include "zyUMAC_proc.h"
#include "zyUMAC_events.h"
#include "zyUMAC_data_hook.h"

#define ETH_P_ZYXEL_80211_MGMT_FRAMES    0x9400

static int zyUMAC_wdev_hook_create(zyUMAC_wal_wdev_t vap)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;

	//add new data structure
	zyUMAC_LOG(LOG_INFO, "Try to allocate wdev");
	zyUMAC_vap_priv = kmalloc(sizeof(zyUMAC_vap_priv_t), GFP_KERNEL);
	if (zyUMAC_vap_priv == NULL) {
		// @TODO:
		zyUMAC_LOG(LOG_ERR, "cannot create vap priv");
		return -1;
	}

	memset(zyUMAC_vap_priv, 0, sizeof(zyUMAC_vap_priv_t));
	zyUMAC_wal_wdev_setdata(vap, zyUMAC_vap_priv);

	//redefine the iw ioctl handler
	zyUMAC_vap_ioctl_attach(vap);
	zyUMAC_proc_attach(vap);
	zyUMAC_acl_attach(vap);
	zyUMAC_loadbalance_attach(vap);

	zyUMAC_vap_priv->iv_sta_leave_reason_tab =
	    kmalloc(sizeof(sta_leave_entry_mgr_t), GFP_KERNEL);
	if (zyUMAC_vap_priv->iv_sta_leave_reason_tab) {
		zyUMAC_vap_priv->iv_sta_leave_reason_tab->next_entry_num = 0;
		zyUMAC_vap_priv->iv_sta_leave_reason_tab->size = 0;
		spin_lock_init(&
			       (zyUMAC_vap_priv->iv_sta_leave_reason_tab->
				leave_lock));
		memset(zyUMAC_vap_priv->iv_sta_leave_reason_tab->entry, 0,
		       sizeof(zyUMAC_vap_priv->iv_sta_leave_reason_tab->entry));
	}

	zyUMAC_vap_priv->iv_sta_fail_status_tab =
	    kmalloc(sizeof(sta_leave_entry_mgr_t), GFP_KERNEL);
	if (zyUMAC_vap_priv->iv_sta_fail_status_tab) {
		zyUMAC_vap_priv->iv_sta_fail_status_tab->next_entry_num = 0;
		zyUMAC_vap_priv->iv_sta_fail_status_tab->size = 0;
		spin_lock_init(&
			       (zyUMAC_vap_priv->iv_sta_fail_status_tab->
				leave_lock));
		memset(zyUMAC_vap_priv->iv_sta_fail_status_tab->entry, 0,
		       sizeof(zyUMAC_vap_priv->iv_sta_fail_status_tab->entry));
	}
#ifdef ZY_BDST_SUPPORT
	zyUMAC_vap_priv->app_config.band_steering = 0;
	zyUMAC_vap_priv->app_config.band_steering_count = 3;
	zyUMAC_vap_priv->app_config.band_steering_time = 10;
	zyUMAC_vap_priv->app_config.band_steering_mode = 2;
	zyUMAC_vap_priv->app_config.band_steering_debug = 0;
	zyUMAC_vap_priv->app_config.band_steering_hash = 0;
#endif

	return 0;
}

static void zyUMAC_wdev_hook_delete(zyUMAC_wal_wdev_t wdev)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
#ifdef CONFIG_DP_SUPPORT
	zyUMAC_stats_vap_netlink_event *vap_event;
	zyUMAC_vap_priv_stats_t *vapstats;
#endif
	zyUMAC_LOG(LOG_INFO, "Try to free wdev");

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (zyUMAC_vap_priv == NULL) {
		zyUMAC_LOG(LOG_ERR, "not allocated or invalid state");
		return; /*not allocated or invalid state*/
	}

	if (zyUMAC_wal_wdev_is_ap_mode(wdev)) {
#ifdef CONFIG_DP_SUPPORT
		vap_event = &zyUMAC_vap_priv->vap_netlink_event;
		vapstats = &vap_event->stats;
		zyUMAC_wal_wdev_update_stats(wdev, vapstats);

		vap_event->rx_fcserr = vapstats->wal_stats.unicast_rx_fcserr +
				     vapstats->wal_stats.multicast_rx_fcserr;
		vap_event->rx_micerr = vapstats->wal_stats.unicast_rx_tkipmic +
				     vapstats->wal_stats.multicast_rx_tkipmic;

		ath_sk_leave_netlink_bsend(wdev, FU_NETLINK_EVENT_VAP,
					vap_event,
					sizeof(zyUMAC_stats_vap_netlink_event),
					NULL,
					1);
#endif
	}

	zyUMAC_proc_detach(wdev);
	zyUMAC_acl_detach(wdev);
	zyUMAC_loadbalance_detach(wdev);

	ASSERT(zyUMAC_vap_priv->iv_sta_leave_reason_tab != NULL);
	kfree(zyUMAC_vap_priv->iv_sta_leave_reason_tab);
	zyUMAC_vap_priv->iv_sta_leave_reason_tab = NULL;

	ASSERT(zyUMAC_vap_priv->iv_sta_fail_status_tab != NULL);
	kfree(zyUMAC_vap_priv->iv_sta_fail_status_tab);
	zyUMAC_vap_priv->iv_sta_fail_status_tab = NULL;

	kfree(zyUMAC_vap_priv);
	zyUMAC_wal_wdev_setdata(wdev, NULL);
}

#ifdef CONFIG_DP_SUPPORT
static void zyUMAC_osif_indicate_mgmt(zyUMAC_wal_wdev_t wdev, struct sk_buff *skb,
				     u_int16_t type, u_int16_t subtype,
				     zyUMAC_wal_rx_status_t rs)
{
	struct net_device *dev = zyUMAC_wal_wdev_to_netdev(wdev);
	struct ethhdr *eth_header;
	zyUMAC_ieee80211_header_t *wh;

	if (skb_headroom(skb) < ETH_HLEN) {
		dev_kfree_skb_any(skb);
		return;
	}

	skb->protocol = htons(ETH_P_ZYXEL_80211_MGMT_FRAMES);
	wh = (zyUMAC_ieee80211_header_t *) skb->data;
	eth_header = (struct ethhdr *)skb_push(skb, ETH_HLEN);
	eth_header->h_proto = htons(ETH_P_ZYXEL_80211_MGMT_FRAMES);
	memcpy(eth_header->h_source,
	       zyUMAC_wal_ieee80211_header_get_addr2(wh),
	       MAC_ADDR_LEN);
	memcpy(eth_header->h_dest,
	       zyUMAC_wal_ieee80211_header_get_addr1(wh),
	       MAC_ADDR_LEN);

	skb->dev = dev;

	// Calls skb_orphan will call into wbuf_dealloc_mgmt_ctrl_block to free the
	// memory for wbuf used in qca-wifi. Without this, the kernel will panic
	// sometimes.
	skb_orphan(skb);

	if (in_interrupt()) {
		netif_rx(skb);
	} else {
		netif_rx_ni(skb);
	}
}
#endif

static int zyUMAC_wdev_hook_rx(zyUMAC_wal_wdev_t wdev, struct sk_buff *skb,
			      u_int16_t type, u_int16_t subtype,
			      zyUMAC_wal_rx_status_t rs)
{
#ifdef CONFIG_DP_SUPPORT
	if (type == IEEE80211_FC0_TYPE_MGT
	    && subtype == IEEE80211_FC0_SUBTYPE_AUTH) {
		zyUMAC_osif_indicate_mgmt(wdev, skb, type, subtype, rs);
		return 0;
	}
#endif

	if (type == IEEE80211_FC0_TYPE_DATA) {
		zyUMAC_WAL_wdev_hook_rx_data_inspec(wdev, skb);
		return 0;
	}	

	return -1;
}

static int zyUMAC_WAL_wdev_hook_node_create(zyUMAC_wal_wnode_t wnode)
{
	zyUMAC_node_priv_t *node_priv = NULL;
#ifdef ZY_BDST_SUPPORT
	zyUMAC_wal_wphy_t wphy = NULL;
	zyUMAC_wal_wdev_t vap = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
#endif

	node_priv =
	    (zyUMAC_node_priv_t *) kmalloc(sizeof(zyUMAC_node_priv_t),
					  GFP_KERNEL);
	if (node_priv == NULL) {
		zyUMAC_LOG(LOG_ERR,
			"can't allocate memory for ni_zyUMAC_node_priv!");
		return -1;
	}

	memset(node_priv, 0, sizeof(zyUMAC_node_priv_t));
	zyUMAC_wal_wnode_setdata(wnode, node_priv);

#ifdef ZY_BDST_SUPPORT
	vap = zyUMAC_wal_wnode_get_wdev(wnode);
	wphy = zyUMAC_wal_wdev_get_wphy(vap);
	zyUMAC_scn_priv =
		(zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(wphy);

	zyUMAC_scn_priv->bdst_client_count++;
#endif
	return 0;
}

static void zyUMAC_WAL_wdev_hook_node_delete(zyUMAC_wal_wnode_t ni)
{
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
#ifdef ZY_BDST_SUPPORT
	zyUMAC_wal_wphy_t wphy = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
#endif
#ifdef ZY_LEAVE_REPORT_SUPPORT
	char *data = NULL;
	int data_len = 0;
#endif
	zyUMAC_wal_wdev_t vap = NULL;

	if (!ni) {
		zyUMAC_LOG(LOG_ERR, "wnode is null!");
		return;
	}

	vap = zyUMAC_wal_wnode_get_wdev(ni);
	zyUMAC_node_priv = (zyUMAC_node_priv_t *) zyUMAC_wal_wnode_getdata(ni);

#ifdef ZY_LEAVE_REPORT_SUPPORT
	data_len = sizeof(sta_leave_rpt_info_t) +
		       sizeof(zyUMAC_stats_node_netlink_event);

	if (zyUMAC_node_priv && vap &&
	    !IEEE80211_ADDR_EQ(zyUMAC_wal_wdev_get_macaddr(vap),
			       zyUMAC_wal_wnode_get_macaddr(ni))) {

		if (zyUMAC_node_priv->node_netlink_event.type == 0) {
			zyUMAC_node_priv->node_netlink_event.type =
			    zyUMAC_EVENT_NODE_LEAVE;
			zyUMAC_node_priv->node_netlink_event.assoc_id =
			    zyUMAC_wal_wnode_get_associd(ni);
			memcpy(zyUMAC_node_priv->node_netlink_event.mac,
			       zyUMAC_wal_wnode_get_macaddr(ni), 6);
			zyUMAC_node_priv->node_netlink_event.radio_type =
			    zyUMAC_wal_wnode_get_radioid(ni);
		}

		data = kmalloc(data_len, GFP_KERNEL);
		if (data) {
			memcpy(data,
			       &(zyUMAC_node_priv->leave_info),
			       sizeof(sta_leave_rpt_info_t));
			memcpy(data + sizeof(sta_leave_rpt_info_t),
			       &(zyUMAC_node_priv->node_netlink_event),
			       sizeof(zyUMAC_stats_node_netlink_event));

			ath_sk_leave_netlink_bsend(vap, 0, data,
				data_len,
				zyUMAC_wal_wnode_get_macaddr(ni),
				zyUMAC_node_priv->skleave_report);

			kfree(data);
		}
	}
#endif

#ifdef ZY_BDST_SUPPORT
	if (vap) {
		wphy = zyUMAC_wal_wdev_get_wphy(vap);
		zyUMAC_scn_priv =
			(zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(wphy);
		zyUMAC_scn_priv->bdst_client_count--;
	}
#endif

	if (zyUMAC_node_priv != NULL) {
		kfree(zyUMAC_node_priv);
		zyUMAC_wal_wnode_setdata(ni, NULL);
	}

	return;
}

static zyUMAC_wdev_hook_table _zyUMAC_wdev_hook_table = {
	zyUMAC_wdev_hook_create,
	zyUMAC_wdev_hook_delete,
	zyUMAC_vap_input_mgmt_filter,
	zyUMAC_vap_output_mgmt_filter,
	zyUMAC_wdev_hook_rx,
	zyUMAC_WAL_wdev_hook_node_create,
	zyUMAC_WAL_wdev_hook_node_delete,
	NULL,
	NULL
};

void zyUMAC_vap_init(zyUMAC_wal_wphy_t wphy)
{
	zyUMAC_wal_wdev_attach(wphy, &_zyUMAC_wdev_hook_table);
}
