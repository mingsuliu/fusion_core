/*copied from ZY_module/ZY_band_steering/sk_bandsteering_module.c*/

#include <linux/err.h>
#include <linux/netdevice.h>
#include <linux/vmalloc.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/jhash.h>
#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC.h"
#include "zyUMAC_band_steering.h"

#ifdef ZY_BDST_SUPPORT

/* */
/*  spin_lock enhanced for Nested spin lock */
/* */
#ifndef OS_NdisAllocateSpinLock
#define OS_NdisAllocateSpinLock(__lock)		\
	spin_lock_init((spinlock_t *)(__lock))
#endif

#ifndef OS_NdisFreeSpinLock
#define OS_NdisFreeSpinLock(lock)		\
	do {} while (0)
#endif

#ifndef OS_SEM_LOCK
#define OS_SEM_LOCK(__lock)		\
	spin_lock_bh((spinlock_t *)(__lock))
#endif

#ifndef OS_SEM_UNLOCK
#define OS_SEM_UNLOCK(__lock)		\
	spin_unlock_bh((spinlock_t *)(__lock))
#endif

void add_band_steering_sta(unsigned char *mac);

static band_sta_info_t *band_sta_hash[BAND_STA_HASH_SIZE];
static band_sta_info_t band_sta_entries[BAND_STA_MAX_CLIENTS];

static int bst_num_cli = 0;

static unsigned long bst_g_client_count_time;
static unsigned long bst_a_client_count_time;
static int bst_allow_g_clients;

static spinlock_t bst_lock;
static unsigned int band_salt __read_mostly;

#define BST_LOCK_INIT()		\
	OS_NdisAllocateSpinLock(&bst_lock)

#define BST_LOCK_UNINIT()	\
	OS_NdisFreeSpinLock(&bst_lock)

#define BST_LOCK()		\
	OS_SEM_LOCK(&bst_lock)

#define BST_UNLOCK()		\
	OS_SEM_UNLOCK(&bst_lock)

void band_sta_hash_init(int first_time)
{
	int i;

	for (i = 0; i < BAND_STA_HASH_SIZE; i++)
		band_sta_hash[i] = NULL;
	for (i = 0; i < BAND_STA_MAX_CLIENTS; i++) {
		band_sta_entries[i].last_pkt_time = ULONG_MAX;
		band_sta_entries[i].next = NULL;
		band_sta_entries[i].g_burst_start_time = 0;
		band_sta_entries[i].g_burst_count = 0;
		band_sta_entries[i].g_seqno = 0;
		band_sta_entries[i].a_burst_start_time = 0;
		band_sta_entries[i].a_burst_count = 0;
		band_sta_entries[i].a_seqno = 0;
		band_sta_entries[i].unlocal = 0;
		band_sta_entries[i].is_support_5G = 0;
		band_sta_entries[i].lb_reject_count = 0;
		band_sta_entries[i].reject_count = 0;
		band_sta_entries[i].band_steering_fail_count = 0;
	}

	bst_g_client_count_time = 0;

	bst_a_client_count_time = 0;

	bst_allow_g_clients = 0;

	if (first_time)
		BST_LOCK_INIT();

	get_random_bytes(&band_salt, sizeof(band_salt));
	zyUMAC_LOG(LOG_INFO, "hash init");
}

int del_bsi_node(band_sta_info_t * bsi)
{

	if (bsi->last_pkt_time != ULONG_MAX) {
		memset(bsi->mac, 0, zyUMAC_IEEE80211_ADDR_LEN);
		bsi->g_burst_start_time = 0;
		bsi->g_burst_count = 0;
		bsi->g_seqno = 0;
		bsi->a_burst_start_time = 0;
		bsi->a_burst_count = 0;
		bsi->a_seqno = 0;
		bsi->unlocal = 0;
		bsi->next = NULL;
		bsi->last_pkt_time = ULONG_MAX;
		bsi->lb_reject_count = 0;
		bsi->is_support_5G = 0;
		bsi->reject_count = 0;
		bsi->band_steering_fail_count = 0;
		bst_num_cli--;
	}
	return 1;
}

static inline int band_mac_hash(const unsigned char *mac)
{
	unsigned int key = get_unaligned((unsigned int *)(mac + 2));
	return jhash_1word(key, band_salt) & (BAND_STA_HASH_SIZE - 1);
}

static void
zyUMAC_bdst_get_sta_count(int *bst_g_client_count, int *bst_a_client_count)
{
	int i;
	int g_count = 0, a_count = 0;
	zyUMAC_wal_wdev_t vap = NULL;
	zyUMAC_wal_wphy_t wphy = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	int chan;

	for (i = 0; i < BAND_IDX_MAX; i++) {
		wphy = zyUMAC_wal_wdev_get_wphy_by_id(i);
		if (!wphy)
			continue;

		zyUMAC_scn_priv =
			(zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(wphy);
		if (!zyUMAC_scn_priv)
			continue;

		vap = zyUMAC_wal_wphy_get_first_wdev(wphy);
		if (!vap)
			continue;

		chan = (int)zyUMAC_wal_wdev_get_chan(vap);

		if (zyUMAC_IS_CHAN_2GHZ(chan))
			g_count += zyUMAC_scn_priv->bdst_client_count;
		else
			a_count += zyUMAC_scn_priv->bdst_client_count;
	}

	*bst_g_client_count = g_count;
	*bst_a_client_count = a_count;
}

/* skspruce band steering new alg */
int bdst_band_sta_24_band_process(zyUMAC_wal_wdev_t vap, unsigned char *mac,
				u_int16_t seqno, int rssi)
{
	band_sta_info_t *bsi;
	int exists = -1;
	int drop = 1;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	int bst_g_client_count, bst_a_client_count;

	BST_LOCK();

	if (mac == NULL)
		goto done;

	if ((zyUMAC_vap_priv =
		(zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv) == NULL) {
		goto done;
	}

	zyUMAC_bdst_get_sta_count(&bst_g_client_count, &bst_a_client_count);

	for (bsi = band_sta_hash[band_mac_hash(mac)]; bsi; bsi = bsi->next) {
		if (IEEE80211_ADDR_EQ(mac, bsi->mac)) {
			break;
		}
	}

	/* the first time add 2.4G sta mac to hash table */
	if (!bsi) {
		if (zyUMAC_vap_priv->app_config.band_steering_debug)
			zyUMAC_LOG(LOG_INFO, MACSTR, MAC2STR(mac));
		BST_UNLOCK();
		add_band_steering_sta(mac);

		for (bsi = band_sta_hash[band_mac_hash(mac)]; bsi;
			bsi = bsi->next) {
			if (IEEE80211_ADDR_EQ(mac, bsi->mac)) {
				break;
			}
		}
		if (bsi) {
			bsi->is_support_5G = 0;
			bsi->reject_count++;
			bsi->g_last_pkt_time = bsi->last_pkt_time = jiffies;
			bsi->g_burst_start_time = jiffies;
		}

#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
		/* notice redis-client query this sta is support 5G */
		sk_redis_client_nl_report(mac, zyUMAC_IEEE80211_ADDR_LEN,
					  BS_NETLINK_REPORT_QUERY_5G_STA);
#endif		

		return drop;
	}

#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
	/* when 2.4G sta auth && isn't 5G,  again query redis-server */
	if (bsi) {
		if (!bsi->is_support_5G) {
			sk_redis_client_nl_report(mac, zyUMAC_IEEE80211_ADDR_LEN,
						  BS_NETLINK_REPORT_QUERY_5G_STA);
		}
	}
#endif	

	/* the continuous auth packet interval time less 50 millisecond then ignore */
	if (time_before_eq(jiffies, bsi->g_last_pkt_time + HZ / 20)) {
		bsi->g_last_pkt_time = bsi->last_pkt_time = jiffies;
		if (zyUMAC_vap_priv->app_config.band_steering_debug) {
			zyUMAC_LOG(LOG_INFO,
				"bsi->last_pkt_time = %lu jiffies = %lu",
				bsi->last_pkt_time, jiffies);
		}
		BST_UNLOCK();
		return drop;
	}

	if (bsi->is_support_5G) {
		unsigned long cur_sec = jiffies;

		if time_after_eq
			(jiffies,
			 bsi->last_pkt_time +
			 (ZY_BAND_STEERING_FAIL_COUNT_AGETIME) * HZ) {
			bsi->band_steering_fail_count = 0;
			}

		/* if this sta can't band steering exceed 10 times */
		if (bsi->band_steering_fail_count >= 10) {
			if (zyUMAC_vap_priv->app_config.band_steering_debug) {
				zyUMAC_LOG(LOG_INFO,
				    "mac address %02x:%02x:%02x:%02x:%02x:%02x band_steering_fail_count %u reject_count %d",
				    mac[0], mac[1], mac[2], mac[3], mac[4],
				    mac[5], bsi->band_steering_fail_count,
				    bsi->reject_count);
			}
			BST_UNLOCK();
			return 0;
		}

		if ((bst_g_client_count == 0)
		    && (bst_a_client_count >= BAND_STA_BAND_PREFERENCE_RATIO)) {
			if (zyUMAC_vap_priv->app_config.band_steering_debug) {
				zyUMAC_LOG(LOG_INFO, "bst_g_client_count:%d",
					bst_g_client_count);
			}
			bst_allow_g_clients = 1;
		} else if ((bst_g_client_count)
			   && (BAND_STA_BAND_PREFERENCE_RATIO *
			       bst_g_client_count < bst_a_client_count)) {
			if (zyUMAC_vap_priv->app_config.band_steering_debug) {
				zyUMAC_LOG(LOG_INFO, "bst_a_client_count:%d",
					bst_a_client_count);
			}
			bst_allow_g_clients = 1;
		} else {
			bst_allow_g_clients = 0;
		}

		if (time_before_eq
		    (cur_sec,
		     bsi->g_last_pkt_time +
		     zyUMAC_vap_priv->app_config.band_steering_time * HZ)) {
			if (bsi->reject_count >=
			    zyUMAC_vap_priv->app_config.band_steering_count) {
				if (zyUMAC_vap_priv->app_config.
				    band_steering_debug) {
					zyUMAC_LOG(LOG_INFO,
					    "bsi->g_burst_count > %d %u",
					    zyUMAC_vap_priv->app_config.
					    band_steering_count,
					    bsi->reject_count);
				}
				bsi->band_steering_fail_count++;
#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
				sk_redis_client_nl_report(mac,
						zyUMAC_IEEE80211_ADDR_LEN,
						BS_NETLINK_REPORT_FAIL_STA);
#endif				
				exists = 0;
				drop = 0;

			} else {
				bsi->reject_count++;

				if (zyUMAC_vap_priv->app_config.
				    band_steering_debug) {
					zyUMAC_LOG(LOG_INFO,
					    "jiffies = %lu bsi->reject_count = %u",
					    jiffies,
					    bsi->reject_count);
					zyUMAC_LOG(LOG_INFO,
					    "band_steering_time:%d band_steering_counter:%d ++ reject %u last_pkt_time: %lu",
					    zyUMAC_vap_priv->app_config.
						band_steering_time,
					    zyUMAC_vap_priv->app_config.
						band_steering_count,
					    bsi->reject_count,
					    cur_sec - bsi->last_pkt_time);
				}
			}
		} else {
			bsi->g_burst_start_time = cur_sec;
			bsi->reject_count = 1;
			if (zyUMAC_vap_priv->app_config.band_steering_debug) {
				zyUMAC_LOG(LOG_INFO,
				    "band_steering_time:%d band_steering_counter:%d ++ reject %u g_burst_start_time: %lu",
				    zyUMAC_vap_priv->app_config.
					band_steering_time,
				    zyUMAC_vap_priv->app_config.
					band_steering_count,
				    bsi->reject_count,
				    cur_sec - bsi->g_burst_start_time);
				zyUMAC_LOG(LOG_INFO,
				    "jiffies = %lu bsi->reject_count = %u",
				    jiffies, bsi->reject_count);
			}
		}

		bsi->last_pkt_time = cur_sec;
		bsi->g_last_pkt_time = cur_sec;

		/*
		 * Now balance across bands
		 */
		if ((zyUMAC_vap_priv->app_config.band_steering_mode == 1)
		    && bst_allow_g_clients) {
			exists = 0;
			drop = 0;
		}

	}
	/* sta don't support 5G */
	else {
		exists = 0;
		drop = 0;
	}

#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
	if (exists) {
		sk_redis_client_nl_report(mac, zyUMAC_IEEE80211_ADDR_LEN,
					  BS_NETLINK_REPORT_DENY_STA);
	}
#endif	

done:
	BST_UNLOCK();
	return drop;
}

int
bdst_band_sta_5_band_process(zyUMAC_wal_wdev_t vap,
			     unsigned char *mac, u_int16_t seqno, int rssi,
			     int flag)
{
	band_sta_info_t *bsi = NULL;
	band_sta_info_t *free_bsi = NULL;
	band_sta_info_t *prev = NULL;
	int i;
	int new_entry = 0;
	unsigned long cur_sec = jiffies;
	int hash;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	int  drop = 0;
	int bst_g_client_count, bst_a_client_count;

	BST_LOCK();
	if (mac == NULL)
		goto done;

	hash = band_mac_hash(mac);
	for (bsi = band_sta_hash[hash]; bsi; bsi = bsi->next) {
		if (IEEE80211_ADDR_EQ(mac, bsi->mac))
			break;
	}

	/* if flag equa 1, this is 5G probe packet */
	if (flag == 1 && bsi) {
		goto done;
	}

	if (vap) {
		zyUMAC_vap_priv = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;

		if (zyUMAC_vap_priv->app_config.band_steering_debug)
			zyUMAC_LOG(LOG_INFO, MACSTR, MAC2STR(mac));
	}

	zyUMAC_bdst_get_sta_count(&bst_g_client_count, &bst_a_client_count);

	if (!bsi) {

		if (bst_num_cli >= BAND_STA_MAX_CLIENTS) {
			unsigned long bsi_last_pkt_time_oldest = jiffies;
			band_sta_info_t *oldbsi = NULL;
			unsigned long bsi_unlocal_pkt_time_oldest = jiffies;
			band_sta_info_t *unlocalbsi = NULL;

			// ageout all stale entries
			for (i = 0; i < BAND_STA_HASH_SIZE; i++) {
				prev = NULL;
				for (bsi = band_sta_hash[i]; bsi;
				     bsi = bsi->next) {
					if (time_after
					    (jiffies,
					     bsi->last_pkt_time +
					     BAND_STA_CLI_TIMEOUT * HZ)) {
						if (prev == NULL) {
							band_sta_hash[i] =
							    bsi->next;
						} else {
							prev->next = bsi->next;
						}

						del_bsi_node(bsi);
						free_bsi = bsi;
					} else {
						if (i == hash) {
							if (time_before
							    (bsi->last_pkt_time,
							     bsi_last_pkt_time_oldest))
							{
								bsi_last_pkt_time_oldest
								    =
								    bsi->
								    last_pkt_time;
								oldbsi = bsi;
							}
					} else if (vap
							   && (bsi->unlocal ==
							       1)
							   &&
							   (time_before
							    (bsi->last_pkt_time,
							     bsi_unlocal_pkt_time_oldest)))
						{
							bsi_unlocal_pkt_time_oldest
							    =
							    bsi->last_pkt_time;
							unlocalbsi = bsi;
						}
					}

					prev = bsi;
				}
			}
			if (oldbsi) {
				del_bsi_node(oldbsi);

				bsi = oldbsi;
				memcpy(bsi->mac, mac, 6);
				bst_num_cli++;
				new_entry = 1;
			} else if (free_bsi) {

				bsi = free_bsi;
				bsi->next = band_sta_hash[hash];
				band_sta_hash[hash] = bsi;

				memcpy(bsi->mac, mac, 6);
				bst_num_cli++;
				new_entry = 1;
			} else if (unlocalbsi) {
				del_bsi_node(unlocalbsi);

				bsi = unlocalbsi;
				memcpy(bsi->mac, mac, 6);
				bst_num_cli++;
				new_entry = 1;
			}
		} else {
			// find an empty slot
			bsi = NULL;
			for (i = 0; i < BAND_STA_MAX_CLIENTS; i++) {
				if (band_sta_entries[i].last_pkt_time ==
				    ULONG_MAX) {
					bsi = &band_sta_entries[i];
					bst_num_cli++;
					break;
				}
			}
			if (bsi) {
				memcpy(bsi->mac, mac, zyUMAC_IEEE80211_ADDR_LEN);
				bsi->g_burst_start_time = 0;
				bsi->g_burst_count = 0;
				bsi->g_seqno = 0;
				bsi->a_burst_start_time = 0;
				bsi->a_burst_count = 0;
				bsi->a_seqno = 0;
				bsi->g_rssi = 0;
				bsi->a_rssi = 0;
				bsi->a_last_pkt_time = jiffies;
				bsi->g_last_pkt_time = 0;
				bsi->is_support_5G = 1;
				bsi->lb_reject_count = 0;
				bsi->reject_count = 0;
				bsi->band_steering_fail_count = 0;

				bsi->next = band_sta_hash[hash];
				band_sta_hash[hash] = bsi;
				new_entry = 1;
			}
		}
	}

	if (bsi) {
		/* if flag equa 1, this is 5G probe packet */
		if (flag == 0 || new_entry) {
			bsi->last_pkt_time = cur_sec;
		}

		if (rssi > DEFAULT_CHAN_REF_NOISE_FLOOR) {
			bsi->a_rssi = rssi;
			bsi->a_last_pkt_time = cur_sec;
		}
	}
	/* 2.4 add sta_mac ni is NULL, 5G add sta_mac ni isn't NULL */
	if (bsi && vap) {
		if (!bsi->is_support_5G)
			/* 2.4G auth first time add to table */
			bsi->is_support_5G = 1;
		new_entry = 1;
	}

	if (vap) {
		if ((bst_g_client_count == 0)
		    && (bst_a_client_count >=
			BAND_STA_BAND_PREFERENCE_RATIO)) {
			bst_allow_g_clients = 1;
		} else if ((bst_g_client_count)
			   && (BAND_STA_BAND_PREFERENCE_RATIO *
			       bst_g_client_count <
			       bst_a_client_count)) {
			bst_allow_g_clients = 1;
		} else {
			bst_allow_g_clients = 0;
		}

		if ((zyUMAC_vap_priv->app_config.band_steering_mode == 1)
		    && bst_allow_g_clients) {
			if (time_before_eq
			    (cur_sec,
			     bsi->last_pkt_time +
			     zyUMAC_vap_priv->app_config.
			     band_steering_time * HZ)) {
				if (bsi->reject_count <
				    zyUMAC_vap_priv->app_config.
				    band_steering_count) {
					bsi->reject_count++;
					drop = 1;
				}
			} else {
				bsi->reject_count = 1;
				drop = 1;
			}

			if (zyUMAC_vap_priv->app_config.
			    band_steering_debug) {
				zyUMAC_LOG(LOG_INFO,
					"bsi->reject_count:%d drop :%d",
					bsi->reject_count, drop);
			}
		}
	}
	/* sta is already success assocatied */
	if (drop) {
		zyUMAC_wal_wnode_t sta = NULL;

		sta = zyUMAC_wal_wnode_get(vap, mac);
		if (sta) {
			if (zyUMAC_wal_wnode_get_associd(sta) &&
			    IEEE80211_ADDR_EQ(
				zyUMAC_wal_wnode_get_bssid(sta),
				zyUMAC_wal_wdev_get_macaddr(vap))) {
				drop = 0;
			}
			zyUMAC_wal_wnode_put(sta);
		}
	}

done:
	BST_UNLOCK();

#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
	if (new_entry && vap) {
		sk_redis_client_nl_report(mac, zyUMAC_IEEE80211_ADDR_LEN,
					  BS_NETLINK_REPORT_ADD_5G_STA);
	}

	if (drop && mac) {
		sk_redis_client_nl_report(mac, zyUMAC_IEEE80211_ADDR_LEN,
					  BS_NETLINK_REPORT_DENY_STA);
	}
#endif	

	return drop;
}

void add_band_steering_sta(unsigned char *mac)
{
	bdst_band_sta_5_band_process(NULL, mac, 0, 0, 1);
}

void zyUMAC_band_steering_debug_dump(void)
{
	band_sta_info_t *bsi;
	int i;
	int bst_g_client_count, bst_a_client_count;

	zyUMAC_bdst_get_sta_count(&bst_g_client_count, &bst_a_client_count);

	zyUMAC_LOG(LOG_INFO, "\n"
		"     sta_mac\t"
		"     unlocal  is_5g  rj_cnt bdflcnt  lb_reject");

	BST_LOCK();
	for (i = 0; i < BAND_STA_MAX_CLIENTS; i++) {
		if (band_sta_entries[i].last_pkt_time != ULONG_MAX) {
			bsi = &band_sta_entries[i];
			zyUMAC_LOG(LOG_INFO, MACSTR "\t%d\t%d\t%d\t%d\t%d",
				MAC2STR(bsi->mac),
				bsi->unlocal, bsi->is_support_5G,
				bsi->reject_count,
				bsi->band_steering_fail_count,
				bsi->lb_reject_count);
		}
	}

	zyUMAC_LOG(LOG_INFO, "\n"
		"------------------\n"
		"Total clients: %d\n"
		"------------------\n",
		bst_num_cli);
	zyUMAC_LOG(LOG_INFO, "bst_g_client_count = %d\tbst_a_client_count=%d",
			bst_g_client_count, bst_a_client_count);

	BST_UNLOCK();
	return;
}

void band_steering_clear_hash(void)
{
	int i;

	for (i = 0; i < BAND_STA_HASH_SIZE; i++)
		band_sta_hash[i] = NULL;
	for (i = 0; i < BAND_STA_MAX_CLIENTS; i++) {
		band_sta_entries[i].last_pkt_time = ULONG_MAX;
		band_sta_entries[i].next = NULL;
		band_sta_entries[i].g_burst_start_time = 0;
		band_sta_entries[i].g_burst_count = 0;
		band_sta_entries[i].g_seqno = 0;
		band_sta_entries[i].a_burst_start_time = 0;
		band_sta_entries[i].a_burst_count = 0;
		band_sta_entries[i].a_seqno = 0;
		band_sta_entries[i].is_support_5G = 0;
		band_sta_entries[i].lb_reject_count = 0;
		band_sta_entries[i].reject_count = 0;
		band_sta_entries[i].band_steering_fail_count = 0;
	}
	return;
}

int lb_admission_handle(unsigned char *mac)
{
	band_sta_info_t *bsi = NULL;
	int exists = -1;

	if (mac == NULL) {
		return 0;
	}

	BST_LOCK();

	for (bsi = band_sta_hash[band_mac_hash(mac)]; bsi; bsi = bsi->next) {
		if (IEEE80211_ADDR_EQ(mac, bsi->mac)) {
			break;
		}
	}

	/* the first time add 2.4G sta mac to hash table */
	if (!bsi) {
		BST_UNLOCK();
		add_band_steering_sta(mac);

		BST_LOCK();
		for (bsi = band_sta_hash[band_mac_hash(mac)]; bsi;
		     bsi = bsi->next) {
			if (IEEE80211_ADDR_EQ(mac, bsi->mac)) {
				break;
			}
		}

		if (bsi) {
			bsi->is_support_5G = 0;
			bsi->reject_count = 1;
			bsi->last_pkt_time = jiffies;
		}
		BST_UNLOCK();

#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
		/* notice redis-client this sta is load_balance reject */
		sk_redis_client_nl_report(mac, zyUMAC_IEEE80211_ADDR_LEN,
					  BS_NETLINK_REPORT_DENY_STA);
#endif		
		return -1;
	}

	if (bsi) {
		/* bug26124 */
		if time_after_eq
			(jiffies,
			 bsi->last_pkt_time +
			 (ZY_BAND_STEERING_FAIL_COUNT_AGETIME) * HZ) {
			bsi->band_steering_fail_count = 0;
			}

		if (bsi->band_steering_fail_count >= 10) {
			BST_UNLOCK();
			return 0;
		}
		/* end bug26124 */
		if time_before_eq
			(jiffies, bsi->last_pkt_time + 10 * HZ) {
			bsi->reject_count++;
		} else {
			bsi->reject_count = 1;
		}

		if (bsi->reject_count > 3) {
			exists = 0;
			bsi->reject_count = 0;
		}

		bsi->last_pkt_time = jiffies;

		if (exists) {
#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
			sk_redis_client_nl_report(mac, zyUMAC_IEEE80211_ADDR_LEN,
						  BS_NETLINK_REPORT_DENY_STA);
#endif			
		}
		else {
#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
			sk_redis_client_nl_report(mac, zyUMAC_IEEE80211_ADDR_LEN,
						  BS_NETLINK_REPORT_FAIL_STA);
#endif
			bsi->band_steering_fail_count++;
		}		
		BST_UNLOCK();
		return exists;
	}

	BST_UNLOCK();
	return exists;
}

void ZY_handle_5G_query_result(unsigned char *mac, unsigned char is_5g)
{
	band_sta_info_t *bsi = NULL;

	BST_LOCK();

	for (bsi = band_sta_hash[band_mac_hash(mac)]; bsi; bsi = bsi->next) {
		if (IEEE80211_ADDR_EQ(mac, bsi->mac)) {
			break;
		}
	}

	if (bsi && is_5g) {
		bsi->is_support_5G = is_5g;
	}

	BST_UNLOCK();

	return;
}

void ZY_handle_deny_or_fail_sta(struct redis_message *message)
{
	band_sta_info_t *bsi = NULL;

	BST_LOCK();

	for (bsi = band_sta_hash[band_mac_hash(message->sta_mac)]; bsi;
	     bsi = bsi->next) {
		if (IEEE80211_ADDR_EQ(message->sta_mac, bsi->mac)) {
			break;
		}
	}

	if (bsi) {
		bsi->reject_count = message->reject_count;
		bsi->band_steering_fail_count =
		    message->band_steering_fail_count;
		bsi->lb_reject_count = message->lb_reject_count;
	}

	BST_UNLOCK();

	return;
}

void ZY_handle_query_deny_sta(struct redis_message *message)
{
	band_sta_info_t *bsi = NULL;

	BST_LOCK();

	for (bsi = band_sta_hash[band_mac_hash(message->sta_mac)]; bsi;
	     bsi = bsi->next) {
		if (IEEE80211_ADDR_EQ(message->sta_mac, bsi->mac)) {
			break;
		}
	}

	if (bsi) {
		bsi->reject_count = message->reject_count;
	}

	BST_UNLOCK();

	return;
}

int handle_redis_client_msg(unsigned char *buf, unsigned char msg_type,
			    int msg_len)
{
	struct redis_message *msg = NULL;

	if (msg_len >= sizeof(struct redis_message)) {
		msg = (struct redis_message *)(buf);
		switch (msg->mes_type) {
		case BS_NETLINK_REPORT_QUERY_5G_STA:
			if (msg_len >= sizeof(struct redis_message)) {
				ZY_handle_5G_query_result(msg->sta_mac,
							   msg->is_support_5G);
			}
			break;

		case BS_NETLINK_REPORT_DENY_STA:
		case BS_NETLINK_REPORT_FAIL_STA:
		case LB_NETLINK_REPORT_DENY_STA:
			if (msg_len >= sizeof(struct redis_message)) {
				ZY_handle_deny_or_fail_sta(msg);
			}
			break;

		case BS_NETLINK_REPORT_QUERY_DENY_STA:
			if (msg_len >= sizeof(struct redis_message)) {
				ZY_handle_query_deny_sta(msg);
			}
			break;

		default:
			zyUMAC_LOG(LOG_INFO, "unknown message type %d",
				msg->mes_type);
		}
	}

	return 0;
}

void zyUMAC_band_steering_handle_probe_req(zyUMAC_wal_wdev_t vap,
				unsigned char *mac, u_int16_t seqno, int rssi)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;

	if (vap == NULL)
		return;

	zyUMAC_vap_priv = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	if (zyUMAC_vap_priv == NULL)
		return;

	/* don't report local administrator mac address to redis server */
	if (mac && (mac[0] & IEEE80211_LOCAL_ADDRESS))
		return;

	if (zyUMAC_vap_priv->app_config.band_steering)
		bdst_band_sta_5_band_process(vap, mac, seqno, rssi, 1);

}

int zyUMAC_band_steering_handle_auth_req(zyUMAC_wal_wdev_t vap,
		unsigned char *mac, u_int16_t seqno, int rssi, int is2G)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	int drop = 0;

	if (vap == NULL)
		return drop;

	zyUMAC_vap_priv = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	if (zyUMAC_vap_priv == NULL)
		return drop;

	if (is2G) {
		if (zyUMAC_vap_priv->app_config.band_steering)
			drop = bdst_band_sta_24_band_process(vap, mac,
				seqno, rssi);
	} else if (zyUMAC_vap_priv->app_config.band_steering) {
		drop = bdst_band_sta_5_band_process(vap, mac,
			seqno, rssi, 0);
	}

	return drop;
}

void zyUMAC_bdst_init(void)
{
	band_sta_hash_init(1);
#ifdef ZY_REDIS_SRV_QUERY_SUPPORT	
	register_redis_client_msg(handle_redis_client_msg);
#endif	
}

void zyUMAC_bdst_exit(void)
{
#ifdef ZY_REDIS_SRV_QUERY_SUPPORT
	unregister_redis_client_msg();
#endif	
}
#else
void zyUMAC_bdst_init(void)
{
	/*dummy*/
}

void zyUMAC_bdst_exit(void)
{
	/*dummy*/
}
#endif /* ZY_BDST_SUPPORT */
