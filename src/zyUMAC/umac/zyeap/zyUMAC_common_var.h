#ifndef ZYXEL_UMAC_COMMON_VAR_HEADER_H
#define ZYXEL_UMAC_COMMON_VAR_HEADER_H

#include "zyUMAC_types.h"
#include "zyUMAC_umac.h"
#include "zyUMAC_log.h"

#define TIMEOUT_LOAD_BALANCE_REPORT_DEFAULT 30

static INLINE systick_t OS_GET_UPTIME(void)
{
	return jiffies - INITIAL_JIFFIES;
}

#define TABLE_SIZE_N(a)    (sizeof (a) / sizeof (a[0]))

#ifndef DEFAULT_CHAN_NOISE_FLOOR
#define DEFAULT_CHAN_NOISE_FLOOR     (-110)
#endif
#ifndef DEFAULT_CHAN_REF_NOISE_FLOOR
#define DEFAULT_CHAN_REF_NOISE_FLOOR (-96)
#endif
#define RSSI_THRESHOLD_UNSET        128

#define GET_INTERFACE_PRINT_NAME(a) \
    (IEEE80211_IS_CHAN_2GHZ(a) ? "interface0" : "interface1")

#endif
