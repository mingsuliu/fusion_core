/*
 * Zyxel Micro UMAC module (zyUMAC).
 */

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif

#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,38)
#ifndef AUTOCONF_INCLUDED
#include <linux/config.h>
#endif
#endif

#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/wireless.h>
#include <net/iw_handler.h>

#include "zyUMAC.h"
#include "zyUMAC_vap.h"
#include "zyUMAC_events.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_node_survey.h"

//current OL_PARAM is running to about 3XX,
//set zyUMAC priv param from 800, still need to check if reused.
#define zyUMAC_PHY_PARAM_START 800
#define zyUMAC_PHY_PARAM_SHIFT 0x1000

enum {
	zyUMAC_PHY_PARAM_SYNC_STA = zyUMAC_PHY_PARAM_START,
	zyUMAC_PHY_PARAM_ASSOC_CTL = zyUMAC_PHY_PARAM_START + 1,
	zyUMAC_PHY_PARAM_ASSOC_MIN_RSSI = zyUMAC_PHY_PARAM_START + 2,
	zyUMAC_PHY_PARAM_ASSOC_NORESP = zyUMAC_PHY_PARAM_START + 3,
	zyUMAC_PHY_PARAM_FRGBLACKTO = zyUMAC_PHY_PARAM_START + 4,
	zyUMAC_PHY_PARAM_GET_BLACKLIST = zyUMAC_PHY_PARAM_START + 5,
	zyUMAC_PHY_PARAM_THR_PKT_LEN = zyUMAC_PHY_PARAM_START + 6,
	zyUMAC_PHY_PARAM_PROBEREQ_RPT_LIM = zyUMAC_PHY_PARAM_START + 7,
	zyUMAC_PHY_PARAM_NODE_SURVEY = zyUMAC_PHY_PARAM_START + 8,
	zyUMAC_PHY_PARAM_MAX = zyUMAC_PHY_PARAM_START + 100,
};

static const struct iw_priv_args extra_iw_priv_args[] = {
#ifdef CONFIG_DP_SUPPORT
	{zyUMAC_PHY_PARAM_SYNC_STA | zyUMAC_PHY_PARAM_SHIFT,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "sync_sta"},
#endif
	{zyUMAC_PHY_PARAM_ASSOC_CTL | zyUMAC_PHY_PARAM_SHIFT,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "assoc_ctl"},
	{zyUMAC_PHY_PARAM_ASSOC_CTL | zyUMAC_PHY_PARAM_SHIFT,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "get_assoc_ctl"},
	{zyUMAC_PHY_PARAM_ASSOC_MIN_RSSI | zyUMAC_PHY_PARAM_SHIFT,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "assocminrssi"},
	{zyUMAC_PHY_PARAM_ASSOC_MIN_RSSI | zyUMAC_PHY_PARAM_SHIFT,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_assocminrssi"},
	{zyUMAC_PHY_PARAM_NODE_SURVEY | zyUMAC_PHY_PARAM_SHIFT,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "nodesurvey"},
	{zyUMAC_PHY_PARAM_NODE_SURVEY | zyUMAC_PHY_PARAM_SHIFT,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_nodesurvey"},
};

#ifdef CONFIG_DP_SUPPORT
static void sync_sta_cb(zyUMAC_wal_wnode_t ni, void *arg)
{
	zyUMAC_node_priv_t *zyUMAC_node_priv =
	    (zyUMAC_node_priv_t *) zyUMAC_wal_wnode_getdata(ni);
	zyUMAC_wal_ssid_t ssid;

	if (zyUMAC_node_priv == NULL) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_node_priv is null");
		return;
	}

	zyUMAC_wal_wdev_get_ssid(zyUMAC_wal_wnode_get_wdev(ni), &ssid);
	notify_dp_add_sta(zyUMAC_wal_wnode_get_macaddr(ni),
			  zyUMAC_wal_wnode_get_bssid(ni),
			  zyUMAC_wal_ssid_get_data(ssid),
			  zyUMAC_wal_ssid_get_len(ssid),
			  zyUMAC_node_priv->ns_stats.auth_uptime,
			  zyUMAC_wal_wnode_get_assocuptime(ni));
}
#endif

int zyUMAC_phy_getparam_SYNC_STA(zyUMAC_wal_wphy_t scn)
{
#ifdef CONFIG_DP_SUPPORT
	zyUMAC_wal_wphy_iterate_wnodes(scn, sync_sta_cb, NULL);
#endif

	return 0;
}

static int zyUMAC_ath_iw_setparam(struct net_device *dev,
				 struct iw_request_info *info,
				 void *w, char *extra, iw_handler orig_handler)
{
	zyUMAC_wal_wphy_t scn = zyUMAC_wal_netdev_to_wphy(dev);
	int *i = (int *)extra;
	int param = i[0];
	int value = i[1];
	zyUMAC_scn_priv_t *zyUMAC_scn_priv;

	if (!(param & zyUMAC_PHY_PARAM_SHIFT))
		goto done;

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) zyUMAC_wal_wphy_getdata(scn);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		goto done;
	}

	param -= zyUMAC_PHY_PARAM_SHIFT;
	switch (param) {
	case zyUMAC_PHY_PARAM_SYNC_STA:
		{
			return zyUMAC_phy_getparam_SYNC_STA(scn);
		}

	case zyUMAC_PHY_PARAM_ASSOC_CTL:
		{
			zyUMAC_scn_priv->ic_assoc_min_rssi_access_ctl = ! !value;
			return 0;
		}

	case zyUMAC_PHY_PARAM_ASSOC_MIN_RSSI:
		{
			zyUMAC_scn_priv->ic_assoc_min_rssi = (u_int16_t) value;
			return 0;
		}
	case zyUMAC_PHY_PARAM_NODE_SURVEY:
	{
		zyUMAC_scn_priv->node_survey_en = (u_int8_t)value;

		if (zyUMAC_scn_priv->node_survey_en)
			zyUMAC_node_survey_flush_scan_entry(scn);
		zyUMAC_wal_wphy_set_nodesurvey(scn);

		return 0;
	}
	default:
		goto done;
	}

done:
	return -1;
}

static int zyUMAC_ath_iw_getparam(struct net_device *dev,
				 struct iw_request_info *info,
				 void *w, char *extra, iw_handler orig_handler)
{
	zyUMAC_wal_wphy_t scn = zyUMAC_wal_netdev_to_wphy(dev);
	int param = *(int *)extra;
	int *val = (int *)extra;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv;

	if (!(param & zyUMAC_PHY_PARAM_SHIFT))
		goto done;

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) zyUMAC_wal_wphy_getdata(scn);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		goto done;
	}

	param -= zyUMAC_PHY_PARAM_SHIFT;
	switch (param) {
	case zyUMAC_PHY_PARAM_SYNC_STA:
		{
			return zyUMAC_phy_getparam_SYNC_STA(scn);
		}

	case zyUMAC_PHY_PARAM_ASSOC_CTL:
		{
			*(int *)val =
			    zyUMAC_scn_priv->ic_assoc_min_rssi_access_ctl;
			return 0;
		}

	case zyUMAC_PHY_PARAM_ASSOC_MIN_RSSI:
		{
			*(int *)val = zyUMAC_scn_priv->ic_assoc_min_rssi;
			return 0;
		}
	case zyUMAC_PHY_PARAM_NODE_SURVEY:
	{
		*(int *)val = zyUMAC_scn_priv->node_survey_en;
		if (zyUMAC_NODE_SURVEY_FILTER(SURVEY_FILTER_DUMP) &
		    zyUMAC_scn_priv->node_survey_en)
			zyUMAC_node_survey_dump_scan_entry(scn);
		return 0;
	}
	default:
		goto done;
	}

done:
	return -1;
}

static zyUMAC_wal_ioctl_hook_table _wphy_ioctl_hook_table = {
	extra_iw_priv_args,
	TABLE_SIZE_N(extra_iw_priv_args),
	(iw_handler_hook) zyUMAC_ath_iw_getparam,
	(iw_handler_hook) zyUMAC_ath_iw_setparam
};

/*
 * Replace all necessary ic function pointer here, call from __ol_ath_attach of qca_ol module
 */
int zyUMAC_phy_ioctl_attach(zyUMAC_wal_wphy_t wphy)
{
	return zyUMAC_wal_wphy_ioctl_attach(wphy, &_wphy_ioctl_hook_table);
}

void zyUMAC_phy_ioctl_detach(zyUMAC_wal_wphy_t wphy)
{
	zyUMAC_wal_wphy_ioctl_detach(wphy);
}
