#ifndef ZYXEL_UMAC_EVENTS_HEADER_H
#define ZYXEL_UMAC_EVENTS_HEADER_H

#include "zyUMAC_dp_events.h"

enum {
	ZY_NETLINK_WIFI_SUB_TYPE_MIN = 0,
	ZY_NETLINK_WIFI_ACCESS_FAIL,
	ZY_NETLINK_WIFI_TXPOWER_CHANGED,
	ZY_NETLINK_WIFI_CHANNEL_CHANGED,
	ZY_NETLINK_WIFI_AUTH_FAIL,
	ZY_NETLINK_WIFI_ASSOC_FAIL,
	ZY_NETLINK_WIFI_SUB_TYPE_MAX,
};

enum {
	ZY_NETLINK_TYPE_MIN = 0,
	ZY_NETLINK_TYPE_IF_UP,
	ZY_NETLINK_TYPE_IF_DOWN,
	ZY_NETLINK_TYPE_WIFI,
	ZY_NETLINK_TYPE_MAX,
};

enum {
	zyUMAC_EVENT_CLIENT_REGISTER = 1,
	zyUMAC_EVENT_CLIENT_DEREGISTER = 2,
	zyUMAC_EVENT_NODE_JOIN = 3,
	zyUMAC_EVENT_NODE_LEAVE = 4,
	zyUMAC_EVENT_NODE_RSSI_MONITOR = 5,
	zyUMAC_EVENT_NODE_CHLOAD = 6,
	zyUMAC_EVENT_NODE_NONERP_JOINED = 7,
	zyUMAC_EVENT_NODE_BG_JOINED = 8,
	zyUMAC_EVENT_NODE_COCHANNEL_AP_CNT = 9,
	zyUMAC_EVENT_CH_HOP_CHANNEL_CHANGE = 10,
};

typedef struct txpower_changed {
	char device[16];
	unsigned int pre_txpower;
	unsigned int cur_txpower;
	char cause[64];
} txpower_changed_t;

typedef struct access_fail {
	char ifname[16];	//athxx
	unsigned int threshold;
	unsigned int sta_total;
	char cause[64];
} access_fail_t;

typedef struct mlme_fail {
	char ifname[16];
	unsigned char mac[6];
	char cause[64];
} mlme_fail_t;

typedef struct txpower_changed_msg {
	unsigned short subtype;
	unsigned short len;
	txpower_changed_t data;
} txpower_changed_msg_t;

typedef struct access_fail_msg {
	unsigned short subtype;
	unsigned short len;
	access_fail_t data;
} access_fail_msg_t;

typedef struct mlme_fail_msg {
	unsigned short subtype;
	unsigned short len;
	mlme_fail_t data;
} mlme_fail_msg_t;

typedef struct zyUMAC_nl_event_dos {
	int frametype;
	u_int8_t stamac[IEEE80211_ADDR_LEN];
} zyUMAC_nl_event_dos_t;

typedef enum {
	ZY_NETLINK_EVENT_STA = 0,
	ZY_NETLINK_EVENT_VAP,
} ath_zystats_event_type;

#define MAX_ATH_EVENT_BUFF 3072

typedef struct _zyUMAC_zystats_netlink_event {
	u_int8_t type;
	u_int8_t radio;
	u_int8_t wlan;
	u_int8_t mac[MAC_ADDR_LEN];
	u_int8_t name[IFNAMSIZ];
	u_int8_t buff[MAX_ATH_EVENT_BUFF];
} zyUMAC_zystats_netlink_event;

struct ZY_zystats_netlink_update_node {
	struct net_device *athdev;
	u_int32_t vapid;
	u_int32_t radioid;
	u_int8_t stamac[6];
	u_int8_t webauth_result;
};

extern int sk_nl_event_report(void *msg, unsigned short len,
			      unsigned short group, unsigned short type);

extern void notify_mlme_fail(unsigned short type, char *ifname,
			     char *macaddr, char *reason);
extern void notify_access_fail(char *ifname,
			       unsigned int threshold, unsigned int sta_total,
			       char *reason);
#define notify_auth_fail(ifname, macaddr, reason)   \
    notify_mlme_fail(ZY_NETLINK_WIFI_AUTH_FAIL, ifname, macaddr, reason)
#define notify_assoc_fail(ifname, macaddr, reason)   \
    notify_mlme_fail(ZY_NETLINK_WIFI_ASSOC_FAIL, ifname, macaddr, reason)
extern void notify_txpower_changed(char *device,
				   unsigned int pre_txpower,
				   unsigned int cur_txpower, char *reason);
extern void notify_channel_changed(char *device, void *pre, void *cur,
				   void *range, char *reason);

extern int ath_sk_leave_netlink_bsend(zyUMAC_wal_wdev_t vap, u_int8_t type,
				      void *buff, int len,
				      char *ni_mac, int report);
extern int notify_dp_add_sta(u_int8_t * mac, u_int8_t * bssid, void *arg,
			     size_t len, unsigned long auth_rx_time,
			     unsigned long assoc_req_time);
extern int notify_dp_del_sta(u_int8_t * mac, u_int8_t * bssid, int report,
			     void *arg, size_t len);
extern int notify_dp_syn_sta(u_int8_t * mac, u_int8_t * bssid, u_int16_t aid,
			     u_int8_t subtype, unsigned long time);
extern int notify_dp_sta_authenticated(u_int8_t * mac, u_int8_t * bssid,
				       u_int8_t wpa, u_int8_t pairwise,
				       u_int8_t macauth, char *username);
extern int notify_dp_txpower_report(u_int8_t * type, u_int8_t radio_id,
				    u_int8_t txpower);
extern int notify_dp_channel_and_txpower_report(u_int8_t radio_id, char *info,
						unsigned long length);
extern int notify_dp_dos_report(u_int8_t * report, int size);
extern int notify_dp_neighbour_report(unsigned char *neighbour_info,
				      unsigned char *ifname,
				      unsigned short length);

#endif
