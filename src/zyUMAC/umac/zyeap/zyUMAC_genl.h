#ifndef __zyUMAC_GENL_H__
#define __zyUMAC_GENL_H__
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/genetlink.h>

#include "zyUMAC_nl80211.h"

int zyUMAC_genl_init(void);
void zyUMAC_genl_exit(void);
int zyUMAC_genl_event_report(void *msg, unsigned short len, unsigned short group,
			    unsigned short cmd, int attrtype);

#endif
