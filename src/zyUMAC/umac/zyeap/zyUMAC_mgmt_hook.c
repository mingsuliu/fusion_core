#include "zyUMAC.h"
#include "zyUMAC_band_steering.h"
#include "zyUMAC_mlme_hook.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_genl.h"
#include "zyUMAC_nl80211.h"
#include "zyUMAC_station.h"

/*
 * pre-filter before ieee80211_recv_mgmt()
 */
int zyUMAC_vap_input_mgmt_filter(zyUMAC_wal_wnode_t ni, wbuf_t wbuf,
				int subtype, zyUMAC_wal_rx_status_t rs)
{
	zyUMAC_wal_wdev_t vap = NULL;
	zyUMAC_ieee80211_header_t *wh;
	struct sk_buff *skb = (struct sk_buff *)wbuf;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_wal_wphy_t scn = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;

	if (!skb) {
		zyUMAC_LOG(LOG_ERR, "skb is null");
		goto err;
	}

	if (ni)
		vap = zyUMAC_wal_wnode_get_wdev(ni);
	else {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
		goto err;
	}

	if (vap)
		scn = zyUMAC_wal_wdev_get_wphy(vap);
	else {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		goto err;
	}

	if (scn)
		zyUMAC_scn_priv =
			(zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(scn);
	else {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		goto err;
	}

	wh = (zyUMAC_ieee80211_header_t *) skb->data;

	zyUMAC_dos_process(ni, wh, subtype, rs->rs_rssi);

	zyUMAC_vap_priv = (zyUMAC_vap_priv_t *) zyUMAC_wal_wdev_getdata(vap);
	if (!zyUMAC_vap_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_vap_priv is null");
		goto err;
	}

	if (subtype == IEEE80211_FC0_SUBTYPE_PROBE_REQ) {
#ifdef ZY_BDST_SUPPORT
		u_int8_t seqno = zyUMAC_wal_ieee80211_header_get_seq(wh);
		int chan = (int)zyUMAC_wal_wdev_get_chan(vap);
		/*TODO: use channel from rxs*/
		char *addr2 = zyUMAC_wal_ieee80211_header_get_addr2(wh);

		if (zyUMAC_vap_priv->app_config.band_steering) {
			if (!zyUMAC_IS_CHAN_2GHZ(chan)) {
				zyUMAC_band_steering_handle_probe_req(vap,
						addr2, seqno, rs->rs_rssi);
			}
		}
#endif

		if (zyUMAC_scn_priv) {
			zyUMAC_scn_priv->scn_radio_stats.rx_probreq++;
		}

	} else if (subtype == IEEE80211_FC0_SUBTYPE_AUTH) {
#ifdef ZY_BDST_SUPPORT
		u_int8_t seqno = zyUMAC_wal_ieee80211_header_get_seq(wh);
		int chan = (int)zyUMAC_wal_wdev_get_chan(vap);
		char *addr2 = zyUMAC_wal_ieee80211_header_get_addr2(wh);
		int is2G = 0;

		if (zyUMAC_IS_CHAN_2GHZ(chan))
			is2G = 1;

		if (zyUMAC_vap_priv->app_config.band_steering) {
			if (zyUMAC_band_steering_handle_auth_req(vap,
				addr2, seqno, rs->rs_rssi, is2G))
				return -EINVAL;
		}
#endif

		if (zyUMAC_scn_priv) {
			zyUMAC_scn_priv->scn_radio_stats.rx_auth++;
		}

	} else if (subtype == IEEE80211_FC0_SUBTYPE_REASSOC_REQ
		   || subtype == IEEE80211_FC0_SUBTYPE_ASSOC_REQ) {

		if (subtype == IEEE80211_FC0_SUBTYPE_REASSOC_REQ) {
			zyUMAC_vap_priv->is_stats.is_rx_reassoc++;
			zyUMAC_scn_priv->scn_radio_stats.rx_reassocreq++;
		} else {
			zyUMAC_vap_priv->is_stats.is_rx_assoc++;
			zyUMAC_scn_priv->scn_radio_stats.rx_assreq++;
		}

	} else if (subtype == IEEE80211_FC0_SUBTYPE_DISASSOC
		   || subtype == IEEE80211_FC0_SUBTYPE_DEAUTH) {
		if (zyUMAC_scn_priv) {
			zyUMAC_update_left_node(vap, ni);
			subtype == IEEE80211_FC0_SUBTYPE_DISASSOC ?
			    zyUMAC_scn_priv->scn_radio_stats.
			    rx_disassoc++ : zyUMAC_scn_priv->scn_radio_stats.
			    rx_deauth++;
		}
	}

	return 0;
err:
	return -1;
}

int zyUMAC_vap_output_mgmt_filter(zyUMAC_wal_wnode_t ni, wbuf_t wbuf, int subtype)
{
	zyUMAC_wal_wdev_t vap = NULL;

	if (wbuf == NULL)
		return -1;

	if (ni) {
		vap = zyUMAC_wal_wnode_get_wdev(ni);
	} else {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
		return -1;
	}	

	if (subtype == IEEE80211_FC0_SUBTYPE_DEAUTH ||
		subtype == IEEE80211_FC0_SUBTYPE_DISASSOC) {
		zyUMAC_update_left_node(vap, ni);
	}	
	
	return 0;
}
