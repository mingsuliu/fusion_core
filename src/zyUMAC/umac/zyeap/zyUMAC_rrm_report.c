#include <ieee80211_var.h>
#include <ieee80211_rrm_priv.h>
#if ATH_BAND_STEERING
#include <ieee80211_band_steering.h>
#endif
#include <ieee80211_api.h>
#include <osif_private.h>
#include "zyUMAC_common_var.h"
#include "zyUMAC_rrm_report.h"
#include "zyUMAC_wal_api.h"

#define DOT11K_BEACON_TABLE_REPORT  1

#ifdef ZY_DOT11K_SUPPORT
int zyUMAC_get_beacon_report(wlan_if_t vap, dot11k_msg_info * dot11k)
{
	struct ieee80211_beacon_report_table *btable = vap->rrm->beacon_table;

	if (btable == NULL) {
		return EOK;
	}
	RRM_BTABLE_LOCK(btable);
	{
		struct ieee80211_beacon_entry *current_beacon_entry;

		TAILQ_FOREACH(current_beacon_entry, &(btable->entry), blist) {
			struct ieee80211_beacon_report *report =
			    &current_beacon_entry->report;

			dot11k->dot11k_info[dot11k->count].rcpi = report->rcpi;
			dot11k->dot11k_info[dot11k->count].rsni = report->rsni;
			dot11k->dot11k_info[dot11k->count].channel =
			    report->ch_num;
			dot11k->dot11k_info[dot11k->count].reg_class =
			    report->reg_class;
			dot11k->dot11k_info[dot11k->count].phy_type =
			    report->frame_info & 0X7F;
			memcpy(dot11k->dot11k_info[dot11k->count].bss_mac,
			       report->bssid, 6);
			dot11k->count++;

			if (dot11k->count >= MAX_DOT11K_TABLE_SIZE) {
				break;
			}
		}
	}
	RRM_BTABLE_UNLOCK(btable);
	return EOK;
}

void zyUMAC_report_dot11k_beacon_table(wlan_if_t vap)
{
	char *buf = NULL;
	dot11k_msg_head *head = NULL;
	unsigned short msg_size = 0;
	osif_dev *osifp = NULL;
	struct net_device *dev = NULL;
	struct ieee80211_node *ni = NULL;
	dot11k_msg_info *dot11k = NULL;

	if (!vap) {
		goto fail;
	}
	ni = zyUMAC_wal_wnode_get(vap, vap->rrm->rrm_macaddr);

	dot11k =
	    (dot11k_msg_info *) OS_MALLOC(vap->iv_ic->ic_osdev,
					  sizeof(dot11k_msg_info), GFP_KERNEL);
	if (!dot11k) {
		goto fail;
	}

	dot11k->count = 0;
	zyUMAC_get_beacon_report(vap, dot11k);

	msg_size =
	    sizeof(dot11k_msg_head) + dot11k->count * sizeof(dot11k_element_t);
	buf = (char *)OS_MALLOC(vap->iv_ic->ic_osdev, msg_size, GFP_KERNEL);
	if (buf == NULL) {
		goto buf_fail;
	}

	head = (dot11k_msg_head *) buf;

	memcpy(head->vap_mac, vap->iv_myaddr, sizeof(head->vap_mac));
	memcpy(head->sta_mac, ni->ni_macaddr, sizeof(head->sta_mac));

	head->rrm_en_cap = head->extend_cap = 0;
	//head->rrm_en_cap |= ni->ni_bcn_passive ? 0x1 : 0x0;
	//head->rrm_en_cap |= ni->ni_bcn_active ? 0x2 : 0x0;
	//head->rrm_en_cap |= ni->ni_bcn_table ? 0x4 : 0x0;
	head->extend_cap |=
	    (ni->ext_caps.ni_ext_capabilities & 0x80000) ? 0x1 : 0x0;
	head->count = htons(dot11k->count);
	osifp = (osif_dev *) wlan_vap_get_registered_handle(vap);
	dev = osifp->netdev;
	strncpy(head->ifname, dev->name, sizeof(head->ifname));
	memcpy(buf + sizeof(dot11k_msg_head), dot11k->dot11k_info,
	       dot11k->count * sizeof(dot11k_element_t));
	//sk_dot11k_nl_event_report(buf, msg_size, DOT11K_BEACON_TABLE_REPORT);

	OS_FREE(buf);
buf_fail:
	OS_FREE(dot11k);
fail:
	if (ni)
		zyUMAC_wal_wnode_put(ni);
	return;
}

int zyUMAC_rrm_channel_update(wlan_if_t vap,
			     ieee80211_rrm_beaconreq_info_t * bcnrpt)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv =
	    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	v_wlan_dot11k_t *dot11k;
	int i;

	if (!zyUMAC_vap_priv || !bcnrpt) {
		return -1;
	}

	dot11k = &zyUMAC_vap_priv->dot11k;
	bcnrpt->num_chanrep = 1;

	if (IEEE80211_IS_CHAN_2GHZ(vap->iv_bsschan)) {
		if (dot11k->change_flag_g) {
			bcnrpt->apchanrep[0].numchans =
			    dot11k->num_channel_rep_g;
			for (i = 0; i < bcnrpt->apchanrep[0].numchans; i++) {
				bcnrpt->apchanrep[0].channum[i] =
				    dot11k->channel_rep_g[i];
			}

			dot11k->change_flag_g = 0;
		}
	} else {

		if (dot11k->change_flag_a) {
			bcnrpt->apchanrep[0].numchans =
			    dot11k->num_channel_rep_a;
			for (i = 0; i < bcnrpt->apchanrep[0].numchans; i++) {
				bcnrpt->apchanrep[0].channum[i] =
				    dot11k->channel_rep_a[i];
			}

			dot11k->change_flag_a = 0;
		}
	}
	return 0;
}

#endif
