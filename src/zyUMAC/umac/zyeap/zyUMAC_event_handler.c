/*
 * Zyxel Micro UMAC module (zyUMAC).
 */

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif

#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,38)
#ifndef AUTOCONF_INCLUDED
#include <linux/config.h>
#endif
#endif
#include <linux/module.h>
#include <linux/netdevice.h>

#include "zyUMAC.h"
#include "zyUMAC_vap.h"
#include "zyUMAC_stats.h"
#include "zyUMAC_genl.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_event.h"

static zyUMAC_wal_event_handler_t wal_event_handlers[zyUMAC_WAL_EVENT_MAX];

static int zyUMAC_wal_event_handler(struct notifier_block *self,
				   unsigned long event, void *param)
{
	zyUMAC_LOG(LOG_INFO, "%d", (int)event);
	if (wal_event_handlers[event] != NULL) {
		wal_event_handlers[event] (event, param);
	}

	return NOTIFY_OK;
}

static struct notifier_block zyUMAC_wal_event_nb = {
	.notifier_call = zyUMAC_wal_event_handler
};

int zyUMAC_umac_event_handler_attach(void)
{
	memset(wal_event_handlers, 0,
	       sizeof(zyUMAC_wal_event_handler_t) * zyUMAC_WAL_EVENT_MAX);

	wal_event_handlers[zyUMAC_WAL_EVENT_MLME_DEAUTH_COMPLETE] =
	    zyUMAC_mlme_deauth_complete_handler;
	wal_event_handlers[zyUMAC_WAL_EVENT_MLME_DISASSOC_COMPLETE] =
	    zyUMAC_mlme_disassoc_complete_handler;
	wal_event_handlers[zyUMAC_WAL_EVENT_MLME_ASSOC_INDICATION] =
	    zyUMAC_mlme_assoc_ind_handler;
	wal_event_handlers[zyUMAC_WAL_EVENT_MLME_AUTH_INDICATION] =
	    zyUMAC_auth_indication_handler;
	wal_event_handlers[zyUMAC_WAL_EVENT_MLME_DEAUTH_INDICATION] =
	    zyUMAC_deauth_indication_handler;
	wal_event_handlers[zyUMAC_WAL_EVENT_MLME_DISASSOC_INDICATION] =
	    zyUMAC_disassoc_indication_handler;

	zyUMAC_wal_event_register_listener(&zyUMAC_wal_event_nb);
	return 0;
}

EXPORT_SYMBOL(zyUMAC_umac_event_handler_attach);

void zyUMAC_umac_event_handler_detach(void)
{
	zyUMAC_wal_event_unregister_listener(&zyUMAC_wal_event_nb);
}

EXPORT_SYMBOL(zyUMAC_umac_event_handler_detach);
