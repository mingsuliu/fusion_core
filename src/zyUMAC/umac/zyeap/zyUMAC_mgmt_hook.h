#ifndef ZYXEL_UMAC_MGMT_HOOKER_HEADER_H
#define ZYXEL_UMAC_MGMT_HOOKER_HEADER_H

int zyUMAC_vap_input_mgmt_filter(zyUMAC_wal_wnode_t ni, wbuf_t wbuf,
				int subtype, zyUMAC_wal_rx_status_t rs);
int zyUMAC_vap_output_mgmt_filter(zyUMAC_wal_wnode_t ni, wbuf_t wbuf, int subtype);

#endif
