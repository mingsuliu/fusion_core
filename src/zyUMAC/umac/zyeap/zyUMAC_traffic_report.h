#ifndef __ZYXEL_UMAC_TRAFFIC_REPORT_HEAD__
#define __ZYXEL_UMAC_TRAFFIC_REPORT_HEAD__

void zyUMAC_loadbalance_attach(zyUMAC_wal_wdev_t vap);
void zyUMAC_loadbalance_detach(zyUMAC_wal_wdev_t vap);
void zyUMAC_loadbalance_report_handle(zyUMAC_wal_wdev_t vap);

#endif
