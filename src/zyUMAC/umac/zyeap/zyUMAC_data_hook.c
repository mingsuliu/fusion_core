#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_vap.h"
#include "zyUMAC_data_hook.h"
#include <linux/ip.h>
#include <linux/udp.h>

static const uint8_t magic_cookie[4] = { 99, 130, 83, 99 };    /* DHCP */

int zyUMAC_inspect_dhcp_packet(zyUMAC_wal_wdev_t wdev, struct sk_buff *skb)
{
	struct ether_header *eh;
	uint16_t ether_type;
	uint8_t *hdr_ptr;
	struct iphdr *iph;
	struct udphdr *udp_hdr;
	uint16_t udp_len;
	dhcp_packet_t *dhcp;
	int i = 0;
	char *option;
	int option_length;

	eh = (struct ether_header *)skb->data;
	ether_type = htons(eh->ether_type);
	hdr_ptr = skb->data;

	/* Handle the VLAN */
	if (ether_type == ETH_P_8021Q) {
		struct ethervlan_header *evh = (struct ethervlan_header *)eh;
		ether_type = htons(evh->ether_type);
		hdr_ptr += sizeof(struct ethervlan_header);
	} else {
		hdr_ptr += sizeof(struct ether_header);
	}

	if (ether_type == ETH_P_IP) {
		iph = (struct iphdr *)hdr_ptr;
		if (iph->protocol == IPPROTO_UDP) {
			udp_hdr = (struct udphdr *)((uint8_t *)(iph) + sizeof(struct iphdr));  
			if (htons(udp_hdr->dest) == DHCP_SERVER_PORT) { /* dhcp request */
				udp_len = udp_hdr->len;
				dhcp = (dhcp_packet_t *) ((uint8_t *)(udp_hdr) + sizeof(struct udphdr));
				option = (char *)dhcp->options;
				option_length = udp_len - sizeof(struct udphdr) - sizeof(dhcp_packet_t);

				/* Validate the option length */
				if (option_length < 0 || option_length > DHCP_OPTIONS_LENGTH)
					return -1;
				/* Validate the magic cookie */
				if(memcmp(dhcp->magic_cookie, magic_cookie, 4))
					return -1;

				while (i < option_length) {
					uint8_t type = option[i];
					uint8_t length = option[i+1];

					/* Get ACK Message Type */
					if (type == DHCP_OPTION_MESSAGE_TYPE &&
						length == 1 && option[i+2] == DHCPREQUEST) {
						/*TODO: Albert need to deliver event here */
						printk("DHCP request mac(%pM) skb data len(%d)\n", eh->ether_shost, skb->len);
						break;
					}
					i+=(2+length);
				}
			}	
		}
	}
	return 0;
}

int zyUMAC_WAL_wdev_hook_rx_data_inspec(zyUMAC_wal_wdev_t wdev,
				   struct sk_buff *skb)
{
	return zyUMAC_inspect_dhcp_packet(wdev, skb);
}
