#ifndef ZYXEL_UMAC_MLME_HOOKER_HEADER_H
#define ZYXEL_UMAC_MLME_HOOKER_HEADER_H

#include "zyUMAC.h"
#include "zyUMAC_common_var.h"

void send_sta_leave_args(zyUMAC_wal_wdev_t vap, u_int8_t * mac_addr,
			 int32_t reason, u_int16_t sender, u_int16_t frame_type,
			 u_int8_t is_passive);
int zyUMAC_change_chan_power_limit(wlan_if_t vap, wlan_chan_t chan);

#endif
