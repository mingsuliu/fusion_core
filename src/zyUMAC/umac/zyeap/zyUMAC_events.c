#include <linux/string.h>
#include <linux/wireless.h>
#include <linux/uaccess.h>
#include <linux/notifier.h>
#include "zyUMAC.h"
#include "zyUMAC_events.h"
#include "zyUMAC_genl.h"
#include "zyUMAC_wal_api.h"

extern int dp_notifier_call_chain(unsigned long val, void *v);

#ifdef ZY_FM_SUPPORT
void notify_mlme_fail(unsigned short type, char *ifname,
		      char *macaddr, char *reason)
{
	mlme_fail_msg_t msg = { 0 };

	if (!ifname || !macaddr || !reason)
		return;

	msg.subtype = type;
	msg.len = sizeof(msg.data);
	strncpy(msg.data.ifname, ifname, sizeof(msg.data.ifname));
	msg.data.ifname[sizeof(msg.data.ifname) - 1] = '\0';
	memcpy(msg.data.mac, macaddr, 6);
	strncpy(msg.data.cause, reason, sizeof(msg.data.cause));
	msg.data.cause[sizeof(msg.data.cause) - 1] = '\0';

	zyUMAC_genl_event_report(&msg, sizeof(msg), zyUMAC_MCGRP_MLME,
				zyUMAC_E_MLME_FAIL, zyUMAC_A_MLME_FAIL);
}

void notify_access_fail(char *ifname,
			unsigned int threshold, unsigned int sta_total,
			char *reason)
{
	access_fail_msg_t msg = { 0 };

	if (!ifname || !reason)
		return;

	msg.subtype = ZY_NETLINK_WIFI_ACCESS_FAIL;
	msg.len = sizeof(msg.data);
	strncpy(msg.data.ifname, ifname, sizeof(msg.data.ifname));
	msg.data.ifname[sizeof(msg.data.ifname) - 1] = '\0';
	msg.data.threshold = threshold;
	msg.data.sta_total = sta_total;
	strncpy(msg.data.cause, reason, sizeof(msg.data.cause));
	msg.data.cause[sizeof(msg.data.cause) - 1] = '\0';

	zyUMAC_genl_event_report(&msg, sizeof(msg), zyUMAC_MCGRP_MLME,
				zyUMAC_E_ACCESS_FAIL, zyUMAC_A_ACCESS_FAIL);
}

void notify_txpower_changed(char *device,
			    unsigned int pre_txpower, unsigned int cur_txpower,
			    char *reason)
{
	txpower_changed_msg_t msg = { 0 };

	if (!device || !reason)
		return;

	msg.subtype = ZY_NETLINK_WIFI_TXPOWER_CHANGED;
	msg.len = sizeof(msg.data);
	strncpy(msg.data.device, device, sizeof(msg.data.device));
	msg.data.device[sizeof(msg.data.device) - 1] = '\0';
	msg.data.pre_txpower = pre_txpower;
	msg.data.cur_txpower = cur_txpower;
	strncpy(msg.data.cause, reason, sizeof(msg.data.cause));
	msg.data.cause[sizeof(msg.data.cause) - 1] = '\0';

	zyUMAC_genl_event_report(&msg, sizeof(msg), zyUMAC_MCGRP_MLME,
				zyUMAC_E_TXPOWER_CHANGED,
				zyUMAC_A_TXPOWER_CHANGED);
}

void notify_channel_changed(char *device, void *pre,
			    void *cur, void *range, char *reason)
{
	channel_changed_msg_t msg = { 0 };

	if (!device || !reason || !pre || !cur || !range)
		return;

	msg.subtype = ZY_NETLINK_WIFI_CHANNEL_CHANGED;
	msg.len = sizeof(msg.data);
	strncpy(msg.data.device, device, sizeof(msg.data.device));
	msg.data.device[sizeof(msg.data.device) - 1] = '\0';
	memcpy(&msg.data.pre, pre, sizeof(struct iw_freq));
	memcpy(&msg.data.cur, cur, sizeof(struct iw_freq));
	memcpy(&msg.data.range, range, sizeof(struct iw_range));
	strncpy(msg.data.cause, reason, sizeof(msg.data.cause));
	msg.data.cause[sizeof(msg.data.cause) - 1] = '\0';

	zyUMAC_genl_event_report(&msg, sizeof(msg),
				zyUMAC_MCGRP_MLME, zyUMAC_E_CHANNEL_CHANGED,
				zyUMAC_A_CHANNEL_CHANGED);
}
#endif

#define ZY_STATS_VAP_DELETE_EVENT  110

int ath_sk_leave_netlink_bsend(zyUMAC_wal_wdev_t vap, u_int8_t type,
			       void *buff, int len,
			       char *ni_mac, int report)
{
	int unit = -1, ret;
	char ifname[IFNAMSIZ]; /*dummy*/
	struct net_device *netdev;
	zyUMAC_zystats_netlink_event *event = NULL;
	zyUMAC_wal_wphy_t wphy;
	int maxvaps;

	if (!buff || !vap)
		return -1;

	netdev = zyUMAC_wal_wdev_to_netdev(vap);
	if (!netdev)
		return -1;

	wphy = zyUMAC_wal_wdev_get_wphy(vap);
	if (!wphy)
		return -1;

	event = (zyUMAC_zystats_netlink_event *)
		kmalloc(sizeof(zyUMAC_zystats_netlink_event), GFP_KERNEL);
	if (!event)
		return -1;

	event->radio = zyUMAC_wal_wphy_get_id(wphy);
	event->type = type;
	/* TODO: record in wdev */
	ret = sscanf(netdev->name, "%16[a-zA-Z]%d", ifname, &unit);
	if (ret > 0)
		unit = -1;

	maxvaps = zyUMAC_wal_wphy_get_wdev_limit(wphy);
	if (unit >= 0 && unit < maxvaps)
		event->wlan = unit % maxvaps;
	else
		event->wlan = -1;
	memcpy(event->mac, zyUMAC_wal_wdev_get_macaddr(vap),
		IEEE80211_ADDR_LEN);
	memset(event->name, 0, IFNAMSIZ);
	strncpy(event->name, netdev->name, IFNAMSIZ);

	memset(event->buff, 0, MAX_ATH_EVENT_BUFF);
	memcpy(event->buff, buff, len);

	if (type == ZY_NETLINK_EVENT_VAP) {
		zyUMAC_genl_event_report((void *)event,
					sizeof(zyUMAC_zystats_netlink_event),
					zyUMAC_MCGRP_STA_INFO,
					zyUMAC_E_STATS_VAP_DELETE,
					zyUMAC_A_STATS_VAP_DELETE);
	} else {
#ifdef CONFIG_DP_SUPPORT
		notify_dp_del_sta(ni_mac, zyUMAC_wal_wdev_get_macaddr(vap),
				  report,
				  (void *)event,
				  sizeof(zyUMAC_zystats_netlink_event));
#endif
	}
	kfree(event);

	return 0;
}

#ifdef CONFIG_DP_SUPPORT
int notify_dp_add_sta(u_int8_t * mac, u_int8_t * bssid, void *arg, size_t len,
		      unsigned long auth_rx_time, unsigned long assoc_req_time)
{
	int rtn = -1;
	zyUMAC_dp_add_sta_t zyUMAC_dp_add_sta =
	    { mac, bssid, arg, len, auth_rx_time, assoc_req_time };

	zyUMAC_LOG(LOG_INFO, "+");
	rtn =
	    dp_notifier_call_chain(zyUMAC_DP_EVENT_ADD_STA,
				   (void *)&zyUMAC_dp_add_sta);
	return rtn;
}

int notify_dp_del_sta(u_int8_t * mac, u_int8_t * bssid, int report, void *arg,
		      size_t len)
{
	int rtn = -1;
	zyUMAC_dp_del_sta_t zyUMAC_dp_del_sta = { mac, bssid, report, arg, len };

	zyUMAC_LOG(LOG_INFO, "+");
	rtn =
	    dp_notifier_call_chain(zyUMAC_DP_EVENT_DEL_STA,
				   (void *)&zyUMAC_dp_del_sta);
	return rtn;
}

int notify_dp_syn_sta(u_int8_t * mac, u_int8_t * bssid, u_int16_t aid,
		      u_int8_t subtype, unsigned long time)
{
	int rtn = -1;
	zyUMAC_dp_sync_sta_t zyUMAC_dp_sync_sta =
	    { mac, bssid, aid, subtype, time };

	zyUMAC_LOG(LOG_INFO, "+");
	rtn =
	    dp_notifier_call_chain(zyUMAC_DP_EVENT_SYNC_STA,
				   (void *)&zyUMAC_dp_sync_sta);
	return rtn;
}

int notify_dp_sta_authenticated(u_int8_t * mac, u_int8_t * bssid, u_int8_t wpa,
				u_int8_t pairwise, u_int8_t macauth,
				char *username)
{
	int rtn = -1;
	zyUMAC_dp_sta_authenticated_t zyUMAC_dp_sta_authenticated =
	    { mac, bssid, wpa, pairwise, macauth, username };

	zyUMAC_LOG(LOG_INFO, "+");
	rtn =
	    dp_notifier_call_chain(zyUMAC_DP_EVENT_STA_AUTHED,
				   (void *)&zyUMAC_dp_sta_authenticated);
	return rtn;
}

int notify_dp_txpower_report(u_int8_t * type, u_int8_t radio_id,
			     u_int8_t txpower)
{
	int rtn = -1;
	return rtn;
}

int notify_dp_channel_and_txpower_report(u_int8_t radio_id, char *info,
					 unsigned long length)
{
	int rtn = -1;
	return rtn;
}

int notify_dp_dos_report(u_int8_t * report, int size)
{
	int rtn = -1;
	return rtn;
}

int notify_dp_neighbour_report(unsigned char *neighbour_info,
			       unsigned char *ifname, unsigned short length)
{
	int rtn = -1;
	return rtn;
}

#endif //END CONFIG_DP_SUPPORT
