#include "zyUMAC_wal_api.h"
#include "zyUMAC_common_var.h"
#include "zyUMAC_node_survey.h"

static zyUMAC_STATUS
zyUMAC_node_survey_add_scan_node(zyUMAC_survey_db_t *scan_db,
				zyUMAC_survey_scan_node_t *scan_node)
{
	uint8_t hash_idx;

	hash_idx = NODE_SURVEY_GET_HASH(scan_node->entry->bssid);

	if (zyUMAC_list_size(&scan_db->hash_tbl[hash_idx]) >=
	    zyUMAC_list_max_size(&scan_db->hash_tbl[hash_idx]))
		return zyUMAC_STATUS_E_RESOURCES;

	spin_lock_bh(&scan_db->spinlock);
	scan_node->dummy = 0x42562072;

	zyUMAC_list_insert_back(&scan_db->hash_tbl[hash_idx],
			       &scan_node->node);

	scan_db->num_entries++;
	spin_unlock_bh(&scan_db->spinlock);

	return zyUMAC_STATUS_SUCCESS;
}

static zyUMAC_STATUS
zyUMAC_node_survey_del_scan_node(zyUMAC_survey_db_t *scan_db,
				zyUMAC_survey_scan_node_t *scan_node)
{
	zyUMAC_STATUS status = zyUMAC_STATUS_SUCCESS;
	uint8_t hash_idx;

	if (!scan_node || !scan_db) {
		zyUMAC_LOG(LOG_ERR, "parameter is null");
		return zyUMAC_STATUS_E_INVAL;
	}

	if (scan_node->dummy != 0x42562072)
		return zyUMAC_STATUS_SUCCESS;

	if (scan_node->entry) {
		hash_idx = NODE_SURVEY_GET_HASH(scan_node->entry->bssid);
	} else {
		zyUMAC_LOG(LOG_ERR, "scan entry is null");
		return zyUMAC_STATUS_E_FAILURE;
	}
	spin_lock_bh(&scan_db->spinlock);
	status = zyUMAC_list_remove_node(&scan_db->hash_tbl[hash_idx],
					&scan_node->node);
	if (status == zyUMAC_STATUS_SUCCESS) {
		kfree(scan_node->entry);
		kfree(scan_node);
	}

	scan_db->num_entries--;
	spin_unlock_bh(&scan_db->spinlock);

	return status;
}

static bool
zyUMAC_node_survey_entry_is_match(zyUMAC_survey_scan_entry_t *entry_1,
				 zyUMAC_survey_scan_entry_t *entry_2)
{
	if (!entry_1 || !entry_2)
		return false;

	if (memcmp(entry_1->bssid,
		   entry_2->bssid,
		   zyUMAC_IEEE80211_ADDR_LEN) == 0)
		return true;
	else
		return false;
}

static bool
zyUMAC_node_survey_find_dup_node(zyUMAC_survey_db_t *scan_db,
				zyUMAC_survey_scan_entry_t *scan_entry,
				zyUMAC_survey_scan_node_t **dup_scan_node)
{
	zyUMAC_list_node_t *cur_node = NULL;
	zyUMAC_survey_scan_node_t *cur_scan_node = NULL;
	uint8_t hash_idx;

	if (!scan_entry) {
		zyUMAC_LOG(LOG_ERR, "scan_entry is null");
		return zyUMAC_STATUS_E_INVAL;
	}

	hash_idx = NODE_SURVEY_GET_HASH(scan_entry->bssid);

	spin_lock_bh(&scan_db->spinlock);
	zyUMAC_list_for_each(cur_node, scan_db->hash_tbl[hash_idx]) {
		cur_scan_node = zyUMAC_node_survey_list_entry(cur_node);
		if (cur_scan_node &&
		    zyUMAC_node_survey_entry_is_match(scan_entry,
						     cur_scan_node->entry)) {
			*dup_scan_node = cur_scan_node;
			spin_unlock_bh(&scan_db->spinlock);
			return true;
		}
	}
	spin_unlock_bh(&scan_db->spinlock);

	return false;
}

zyUMAC_STATUS
zyUMAC_node_survey_add_scan_entry(zyUMAC_wal_wphy_t wphy,
				 zyUMAC_survey_scan_entry_t *scan_entry)
{
	zyUMAC_STATUS status = zyUMAC_STATUS_E_INVAL;
	zyUMAC_survey_db_t *scan_db = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_survey_scan_node_t *scan_node = NULL;
	zyUMAC_survey_scan_node_t *dup_node = NULL;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		goto done;
	}

	if (!scan_entry) {
		zyUMAC_LOG(LOG_ERR, "scan_entry is null");
		goto done;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(wphy);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		goto done;
	}

	scan_db = &zyUMAC_scn_priv->note_survey_db;

	if (scan_db->num_entries >= NODE_SURVEY_MAX_SCAN_SIZE) {
		status = zyUMAC_STATUS_E_RESOURCES;
		zyUMAC_LOG(LOG_ERR, "scan_db is full");
		goto done;
	}

	scan_node = kmalloc(sizeof(zyUMAC_survey_scan_node_t), GFP_KERNEL);
	if (!scan_node) {
		status = zyUMAC_STATUS_E_NOMEM;
		zyUMAC_LOG(LOG_ERR, "can't alloc scan_node");
		goto done;
	}

	zyUMAC_node_survey_find_dup_node(scan_db, scan_entry, &dup_node);
	scan_node->entry = scan_entry;

	/*consider:
	 * 1. append new node and remove duplicated one
	 * 2. do not append existed one
	 */
	if (dup_node)
		zyUMAC_node_survey_del_scan_node(scan_db, dup_node);

	status = zyUMAC_node_survey_add_scan_node(scan_db, scan_node);

	if (status != zyUMAC_STATUS_SUCCESS) {
		kfree(scan_node->entry);
		kfree(scan_node);
	}

done:
	return status;
}

void zyUMAC_node_survey_dump_scan_entry(zyUMAC_wal_wphy_t wphy)
{
	zyUMAC_survey_db_t *scan_db = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	int i, count;
	zyUMAC_list_node_t *cur_node = NULL;
	zyUMAC_survey_scan_node_t *cur_scan_node = NULL;
	zyUMAC_survey_scan_entry_t *entry = NULL;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		goto done;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(wphy);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		goto done;
	}

	scan_db = &zyUMAC_scn_priv->note_survey_db;
	zyUMAC_LOG(LOG_INFO, "Num of entries: %d\n", scan_db->num_entries);

	for (i = 0 ; i < NODE_SURVEY_HASH_SIZE; i++) {
		count = zyUMAC_list_size(&scan_db->hash_tbl[i]);
		if (!count)
			continue;

		zyUMAC_LOG(LOG_INFO, "\nHash[%d] count:%d", i, count);
		spin_lock_bh(&scan_db->spinlock);
		zyUMAC_list_for_each(cur_node, scan_db->hash_tbl[i]) {
			cur_scan_node = zyUMAC_node_survey_list_entry(cur_node);
			if (cur_scan_node && cur_scan_node->entry) {
				entry = cur_scan_node->entry;
				if (!entry)
					continue;
				zyUMAC_LOG(LOG_INFO,
					  MACSTR, MAC2STR(entry->bssid));
				zyUMAC_LOG(LOG_INFO,
					  "%s", entry->type ? "STA" : "AP");
				zyUMAC_LOG(LOG_INFO,
					  "%d",  entry->rssi);
			}
		}
		spin_unlock_bh(&scan_db->spinlock);
	}
done:
	return;
}

void zyUMAC_node_survey_flush_scan_entry(zyUMAC_wal_wphy_t wphy)
{
	zyUMAC_survey_db_t *scan_db = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	int i, count;
	zyUMAC_list_node_t *cur_node = NULL;
	zyUMAC_survey_scan_node_t *cur_scan_node = NULL;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(wphy);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		return;
	}

	scan_db = &zyUMAC_scn_priv->note_survey_db;

	for (i = 0 ; i < NODE_SURVEY_HASH_SIZE; i++) {
		count = zyUMAC_list_size(&scan_db->hash_tbl[i]);
		if (!count)
			continue;

		spin_lock_bh(&scan_db->spinlock);
		zyUMAC_list_for_each(cur_node, scan_db->hash_tbl[i]) {
			cur_scan_node = zyUMAC_node_survey_list_entry(cur_node);

			kfree(cur_scan_node->entry);
			kfree(cur_scan_node);

			scan_db->num_entries--;
		}
		zyUMAC_list_create(&scan_db->hash_tbl[i],
			NODE_SURVEY_MAX_SCAN_SIZE / NODE_SURVEY_HASH_SIZE);
		spin_unlock_bh(&scan_db->spinlock);
	}

}

void
zyUMAC_node_survey_init(zyUMAC_wal_wphy_t wphy)
{
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_survey_db_t *scan_db = NULL;
	int i;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(wphy);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		return;
	}

	scan_db = &zyUMAC_scn_priv->note_survey_db;

	scan_db->num_entries = 0;
	spin_lock_init(&scan_db->spinlock);

	for (i = 0; i < NODE_SURVEY_HASH_SIZE; i++)
		zyUMAC_list_create(&scan_db->hash_tbl[i],
			NODE_SURVEY_MAX_SCAN_SIZE / NODE_SURVEY_HASH_SIZE);
}

void
zyUMAC_node_survey_deinit(zyUMAC_wal_wphy_t wphy)
{
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_survey_db_t *scan_db = NULL;
	int i;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *)zyUMAC_wal_wphy_getdata(wphy);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		return;
	}

	scan_db = &zyUMAC_scn_priv->note_survey_db;

	zyUMAC_node_survey_flush_scan_entry(wphy);

	for (i = 0; i < NODE_SURVEY_HASH_SIZE; i++)
		zyUMAC_list_deprecate_check(&scan_db->hash_tbl[i]);
}
