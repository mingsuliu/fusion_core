#ifndef ZYXEL_UMAC_STATION_HEADER_H
#define ZYXEL_UMAC_STATION_HEADER_H

int zyUMAC_find_left_node(zyUMAC_wal_wdev_t wdev, const u_int8_t *mac);
void zyUMAC_update_left_node(zyUMAC_wal_wdev_t wdev,
			    zyUMAC_wal_wnode_t ni);
int zyUWAL_wdev_hook_get_left_station_stats(zyUMAC_wal_wdev_t wdev,
					   zyUMAC_wal_station_txrx_stats_t *stats, const u_int8_t *mac);

#endif
