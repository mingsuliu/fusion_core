/*
 * Zyxel  Micro UMAC module (zyUMAC).
 */

#include <linux/netdevice.h>
#include <linux/wireless.h>
#include <net/iw_handler.h>

#include "zyUMAC_wal_api.h"
#include "zyUMAC.h"
#include "zyUMAC_mlme_hook.h"
#include "zyUMAC_events.h"
#ifdef ZY_DOT11K_SUPPORT
#include "zyUMAC_rrm_report.h"
#endif
#ifdef ZY_BDST_SUPPORT
#include "zyUMAC_band_steering.h"
#endif

#include "zyUMAC_ieee80211.h"

// @TODO: needs to defined as a WAL independent solution
#define zyUMAC_IEEE80211_PARAM_MACCMD  17
#define zyUMAC_IEEE80211_ACL_FLAG_ACL_LIST_1    1 << 1

#ifdef ZY_DOT11K_SUPPORT
static iw_handler orig_ieee80211_dbgreq = NULL;
#endif
static iw_handler orig_ieee80211_siwfreq = NULL;
static iw_handler orig_ieee80211_giwfreq = NULL;
static iw_handler orig_ieee80211_siwmode = NULL;
static iw_handler orig_ieee80211_siwtxpow = NULL;
static iw_handler orig_ieee80211_giwrange = NULL;

//current IEEE80211_PARAM is running to about 6XX,
//set zyUMAC priv param from 1000, still need to check if reused.
#define ZY_IEEE80211_PARAM_START 1000	//add by luodahong

#define IW_PRIV_TYPE_ACKICK \
    IW_PRIV_TYPE_BYTE | sizeof(struct kick_sta_info)

enum {
	IEEE80211_ZY_BAND_STEERING_ENABLE = ZY_IEEE80211_PARAM_START,
	IEEE80211_ZY_BAND_STEERING_MODE = ZY_IEEE80211_PARAM_START + 1,
	IEEE80211_ZY_BAND_STEERING_TIME = ZY_IEEE80211_PARAM_START + 2,
	IEEE80211_ZY_BAND_STEERING_COUNT = ZY_IEEE80211_PARAM_START + 3,
	IEEE80211_ZY_BAND_STEERING_DEBUG = ZY_IEEE80211_PARAM_START + 4,
	IEEE80211_ZY_BAND_STEERING_HASH = ZY_IEEE80211_PARAM_START + 5,
	IEEE80211_ZY_BAND_STEERING_SHOW = ZY_IEEE80211_PARAM_START + 6,

	IEEE80211_ZY_SPECIAL_SSID_DISABLE = ZY_IEEE80211_PARAM_START + 7,
	IEEE80211_ZY_CAPWAP_TUNNEL = ZY_IEEE80211_PARAM_START + 8,
	IEEE80211_ZY_LOCAL_DHCP_ENABLE = ZY_IEEE80211_PARAM_START + 9,
	IEEE80211_ZY_LOCAL_ASSOC_ENABLE = ZY_IEEE80211_PARAM_START + 10,
	IEEE80211_ZY_WEB_AUTH_ENABLE = ZY_IEEE80211_PARAM_START + 11,
	IEEE80211_ZY_VLAN = ZY_IEEE80211_PARAM_START + 12,
	IEEE80211_ZY_UPDATE_VAP = ZY_IEEE80211_PARAM_START + 13,

	IEEE80211_ZY_LC_RSSI_THRESHOLD = ZY_IEEE80211_PARAM_START + 14,
	IEEE80211_ZY_LC_DEBUG = ZY_IEEE80211_PARAM_START + 15,
	IEEE80211_ZY_LC_NO_PROMISC = ZY_IEEE80211_PARAM_START + 16,

	IEEE80211_ZY_11K_CH_A_LIST_FLASH = ZY_IEEE80211_PARAM_START + 17,
	IEEE80211_ZY_11K_CH_A_LIST = ZY_IEEE80211_PARAM_START + 18,
	IEEE80211_ZY_11K_CH_G_LIST_FLASH = ZY_IEEE80211_PARAM_START + 19,
	IEEE80211_ZY_11K_CH_G_LIST = ZY_IEEE80211_PARAM_START + 20,
	IEEE80211_ZY_11K_ENABLE = ZY_IEEE80211_PARAM_START + 21,
	IEEE80211_ZY_11K_BASE_SNR = ZY_IEEE80211_PARAM_START + 22,
	IEEE80211_ZY_11V_TRANS_ENABLE = ZY_IEEE80211_PARAM_START + 23,
	IEEE80211_ZY_11R_ROAM_ASSIST = ZY_IEEE80211_PARAM_START + 24,
	IEEE80211_ZY_11k_RPT_INTERVAL = ZY_IEEE80211_PARAM_START + 25,

	IEEE80211_ZY_LC_ENABLE = ZY_IEEE80211_PARAM_START + 27,
	IEEE80211_ZY_AC_KICK_MAC = ZY_IEEE80211_PARAM_START + 28,

//this is new SBB requirement
	IEEE80211_PARAM_SBB_PROBEREQ_LIM = ZY_IEEE80211_PARAM_START + 29,
	IEEE80211_PARAM_SBB_ASSOCREQ_LIM = ZY_IEEE80211_PARAM_START + 30,
	IEEE80211_PARAM_SBB_AUTH_LIM = ZY_IEEE80211_PARAM_START + 31,
	IEEE80211_PARAM_SBB_AUTH_IGNORE = ZY_IEEE80211_PARAM_START + 32,
	IEEE80211_PARAM_SBB_RSSI_KO = ZY_IEEE80211_PARAM_START + 33,
	IEEE80211_PARAM_SBB_RATE_KO = ZY_IEEE80211_PARAM_START + 34,
	IEEE80211_PARAM_SBB_RATE_KO_CNT = ZY_IEEE80211_PARAM_START + 35,
	IEEE80211_PARAM_SBB_ANYPROBEREQ_FILTER = ZY_IEEE80211_PARAM_START + 36,
	IEEE80211_PARAM_SBB_FRGKICKOUT_EN = ZY_IEEE80211_PARAM_START + 37,
	IEEE80211_PARAM_SBB_TXW52POWER = ZY_IEEE80211_PARAM_START + 38,
};

static const struct iw_priv_args extra_vap_iw_priv_args[] = {
#ifdef ZY_BDST_SUPPORT
	{IEEE80211_ZY_BAND_STEERING_ENABLE,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "bdst_enable"},
	{IEEE80211_ZY_BAND_STEERING_ENABLE,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_bdst_enable"},
	{IEEE80211_ZY_BAND_STEERING_MODE,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "bdst_mode"},
	{IEEE80211_ZY_BAND_STEERING_MODE,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_bdst_mode"},
	{IEEE80211_ZY_BAND_STEERING_TIME,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "bdst_time"},
	{IEEE80211_ZY_BAND_STEERING_TIME,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_bdst_time"},
	{IEEE80211_ZY_BAND_STEERING_COUNT,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "bdst_count"},
	{IEEE80211_ZY_BAND_STEERING_COUNT,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_bdst_count"},
	{IEEE80211_ZY_BAND_STEERING_DEBUG,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "bdst_debug"},
	{IEEE80211_ZY_BAND_STEERING_DEBUG,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_bdst_debug"},
	{IEEE80211_ZY_BAND_STEERING_HASH,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "bdst_hash"},
	{IEEE80211_ZY_BAND_STEERING_HASH,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_bdst_hash"},
	{IEEE80211_ZY_BAND_STEERING_SHOW,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "show_bdst"},
#endif

#ifdef CONFIG_DP_SUPPORT
	{IEEE80211_ZY_SPECIAL_SSID_DISABLE,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "dis_spec_ssid"},
	{IEEE80211_ZY_CAPWAP_TUNNEL,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "tunnel"},
	{IEEE80211_ZY_LOCAL_DHCP_ENABLE,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "local_dhcp"},
	{IEEE80211_ZY_LOCAL_ASSOC_ENABLE,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "local_assoc"},
	{IEEE80211_ZY_WEB_AUTH_ENABLE,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "web_auth"},
	{IEEE80211_ZY_VLAN,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "vlan"},
	{IEEE80211_ZY_UPDATE_VAP,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "update_vap"},
#endif


#ifdef ZY_DOT11K_SUPPORT
	{IEEE80211_ZY_11K_CH_A_LIST_FLASH,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "chan_a_flush"},

	{IEEE80211_ZY_11K_CH_A_LIST,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "chan_a_add"},

	{IEEE80211_ZY_11K_CH_A_LIST,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "chan_a_get"},

	{IEEE80211_ZY_11K_CH_G_LIST_FLASH,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "chan_bg_flush"},

	{IEEE80211_ZY_11K_CH_G_LIST,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "chan_bg_add"},

	{IEEE80211_ZY_11K_CH_G_LIST,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "chan_bg_get"},

	{IEEE80211_ZY_11K_ENABLE,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "dot11k_en"},

	{IEEE80211_ZY_11K_ENABLE,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_dot11k_en"},

	{IEEE80211_ZY_11K_BASE_SNR,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "base_snr"},

	{IEEE80211_ZY_11K_BASE_SNR,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_base_snr"},

	{IEEE80211_ZY_11R_ROAM_ASSIST,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "roam_assist"},

	{IEEE80211_ZY_11R_ROAM_ASSIST,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_roam_assist"},

	{IEEE80211_ZY_11V_TRANS_ENABLE,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "trans_en"},

	{IEEE80211_ZY_11V_TRANS_ENABLE,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_trans_en"},

	{IEEE80211_ZY_11k_RPT_INTERVAL,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0, "rpt_intval"},

	{IEEE80211_ZY_11k_RPT_INTERVAL,
	 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, "g_rpt_intval"},
#endif
	{IEEE80211_ZY_AC_KICK_MAC,
	 IW_PRIV_TYPE_ACKICK | IW_PRIV_SIZE_FIXED, 0, "ac_kickmac"},
};

#ifdef ZY_FM_SUPPORT
static int get_freq_range(struct net_device *dev, struct iw_range *range)
{
	struct iw_request_info info;
	struct iw_point data;
	int ret = -1;

	info.cmd = SIOCGIWRANGE;
	info.flags = 0;

	if (orig_ieee80211_giwrange) {
		ret =
		    orig_ieee80211_giwrange(dev, &info, (void *)&data,
					    (char *)range);
	}

	return ret;
}

static int get_cur_freq(struct net_device *dev, struct iw_freq *freq)
{
	struct iw_request_info info;
	int ret = -1;

	info.cmd = SIOCGIWFREQ;
	info.flags = 0;

	if (orig_ieee80211_giwfreq) {
		ret = orig_ieee80211_giwfreq(dev, &info, (void *)freq, NULL);
	}

	return ret;
}
#endif

static int zyUMAC_ieee80211_siwfreq(struct net_device *dev,
				   struct iw_request_info *info,
				   void *w, char *extra)
{
	int ret;

#ifdef ZY_FM_SUPPORT
	struct iw_freq pre;
	struct iw_freq cur;
	struct iw_range range;
	zyUMAC_wal_wdev_t vap = zyUMAC_wal_netdev_to_wdev(dev);
	zyUMAC_wal_wphy_t wphy = zyUMAC_wal_wdev_get_wphy(vap);
	struct net_device *wphy_dev = zyUMAC_wal_wphy_to_netdev(wphy);

	get_cur_freq(dev, &pre);
	get_freq_range(dev, &range);
#endif

	if (orig_ieee80211_siwfreq) {
		ret = orig_ieee80211_siwfreq(dev, info, w, extra);
	} else {
		ret = -1;	//ENOTSUP;
	}

#ifdef ZY_FM_SUPPORT
	if (wphy_dev) {
		get_cur_freq(dev, &cur);
		notify_channel_changed(wphy_dev->name, &pre, &cur, &range,
				       "admin configure");
	}
#endif

	return ret;
}

static int zyUMAC_ieee80211_siwmode(struct net_device *dev,
				   struct iw_request_info *info,
				   void *w, char *extra)
{
	return orig_ieee80211_siwmode(dev, info, w, extra);
}

static int zyUMAC_ieee80211_siwtxpow(struct net_device *dev,
				    struct iw_request_info *info,
				    void *w, char *extra)
{
	zyUMAC_wal_wdev_t vap = zyUMAC_wal_netdev_to_wdev(dev);
	zyUMAC_vap_priv_t *zyUMAC_vap_priv =
	    (zyUMAC_vap_priv_t *) zyUMAC_wal_wdev_getdata(vap);
	struct iw_param *rrq = (struct iw_param *)w;

#ifdef ZY_FM_SUPPORT
	int txpow = zyUMAC_wal_wdev_get_txpow(vap);

	if (txpow != 50) {
		notify_txpower_changed(dev->name, txpow,
				       rrq->value > 0
				       && rrq->value <= 26 ? rrq->value : 26,
				       "admin configure");
	}
#endif

	zyUMAC_vap_priv->txpower = (u_int8_t) rrq->value;
	if (orig_ieee80211_siwtxpow) {
		return orig_ieee80211_siwtxpow(dev, info, w, extra);
	} else {
		return -1;	//ENOTSUP;
	}
}

static int zyUMAC_vap_setparam(struct net_device *dev,
			      struct iw_request_info *info,
			      void *w, char *extra, iw_handler orig_handler)
{
	zyUMAC_wal_wdev_t vap = zyUMAC_wal_netdev_to_wdev(dev);
	zyUMAC_vap_priv_t *zyUMAC_vap_priv =
	    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	int *i = (int *)extra;
	int param = i[0];	/* parameter id is 1st */
	int value = i[1];	/* NB: most values are TYPE_INT */
	int res = 0;

	switch (param) {
#ifdef ZY_BDST_SUPPORT
	case IEEE80211_ZY_BAND_STEERING_ENABLE:
		zyUMAC_vap_priv->app_config.band_steering = ! !value;
		break;
	case IEEE80211_ZY_BAND_STEERING_MODE:
		zyUMAC_vap_priv->app_config.band_steering_mode = value;
		break;
	case IEEE80211_ZY_BAND_STEERING_TIME:
		zyUMAC_vap_priv->app_config.band_steering_time = value;
		break;
	case IEEE80211_ZY_BAND_STEERING_COUNT:
		zyUMAC_vap_priv->app_config.band_steering_count = value;
		break;
	case IEEE80211_ZY_BAND_STEERING_DEBUG:
		zyUMAC_vap_priv->app_config.band_steering_debug = ! !value;
		break;
	case IEEE80211_ZY_BAND_STEERING_HASH:
		zyUMAC_vap_priv->app_config.band_steering_hash = value;
		break;
#endif

#ifdef ZY_LEAVE_REPORT_SUPPORT
	case IEEE80211_ZY_AC_KICK_MAC:
		{
			zyUMAC_wal_req_mlme_t mlme;
			/* Setup a MLME request for disassociation of the given MAC */
			kick_sta_info_t *p_sta_info =
			    (kick_sta_info_t *) (&i[1]);

			mlme.mlme_op = zyUMAC_WAL_MLME_REQ_DISASSOC;
			mlme.mlme_sender = zyUMAC_AC_PROTO;
			mlme.mlme_reason = p_sta_info->reason;
			memcpy(mlme.mlme_macaddr, p_sta_info->mac,
			       zyUMAC_IEEE80211_ADDR_LEN);
			return zyUMAC_wal_wdev_mlme_request(vap, &mlme);
		}
#endif

#ifdef CONFIG_DP_SUPPORT
	case IEEE80211_ZY_SPECIAL_SSID_DISABLE:
		zyUMAC_vap_priv->disable_special_ssid = ! !value;
		return res;
	case IEEE80211_ZY_CAPWAP_TUNNEL:
		zyUMAC_vap_priv->tunnel = value;
		return res;
	case IEEE80211_ZY_LOCAL_DHCP_ENABLE:
		zyUMAC_vap_priv->local_dhcp = ! !value;
		return res;
	case IEEE80211_ZY_LOCAL_ASSOC_ENABLE:
		zyUMAC_vap_priv->local_assoc = ! !value;
		return res;
	case IEEE80211_ZY_WEB_AUTH_ENABLE:
		zyUMAC_vap_priv->enable_web_auth = ! !value;
		return res;
	case IEEE80211_ZY_VLAN:
		zyUMAC_vap_priv->vlan = value;
		return res;
#endif

#ifdef ZY_DOT11K_SUPPORT
	case IEEE80211_ZY_11K_CH_A_LIST:
		if (zyUMAC_vap_priv->dot11k.num_channel_rep_a >=
		    MAX_A_BAND_CHANNEL_NUM) {
			zyUMAC_vap_priv->dot11k.
			    channel_rep_a[MAX_A_BAND_CHANNEL_NUM - 1] = value;
		} else {
			zyUMAC_vap_priv->dot11k.channel_rep_a[zyUMAC_vap_priv->
							     dot11k.
							     num_channel_rep_a]
			    = value;
			zyUMAC_vap_priv->dot11k.num_channel_rep_a++;
		}
		zyUMAC_vap_priv->dot11k.change_flag_a = 1;
		break;
	case IEEE80211_ZY_11K_CH_G_LIST:
		if (zyUMAC_vap_priv->dot11k.num_channel_rep_g >=
		    MAX_G_BAND_CHANNEL_NUM) {
			zyUMAC_vap_priv->dot11k.
			    channel_rep_g[MAX_G_BAND_CHANNEL_NUM - 1] = value;
		} else {
			zyUMAC_vap_priv->dot11k.channel_rep_g[zyUMAC_vap_priv->
							     dot11k.
							     num_channel_rep_g]
			    = value;
			zyUMAC_vap_priv->dot11k.num_channel_rep_g++;
		}
		zyUMAC_vap_priv->dot11k.change_flag_g = 1;
		return res;

	case IEEE80211_ZY_11K_ENABLE:
		zyUMAC_vap_priv->dot11k.dot11k_eb = ! !value;
		return res;
	case IEEE80211_ZY_11K_BASE_SNR:
		zyUMAC_vap_priv->dot11k.base_snr = value;
		return res;
	case IEEE80211_ZY_11V_TRANS_ENABLE:
		zyUMAC_vap_priv->dot11k.dot11v_transition_eb = ! !value;
		return res;
	case IEEE80211_ZY_11R_ROAM_ASSIST:
		zyUMAC_vap_priv->dot11k.roam_assist_eb = ! !value;
		return res;
	case IEEE80211_ZY_11k_RPT_INTERVAL:
		if (value >= 10 && value <= 200)
			zyUMAC_vap_priv->dot11k.beacon_rep_interval = value;
		else
			zyUMAC_vap_priv->dot11k.beacon_rep_interval = 60;
		return res;
#endif

	case zyUMAC_IEEE80211_PARAM_MACCMD:
		if (orig_handler) {
			res = orig_handler(dev, info, w, extra);
			if (res == 0) {
				zyUMAC_acl_setpolicy_from_file(vap, value,
							      zyUMAC_IEEE80211_ACL_FLAG_ACL_LIST_1);
			}
		}
		return 1;

	default:
		(void)zyUMAC_vap_priv;
		res = -1;
		break;
	}

	return res;
}

int zyUMAC_vap_getparam(struct net_device *dev, struct iw_request_info *info,
		       void *w, char *extra, iw_handler orig_handler)
{
	zyUMAC_wal_wdev_t vap = zyUMAC_wal_netdev_to_wdev(dev);
	zyUMAC_vap_priv_t *zyUMAC_vap_priv =
	    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	int *param = (int *)extra;
	int res = 0;

	switch (*param) {
#ifdef ZY_BDST_SUPPORT
	case IEEE80211_ZY_BAND_STEERING_ENABLE:
		*param = zyUMAC_vap_priv->app_config.band_steering;
		break;
	case IEEE80211_ZY_BAND_STEERING_MODE:
		*param = zyUMAC_vap_priv->app_config.band_steering_mode;
		break;
	case IEEE80211_ZY_BAND_STEERING_TIME:
		*param = zyUMAC_vap_priv->app_config.band_steering_time;
		break;
	case IEEE80211_ZY_BAND_STEERING_COUNT:
		*param = zyUMAC_vap_priv->app_config.band_steering_count;
		break;
	case IEEE80211_ZY_BAND_STEERING_DEBUG:
		*param = zyUMAC_vap_priv->app_config.band_steering_debug;
		break;
	case IEEE80211_ZY_BAND_STEERING_HASH:
		*param = zyUMAC_vap_priv->app_config.band_steering_hash;
		break;
	case IEEE80211_ZY_BAND_STEERING_SHOW:
		zyUMAC_band_steering_debug_dump();
		break;
#endif

#ifdef ZY_DOT11K_SUPPORT
	case IEEE80211_ZY_11K_CH_A_LIST_FLASH:
		memset(zyUMAC_vap_priv->dot11k.channel_rep_a, 0,
		       sizeof(zyUMAC_vap_priv->dot11k.channel_rep_a));
		zyUMAC_vap_priv->dot11k.num_channel_rep_a = 0;
		zyUMAC_vap_priv->dot11k.change_flag_a = 1;
		*param = zyUMAC_vap_priv->dot11k.num_channel_rep_a;
		break;

	case IEEE80211_ZY_11K_CH_A_LIST:
		zyUMAC_LOG(LOG_INFO, "dot11k a band channe num:%d",
		       zyUMAC_vap_priv->dot11k.num_channel_rep_a);
		if (zyUMAC_vap_priv->dot11k.num_channel_rep_a > 0) {
			int i = 0;
			for (i = 0;
			     i < zyUMAC_vap_priv->dot11k.num_channel_rep_a; i++)
				zyUMAC_LOG(LOG_INFO, "num[%d]:%d", i,
				       zyUMAC_vap_priv->dot11k.channel_rep_a[i]);

		}
		*param = zyUMAC_vap_priv->dot11k.num_channel_rep_a;
		break;

	case IEEE80211_ZY_11K_CH_G_LIST_FLASH:
		memset(zyUMAC_vap_priv->dot11k.channel_rep_g, 0,
		       sizeof(zyUMAC_vap_priv->dot11k.channel_rep_g));
		zyUMAC_vap_priv->dot11k.num_channel_rep_g = 0;
		zyUMAC_vap_priv->dot11k.change_flag_g = 1;
		*param = zyUMAC_vap_priv->dot11k.num_channel_rep_g;
		break;

	case IEEE80211_ZY_11K_CH_G_LIST:
		zyUMAC_LOG(LOG_INFO, "dot11k g band channe num:%d",
		       zyUMAC_vap_priv->dot11k.num_channel_rep_g);
		if (zyUMAC_vap_priv->dot11k.num_channel_rep_g > 0) {
			int i = 0;
			for (i = 0;
			     i < zyUMAC_vap_priv->dot11k.num_channel_rep_g; i++)
				zyUMAC_LOG(LOG_INFO, "num[%d]:%d", i,
				       zyUMAC_vap_priv->dot11k.channel_rep_g[i]);

		}
		*param = zyUMAC_vap_priv->dot11k.num_channel_rep_g;
		break;

	case IEEE80211_ZY_11K_ENABLE:
		*param = zyUMAC_vap_priv->dot11k.dot11k_eb;
		break;
	case IEEE80211_ZY_11K_BASE_SNR:
		*param = zyUMAC_vap_priv->dot11k.base_snr;
		break;
	case IEEE80211_ZY_11V_TRANS_ENABLE:
		*param = zyUMAC_vap_priv->dot11k.dot11v_transition_eb;
		break;
	case IEEE80211_ZY_11R_ROAM_ASSIST:
		*param = zyUMAC_vap_priv->dot11k.roam_assist_eb;
		break;
	case IEEE80211_ZY_11k_RPT_INTERVAL:
		*param = zyUMAC_vap_priv->dot11k.beacon_rep_interval;
		break;
#endif
	/* IEEE80211_PARAM_TXRX_FW_STATS is prohibited */
	default:
		(void)zyUMAC_vap_priv;
		res = -1;
		break;
	}

	return res;
}

#ifdef ZY_DOT11K_SUPPORT
static int zyUMAC_ieee80211_dbgreq(struct net_device *dev,
				  struct iw_request_info *info,
				  void *w, char *extra)
{
	struct ieee80211req_athdbg *req = (struct ieee80211req_athdbg *)extra;
	zyUMAC_wal_wdev_t vap = zyUMAC_wal_netdev_to_wdev(dev);

	switch (req->cmd) {
	case IEEE80211_DBGREQ_SENDBCNRPT:	//wlan_send_beacon_measreq
		if (!IEEE80211_ADDR_EQ(vap->iv_myaddr, req->dstmac)) {	//ieee80211_add_measreq_beacon_ie
			ieee80211_rrm_beaconreq_info_t *bcnrpt =
			    &req->data.bcnrpt;
			zyUMAC_rrm_channel_update(vap, bcnrpt);
		}
		break;
	default:
		break;
	}

	return orig_ieee80211_dbgreq(dev, info, w, extra);
}
#endif

static zyUMAC_wal_ioctl_hook_table _wdev_ioctl_hook_table = {
	extra_vap_iw_priv_args,
	TABLE_SIZE_N(extra_vap_iw_priv_args),
	(iw_handler_hook) zyUMAC_vap_getparam,
	(iw_handler_hook) zyUMAC_vap_setparam
};

/*
 * Replace handlers for ieee80211_ioctl_vattach(dev)
 */
void zyUMAC_vap_ioctl_attach(zyUMAC_wal_wdev_t vap)
{
#define IW(idx) (idx - SIOCSIWCOMMIT)
#ifdef CONFIG_WEXT_PRIV
	iw_handler *private_handlers = NULL;
#endif	
	iw_handler *standard_handlers = NULL;
	struct net_device *dev = NULL;
	struct iw_handler_def *orig_vap_iw_handler_def = NULL;

	if (vap == NULL) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	zyUMAC_LOG(LOG_INFO, "Attach wdev zyUMAC ioctl");

	dev = zyUMAC_wal_wdev_to_netdev(vap);

	//replace original iwpriv table with new one
	orig_vap_iw_handler_def =
	    (struct iw_handler_def *)dev->wireless_handlers;
	ASSERT(orig_vap_iw_handler_def != NULL);

	zyUMAC_wal_wdev_ioctl_attach(vap, &_wdev_ioctl_hook_table);

	standard_handlers = (iw_handler *) orig_vap_iw_handler_def->standard;
#ifdef CONFIG_WEXT_PRIV
	private_handlers = (iw_handler *) orig_vap_iw_handler_def->private;
#endif	

    /*** standard ***/
	if (standard_handlers) {
		if (orig_ieee80211_siwfreq == NULL) {
			orig_ieee80211_siwfreq =
			    standard_handlers[IW(SIOCSIWFREQ)];
			standard_handlers[IW(SIOCSIWFREQ)] =
			    (iw_handler) zyUMAC_ieee80211_siwfreq;
		}

		if (orig_ieee80211_giwfreq == NULL) {
			orig_ieee80211_giwfreq =
			    standard_handlers[IW(SIOCGIWFREQ)];
		}

		if (orig_ieee80211_siwmode == NULL) {
			orig_ieee80211_siwmode =
			    standard_handlers[IW(SIOCSIWMODE)];
			standard_handlers[IW(SIOCSIWMODE)] =
			    (iw_handler) zyUMAC_ieee80211_siwmode;
		}

		if (orig_ieee80211_siwtxpow == NULL) {
			orig_ieee80211_siwtxpow =
			    standard_handlers[IW(SIOCSIWTXPOW)];
			standard_handlers[IW(SIOCSIWTXPOW)] =
			    (iw_handler) zyUMAC_ieee80211_siwtxpow;
		}

		if (orig_ieee80211_giwrange == NULL) {
			orig_ieee80211_giwrange =
			    standard_handlers[IW(SIOCGIWRANGE)];
		}
	} else {
		zyUMAC_LOG(LOG_WARN,
			"Original wireless standard handler not found");
	}

#ifdef CONFIG_WEXT_PRIV
    /*** private ***/
	if (private_handlers) {
#ifdef ZY_DOT11K_SUPPORT
		if (orig_ieee80211_dbgreq == NULL) {	/* SIOCWFIRSTPRIV+24 */
			orig_ieee80211_dbgreq = private_handlers[24];
			private_handlers[24] =
			    (iw_handler) zyUMAC_ieee80211_dbgreq;
		}
#endif
	} else {
		zyUMAC_LOG(LOG_WARN,
		       "Original wireless private handler not found");
	}
#endif

#undef IW
}

//call only when driver unload/detach
void zyUMAC_vap_ioctl_detach()
{
	zyUMAC_LOG(LOG_INFO, "Detach wdev zyUMAC ioctl");
	zyUMAC_wal_wdev_ioctl_detach();
}
