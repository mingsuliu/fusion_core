#include <linux/proc_fs.h>
#include "zyUMAC_wal_api.h"
#include "zyUMAC_proc.h"
#include "zyUMAC_events.h"
#include "zyUMAC_ieee80211.h"

#ifdef ZY_LEAVE_REPORT_SUPPORT

static char *sk_get_sender_str(int32_t sender, char *str, int STR_LENGTH)
{
	if (!str || STR_LENGTH <= 0) {
		zyUMAC_LOG(LOG_ERR, "str is null or length is 0");
		return NULL;
	}
	switch (sender) {
	case zyUMAC_STA_PROTO:
		snprintf(str, STR_LENGTH, "Station");
		break;
	case zyUMAC_AP_PROTO:
		snprintf(str, STR_LENGTH, "AP Proto");
		break;
	case zyUMAC_AC_PROTO:
		snprintf(str, STR_LENGTH, "AC");
		break;
	case zyUMAC_AP_WEAK_RSSI:
		snprintf(str, STR_LENGTH, "Sta Rssi");
		break;
	case zyUMAC_AP_DOS_ATTACK:
		snprintf(str, STR_LENGTH, "Dos");
		break;
	case zyUMAC_AP_BAlANCE_STEERED:
		snprintf(str, STR_LENGTH, "BandSteer");
		break;
	case zyUMAC_AP_LOAD_BALANCE:
		snprintf(str, STR_LENGTH, "Loadb");
		break;
	case zyUMAC_AP_ACL:
		snprintf(str, STR_LENGTH, "ACL ");
		break;
	case zyUMAC_AP_VAP_MAX_STA:
		snprintf(str, STR_LENGTH, "Max Sta");
		break;
	case zyUMAC_AP_DP_DEL:
		snprintf(str, STR_LENGTH, "DP Del");
		break;
	case zyUMAC_AP_TIMEOUT:
		snprintf(str, STR_LENGTH, "TimeOut");
		break;
	case zyUMAC_AP_KICK_OUT:
		snprintf(str, STR_LENGTH, "Kick out");
		break;
	case zyUMAC_AP_ROUGE_AP:
		snprintf(str, STR_LENGTH, "Rouge ap");
		break;
	case zyUMAC_AP_HOSTAPD:
		snprintf(str, STR_LENGTH, "Hostapd");
		break;
	case zyUMAC_AP_MAC_AUTH:
		snprintf(str, STR_LENGTH, "Mac AUTH");
		break;
	case zyUMAC_AP_NODE_LEAVE:
		snprintf(str, STR_LENGTH, "Node Leave");
		break;
	case zyUMAC_AP_WIM:
		snprintf(str, STR_LENGTH, "WIM");
		break;
	case zyUMAC_AP_IWPRIV_KICK_OUT:
		snprintf(str, STR_LENGTH, "Iwpriv");
		break;
	case zyUMAC_AP_DOT11V:
		snprintf(str, STR_LENGTH, "DOT11V");
		break;
	default:
		snprintf(str, STR_LENGTH, "Unknown Sender: %d", sender);
	}
	return str;
}

static char *sk_get_ac_reason_str(int32_t reason, char *str, int STR_LENGTH)
{
	if (!str || STR_LENGTH <= 0) {
		zyUMAC_LOG(LOG_ERR, "str is null or length is 0");
		return NULL;
	}

	switch (reason) {
	case zyUMAC_AC_USER_LOGOUT:
		snprintf(str, STR_LENGTH, "User Logout ");
		break;
	case zyUMAC_IDLE_TIMEOUT:
		snprintf(str, STR_LENGTH, "Idle Timeout");
		break;
	case zyUMAC_SESSION_TIMEOUT:
		snprintf(str, STR_LENGTH, "Session Timeout");
		break;
	case zyUMAC_ADMIN_RESET:
		snprintf(str, STR_LENGTH, "Admin Reset");
		break;
	case zyUMAC_USER_INFT_TRUSET:
		snprintf(str, STR_LENGTH, "User Interface No trust to trust");
		break;
	case zyUMAC_USER_ERROR:
		snprintf(str, STR_LENGTH, "User Error");
		break;
	case zyUMAC_MASTER_REQUEST:
		snprintf(str, STR_LENGTH, "Master Request");
		break;
	case zyUMAC_RADIUS_DM:
		snprintf(str, STR_LENGTH, "COA DM ");
		break;
	case zyUMAC_USERMISS_IP_CHANGE:
		snprintf(str, STR_LENGTH, "Usermiss Change IP");
		break;
	case zyUMAC_GET_AAA_PROFILE_FAIL:
		snprintf(str, STR_LENGTH, "Get AAA profile Fail");
		break;
	case zyUMAC_GET_USER_ROLE_FAIL:
		snprintf(str, STR_LENGTH, "Get User Role Fail");
		break;
	case zyUMAC_BSS_DEL:
		snprintf(str, STR_LENGTH, "Bss Delete");
		break;
	case zyUMAC_REDUNDANCY_DEL:
		snprintf(str, STR_LENGTH, "Redundany del");
		break;
	case zyUMAC_ROAMING_DEL:
		snprintf(str, STR_LENGTH, "Roaming Delete");
		break;
	default:
		snprintf(str, STR_LENGTH, "Unknown AC Reason: %d", reason);
	}
	return str;
}

static char *sk_get_ap_reason_str(int32_t reason, char *str, int STR_LENGTH)
{
	if (!str || STR_LENGTH <= 0) {
		zyUMAC_LOG(LOG_ERR, "str is null or length is 0");
		return NULL;
	}
	switch (reason) {
	case IEEE80211_REASON_UNSPECIFIED:
		snprintf(str, STR_LENGTH, "Unspecified reason");
		break;
	case IEEE80211_REASON_AUTH_EXPIRE:
		snprintf(str, STR_LENGTH, "Previous auth no longer valid");
		break;
	case IEEE80211_REASON_AUTH_LEAVE:
		snprintf(str, STR_LENGTH,
			 "Station is leaving (or has left) IBSS or ESS");
		break;
	case IEEE80211_REASON_ASSOC_EXPIRE:
		snprintf(str, STR_LENGTH, "Disassociated due to inactivity");
		break;
	case IEEE80211_REASON_ASSOC_TOOMANY:
		snprintf(str, STR_LENGTH,
			 "Disassociated because AP is unable to handle all current associated stations");
		break;
	case IEEE80211_REASON_NOT_AUTHED:
		snprintf(str, STR_LENGTH,
			 "Class 2 frame received from nonauthenticated station");
		break;
	case IEEE80211_REASON_NOT_ASSOCED:
		snprintf(str, STR_LENGTH,
			 "Class 3 frame received from nonassociated station");
		break;
	case IEEE80211_REASON_ASSOC_LEAVE:
		snprintf(str, STR_LENGTH,
			 "Disassociated station is leaving (or has left) BSS");
		break;
	case IEEE80211_REASON_ASSOC_NOT_AUTHED:
		snprintf(str, STR_LENGTH,
			 "Station requesting (re)association is not authenticated with responding station");
		break;
	case IEEE80211_REASON_IE_INVALID:
		snprintf(str, STR_LENGTH, "Invalid information element");
		break;
	case IEEE80211_REASON_MIC_FAILURE:
		snprintf(str, STR_LENGTH,
			 "Message integrity code (MIC) failure");
		break;
	case IEEE80211_REASON_INVALID_GROUP_CIPHER:
		snprintf(str, STR_LENGTH, "Invalid group cipher");
		break;
	case IEEE80211_REASON_INVALID_PAIRWISE_CIPHER:
		snprintf(str, STR_LENGTH, "Invalid pairwise cipher");
		break;
	case IEEE80211_REASON_INVALID_AKMP:
		snprintf(str, STR_LENGTH, "Invalid AKMP");
		break;
	case IEEE80211_REASON_TDLS_UNREACHABLE:
		snprintf(str, STR_LENGTH,
			 "TDLS direct-link teardown due to TDLS peer STA unreachable via the TDLS direct link");
		break;
	case IEEE80211_REASON_TDLS_UNSPCIFIED:
		snprintf(str, STR_LENGTH,
			 "TDLS direct-link teardown for unspecified reason");
		break;
	case IEEE80211_REASON_DISASSOCIATE_SESSION_SSP:
		snprintf(str, STR_LENGTH,
			 "Disassociated because session terminated by SSP request");
		break;
	case IEEE80211_REASON_DISASSOCIATE_LACK_SSP:
		snprintf(str, STR_LENGTH,
			 "Disassociated because of lack of SSP roaming agreement");
		break;
	case IEEE80211_REASON_REQUESTED_SERVICE_REJECTED:
		snprintf(str, STR_LENGTH,
			 "Requested service rejected because of SSP chiper suite or AKM requiremnet service");
		break;
	case IEEE80211_REASON_REQUESTED_SERVICE_NOT_AUTH:
		snprintf(str, STR_LENGTH,
			 "Requested service is not authorized in this location");
		break;
	case IEEE80211_REASON_REQUESTED_QOS_AP_LACK:
		snprintf(str, STR_LENGTH,
			 "TS deleted because QoS AP lacks sufficient bandwith for this STA due to a change in BSS service or operational mode");
		break;
	case IEEE80211_REASON_QOS:
		snprintf(str, STR_LENGTH,
			 "Disassociated for unspecified, QoS-related reason");
		break;
	case IEEE80211_REASON_LACK_BANDWIDTH:
		snprintf(str, STR_LENGTH,
			 "Disassociated because QoS AP lacks sufficient bandwidth for this QoS STA");
		break;
	case IEEE80211_REASON_FRAME_NOT_ACK:
		snprintf(str, STR_LENGTH,
			 "Disassociated because excessive number of frames need to be acknowledged");
		break;
	case IEEE80211_REASON_OUTSIDE_LIMIT:
		snprintf(str, STR_LENGTH,
			 "Disassociated because QSTA is transmitting outside the limits of its TXOPs");
		break;
	case IEEE80211_REASON_LEAVE_QBSS:
		snprintf(str, STR_LENGTH,
			 "Requested from peer QSTA as the QSTA is leaving the QBSS(or resetting)");
		break;
	case IEEE80211_REASON_NOT_USE_QSTA_MECHANISM:
		snprintf(str, STR_LENGTH,
			 "Requested from peer QSTA as it does not want to use the mechanism");
		break;
	case IEEE80211_REASON_QSTA_REQUIRE_SETUP:
		snprintf(str, STR_LENGTH,
			 "Requested from peer QSTA as the QSTA received frames using the mechanism for which a setup is required");
		break;
	case IEEE80211_REASON_QSTA_TIMEOUT:
		snprintf(str, STR_LENGTH,
			 "Requested from peer QSTA due to timeout");
		break;
	case IEEE80211_REASON_QSTA_UNSUPPORT_CIPHER_SUITE:
		snprintf(str, STR_LENGTH,
			 "Peer QSTA does not support the requested cipher suite");
		break;
	case ZY_REASON_KICK_BY_IWPRIV:
		snprintf(str, STR_LENGTH,
			 "Use iwpriv to kick out STA from user space");
		break;
	case ZY_REASON_WEAK_RSSI:
		snprintf(str, STR_LENGTH,
			 "Disassociated because station's rssi is too weak");
		break;
	case IEEE80211_REASON_KICK_OUT:
		snprintf(str, STR_LENGTH, "AP kicked STA out");
		break;
	case IEEE80211_REASON_ASSOC_BSSTM:
		snprintf(str, STR_LENGTH,
			 "Disassociated due to BSS Transition Management");
		break;
	case ZY_REASON_MAC_AUTH:
		snprintf(str, STR_LENGTH, "MAC auth failure");
		break;
	default:
		snprintf(str, STR_LENGTH, "Reserved");
	}
	return str;
}

static char *sk_get_frame_type_str(int32_t frame_type, char *str,
				   int STR_LENGTH)
{
	if (!str || STR_LENGTH <= 0) {
		zyUMAC_LOG(LOG_ERR, "str is null or length is 0");
		return NULL;
	}
	switch (frame_type) {
	case zyUMAC_RECORD_TYPE_DEAUTH:
		snprintf(str, STR_LENGTH, "De-auth");
		break;
	case zyUMAC_RECORD_TYPE_DISASSOC:
		snprintf(str, STR_LENGTH, "Dis-assoc");
		break;
	case zyUMAC_RECORD_TYPE_AUTH:
		snprintf(str, STR_LENGTH, "Auth");
		break;
	case zyUMAC_RECORD_TYPE_ASSOC_RESP:
		snprintf(str, STR_LENGTH, "Assoc-resp");
		break;
	case zyUMAC_RECORD_TYPE_DROP:
		snprintf(str, STR_LENGTH, "Drop");
		break;
	default:
		snprintf(str, STR_LENGTH, "Unknown: %d", frame_type);
	}
	return str;
}

static char *sk_get_fail_status_str(int32_t fail_status, char *str,
				    int STR_LENGTH)
{
	if (!str || STR_LENGTH <= 0) {
		zyUMAC_LOG(LOG_ERR, "str is null or length is 0");
		return NULL;
	}
	switch (fail_status) {
	case 0:
		snprintf(str, STR_LENGTH, "Successful");
		break;
	case IEEE80211_STATUS_UNSPECIFIED:
		snprintf(str, STR_LENGTH, "Unspecified failure");
		break;
	case ZY_STATUS_WEAK_RSSI:
		snprintf(str, STR_LENGTH, "Week Rssi(custom)");
		break;
	case IEEE80211_STATUS_TDLS_WAKEUP_PROIVIDED:
		snprintf(str, STR_LENGTH,
			 "TDLS wakeup schedule rejected but  alternative schedule");
		break;
		/* temp modified of bug 24973 */
	case IEEE80211_STATUS_TDLS_WAKEUP_REJECTED:
		//snprintf(str, STR_LENGTH, "TDLS wakeup schedule rejected");
		snprintf(str, STR_LENGTH, "Weak Rssi(custom)");
		break;
	case IEEE80211_STATUS_SECURITY_DISABLE:
		snprintf(str, STR_LENGTH, "Security disable");
		break;
	case IEEE80211_STATUS_UNACCEPTEABLE_LIFETIME:
		snprintf(str, STR_LENGTH, "unacceptable lifetime");
		break;
	case IEEE80211_STATUS_NOT_IN_SAME_BSS:
		snprintf(str, STR_LENGTH, "not in same BSS");
		break;
	case IEEE80211_STATUS_CAPINFO:
		snprintf(str, STR_LENGTH,
			 "Cannot support all requested capabilities ");
		break;
	case IEEE80211_STATUS_NOT_ASSOCED:
		snprintf(str, STR_LENGTH,
			 "Reassoc denied due to inability to confirm that assoc exists");
		break;
	case IEEE80211_STATUS_OTHER:
		snprintf(str, STR_LENGTH,
			 "Assoc denied due to reason outside the scope of this standard");
		break;
	case IEEE80211_STATUS_ALG:
		snprintf(str, STR_LENGTH,
			 "Responding station does not support the specified auth algorithm");
		break;
	case IEEE80211_STATUS_SEQUENCE:
		snprintf(str, STR_LENGTH,
			 "Received an Auth frame Seq num out of expected sequence");
		break;
	case IEEE80211_STATUS_CHALLENGE:
		snprintf(str, STR_LENGTH,
			 "Auth rejected because of challenge failure");
		break;
	case IEEE80211_STATUS_TIMEOUT:
		snprintf(str, STR_LENGTH,
			 "Auth rejected due to timeout waiting for next frame     in seq");
		break;
	case IEEE80211_STATUS_TOOMANY:
		snprintf(str, STR_LENGTH,
			 "Assoc denied , Ap is unable to handle additional associated stations");
		break;
	case IEEE80211_STATUS_BASIC_RATE:
		snprintf(str, STR_LENGTH,
			 "Assoc denied , requesting STA not supporting all of the data rates ");
		break;
	case IEEE80211_STATUS_SP_REQUIRED:
		snprintf(str, STR_LENGTH,
			 "Association denied due to requesting station not supporting the short preamble option");
		break;
	case IEEE80211_STATUS_PBCC_REQUIRED:
		snprintf(str, STR_LENGTH,
			 "Association denied due to requesting station not supporting the PBCC modulation option");
		break;
	case IEEE80211_STATUS_CA_REQUIRED:
		snprintf(str, STR_LENGTH,
			 "Association denied due to requesting station not supporting the Channel Agility option");
		break;
	case IEEE80211_STATUS_TOO_MANY_STATIONS:
		snprintf(str, STR_LENGTH,
			 "Association request rejected because Spectrum Management capability is required");
		break;
	case IEEE80211_STATUS_RATES:
		snprintf(str, STR_LENGTH,
			 "Association request rejected because the information in the Power Capability element is unacceptable");
		break;
	case IEEE80211_STATUS_CHANNEL_NOT_SUPPORT:
		snprintf(str, STR_LENGTH,
			 "Association request rejected because the information in the Supported Channels element is unacceptable");
		break;
	case IEEE80211_STATUS_SHORTSLOT_REQUIRED:
		snprintf(str, STR_LENGTH,
			 "Association denied due to requesting station not supporting the Short Slot Time option");
		break;
	case IEEE80211_STATUS_DSSSOFDM_REQUIRED:
		snprintf(str, STR_LENGTH,
			 "Association denied due to requesting station not supporting the DSSS-OFDM option");
		break;
	case IEEE80211_STATUS_NO_HT:
		snprintf(str, STR_LENGTH,
			 "Association denied not supporting HT option");
		break;
	case IEEE80211_STATUS_ROKH_UNREACHABLE:
		snprintf(str, STR_LENGTH, "R0KH unreachable");
		break;
	case IEEE80211_STATUS_NOT_SUPPORT_THE_PHASED_COEXITENCE:
		snprintf(str, STR_LENGTH,
			 "STA not ssupport the phased coexistence operation");
		break;
	case IEEE80211_STATUS_REJECT_TEMP:
		snprintf(str, STR_LENGTH, "reject temporarily");
		break;
	case IEEE80211_STATUS_MFP_VIOLATION:
		snprintf(str, STR_LENGTH,
			 "Ribuset management frame policy violation");
		break;
	case IEEE80211_STATUS_UNSPECIFIED_FOR_QSTA:
		snprintf(str, STR_LENGTH, "Unspecified, QoS-related failure");
		break;
	case IEEE80211_STATUS_LACK_BANDWIDTH:
		snprintf(str, STR_LENGTH,
			 "Association denied because QAP has insufficient bandwidth to handle another QSTA");
		break;
//remove for redundant case
//        case IEEE80211_STATUS_FRAME_NOT_ACK:
//            snprintf(str, STR_LENGTH, "Association denied due to excessive frame loss rates and/or poor conditions on current operating channel");
//            break;
	case IEEE80211_STATUS_UNSUPPORT_QSTA:
		snprintf(str, STR_LENGTH,
			 "Association (with QBSS) denied because the requesting STA does not support the QoS facility");
		break;
	case IEEE80211_STATUS_REFUSED:
		snprintf(str, STR_LENGTH, "The request has been declined");
		break;
	case IEEE80211_STATUS_INVALID_PARAM:
		snprintf(str, STR_LENGTH,
			 "The request has not been successful as one or more parameters have invalid values");
		break;
	case IEEE80211_STATUS_REQUEST_NOT_BE_HONORED:
		snprintf(str, STR_LENGTH,
			 "The TS has not been created because the request cannot be honored; however, a suggested");
		break;
	case IEEE80211_STATUS_INVALID_ELEMENT:
		snprintf(str, STR_LENGTH, "Invalid information element");
		break;
	case IEEE80211_STATUS_INVALID_GROUP_CIPHER:
		snprintf(str, STR_LENGTH, "Invalid group cipher");
		break;
	case IEEE80211_STATUS_INVALID_PAIRWISE_CIPHER:
		snprintf(str, STR_LENGTH, "Invalid pairwise cipher");
		break;
	case IEEE80211_STATUS_INVALID_AKMP:
		snprintf(str, STR_LENGTH, "Invalid AKMP");
		break;
		//case IEEE80211_STATUS_INVALID_RSN_VER:
	case IEEE80211_STATUS_UNSUPPORTED_RSNE_VER:
		snprintf(str, STR_LENGTH,
			 "Unsupported RSN information element version");
		break;
	case IEEE80211_STATUS_INVALID_RSN_CAP:
		snprintf(str, STR_LENGTH,
			 "Invalid RSN information element capabilities");
		break;
	case IEEE80211_STATUS_INVALID_CIPHER_SUITE:
		snprintf(str, STR_LENGTH,
			 "Cipher suite rejected because of security policy");
		break;
	case IEEE80211_STATUS_DLS_NOT_ALLOWED:
		snprintf(str, STR_LENGTH,
			 "Direct link is not allowed in the BSS by policy");
		break;
	case IEEE80211_STATUS_RADAR_DETECTED:
		snprintf(str, STR_LENGTH, "Detacted radar");
		break;
	case IEEE80211_STATUS_POOR_CHAN_CONDITION:
		snprintf(str, STR_LENGTH, "Poor channel condition");
		break;
	default:
		snprintf(str, STR_LENGTH, "Reserved");
	}
	return str;
}

static char *sk_get_reason_str(sta_leave_entry_t * entry, char *str,
			       int STR_LENGTH)
{
	if (!entry || !str || STR_LENGTH <= 0) {
		zyUMAC_LOG(LOG_ERR, "entry/str is null or length is 0");
		return NULL;
	}
	if (entry->sender == zyUMAC_AC_PROTO)
		return sk_get_ac_reason_str(entry->reason, str, STR_LENGTH);
	else
		return sk_get_ap_reason_str(entry->reason, str, STR_LENGTH);
}

#define LEAVE_TAB_HEAD_FORMAT "%-22s%-24s%-15s%-24s%-16s%s\n"
#define LEAVE_TAB_DATA_FORMAT "%-22pM%-24s%-15s%-24s%-16d[%04d]:%s\n"

char *sk_get_time_str_from_time(struct timeval *tv, char *time_str, int len)
{
	struct rtc_time rtc_tv;

	if (!time_str || len <= 0) {
		zyUMAC_LOG(LOG_ERR, "arguments is invalid!");
		return NULL;
	}
	rtc_time_to_tm(tv->tv_sec, &rtc_tv);
	snprintf(time_str, len, "%04d-%02d-%02d %02d:%02d:%02d",
		 rtc_tv.tm_year + 1900, rtc_tv.tm_mon + 1, rtc_tv.tm_mday,
		 rtc_tv.tm_hour, rtc_tv.tm_min, rtc_tv.tm_sec);
	return time_str;
}

static int proc_sta_fail_status_show(struct seq_file *m, void *v)
{
	zyUMAC_wal_wdev_t vap = (zyUMAC_wal_wdev_t) m->private;
	seq_printf(m, "Sta auth/assoc fail status table:\n");
	seq_printf(m, LEAVE_TAB_HEAD_FORMAT, "MAC Address", "Leave Time",
		   "Frame", "Sender", "Online(min)", "Reason/Status");
	if (vap && vap->iv_zyUMAC_vap_priv) {
		zyUMAC_vap_priv_t *tmpPtr =
		    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
		int i, cnt;
		sta_leave_entry_mgr_t *tab = tmpPtr->iv_sta_fail_status_tab;
		sta_leave_entry_t *entry = NULL;
		if (!tab) {
			zyUMAC_LOG(LOG_ERR, "Invalid arguments");
			return -1;
		}
		spin_lock(&(tab->leave_lock));
		i = tab->size <
		    MAX_RECORD_STA_PER_VAP ? 0 : tab->next_entry_num;

		for (cnt = 0; cnt < tab->size; cnt++, i++) {
			char time_str[32] = { 0 };
			char frame_type_str[32] = { 0 };
			char sender_str[32] = { 0 };
			char reason_str[128] = { 0 };

			i %= MAX_RECORD_STA_PER_VAP;
			entry = &(tab->entry[i]);
			if (entry->sender == zyUMAC_AP_HOSTAPD
			    && entry->frame_type !=
			    zyUMAC_RECORD_TYPE_ASSOC_RESP) {
				sk_get_reason_str(entry, reason_str,
						  sizeof(reason_str));
			} else {
				sk_get_fail_status_str(entry->reason,
						       reason_str,
						       sizeof(reason_str));
			}
			seq_printf(m, LEAVE_TAB_DATA_FORMAT, entry->mac,
				   sk_get_time_str_from_time(&entry->
							     record_time,
							     time_str,
							     sizeof(time_str)),
				   sk_get_frame_type_str(entry->frame_type,
							 frame_type_str,
							 sizeof
							 (frame_type_str)),
				   sk_get_sender_str(entry->sender, sender_str,
						     sizeof(sender_str)),
				   entry->online_time / 60, entry->reason,
				   reason_str);
		}
		seq_printf(m, "\nTotal record szie: %d\n", tab->size);
		spin_unlock(&(tab->leave_lock));
	} else {
		seq_printf(m, "\nTotal record szie: %d\n", 0);
	}
	return 0;
}

static int proc_sta_leave_reason_show(struct seq_file *m, void *v)
{
	zyUMAC_wal_wdev_t vap = (zyUMAC_wal_wdev_t) m->private;
	seq_printf(m, "Sta leave reason table:\n");
	seq_printf(m, LEAVE_TAB_HEAD_FORMAT, "MAC Address", "Leave Time",
		   "Frame", "Sender", "Online(min)", "Reason/Status");
	if (vap && vap->iv_zyUMAC_vap_priv) {
		zyUMAC_vap_priv_t *tmpPtr =
		    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
		int i, cnt;
		sta_leave_entry_mgr_t *tab = tmpPtr->iv_sta_leave_reason_tab;
		sta_leave_entry_t *entry = NULL;
		if (!tab) {
			zyUMAC_LOG(LOG_ERR, "Invalid arguments");
			return -1;
		}
		spin_lock(&(tab->leave_lock));
		i = tab->size <
		    MAX_RECORD_STA_PER_VAP ? 0 : tab->next_entry_num;

		for (cnt = 0; cnt < tab->size; cnt++, i++) {
			char time_str[32] = { 0 };
			char frame_type_str[32] = { 0 };
			char sender_str[32] = { 0 };
			char reason_str[128] = { 0 };

			i %= MAX_RECORD_STA_PER_VAP;
			entry = &(tab->entry[i]);
			seq_printf(m, LEAVE_TAB_DATA_FORMAT, entry->mac,
				   sk_get_time_str_from_time(&entry->
							     record_time,
							     time_str,
							     sizeof(time_str)),
				   sk_get_frame_type_str(entry->frame_type,
							 frame_type_str,
							 sizeof
							 (frame_type_str)),
				   sk_get_sender_str(entry->sender, sender_str,
						     sizeof(sender_str)),
				   entry->online_time / 60, entry->reason,
				   sk_get_reason_str(entry, reason_str,
						     sizeof(reason_str)));
		}
		seq_printf(m, "\nTotal record szie: %d\n", tab->size);
		spin_unlock(&(tab->leave_lock));
	} else {
		seq_printf(m, "\nTotal record szie: %d\n", 0);
	}
	return 0;
}
#endif

static int proc_hapd_resp_show(struct seq_file *m, void *v)
{
	seq_printf(m, "undefined\n");
	return 0;
}

ssize_t proc_hapd_resp_write(struct file * file, const char *buffer,
			     size_t count, loff_t * ppos)
{
	return count;
}

static int proc_auth_result_show(struct seq_file *m, void *v)
{
	seq_printf(m, "undefined\n");
	return 0;
}

ssize_t proc_auth_result_write(struct file * file, const char *buffer,
			       size_t count, loff_t * ppos)
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,19,0)
	zyUMAC_wal_wdev_t vap =
	    (zyUMAC_wal_wdev_t) PDE_DATA(file->f_dentry->d_inode);
#else
	zyUMAC_wal_wdev_t vap =
	    (zyUMAC_wal_wdev_t) PDE_DATA(file->f_path.dentry->d_inode);
#endif
	unsigned char *mac = (unsigned char *)buffer;
	zyUMAC_wal_wnode_t ni = zyUMAC_wal_wnode_get(vap, mac);
	if (!ni)
		return count;
#ifdef CONFIG_DP_SUPPORT
	notify_dp_sta_authenticated(mac,
		zyUMAC_wal_wdev_get_macaddr(vap),
		mac[6], mac[7], mac[8], (char *)&mac[9]);
#endif
	zyUMAC_wal_wnode_put(ni);
	return count;
}

static int proc_node_count_show(struct seq_file *m, void *v)
{
	zyUMAC_wal_wdev_t vap = (zyUMAC_wal_wdev_t) m->private;
	seq_printf(m, "%u\n", zyUMAC_wal_wdev_get_sta_count(vap));
	return 0;
}

static int proc_is_rx_probreq_show(struct seq_file *m, void *v)
{
	zyUMAC_wal_wdev_t vap = (zyUMAC_wal_wdev_t) m->private;
	if (vap && vap->iv_zyUMAC_vap_priv) {
		zyUMAC_vap_priv_t *tmpPtr =
		    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
		zyUMAC_vap_priv_stats_t *stats = &tmpPtr->is_stats;
		if (!stats) {
			zyUMAC_LOG(LOG_ERR, "Invalid arguments");
			return -1;
		}
		seq_printf(m, "%llu\n", stats->is_rx_probreq);
	} else {
		seq_printf(m, "%d\n", 0);
	}
	return 0;
}

ssize_t proc_is_rx_probreq_write(struct file * file, const char *buffer,
				 size_t count, loff_t * ppos)
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,19,0)
	zyUMAC_wal_wdev_t vap =
	    (zyUMAC_wal_wdev_t) PDE_DATA(file->f_dentry->d_inode);
#else
	zyUMAC_wal_wdev_t vap =
	    (zyUMAC_wal_wdev_t) PDE_DATA(file->f_path.dentry->d_inode);
#endif
	if (vap && vap->iv_zyUMAC_vap_priv) {
		int rc;
		zyUMAC_vap_priv_t *tmpPtr =
		    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
		zyUMAC_vap_priv_stats_t *stats = &tmpPtr->is_stats;

		rc = kstrtouint_from_user(buffer, count, 0,
					  (unsigned int *)&stats->
					  is_rx_probreq);
		if (rc == 0) {
			return count;
		}
	}
	return 0;
}

struct osif_proc_info {
	const char *name;
	struct file_operations *ops;
	int extra;
};

#define PROC_FOO(func_base, extra) { #func_base, &proc_ieee80211_##func_base##_ops, extra }

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
#define GENERATE_PROC_STRUCTS(func_base)                \
    static int proc_##func_base##_open(struct inode *inode, \
                       struct file *file)        \
    {                                \
        struct proc_dir_entry *dp = PDE(inode);            \
        return single_open(file, proc_##func_base##_show, dp->data); \
    }                                \
                                    \
    static struct file_operations proc_ieee80211_##func_base##_ops = { \
        .open        = proc_##func_base##_open,    \
        .read        = seq_read,                \
        .llseek        = seq_lseek,                \
        .release    = single_release,            \
    };
#else
#define GENERATE_PROC_STRUCTS(func_base)                \
    static int proc_##func_base##_open(struct inode *inode, \
                       struct file *file)        \
    {                                \
        return single_open(file, proc_##func_base##_show, PDE_DATA(inode)); \
    } \
                                    \
                                    \
    static struct file_operations proc_ieee80211_##func_base##_ops = { \
        .open        = proc_##func_base##_open,    \
        .read        = seq_read,                \
        .llseek        = seq_lseek,                \
        .release    = single_release,            \
    };
#endif

GENERATE_PROC_STRUCTS(hapd_resp);
GENERATE_PROC_STRUCTS(auth_result);
GENERATE_PROC_STRUCTS(node_count);
#ifdef ZY_LEAVE_REPORT_SUPPORT
GENERATE_PROC_STRUCTS(sta_fail_status);
GENERATE_PROC_STRUCTS(sta_leave_reason);
#endif
GENERATE_PROC_STRUCTS(is_rx_probreq);

const struct osif_proc_info zyUMAC_proc_infos[] = {
	PROC_FOO(hapd_resp, 0),
	PROC_FOO(auth_result, 0),
	PROC_FOO(node_count, 0),
#ifdef ZY_LEAVE_REPORT_SUPPORT
	PROC_FOO(sta_fail_status, 0),
	PROC_FOO(sta_leave_reason, 0),
#endif
	PROC_FOO(is_rx_probreq, 0),
};

#define NUM_zyUMAC_PROC_INFOS (sizeof(zyUMAC_proc_infos) / sizeof(zyUMAC_proc_infos[0]))

void zyUMAC_proc_attach(zyUMAC_wal_wdev_t vap)
{
	zyUMAC_LOG(LOG_INFO, "Create wdev proc entries");

	if (zyUMAC_wal_wdev_get_proc(vap)) {
		int i;
		proc_ieee80211_hapd_resp_ops.write = proc_hapd_resp_write;
		proc_ieee80211_auth_result_ops.write = proc_auth_result_write;
		proc_ieee80211_is_rx_probreq_ops.write =
		    proc_is_rx_probreq_write;
		for (i = 0; i < NUM_zyUMAC_PROC_INFOS; ++i) {
			struct proc_dir_entry *entry;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
			entry = proc_create_data(zyUMAC_proc_infos[i].name,
						 0644,
						 zyUMAC_wal_wdev_get_proc(vap),
						 (const struct file_operations
						  *)zyUMAC_proc_infos[i].ops,
						 (void *)vap);
#else
			entry = create_proc_entry(zyUMAC_proc_infos[i].name,
						  0644,
						  zyUMAC_wal_wdev_get_proc(vap));
#endif
			if (entry == NULL) {
				zyUMAC_LOG(LOG_ERR,
				       "Proc Entry creation failed!");
				return;
			}

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
			entry->data = (void *)vap;
			entry->proc_fops = osif_proc_infos[i].ops;
#endif
		}
	} else {
		zyUMAC_LOG(LOG_ERR, "No Proc dir path found!");
	}
}

void zyUMAC_proc_detach(zyUMAC_wal_wdev_t vap)
{
	zyUMAC_LOG(LOG_INFO, "Remove wdev proc entries");

	if (zyUMAC_wal_wdev_get_proc(vap)) {
		int i;
		for (i = 0; i < NUM_zyUMAC_PROC_INFOS; ++i)
			remove_proc_entry(zyUMAC_proc_infos[i].name,
					  zyUMAC_wal_wdev_get_proc(vap));
	}
}
