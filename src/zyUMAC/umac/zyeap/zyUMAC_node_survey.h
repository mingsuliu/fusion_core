#ifndef __ZYXEL_UMAC_NODE_SURVEY_HEAD__
#define __ZYXEL_UMAC_NODE_SURVEY_HEAD__

#include "zyUMAC_wal_types.h"
#include "zyUMAC_list.h"

#define NODE_SURVEY_MAX_SCAN_SIZE 1024
#define NODE_SURVEY_HASH_SIZE 64
#define NODE_SURVEY_GET_HASH(addr) \
	(((const uint8_t *)(addr))[5] % NODE_SURVEY_HASH_SIZE)

enum zyUMAC_survey_type {
	NODE_TYPE_AP,
	NODE_TYPE_STA
};

typedef struct _zyUMAC_survey_db {
	uint32_t num_entries;
	spinlock_t spinlock;
	zyUMAC_list_t hash_tbl[NODE_SURVEY_HASH_SIZE];
} zyUMAC_survey_db_t;

typedef struct _zyUMAC_survey_scan_entry {
	enum zyUMAC_survey_type type;
	uint8_t bssid[zyUMAC_IEEE80211_ADDR_LEN];
	int rssi;
} zyUMAC_survey_scan_entry_t;

typedef struct _zyUMAC_survey_scan_node {
	int dummy;
	zyUMAC_list_node_t node;
	zyUMAC_survey_scan_entry_t *entry;
} zyUMAC_survey_scan_node_t;

#define zyUMAC_node_survey_list_entry(ptr) \
	zyUMAC_list_entry(ptr, struct _zyUMAC_survey_scan_node, node)

zyUMAC_STATUS
zyUMAC_node_survey_add_scan_entry(zyUMAC_wal_wphy_t wphy,
				 zyUMAC_survey_scan_entry_t *scan_entry);
void zyUMAC_node_survey_dump_scan_entry(zyUMAC_wal_wphy_t wphy);
void zyUMAC_node_survey_flush_scan_entry(zyUMAC_wal_wphy_t wphy);
void zyUMAC_node_survey_init(zyUMAC_wal_wphy_t wphy);
void zyUMAC_node_survey_deinit(zyUMAC_wal_wphy_t wphy);
#endif
