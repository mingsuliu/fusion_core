#ifndef ZYXEL_UMAC_PROC_HEADER_H
#define ZYXEL_UMAC_PROC_HEADER_H

#include "zyUMAC.h"
#include "zyUMAC_common_var.h"

void zyUMAC_proc_attach(zyUMAC_wal_wdev_t vap);
void zyUMAC_proc_detach(zyUMAC_wal_wdev_t vap);

#endif
