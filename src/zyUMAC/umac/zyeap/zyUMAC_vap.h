#ifndef ZYXEL_UMAC_IC_VAP_HEADER_H
#define ZYXEL_UMAC_IC_VAP_HEADER_H

#include "zyUMAC.h"
#include "zyUMAC_wal_types.h"
#include "zyUMAC_common_var.h"

void zyUMAC_vap_init(zyUMAC_wal_wphy_t wdev);

#endif
