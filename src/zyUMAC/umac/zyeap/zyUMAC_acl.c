#include "zyUMAC_wal_api.h"
#include "zyUMAC_vap.h"

static ssize_t zyUMAC_macacl_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct net_device *net = to_net_dev(dev);
	zyUMAC_wal_wdev_t vap = zyUMAC_wal_netdev_to_wdev(net);
	zyUMAC_vap_priv_t *zyUMAC_vap_priv =
	    (zyUMAC_vap_priv_t *) zyUMAC_wal_wdev_getdata(vap);

	return sprintf(buf, "%s\n", zyUMAC_vap_priv->vap_acl_file);
}

static ssize_t zyUMAC_macacl_restore(struct device *dev,
				    struct device_attribute *attr,
				    const char *buf, size_t count)
{
	struct net_device *net = to_net_dev(dev);
	zyUMAC_wal_wdev_t vap = zyUMAC_wal_netdev_to_wdev(net);
	zyUMAC_vap_priv_t *zyUMAC_vap_priv =
	    (zyUMAC_vap_priv_t *) zyUMAC_wal_wdev_getdata(vap);

	if (buf == NULL || count <= 0 || count >= MAX_VAP_ACL_LEN) {
		return -1;
	}

	memcpy(zyUMAC_vap_priv->vap_acl_file, buf, count);
	if (zyUMAC_vap_priv->vap_acl_file[count - 1] == '\n') {
		zyUMAC_vap_priv->vap_acl_file[count - 1] = '\0';
	}

	return count;
}

#ifdef UMAC_SUPPORT_CFG80211
static DEVICE_ATTR(macacl, 0660, zyUMAC_macacl_show,
		   zyUMAC_macacl_restore);
#else
static DEVICE_ATTR(macacl, 0660, zyUMAC_macacl_show,
		   zyUMAC_macacl_restore);
#endif

static struct attribute *zyUMAC_macacl_device_attrs[] = {
	&dev_attr_macacl.attr,
	NULL
};

static struct attribute_group zyUMAC_macacl_attr_group = {
	.attrs = zyUMAC_macacl_device_attrs,
};

int zyUMAC_acl_setpolicy_from_file(zyUMAC_wal_wdev_t vap, int policy,
				  u_int8_t acl_list_id)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv =
	    (zyUMAC_vap_priv_t *) zyUMAC_wal_wdev_getdata(vap);
	struct file *pfile = NULL;
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,2,0)
	mm_segment_t old_fs;
#endif
	struct inode *inode = NULL;
	off_t filesize = 0;
	char *buff = NULL;
	int i = 0;
	char *macptr = NULL;
	int rc;
	char *next;

	ASSERT(zyUMAC_vap_priv != NULL);

	if (strlen(zyUMAC_vap_priv->vap_acl_file) == 0) {
		return 0;
	}

	zyUMAC_LOG(LOG_INFO, "VAP ACL file %s", zyUMAC_vap_priv->vap_acl_file);
	pfile = filp_open(zyUMAC_vap_priv->vap_acl_file, O_RDONLY, 0);
	if (pfile == NULL || IS_ERR(pfile)) {
		//perror(__const char * __s)
		zyUMAC_LOG(LOG_ERR, "openfile error, %ld", PTR_ERR(pfile));
		return -1;
	}
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,19,0)
	inode = file_inode(pfile);
#else
	inode = pfile->f_dentry->d_inode;
#endif

	filesize = inode->i_size;

	buff = (char *)kmalloc(filesize + 1, GFP_KERNEL);
	if (buff == NULL) {
		zyUMAC_LOG(LOG_ERR, "kmalloc error");
		goto fail;
	}

	memset(buff, 0, filesize + 1);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0)
	rc = kernel_read(pfile, 0, buff, filesize);
	if (rc <= 0) {
		goto fail;
	}
#else
	old_fs = get_fs();
	set_fs(KERNEL_DS);
	pfile->f_op->read(pfile, buff, filesize, &(pfile->f_pos));
	set_fs(old_fs);
#endif

	buff[filesize] = '\0';
	if (buff[filesize - 1] == '\n') {
		buff[filesize - 1] = '\0';
	}
	filp_close(pfile, 0);

	next = buff;
	for (i = 0, macptr = strsep(&next, ";"); macptr;
	     macptr = strsep(&next, ";"), i++) {
		if (strlen(macptr) != 17) {	/* Mac address acceptable format 01:02:03:04:05:06 length 17 */
			continue;
		}

		rc = zyUMAC_wal_wdev_acl_add(vap, macptr, acl_list_id);
	}

	kfree(buff);
	return 0;

fail:
	if (pfile != NULL) {
		filp_close(pfile, 0);
	}

	if (buff != NULL) {
		kfree(buff);
	}

	return -1;
}

int zyUMAC_acl_attach(zyUMAC_wal_wdev_t vap)
{
	struct net_device *dev = zyUMAC_wal_wdev_to_netdev(vap);
	zyUMAC_vap_priv_t *zyUMAC_vap_priv =
	    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;

	zyUMAC_vap_priv->vap_acl_file[0] = '\0';
	sysfs_create_group(&dev->dev.kobj, &zyUMAC_macacl_attr_group);
	zyUMAC_LOG(LOG_INFO, "zyUMAC acl attached");
	return 0;
}

int zyUMAC_acl_detach(zyUMAC_wal_wdev_t vap)
{
#ifndef UMAC_SUPPORT_CFG80211
	struct net_device *dev = zyUMAC_wal_wdev_to_netdev(vap);

	sysfs_remove_group(&dev->dev.kobj, &zyUMAC_macacl_attr_group);
#endif
	zyUMAC_LOG(LOG_INFO, "zyUMAC acl detached");
	return 0;
}
