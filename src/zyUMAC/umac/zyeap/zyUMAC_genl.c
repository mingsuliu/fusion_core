#include <linux/version.h>
#include "zyUMAC_genl.h"
#include "zyUMAC_log.h"

#ifdef GENL_SUPPORT
static int _is_zyUMAC_genl_init = 0;

static inline int zyUMAC_genl_echo_doit(struct sk_buff *skb,
				       struct genl_info *info);

static struct genl_multicast_group zyUMAC_genl_mc_groups[] = {
	[zyUMAC_MCGRP_MLME] = {.name = "mlme",},
	[zyUMAC_MCGRP_STA_INFO] = {.name = "sta-info",}
};

struct genl_family zyUMAC_genl_family = {
	.id = GENL_ID_GENERATE,
	.hdrsize = 0,
	.name = ZYXEL_UMAC_GENL_FAM_NAME,
	.version = 1,
	.maxattr = zyUMAC_A_MAX,
};

/* attribute policy */
struct nla_policy zyUMAC_genl_policy[zyUMAC_A_MAX + 1] = {
	[zyUMAC_A_ECHO] = {.type = NLA_NUL_STRING},
};

/* operation definition */
struct genl_ops zyUMAC_genl_ops[] = {
	{
	 .cmd = zyUMAC_C_ECHO,
	 .flags = 0,
	 .policy = zyUMAC_genl_policy,
	 .doit = zyUMAC_genl_echo_doit,
	 .dumpit = NULL,
	 },
};

static int zyUMAC_genl_echo_doit(struct sk_buff *skb, struct genl_info *info)
{
	return 0;
}

int zyUMAC_genl_event_report(void *msg, unsigned short len, unsigned short group,
			    unsigned short cmd, int attrtype)
{
	int size = 0;
	int res = 0;
	struct sk_buff *skb = NULL;
	void *genl_msg;

	if (!msg) {
		zyUMAC_LOG(LOG_ERR, "argument is NULL !");
		return -1;
	}

	/*
	   if(group < 1) {
	   printk("[%s] group error group=%d !\n", __func__,group);
	   return -1;
	   }
	 */
	size = NLMSG_SPACE(len);
	skb = nlmsg_new(size, GFP_KERNEL);
	if (!skb) {
		zyUMAC_LOG(LOG_ERR, "alloc netlink skb fail !");
		//spin_unlock(&multi_lock);
		return -1;
	}

	genl_msg = genlmsg_put(skb, 0,	/* portid */
			       0,	/* seq */
			       &zyUMAC_genl_family, 0,	/* flags */
			       cmd);

	if (genl_msg == NULL) {
		nlmsg_free(skb);
		zyUMAC_LOG(LOG_ERR, "failed in genlmsg_put!");
		return -1;
	}

	res = nla_put(skb, attrtype, len, msg);
	if (res != 0) {
		zyUMAC_LOG(LOG_ERR, "nla_put failed");
		goto err;
	}

	genlmsg_end(skb, genl_msg);
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 13, 0)
	res = genlmsg_multicast(&zyUMAC_genl_family, skb, 0 /* portid */ ,
				group /* the index of the mcast grp */ ,
				GFP_KERNEL);
#else
	res = genlmsg_multicast(skb, 0, zyUMAC_genl_mc_groups[0].id, GFP_KERNEL);
#endif
	return res;		/* Always success, I guess ;-) */

err:
	genlmsg_cancel(skb, genl_msg);
	nlmsg_free(skb);
	return res;
}

int zyUMAC_genl_init(void)
{
	int res;

	if (_is_zyUMAC_genl_init) {
		return 0;
	}
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 13, 0)
	res =
	    genl_register_family_with_ops_groups(&zyUMAC_genl_family,
						 zyUMAC_genl_ops,
						 zyUMAC_genl_mc_groups);
	if (res) {
		zyUMAC_LOG(LOG_ERR,
			"genl_register_family zyUMAC family error!!!");
		return -1;
	}
#else
	res = genl_register_family(&zyUMAC_genl_family);
	if (res != 0) {
		zyUMAC_LOG(LOG_ERR,
			"genl_register_family zyUMAC family error!!!");
		return -1;
	}

	res = genl_register_ops(&zyUMAC_genl_family, zyUMAC_genl_ops);
	if (res != 0) {
		zyUMAC_LOG(LOG_ERR,
			"genl_register_ops zyUMAC family error!!!");
		genl_unregister_family(&zyUMAC_genl_family);
		return -1;
	}

	res = genl_register_mc_group(&zyUMAC_genl_family, zyUMAC_genl_mc_groups);
	if (res != 0) {
		zyUMAC_LOG(LOG_ERR,
			"genl_register_mc_group zyUMAC family error!!!");
		genl_unregister_ops(&zyUMAC_genl_family, zyUMAC_genl_ops);
		genl_unregister_family(&zyUMAC_genl_family);
		return -1;
	}
#endif

	_is_zyUMAC_genl_init = 1;
	return 0;
}

void zyUMAC_genl_exit(void)
{
	if (_is_zyUMAC_genl_init) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 13, 0)
		genl_unregister_ops(&zyUMAC_genl_family, zyUMAC_genl_ops);
		genl_unregister_mc_group(&zyUMAC_genl_family,
					 zyUMAC_genl_mc_groups);
#endif
		genl_unregister_family(&zyUMAC_genl_family);
		_is_zyUMAC_genl_init = 0;
	}
}
#else
int zyUMAC_genl_init(void)
{
	return 0;
}

void zyUMAC_genl_exit(void)
{
}
#endif /* GENL_SUPPORT */
