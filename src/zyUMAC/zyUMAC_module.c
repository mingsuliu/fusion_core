#include <linux/module.h>
#include "zyUMAC_log.h"
#include "zyUMAC_umac_api.h"
#include "zyUMAC_wal_api.h"

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif

static zyUMAC_module_hooks_table _zyUMAC_module_hooks_table = {
	zyUMAC_umac_attach,
	zyUMAC_umac_detach
};

int zyUMAC_init(void)
{
	zyUMAC_LOG(LOG_INFO, "Initializing zyUMAC");

	zyUMAC_wal_init(&_zyUMAC_module_hooks_table);
	zyUMAC_umac_init();
	return 0;
}
EXPORT_SYMBOL(zyUMAC_init);

void zyUMAC_exit(void)
{
	zyUMAC_LOG(LOG_INFO, "zyUMAC exited");

	zyUMAC_umac_exit();
	zyUMAC_wal_exit();
}
EXPORT_SYMBOL(zyUMAC_exit);
