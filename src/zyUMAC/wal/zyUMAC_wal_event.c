#include <linux/module.h>
#include <linux/notifier.h>

#include "zyUMAC_wal_api.h"

static BLOCKING_NOTIFIER_HEAD(zyUMAC_wal_event_listener);

int zyUMAC_wal_event_register_listener(struct notifier_block *nb)
{
	return blocking_notifier_chain_register(&zyUMAC_wal_event_listener, nb);
}

EXPORT_SYMBOL_GPL(zyUMAC_wal_event_register_listener);

int zyUMAC_wal_event_unregister_listener(struct notifier_block *nb)
{
	return blocking_notifier_chain_unregister(&zyUMAC_wal_event_listener,
						  nb);
}

EXPORT_SYMBOL_GPL(zyUMAC_wal_event_unregister_listener);

int zyUMAC_wal_event_publish(unsigned long type, void *event)
{
	return blocking_notifier_call_chain(&zyUMAC_wal_event_listener, type,
					    event);
}

EXPORT_SYMBOL_GPL(zyUMAC_wal_event_publish);
