#ifndef ZYXEL_UMAC_WAL_VAP_HEADER_H
#define ZYXEL_UMAC_WAL_VAP_HEADER_H

void zyUMAC_vap_create_completed(struct wifi_dev *wdev);
void zyUMAC_vap_delete_completed(struct wifi_dev *wdev);
void zyUMAC_vap_init_completed(struct wifi_dev *wdev);
void zyUMAC_vap_deinit_completed(struct wifi_dev *wdev);

void zyUMAC_wal_wnode_create(zyUMAC_wal_wdev_t wdev, unsigned char *mac);
void zyUMAC_wal_wnode_delete(zyUMAC_wal_wdev_t wdev, unsigned char *mac);

extern zyUMAC_wdev_hook_table *_wdev_hook_table;

#endif
