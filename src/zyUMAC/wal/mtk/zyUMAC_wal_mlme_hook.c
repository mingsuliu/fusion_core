#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_types.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_event.h"
#include "zyUMAC_wal_vap.h"
#include "zyUMAC_wal_mgmt_hook.h"

static int (*orig_send_mlme_pkt)(RTMP_ADAPTER *pAd,
				 PNDIS_PACKET pkt,
				 struct wifi_dev *wdev,
				 UCHAR q_idx,
				 BOOLEAN is_data_queue) = NULL;
static int (*orig_mlme_mgmtq_tx)(RTMP_ADAPTER *pAd,
				 struct wifi_dev *wdev,
				 TX_BLK *tx_blk) = NULL;

static int (*orig_rx_pkt_allowed)(RTMP_ADAPTER *pAd,
				  struct wifi_dev *wdev,
				  RX_BLK *pRxBlk) = NULL;

static STATE_MACHINE_FUNC orig_assoc_action = NULL;
static STATE_MACHINE_FUNC orig_reassoc_action = NULL;
static STATE_MACHINE_FUNC orig_disassoc_action = NULL;
static STATE_MACHINE_FUNC orig_disassoc_req_action = NULL;

static void zyUMAC_wal_mlme_associaction(struct wifi_dev *wdev,
					unsigned char *p_addr,
					int reason)
{
	zyUMAC_wal_event_mlme_assoc_ind_t event = {
		.wdev = wdev,
		.macaddr = p_addr,
		.result = reason,
		.reassoc = false
	};

	zyUMAC_wal_event_publish
	    (zyUMAC_WAL_EVENT_MLME_ASSOC_INDICATION, &event);
}

static void
zyUMAC_wal_mlme_disassocaction(struct wifi_dev *wdev,
						u_int8_t *p_addr,
						u_int16_t reason)
{
	zyUMAC_wal_event_mlme_disassoc_ind_t event;
	zyUMAC_wal_wnode_t ni = NULL;

	ni = zyUMAC_wal_wnode_get(wdev, p_addr);
	if (!ni) {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
		return;
	}

	memset(&event, 0, sizeof(zyUMAC_wal_event_mlme_disassoc_ind_t));
	event.wdev = wdev;
	event.macaddr = p_addr;
	event.associd = ni->Aid;
	event.reason = reason;

	zyUMAC_wal_event_publish
	    (zyUMAC_WAL_EVENT_MLME_DISASSOC_INDICATION, &event);
}

static void zyUMAC_wal_mlme_deauth(struct wifi_dev *wdev,
				  unsigned char *p_addr,
				  int reason)
{
	zyUMAC_wal_event_mlme_deauth_ind_t event;
	zyUMAC_wal_wnode_t ni = NULL;

	ni = zyUMAC_wal_wnode_get(wdev, p_addr);
	if (!ni) {
		zyUMAC_LOG(LOG_ERR, "wnode is null");
		return;
	}

	memset(&event, 0, sizeof(zyUMAC_wal_event_mlme_deauth_ind_t));
	event.wdev = wdev;
	event.macaddr = p_addr;
	event.reason = reason;
	event.associd = ni->Aid;

	zyUMAC_wal_event_publish
	    (zyUMAC_WAL_EVENT_MLME_DEAUTH_INDICATION, &event);
}

/* callback from mtk's driver: RtmpOsSendWirelessEvent() */
void zyUMAC_wal_iwevent_transfer(void *pAd,
				unsigned short event_flag,
				unsigned char *pAddr,
				unsigned char wdev_idx,
				char rssi)
{
#if 0
	unsigned short type = event_flag & 0xFF00;
	unsigned short event = event_flag & 0x00FF;
#endif
	struct wifi_dev *wdev = ((RTMP_ADAPTER *)pAd)->wdev_list[wdev_idx];

	/*
	* turn on for debug
	* extern char const *pWirelessSysEventText[IW_SYS_EVENT_TYPE_NUM];
	* printk("%s: %s\n", __func__, pWirelessSysEventText[event]);
	*/
	switch (event_flag) {
	case IW_ASSOC_EVENT_FLAG: /*MLME_SUCCESS*/
		zyUMAC_wal_mlme_associaction(wdev, pAddr,
					    IEEE80211_STATUS_SUCCESS);
		break;
	case IW_STA_MODE_EVENT_FLAG: /*MLME_ASSOC_REJ_DATA_RATE*/
		zyUMAC_wal_mlme_associaction(wdev, pAddr,
					    IEEE80211_STATUS_RATES);
		break;
	case IW_MAC_FILTER_LIST_EVENT_FLAG: /*MLME_UNSPECIFY_FAILURE*/
		zyUMAC_wal_mlme_associaction(wdev, pAddr,
					    IEEE80211_STATUS_UNSPECIFIED);
#if 0 /* Albert: TODO */
	case IW_MAC_TABLE_FULL_FLAG: /*mac table full during auth*/
		zyUMAC_wal_mlme_associaction(wdev, pAddr,
					    IEEE80211_STATUS_TOO_MANY_STATIONS);
		break;
#endif
	case IW_DISASSOC_EVENT_FLAG:
		zyUMAC_wal_mlme_disassocaction(wdev, pAddr, 0);
		break;
	case IW_DEAUTH_EVENT_FLAG:
		zyUMAC_wal_mlme_deauth(wdev, pAddr, 0);
		break;
	default:
		break;
	}
}

void zyUMAC_wal_event_process_custom(struct net_device *p_netdev,
				    int flags,
				    unsigned char *p_data,
				    unsigned int data_len)
{
	zyUMAC_wal_wdev_t wdev;

	if (!p_netdev || !p_data)
		return;

	wdev = zyUMAC_wal_netdev_to_wdev(p_netdev);

	switch (flags) {
	case FWD_CMD_ADD_TX_SRC:
		zyUMAC_wal_wnode_create(wdev, p_data);
		break;
	case FWD_CMD_DEL_TX_SRC:
		zyUMAC_wal_wnode_delete(wdev, p_data);
		break;
	default:
		break;
	}
}

/*
 * Replace mtk's driver RtmpOSWrielessEventSend()
 * Note: different with RtmpOsSendWirelessEvent(), naming sucks
 * Take care of wireless_send_event()
 */
int zyUMAC_wal_wrieless_event_process(struct net_device *p_netdev,
				     unsigned int event_type,
				     int flags,
				     unsigned char *p_mac,
				     unsigned char *p_data,
				     unsigned int data_len)
{
	switch (event_type) {
	case IWEVCUSTOM:
		zyUMAC_wal_event_process_custom(p_netdev,
					       flags,
					       p_data,
					       data_len);
		break;
	default:
		break;
	}

	return 0;
}

int zyUMAC_vap_send_mlme_pkt(RTMP_ADAPTER *pAd,
			    PNDIS_PACKET pkt,
			    struct wifi_dev *wdev,
			    UCHAR q_idx,
			    BOOLEAN is_data_queue)
{
	int ret = -1;

	if (orig_send_mlme_pkt)
		ret = orig_send_mlme_pkt(pAd, pkt, wdev, q_idx, is_data_queue);

	return ret;
}

int zyUMAC_wal_vap_mlme_mgmtq(RTMP_ADAPTER *pAd,
			     struct wifi_dev *wdev,
			     TX_BLK *pTxBlk)
{
	struct _RTMP_CHIP_CAP *cap;
	UINT8 tx_hw_hdr_len;
	PHEADER_802_11 pHeader_802_11;
	FRAME_CONTROL *fc;
	int ret = -1;

	if (orig_mlme_mgmtq_tx)
		ret = orig_mlme_mgmtq_tx(pAd, wdev, pTxBlk);

	cap = hc_get_chip_cap(pAd->hdev_ctrl);
	if (!cap)
		return ret;

	tx_hw_hdr_len = cap->tx_hw_hdr_len;
	pHeader_802_11 =
		(HEADER_802_11 *)(pTxBlk->pSrcBufHeader + tx_hw_hdr_len);
	fc = &pHeader_802_11->FC;

	/* do the out mgmt filter and statistics */
	zyUMAC_wal_mgmt_tx_process(pTxBlk, wdev, fc);

	return ret;
}

static void
zyUMAC_wal_vap_recv_drop_count(struct wifi_dev *wdev, RX_BLK *pRxBlk)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_vap_priv_stats_t *stats_vap;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (!zyUMAC_vap_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_vap_priv is null");
		return;
	}

	stats_vap = &zyUMAC_vap_priv->is_stats;

	stats_vap->is_rx_drop++;
	stats_vap->is_rx_drop_bytes += pRxBlk->MPDUtotalByteCnt;
}

static void
zyUMAC_wal_vap_recv_count(struct wifi_dev *wdev, RX_BLK *pRxBlk)
{
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_vap_priv_stats_t *stats_vap;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	wphy = zyUMAC_wal_wdev_get_wphy(wdev);
	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	zyUMAC_scn_priv = wphy->scn_zyUMAC_priv;
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		return;
	}

	stats = &zyUMAC_scn_priv->scn_radio_stats;

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (!zyUMAC_vap_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_vap_priv is null");
		return;
	}

	stats_vap = &zyUMAC_vap_priv->is_stats;

	stats->rx_data_bytes += pRxBlk->MPDUtotalByteCnt;

	stats_vap->is_rx_packets++;
	stats_vap->is_rx_bytes += pRxBlk->MPDUtotalByteCnt;
	stats_vap->is_rx_data++;
	stats_vap->is_rx_data_bytes += pRxBlk->MPDUtotalByteCnt;

	if (IS_MULTICAST_MAC_ADDR(pRxBlk->Addr3)) {
		stats_vap->is_rx_nunicast++;
		stats_vap->is_rx_nunicast_bytes += pRxBlk->MPDUtotalByteCnt;
	} else if (IS_BROADCAST_MAC_ADDR(pRxBlk->Addr3)) {
		stats_vap->is_rx_bcast++;
		stats_vap->is_rx_bcast_bytes += pRxBlk->MPDUtotalByteCnt;
	} else {
		stats_vap->is_rx_unicast++;
		stats_vap->is_rx_unicast_bytes += pRxBlk->MPDUtotalByteCnt;
	}
}

static void
zyUMAC_wal_sta_recv_drop_count(zyUMAC_wal_wnode_t pEntry, RX_BLK *pRxBlk)
{
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_node_priv_stats_t *node_stats;

	if (!pEntry)
		return;

	zyUMAC_node_priv = zyUMAC_wal_wnode_getdata(pEntry);
	if (!zyUMAC_node_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_node_priv is null");
		return;
	}

	node_stats = &zyUMAC_node_priv->ns_stats;

	if (CLIENT_STATUS_TEST_FLAG(pEntry, fCLIENT_STATUS_WMM_CAPABLE)) {
		UCHAR ac = WMM_UP2AC_MAP[pRxBlk->UserPriority];

		ac = __zyUMAC_wal_get_ac(ac);
		node_stats->an_framediscard[ac]++;
		node_stats->an_bytesdiscard[ac] += pRxBlk->MPDUtotalByteCnt;
	}
}

static void
zyUMAC_wal_sta_recv_count(zyUMAC_wal_wnode_t pEntry, RX_BLK *pRxBlk)
{
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_node_priv_stats_t *node_stats;

	if (!pEntry)
		return;

	zyUMAC_node_priv = zyUMAC_wal_wnode_getdata(pEntry);
	if (!zyUMAC_node_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_node_priv is null");
		return;
	}

	node_stats = &zyUMAC_node_priv->ns_stats;

	if (CLIENT_STATUS_TEST_FLAG(pEntry, fCLIENT_STATUS_WMM_CAPABLE)) {
		UCHAR ac = WMM_UP2AC_MAP[pRxBlk->UserPriority];

		ac = __zyUMAC_wal_get_ac(ac);
		node_stats->an_bytesreceive[ac] += pRxBlk->MPDUtotalByteCnt;
	}
}

int zyUMAC_wal_vap_rx_pkt_allowed(RTMP_ADAPTER *pAd,
				 struct wifi_dev *wdev,
				 RX_BLK *pRxBlk)
{
	MAC_TABLE_ENTRY *pEntry = NULL;
	int ret = -1;

	pEntry = PACInquiry(pAd, pRxBlk->wcid);

	if (orig_rx_pkt_allowed)
		ret = orig_rx_pkt_allowed(pAd, wdev, pRxBlk);

	if (!ret) {
		zyUMAC_wal_vap_recv_drop_count(wdev, pRxBlk);
		zyUMAC_wal_sta_recv_drop_count(pEntry, pRxBlk);
	}

	if (pEntry && IS_ENTRY_CLIENT(pEntry)) {
		zyUMAC_wal_vap_recv_count(wdev, pRxBlk);
		zyUMAC_wal_sta_recv_count(pEntry, pRxBlk);
	}

	return ret;
}

/* add the hook first, we can extend our functions later */
void zyUMAC_wal_assoc_action(RTMP_ADAPTER *p_ad, MLME_QUEUE_ELEM *p_mlme)
{
	if (orig_assoc_action)
		orig_assoc_action(p_ad, p_mlme);
}

void zyUMAC_wal_reassoc_action(RTMP_ADAPTER *p_ad, MLME_QUEUE_ELEM *p_mlme)
{
	if (orig_reassoc_action)
		orig_reassoc_action(p_ad, p_mlme);
}

void zyUMAC_wal_disassoc_action(RTMP_ADAPTER *p_ad, MLME_QUEUE_ELEM *p_mlme)
{
	if (orig_disassoc_action)
		orig_disassoc_action(p_ad, p_mlme);
}

void zyUMAC_wal_disassoc_req_action(RTMP_ADAPTER *p_ad, MLME_QUEUE_ELEM *p_mlme)
{
	if (orig_disassoc_req_action)
		orig_disassoc_req_action(p_ad, p_mlme);
}

/* hook vap's ops */
void zyUMAC_wal_vap_mlme_hook(struct wifi_dev *wdev)
{
	struct wifi_dev_ops *ops = wdev->wdev_ops;
	struct _assoc_api_ops *assoc_api = (struct _assoc_api_ops*)wdev->assoc_api;

	/* hook tx mlme packet process */
	if (ops && ops->send_mlme_pkt) {
		if (orig_send_mlme_pkt == NULL) {
			orig_send_mlme_pkt = ops->send_mlme_pkt;
			ops->send_mlme_pkt = zyUMAC_vap_send_mlme_pkt;
		}
	}

	/* hook tx mgmt */
	if (ops && ops->mlme_mgmtq_tx) {
		if (orig_mlme_mgmtq_tx == NULL) {
			orig_mlme_mgmtq_tx = ops->mlme_mgmtq_tx;
			ops->mlme_mgmtq_tx = zyUMAC_wal_vap_mlme_mgmtq;
		}
	}

	/* hook rx packet allowed function */
	if (ops && ops->rx_pkt_allowed) {
		if (orig_rx_pkt_allowed == NULL) {
			orig_rx_pkt_allowed = ops->rx_pkt_allowed;
			ops->rx_pkt_allowed =
				zyUMAC_wal_vap_rx_pkt_allowed;
		}
	}

	/* hook assoc_api */
	if (orig_assoc_action == NULL) {
		if (assoc_api && assoc_api->peer_assoc_req_action)
			orig_assoc_action = (STATE_MACHINE_FUNC) assoc_api->peer_assoc_req_action;
	}	
	if (orig_reassoc_action == NULL) {
		if (assoc_api && assoc_api->peer_reassoc_req_action)
			orig_reassoc_action = (STATE_MACHINE_FUNC) assoc_api->peer_reassoc_req_action;
	}
	if (orig_disassoc_action == NULL) {
		if (assoc_api && assoc_api->peer_disassoc_action)
			orig_disassoc_action = (STATE_MACHINE_FUNC) assoc_api->peer_disassoc_action;
	}	
	if (orig_disassoc_req_action == NULL) {
		if (assoc_api && assoc_api->mlme_disassoc_req_action)
			orig_disassoc_req_action = (STATE_MACHINE_FUNC) assoc_api->mlme_disassoc_req_action;
	}
}

/* patch for ap_tx_drop_update() */
void zyUMAC_wal_tx_drop(struct wifi_dev *wdev, TX_BLK *pTxBlk)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_vap_priv_stats_t *stats_vap;
	zyUMAC_node_priv_stats_t *stats_node;
	BSS_STRUCT *mbss = NULL;
	MAC_TABLE_ENTRY *pEntry = NULL;

	if (!wdev || !pTxBlk) {
		zyUMAC_LOG(LOG_ERR, "invalid arguments");
		return;
	}

	mbss = pTxBlk->pMbss;
	pEntry = pTxBlk->pMacEntry;

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (!zyUMAC_vap_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_vap_priv is null");
		return;
	}

	stats_vap = &zyUMAC_vap_priv->is_stats;

	if (mbss) {
		stats_vap->is_tx_drop++;
		stats_vap->is_tx_drop_bytes += pTxBlk->SrcBufLen;
	}

	if (pEntry && IS_ENTRY_CLIENT(pEntry)) {
		zyUMAC_node_priv = zyUMAC_wal_wnode_getdata(pEntry);
		if (zyUMAC_node_priv) {
			stats_node = &zyUMAC_node_priv->ns_stats;

			stats_node->tx_bad_pkts++;
			stats_node->tx_bad_bytes
				+= pTxBlk->SrcBufLen;
		} else {
			zyUMAC_LOG(LOG_ERR, "zyUMAC_node_priv is null");
		}
	}

}

/* patch for ap_tx_ok_update() */
void
zyUMAC_wal_tx_done(RTMP_ADAPTER *pAd, struct wifi_dev *wdev, TX_BLK *pTxBlk)
{
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_vap_priv_stats_t *stats_vap;
	zyUMAC_node_priv_stats_t *stats_node;
	MAC_TABLE_ENTRY *pEntry;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	wphy = zyUMAC_wal_wdev_get_wphy(wdev);
	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	zyUMAC_scn_priv = wphy->scn_zyUMAC_priv;
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		return;
	}

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (!zyUMAC_vap_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_vap_priv is null");
		return;
	}

	pEntry = pTxBlk->pMacEntry;

	stats = &zyUMAC_scn_priv->scn_radio_stats;
	stats_vap = &zyUMAC_vap_priv->is_stats;

	stats->tx_data_bytes += pTxBlk->SrcBufLen;

	stats_vap->is_tx_packets++;
	stats_vap->is_tx_bytes += pTxBlk->SrcBufLen;
	stats_vap->is_tx_data++;
	stats_vap->is_tx_data_bytes += pTxBlk->SrcBufLen;

	if (IS_MULTICAST_MAC_ADDR(pTxBlk->pSrcBufHeader)) {
		stats->tx_mcast++;
		stats->tx_mcast_bytes += pTxBlk->SrcBufLen;
	} else if (IS_BROADCAST_MAC_ADDR(pTxBlk->pSrcBufHeader)) {
		stats_vap->is_tx_bcast++;
		stats_vap->is_tx_bcast_bytes += pTxBlk->SrcBufLen;
	} else {
		stats->tx_ucast++;
		stats->tx_ucast_bytes += pTxBlk->SrcBufLen;

		stats_vap->is_tx_unicast++;
		stats_vap->is_tx_unicast_bytes += pTxBlk->SrcBufLen;
	}

	if ((pEntry &&
	     IS_ENTRY_CLIENT(pEntry)) && (pEntry->Sst == SST_ASSOC)) {
		UCHAR pUserPriority, ac;

#if 0
		RTMPGetUserPriority(pAd, pTxBlk->pPacket, wdev,
				    &pUserPriority, &ac);
#endif		
		ac = __zyUMAC_wal_get_ac(ac);
		zyUMAC_node_priv = zyUMAC_wal_wnode_getdata(pEntry);
		if (zyUMAC_node_priv) {
			stats_node = &zyUMAC_node_priv->ns_stats;

			stats_node->an_bytestransmit[ac] += pTxBlk->SrcBufLen;

			stats_node->tx_good_bytes +=
				pTxBlk->SrcBufLen;
			stats_node->tx_good_pkts++;
		} else {
			zyUMAC_LOG(LOG_ERR, "zyUMAC_node_priv is null");
		}
	}

}
