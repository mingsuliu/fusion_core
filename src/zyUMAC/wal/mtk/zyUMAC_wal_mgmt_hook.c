#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_types.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_vap.h"
#include "zyUMAC_node_survey.h"

static int zyUMAC_wal_get_band(RTMP_ADAPTER *pAd, RX_BLK *pRxBlk)
{

	RXD_BASE_STRUCT *rxd_base;

	if (pRxBlk)
		rxd_base = (RXD_BASE_STRUCT *)pRxBlk->rmac_info;
	else
		return 0;

	return HcGetBandByChannel(pAd, rxd_base->RxD1.ChFreq);
}

void zyUMAC_wal_mgmt_tx_process(TX_BLK *pTxBlk,
			       struct wifi_dev *wdev,
			       FRAME_CONTROL *fc)
{
	zyUMAC_wal_wphy_t wphy = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_vap_priv_stats_t *stats_vap;
	zyUMAC_node_priv_stats_t *stats_node = NULL;
	MAC_TABLE_ENTRY *pEntry = NULL;
	ULONG now = 0;
	UINT length = 0;
	PRTMP_ADAPTER pAd;
	struct _RTMP_CHIP_CAP *cap;
	UINT8 tx_hw_hdr_len;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	if (!fc) {
		zyUMAC_LOG(LOG_ERR, "fc is null");
		return;
	}

	wphy = zyUMAC_wal_wdev_get_wphy(wdev);
	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	zyUMAC_scn_priv = wphy->scn_zyUMAC_priv;
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		return;
	}

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (!zyUMAC_vap_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_vap_priv is null");
		return;
	}

	stats = &zyUMAC_scn_priv->scn_radio_stats;
	stats_vap = &zyUMAC_vap_priv->is_stats;

	if (pTxBlk) {
		length = pTxBlk->SrcBufLen;
		pEntry = pTxBlk->pMacEntry;
		if (pEntry) {
			zyUMAC_node_priv = zyUMAC_wal_wnode_getdata(pEntry);
			if (zyUMAC_node_priv)
				stats_node = &zyUMAC_node_priv->ns_stats;
		}
	}

	/* to check the mgmt output filter */
	if (_wdev_hook_table && _wdev_hook_table->wdev_hook_mgmt_output_filter &&
		(fc->SubType == SUBTYPE_DEAUTH || fc->SubType == SUBTYPE_DISASSOC)) {
		pAd = wphy->pAd;
		cap = hc_get_chip_cap(pAd->hdev_ctrl);
		_wdev_hook_table->wdev_hook_mgmt_output_filter((zyUMAC_wal_wnode_t)pTxBlk->pMacEntry, (struct sk_buff
		*)(pTxBlk->pSrcBufHeader + tx_hw_hdr_len), fc->SubType);
	}	

	if (fc->Type == FC_TYPE_MGMT) {
		stats->tx_mgmt++;
		stats_vap->is_tx_mgmt++;
		stats_vap->is_tx_mgmt_bytes += length;

		switch (fc->SubType) {
		case SUBTYPE_PROBE_RSP:
			stats->tx_probresp++;
			break;
		case SUBTYPE_ASSOC_RSP:
			stats->tx_assocresp++;
			stats_vap->is_tx_assoc_resp++;
			if (stats_node) {
				NdisGetSystemUpTime(&now);
				stats_node->assoc_resp_uptime = now;
			}
			break;
		case SUBTYPE_REASSOC_RSP:
			stats->tx_reassocresp++;
			break;
		default:
			break;
		}
	}
}

int zyUMAC_wal_mgmt_rx_process(RX_BLK *pRxBlk,
			       MAC_TABLE_ENTRY *pEntry,
			       struct wifi_dev *wdev,
			       FRAME_CONTROL *fc)
{
	zyUMAC_wal_wphy_t wphy = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_scn_priv_radio_stats_t *stats;
	zyUMAC_vap_priv_stats_t *stats_vap;
	zyUMAC_node_priv_stats_t *stats_node = NULL;
	struct __rx_status rs;/*TODO: redefine struct*/
	int subtype;
	ULONG now = 0;
	int ret = 0;
	PRTMP_ADAPTER pAd;
	RSSI_SAMPLE rssi_sample;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		goto done;
	}

	if (!fc) {
		zyUMAC_LOG(LOG_ERR, "fc is null");
		goto done;
	}

	wphy = zyUMAC_wal_wdev_get_wphy(wdev);
	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		goto done;
	}

	zyUMAC_scn_priv = wphy->scn_zyUMAC_priv;
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null");
		goto done;
	}

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (!zyUMAC_vap_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_vap_priv is null");
		goto done;
	}
	pAd = wphy->pAd;

	stats = &zyUMAC_scn_priv->scn_radio_stats;
	stats_vap = &zyUMAC_vap_priv->is_stats;

	if (pEntry) {
		zyUMAC_node_priv = zyUMAC_wal_wnode_getdata(pEntry);
		if (zyUMAC_node_priv)
			stats_node = &zyUMAC_node_priv->ns_stats;
	}

	rssi_sample.AvgRssi[0] = pRxBlk->rx_signal.raw_rssi[0];
	rssi_sample.AvgRssi[1] = pRxBlk->rx_signal.raw_rssi[1];
	rssi_sample.AvgRssi[2] = pRxBlk->rx_signal.raw_rssi[2];
	rssi_sample.AvgRssi[3] = pRxBlk->rx_signal.raw_rssi[3];
	rs.rs_rssi = RTMPAvgRssi(pAd, &rssi_sample);
	rs.channel = pRxBlk->channel_freq;
	subtype = IEEE80211_RAW_SUBTYPE(fc->SubType); /*use raw format*/
	if (_wdev_hook_table && _wdev_hook_table->wdev_hook_mgmt_input_filter) {
		if (pEntry && pRxBlk->pRxPacket)
		ret = _wdev_hook_table->wdev_hook_mgmt_input_filter(
					(zyUMAC_wal_wnode_t)pEntry,
					(struct sk_buff *)pRxBlk->pRxPacket,
					subtype,
					&rs);
	}

	switch (fc->SubType) {
	case SUBTYPE_ASSOC_REQ:
		stats->rx_assreq++;
		stats_vap->is_rx_assoc++;
		if (stats_node) {
			NdisGetSystemUpTime(&now);
			stats_node->assoc_req_uptime = now;
		}
		break;
	case SUBTYPE_DISASSOC:
		stats->rx_disassoc++;
		break;
	case SUBTYPE_AUTH:
		stats->rx_auth++;
		break;
	case SUBTYPE_DEAUTH:
		stats->rx_deauth++;
		break;
	case SUBTYPE_REASSOC_REQ:
		stats->rx_reassocreq++;
		stats_vap->is_rx_reassoc++;
		break;
	case SUBTYPE_PROBE_REQ:
		stats->rx_probreq++;
		stats_vap->is_rx_probreq++;
		break;
	default:
		break;
	}

	stats_vap->is_rx_mgmt_bytes += pRxBlk->DataSize;
	stats_vap->is_rx_mgmt++;
done:
	return ret;
}

void zyUMAC_wal_mgmt_hook(RTMP_ADAPTER *pAd)
{
#if 0
	RTMP_ARCH_OP *ops = &pAd->archOps;
	/* risk:hook all rx packet includes fw download
	 * hook rx packet process
	 */
	if (ops && ops->rx_pkt_process) {
		if (orig_rx_pkt_process == NULL) {
			orig_rx_pkt_process = ops->rx_pkt_process;
			ops->rx_pkt_process = zyUMAC_rx_pkt_process;
		}
	}
#endif
}

void zyUMAC_wal_node_survey_report(zyUMAC_wal_wphy_t wphy,
				  char *addr,
				  int type,
				  int rssi)
{
	zyUMAC_survey_scan_entry_t *scan_entry;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	scan_entry = kmalloc(sizeof(zyUMAC_survey_scan_entry_t), GFP_KERNEL);
	if (!scan_entry) {
		zyUMAC_LOG(LOG_ERR, "can't allocate scan_entry");
		return;
	}

	memcpy(scan_entry->bssid, addr, zyUMAC_IEEE80211_ADDR_LEN);
	scan_entry->type = type;
	scan_entry->rssi = rssi;

	zyUMAC_node_survey_add_scan_entry(wphy, scan_entry);
}

void zyUMAC_wal_mgmt_rpt_process(RTMP_ADAPTER *pAd, RX_BLK *pRxBlk)
{
	zyUMAC_wal_wphy_t wphy;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	FRAME_CONTROL *fc = NULL;
	int band;
	bool is_report = false;
	int report_type;

	if (!pRxBlk) {
		zyUMAC_LOG(LOG_ERR, "pRxBlk is null");
		return;
	}

	fc = (FRAME_CONTROL *)pRxBlk->FC;
	band = zyUMAC_wal_get_band(pAd, pRxBlk);
	wphy = zyUMAC_wal_wdev_get_wphy_by_id(band);

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null for band %d", band);
		return;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) zyUMAC_wal_wphy_getdata(wphy);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null!");
		return;
	}

	if (IS_BROADCAST_MAC_ADDR(pRxBlk->Addr1)) {
		if (fc->SubType == SUBTYPE_BEACON) {
			if (zyUMAC_NODE_SURVEY_FILTER(SURVEY_FILTER_BEACON) &
			    zyUMAC_scn_priv->node_survey_en) {
				is_report = true;
				report_type = NODE_TYPE_AP;
			}
		} else if (fc->SubType == SUBTYPE_PROBE_REQ) {
			if (zyUMAC_NODE_SURVEY_FILTER(SURVEY_FILTER_PROBE_REQ) &
			    zyUMAC_scn_priv->node_survey_en) {
				is_report = true;
				report_type = NODE_TYPE_STA;
			}
		}

	} else {
		if (fc->SubType == SUBTYPE_ASSOC_REQ ||
		    fc->SubType == SUBTYPE_REASSOC_REQ) {
			if (zyUMAC_NODE_SURVEY_FILTER(SURVEY_FILTER_ASSOC_REQ) &
			    zyUMAC_scn_priv->node_survey_en) {
				is_report = true;
			}
		} else if (fc->SubType == SUBTYPE_AUTH) {
			if (zyUMAC_NODE_SURVEY_FILTER(SURVEY_FILTER_AUTH) &
			    zyUMAC_scn_priv->node_survey_en) {
				if (memcmp(pRxBlk->Addr1,
					   pRxBlk->Addr3,
					   zyUMAC_IEEE80211_ADDR_LEN) == 0)
					is_report = true;
			}

		}
		report_type = NODE_TYPE_STA;
	}

	if (is_report)
		zyUMAC_wal_node_survey_report(wphy,
				report_type ? pRxBlk->Addr2 : pRxBlk->Addr3,
				report_type,
				pRxBlk->rx_signal.raw_rssi[0]);

}

/* patch for dev_rx_mgmt_frm() */
void zyUMAC_wal_dev_rx_mgmt_frm(RTMP_ADAPTER *pAd, RX_BLK *pRxBlk)
{
	FRAME_CONTROL *fc = NULL;
	PNDIS_PACKET rx_packet = NULL;
	struct wifi_dev *wdev = NULL;
	MAC_TABLE_ENTRY *pEntry = NULL;
	int ret = 0;
	int wcid;

	if (!pRxBlk) {
		zyUMAC_LOG(LOG_ERR, "pRxBlk is null");
		return;
	}

	fc = (FRAME_CONTROL *)pRxBlk->FC;
	rx_packet = pRxBlk->pRxPacket;

	wdev = wdev_search_by_address(pAd, pRxBlk->Addr1);

	/*ref: BssInfoArgumentLink()*/
	if (!wdev) {
		 /*i.e. probereq*/
		if (IS_BROADCAST_MAC_ADDR(pRxBlk->Addr3)) {
			int apidx;

			for (apidx = 0; apidx < pAd->ApCfg.BssidNum; apidx++) {
				wdev = &pAd->ApCfg.MBSSID[apidx].wdev;

				if (wdev->if_dev == NULL ||
				!RTMP_OS_NETDEV_STATE_RUNNING(wdev->if_dev)) {
					continue;
				}	
				
				ret = zyUMAC_wal_mgmt_rx_process(
					pRxBlk, NULL, wdev, fc);
			}
			wdev = NULL;
		}

		zyUMAC_wal_mgmt_rpt_process(pAd, pRxBlk);

	} else {
		if (VALID_UCAST_ENTRY_WCID(pAd, pRxBlk->wcid))
			pEntry = &pAd->MacTab.Content[pRxBlk->wcid];
		else {
			if (pRxBlk->Addr2)
				pEntry = MacTableLookup(pAd, pRxBlk->Addr2);

			if (wdev && !pEntry) { /* SUBTYPE_AUTH */
				wcid = wdev->bss_info_argument.bmc_wlan_idx;
				pEntry = &pAd->MacTab.Content[wcid];
			}
		}

		ret = zyUMAC_wal_mgmt_rx_process(
			pRxBlk, pEntry, wdev, fc);
	}

	/*
	 * Note: drop might cause some wrong state
	 */
	if (ret != 0 && rx_packet) /*drop*/
		RELEASE_NDIS_PACKET(pAd, rx_packet, NDIS_STATUS_SUCCESS);
	else /*call original handler*/
		dev_rx_mgmt_frm(pAd, pRxBlk);
}
