#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_mlme_hook.h"
#include "zyUMAC_wal_vap_ioctl.h"

zyUMAC_wdev_hook_table *_wdev_hook_table = NULL;

static int (*orig_wifi_sys_linkup)(struct wifi_dev *wdev,
				   struct _MAC_TABLE_ENTRY *entry);
static int (*orig_wifi_sys_linkdown)(struct wifi_dev *wdev);

static int (*orig_ieee_802_3_data_rx)(RTMP_ADAPTER *pAd,
									struct wifi_dev *wdev,
									RX_BLK *pRxBlk,
									MAC_TABLE_ENTRY *pEntry) = NULL;

static int (*orig_ieee_802_11_data_rx)(RTMP_ADAPTER *pAd,
									struct wifi_dev *wdev,
									RX_BLK *pRxBlk,
									MAC_TABLE_ENTRY *pEntry) = NULL;

int __zyUMAC_wal_wdev_attach(zyUMAC_wal_wphy_t wphy,
			    zyUMAC_wdev_hook_table * wdev_hooks)
{
	_wdev_hook_table = wdev_hooks;
	return 0;
}

void zyUMAC_vap_create_completed(struct wifi_dev *wdev)
{
	struct net_device *dev = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	PRTMP_ADAPTER pAd = NULL;
	MAC_TABLE_ENTRY *pEntry = NULL;
	int wcid;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null!");
		return;
	}

	wdev->iv_proc = NULL;
	dev = wdev->if_dev;

	if (wdev->wdev_type != WDEV_TYPE_AP) {
		return;
	}

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (zyUMAC_vap_priv != NULL)
		return;

	if (dev) {
		zyUMAC_wal_vap_ioctl_attach(dev);
	} else {
		zyUMAC_LOG(LOG_ERR, "netdev is null!");
	}

	if (_wdev_hook_table && _wdev_hook_table->wdev_hook_create) {
		_wdev_hook_table->wdev_hook_create(wdev);
	}

	zyUMAC_wal_vap_mlme_hook(wdev);

	wcid = wdev->bss_info_argument.bmc_wlan_idx;
	pAd = RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev);
	if (pAd)
		pEntry = &pAd->MacTab.Content[wcid];
	if (pEntry)
		pEntry->wdev = wdev;
}

void zyUMAC_vap_delete_completed(struct wifi_dev *wdev)
{
	struct net_device *dev = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null!");
		return;
	}

	if (wdev->wdev_type != WDEV_TYPE_AP) {
		return;
	}

	zyUMAC_vap_priv = zyUMAC_wal_wdev_getdata(wdev);
	if (zyUMAC_vap_priv == NULL)
		return;

	if (_wdev_hook_table && _wdev_hook_table->wdev_hook_delete) {
		_wdev_hook_table->wdev_hook_delete(wdev);
	}

	dev = wdev->if_dev;
	if (dev)
		zyUMAC_wal_vap_ioctl_detach(dev);

	proc_remove(zyUMAC_wal_wdev_get_proc(wdev));
	wdev->iv_proc = NULL;
}

int zyUMAC_wal_vap_recv_802_3_data(RTMP_ADAPTER *pAd,
								struct wifi_dev *wdev,
				                RX_BLK *pRxBlk,
								MAC_TABLE_ENTRY *pEntry)
{
	    int ret = -1;
		struct __rx_status rs;/*TODO: redefine struct*/

		if (likely(_wdev_hook_table && _wdev_hook_table->wdev_hook_rx)) {
			_wdev_hook_table->wdev_hook_rx(wdev, (struct sk_buff *)pRxBlk->pRxPacket, IEEE80211_FC0_TYPE_DATA, 0, &rs);
		}

		if (orig_ieee_802_3_data_rx)
			ret = orig_ieee_802_3_data_rx(pAd, wdev, pRxBlk, pEntry);

		return ret;
}

int zyUMAC_wal_vap_recv_802_11_data(RTMP_ADAPTER *pAd,
								struct wifi_dev *wdev,
								RX_BLK *pRxBlk,
								MAC_TABLE_ENTRY *pEntry)
{
	    int ret = -1;

		if (orig_ieee_802_11_data_rx)
			ret = orig_ieee_802_11_data_rx(pAd, wdev, pRxBlk, pEntry);

		return ret;
}

int zyUMAC_wal_vap_linkup(struct wifi_dev *wdev,
			 struct _MAC_TABLE_ENTRY *entry)
{
	int ret = 0;

	if (orig_wifi_sys_linkup)
		ret = orig_wifi_sys_linkup(wdev, entry);
	zyUMAC_vap_create_completed(wdev);

	return ret;
}

int zyUMAC_wal_vap_linkdown(struct wifi_dev *wdev)
{
	int ret = 0;

	if (orig_wifi_sys_linkdown)
		ret = orig_wifi_sys_linkdown(wdev);
	zyUMAC_vap_delete_completed(wdev);

	return ret;
}

void zyUMAC_vap_init_completed(struct wifi_dev *wdev)
{
	PRTMP_ADAPTER pAd;
	struct wifi_dev_ops *ops = NULL;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null!");
		return;
	}

	if (wdev->wdev_type != WDEV_TYPE_AP)
		return;

	pAd = RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev);
	ops = wdev->wdev_ops;

	if (orig_wifi_sys_linkup == NULL)
		orig_wifi_sys_linkup = ops->linkup;
	if (orig_wifi_sys_linkdown == NULL)
		orig_wifi_sys_linkdown = ops->linkdown;
	
	/* hook rx data fhnctions */
	if (orig_ieee_802_3_data_rx == NULL)
		orig_ieee_802_3_data_rx = ops->ieee_802_3_data_rx;
	if (orig_ieee_802_11_data_rx == NULL)
		orig_ieee_802_11_data_rx = ops->ieee_802_11_data_rx;

	ops->linkup = zyUMAC_wal_vap_linkup;
	ops->linkdown = zyUMAC_wal_vap_linkdown;

	ops->ieee_802_3_data_rx = zyUMAC_wal_vap_recv_802_3_data;
	ops->ieee_802_11_data_rx = zyUMAC_wal_vap_recv_802_11_data;
}

void zyUMAC_vap_deinit_completed(struct wifi_dev *wdev)
{
	/*all vaps share same wifi_dev_ops*/
}

void zyUMAC_wal_wnode_create(zyUMAC_wal_wdev_t wdev, unsigned char *mac)
{
	zyUMAC_wal_wnode_t ni = zyUMAC_wal_wnode_get(wdev, mac);

	_wdev_hook_table->wdev_hook_node_create(ni);
}

void zyUMAC_wal_wnode_delete(zyUMAC_wal_wdev_t wdev, unsigned char *mac)
{
	zyUMAC_wal_wnode_t ni = zyUMAC_wal_wnode_get(wdev, mac);

	_wdev_hook_table->wdev_hook_node_delete(ni);
}
