/*
 * Zyxel Micro UMAC module (zyUMAC).
 */
#include <linux/version.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/wireless.h>
#include <net/iw_handler.h>
#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_types.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_vap.h"
#include "zyUMAC_platform_ioctl.h"
#include "zyUMAC_log.h"
#include "zyUMAC_node_survey.h"

struct iw_handler_def zyUMAC_wphy_iw_handler;
static struct iw_priv_args *zyUMAC_wphy_iw_priv_args = NULL;

static iw_handler_hook _zyUMAC_iw_setparam = NULL;
static iw_handler_hook _zyUMAC_iw_getparam = NULL;

struct iw_priv_args zyUMAC_wphy_privtab[] = {
/* Add for sub-cmd to match iwpriv tool */
	{zyUMAC_IOCTL_SETPARAM, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0,
	 ""},
	{zyUMAC_IOCTL_SETPARAM, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 2, 0,
	 ""},
	{zyUMAC_IOCTL_SETPARAM, IW_PRIV_TYPE_BYTE | IW_PRIV_SIZE_FIXED | 8, 0,
	 ""},
	{zyUMAC_IOCTL_SETPARAM,
	 IW_PRIV_TYPE_BYTE | IW_PRIV_SIZE_FIXED | IEEE80211_ADDR_LEN, 0, ""},
	{zyUMAC_IOCTL_GETPARAM, 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1,
	 ""},
	{zyUMAC_IOCTL_GETPARAM, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, ""},
	{zyUMAC_IOCTL_GETPARAM, 0,
	 IW_PRIV_TYPE_BYTE | IW_PRIV_SIZE_FIXED | IEEE80211_ADDR_LEN, ""},
	{zyUMAC_IOCTL_GETPARAM, 0, IW_PRIV_TYPE_CHAR | IW_PRIV_SIZE_FIXED | 16,
	 ""},
	{zyUMAC_IOCTL_GETPARAM, 0,
	 IW_PRIV_TYPE_CHAR | IW_PRIV_SIZE_FIXED | (IEEE80211_ADDR_LEN * 2), ""},
};

int zyUMAC_wal_wphy_open(struct net_device *dev)
{
	return 0;
}

int zyUMAC_wal_wphy_stop(struct net_device *dev)
{
	return 0;
}

int zyUMAC_wal_wphy_xmit(struct sk_buff *skb, struct net_device *ndev)
{
	if (skb) {
		kfree_skb(skb);
	}
	return NETDEV_TX_OK;
}

static int zyUMAC_wal_wphy_setparam(struct net_device *dev,
				   struct iw_request_info *info,
				   void *w, char *extra)
{
	int res;
	if (_zyUMAC_iw_setparam) {
		res = _zyUMAC_iw_setparam(dev, info, w, extra, NULL);
	}

	return 0;
}

static int zyUMAC_wal_wphy_getparam(struct net_device *dev,
				   struct iw_request_info *info,
				   void *w, char *extra)
{
	int res;
	if (_zyUMAC_iw_getparam) {
		res = _zyUMAC_iw_getparam(dev, info, w, extra, NULL);
	}

	return 0;
}

void __zyUMAC_wal_wphy_set_nodesurvey(zyUMAC_wal_wphy_t wphy)
{
	PRTMP_ADAPTER pAd = wphy->pAd;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	UINT32 value, cr;
	int band;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null!");
		return;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) zyUMAC_wal_wphy_getdata(wphy);
	if (!zyUMAC_scn_priv) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv is null!");
		return;
	}

	band = __zyUMAC_wal_wphy_get_id(wphy);

	if (band)
		cr = RMAC_RFCR_BAND_1;
	else
		cr = RMAC_RFCR_BAND_0;

	RTMP_IO_READ32(pAd, cr, &value);

	if (zyUMAC_NODE_SURVEY_FILTER(SURVEY_FILTER_BEACON) &
	    zyUMAC_scn_priv->node_survey_en) {
		value &= ~DROP_DIFF_BSSID_BCN;
	} else {
		value |= DROP_DIFF_BSSID_BCN;
	}

	if ((zyUMAC_NODE_SURVEY_FILTER(SURVEY_FILTER_ASSOC_REQ) &
	     zyUMAC_scn_priv->node_survey_en) ||
	    (zyUMAC_NODE_SURVEY_FILTER(SURVEY_FILTER_AUTH) &
	     zyUMAC_scn_priv->node_survey_en)) {
		value &= ~DROP_NOT_UC2ME;
	} else {
		value |= DROP_NOT_UC2ME;
	}

	/*note: DROP_PROBE_REQ should not be enabled*/
	RTMP_IO_WRITE32(pAd, cr, value);
}

int zyUMAC_wal_wphy_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd)
{
	int rtn = -1;
	struct iwreq *iwr = (struct iwreq *)ifr;
	struct iw_request_info info = {.cmd = cmd,.flags = 0 };

	switch (cmd) {
	case zyUMAC_IOCTL_GETPARAM:
		{
			rtn =
			    zyUMAC_wal_wphy_getparam(dev, &info,
						    &(iwr->u.data.pointer),
						    (char *)&(iwr->u.data.
							      pointer));
			break;
		}
	case zyUMAC_IOCTL_SETPARAM:
		{
			rtn =
			    zyUMAC_wal_wphy_setparam(dev, &info,
						    &(iwr->u.data.pointer),
						    (char *)&(iwr->u.data.
							      pointer));
			break;
		}
	}

	return rtn;
}

/*
 * Replace all necessary ic function pointer here, call from __ol_ath_attach of qca_ol module
 */
int __zyUMAC_wal_wphy_ioctl_attach(zyUMAC_wal_wphy_t wphy,
				  zyUMAC_wal_ioctl_hook_table * ioctl_hook_table)
{
	struct net_device *dev = NULL;
#ifdef CONFIG_WEXT_PRIV
	int wphy_iw_priv_args_size =
	    sizeof(zyUMAC_wphy_privtab) / sizeof(struct iw_priv_args);
#endif

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null!");
		goto error;
	}

	dev = wphy->dev;

	if (!dev) {
		zyUMAC_LOG(LOG_ERR, "netdev is null!");
		goto error;
	}

	_zyUMAC_iw_setparam = ioctl_hook_table->zyUMAC_param_set_handler;
	_zyUMAC_iw_getparam = ioctl_hook_table->zyUMAC_param_get_handler;

#ifdef CONFIG_WEXT_PRIV
	zyUMAC_wphy_iw_handler.private_args = zyUMAC_wphy_iw_priv_args;
	zyUMAC_wphy_iw_handler.num_private_args =
	    wphy_iw_priv_args_size + ioctl_hook_table->zyUMAC_params_size;
	zyUMAC_wphy_iw_priv_args =
	    kmalloc(zyUMAC_wphy_iw_handler.num_private_args *
		    sizeof(struct iw_priv_args), GFP_KERNEL);

	memcpy(zyUMAC_wphy_iw_priv_args,
	       zyUMAC_wphy_privtab,
	       wphy_iw_priv_args_size * sizeof(struct iw_priv_args));
	memcpy(zyUMAC_wphy_iw_priv_args + wphy_iw_priv_args_size,
	       ioctl_hook_table->zyUMAC_params,
	       ioctl_hook_table->zyUMAC_params_size *
	       sizeof(struct iw_priv_args));
#endif

	dev->wireless_handlers =
	    (struct iw_handler_def *)&zyUMAC_wphy_iw_handler;

	return 0;

error:
	return -1;
}

void __zyUMAC_wal_wphy_ioctl_detach(zyUMAC_wal_wphy_t wphy)
{
	if (zyUMAC_wphy_iw_priv_args) {
		kfree(zyUMAC_wphy_iw_priv_args);
		zyUMAC_wphy_iw_priv_args = NULL;
	}
}
