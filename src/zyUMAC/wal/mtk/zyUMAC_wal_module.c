#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/rtnetlink.h>
#include "zyUMAC_umac.h"
#include "zyUMAC_wal_mgmt_hook.h"
#include "zyUMAC_log.h"

extern zyUMAC_module_hooks_table *_zyUMAC_module_hooks;
extern int zyUMAC_wal_wphy_ioctl(struct net_device *dev, struct ifreq *ifr,
				int cmd);
extern int zyUMAC_wal_wphy_open(struct net_device *dev);
extern int zyUMAC_wal_wphy_stop(struct net_device *dev);
extern int zyUMAC_wal_wphy_xmit(struct sk_buff *skb, struct net_device *ndev);

//virtual radios
zyUMAC_wal_wphy_t g_zyUMAC_wphy;
int g_num_radios;

static struct net_device_ops radio_dev_ops;

static void zyUMAC_wal_wphy_create(RTMP_ADAPTER * pAd, int num_radios)
{
	int i;
	zyUMAC_wal_wphy_t wphy;
	char name[IFNAMSIZ];
	int islocked;

	g_num_radios = num_radios;

	if (!g_zyUMAC_wphy) {
		zyUMAC_LOG(LOG_ERR, "global virtual wphy is null!");
		return;
	}

	for (i = 0; i < num_radios; i++) {
		wphy = (g_zyUMAC_wphy + i);

		wphy->pAd = pAd;
		wphy->scn_zyUMAC_priv = NULL;
		wphy->wdev_count = 0;
		wphy->band_idx = i;	//BAND_IDX_2G, BAND_IDX_5G, ...
		snprintf(name, IFNAMSIZ, WPHY_NAME"%d", i);
		strncpy(wphy->name, name, IFNAMSIZ);
		wphy->proc = proc_mkdir(name, NULL);

		if (wphy->dev) {
			zyUMAC_LOG(LOG_WARN, "netdev of wphy already allocated");
			continue;
		}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,19,0)
		wphy->dev = alloc_netdev(0, name, 0, ether_setup);
#else
		wphy->dev = alloc_netdev(0, name, ether_setup);
#endif

		wphy->dev->netdev_ops = &radio_dev_ops;
		radio_dev_ops.ndo_do_ioctl = zyUMAC_wal_wphy_ioctl;
		radio_dev_ops.ndo_open = zyUMAC_wal_wphy_open;
		radio_dev_ops.ndo_stop = zyUMAC_wal_wphy_stop;
		radio_dev_ops.ndo_start_xmit = zyUMAC_wal_wphy_xmit;

		islocked = rtnl_is_locked();

		if (!islocked)
			rtnl_lock();

		register_netdevice(wphy->dev);

		if (!islocked)
			rtnl_unlock();
	}
}

void zyUMAC_wal_wphy_delete(void)
{
	zyUMAC_wal_wphy_t wphy = NULL;
	int i;
	int islocked;

	if (!g_zyUMAC_wphy) {
		zyUMAC_LOG(LOG_ERR, "global virtual wphy is null!");
		return;
	}

	for (i = 0; i < g_num_radios; i++) {
		wphy = (g_zyUMAC_wphy + i);

		_zyUMAC_module_hooks->module_hook_detach(wphy);

		if (wphy->dev &&
		    wphy->dev->reg_state == NETREG_REGISTERED) {

			islocked = rtnl_is_locked();
			if (!islocked)
				rtnl_lock();

			unregister_netdevice(wphy->dev);

			if (!islocked)
				rtnl_unlock();

			free_netdev(wphy->dev);
		}
	}
}

zyUMAC_wal_wphy_t __zyUMAC_wal_wdev_get_wphy_by_id(int band)
{
	if (!g_zyUMAC_wphy || band >= BAND_IDX_MAX)
		return NULL;

	return (g_zyUMAC_wphy + band);
}

zyUMAC_wal_wphy_t __zyUMAC_wal_wdev_get_wphy(zyUMAC_wal_wdev_t wdev)
{
	int band_idx;

	if (!g_zyUMAC_wphy) {
		zyUMAC_LOG(LOG_ERR, "global virtual wphy is null!");
		return NULL;
	}

	if (WMODE_CAP_2G(wdev->PhyMode)) {
		band_idx = BAND_IDX_2G;
	} else if (WMODE_CAP_5G(wdev->PhyMode)) {
		band_idx = BAND_IDX_5G;
	} else {
		return NULL;
	}

	return (g_zyUMAC_wphy + band_idx);
}

zyUMAC_wal_wphy_t __zyUMAC_wal_netdev_to_wphy(struct net_device * ndev)
{
	zyUMAC_wal_wphy_t wphy = NULL;
	int i;

	if (!g_zyUMAC_wphy) {
		zyUMAC_LOG(LOG_ERR, "global virtual wphy is null!");
		return NULL;
	}

	for (i = 0; i < g_num_radios; i++) {
		zyUMAC_wal_wphy_t t = (g_zyUMAC_wphy + i);
		if (t->dev->ifindex == ndev->ifindex) {
			wphy = t;
			break;
		}
	}
	return wphy;
}

int zyUMAC_wal_attach(RTMP_ADAPTER * pAd, int num_radios)
{
	int ret = -1, i;

	zyUMAC_LOG(LOG_INFO, "Attaching Zyxel WAL");

	g_zyUMAC_wphy =
	    kmalloc(num_radios * sizeof(__zyUMAC_wal_mt_radio_dev_t),
		    GFP_KERNEL);
	if (!g_zyUMAC_wphy) {
		zyUMAC_LOG(LOG_ERR, "can't allocate phy devices!");
		return -1;
	}
	memset(g_zyUMAC_wphy, 0,
	       num_radios * sizeof(__zyUMAC_wal_mt_radio_dev_t));

	zyUMAC_wal_wphy_create(pAd, num_radios);

	for (i = 0; i < num_radios; i++) {
		zyUMAC_wal_wphy_t wphy = (g_zyUMAC_wphy + i);
		if (wphy) {
			ret = _zyUMAC_module_hooks->module_hook_attach(wphy);	//zyUMAC_umac_attach
		} else {
			ret = -1;
			zyUMAC_LOG(LOG_ERR, "can't hook phy device!");
		}
	}

	zyUMAC_wal_mgmt_hook(pAd);

	return ret;
}

void zyUMAC_wal_detach(RTMP_ADAPTER * pAd)
{
	zyUMAC_LOG(LOG_INFO, "Detaching Zyxel WAL");
	zyUMAC_wal_wphy_delete();
	if (g_zyUMAC_wphy) {
		kfree(g_zyUMAC_wphy);
	}
}

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif
EXPORT_SYMBOL(zyUMAC_wal_attach);
EXPORT_SYMBOL(zyUMAC_wal_detach);
