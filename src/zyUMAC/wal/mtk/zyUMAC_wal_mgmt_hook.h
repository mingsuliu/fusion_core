#ifndef ZYXEL_UMAC_WAL_MGMT_HOOK_HEADER_H
#define ZYXEL_UMAC_WAL_MGMT_HOOK_HEADER_H

void zyUMAC_wal_mgmt_hook(RTMP_ADAPTER *pAd);
void zyUMAC_wal_mgmt_tx_process(TX_BLK *pTxBlk,
			       struct wifi_dev *wdev,
			       FRAME_CONTROL *fc);
void zyUMAC_wal_mgmt_rx_process(RX_BLK *pRxBlk,
			       MAC_TABLE_ENTRY *pEntry,
			       FRAME_CONTROL *fc);
#endif
