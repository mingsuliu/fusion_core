#include "zyUMAC_common_var.h"
#include "zyUMAC_platform_ioctl.h"
#include "zyUMAC_ioctl.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_vap.h"

static struct iw_priv_args *zyUMAC_vap_iw_priv_args = NULL;
static iw_handler_hook _zyUMAC_ieee80211_setparam = NULL;
static iw_handler_hook _zyUMAC_ieee80211_getparam = NULL;

/* replace ndo_do_ioctl */
static int (*orig_ra_ioctl) (struct net_device * dev, struct ifreq * ifr,
			     int cmd) = NULL;

/* command are copied from ap_privtab[]
 * TODO: use mt_wifi's table by CONFIG_APSTA_MIXED_SUPPORT but it sucks
 */
struct iw_priv_args zyUMAC_ap_privtab[] = {
/* Add for sub-cmd to match iwpriv tool */
	{zyUMAC_IOCTL_SETPARAM, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, 0,
	 ""},
	{zyUMAC_IOCTL_SETPARAM, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 2, 0,
	 ""},
	{zyUMAC_IOCTL_SETPARAM, IW_PRIV_TYPE_BYTE | IW_PRIV_SIZE_FIXED | 8, 0,
	 ""},
	{zyUMAC_IOCTL_SETPARAM,
	 IW_PRIV_TYPE_BYTE | IW_PRIV_SIZE_FIXED | IEEE80211_ADDR_LEN, 0, ""},
	{zyUMAC_IOCTL_GETPARAM, 0, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1,
	 ""},
	{zyUMAC_IOCTL_GETPARAM, IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1,
	 IW_PRIV_TYPE_INT | IW_PRIV_SIZE_FIXED | 1, ""},
	{zyUMAC_IOCTL_GETPARAM, 0,
	 IW_PRIV_TYPE_BYTE | IW_PRIV_SIZE_FIXED | IEEE80211_ADDR_LEN, ""},
	{zyUMAC_IOCTL_GETPARAM, 0, IW_PRIV_TYPE_CHAR | IW_PRIV_SIZE_FIXED | 16,
	 ""},
	{zyUMAC_IOCTL_GETPARAM, 0,
	 IW_PRIV_TYPE_CHAR | IW_PRIV_SIZE_FIXED | (IEEE80211_ADDR_LEN * 2), ""},

/* MTK's private ioctl */
	{RTPRIV_IOCTL_SET, IW_PRIV_TYPE_CHAR | 1536, 0,
	 "set"},
	{RTPRIV_IOCTL_SHOW, IW_PRIV_TYPE_CHAR | 1024, 0,
	 "show"},
	{RTPRIV_IOCTL_GSITESURVEY, IW_PRIV_TYPE_CHAR | 1024,
	 IW_PRIV_TYPE_CHAR | 1024,
	 "get_site_survey"},
	{RTPRIV_IOCTL_SET_WSCOOB, IW_PRIV_TYPE_CHAR | 1024,
	 IW_PRIV_TYPE_CHAR | 1024,
	 "set_wsc_oob"},
	{RTPRIV_IOCTL_GET_MAC_TABLE, IW_PRIV_TYPE_CHAR | 1024,
	 IW_PRIV_TYPE_CHAR | 1024,
	 "get_mac_table"},
	{RTPRIV_IOCTL_E2P, IW_PRIV_TYPE_CHAR | 1024, IW_PRIV_TYPE_CHAR | 1024,
	 "e2p"},
#if defined(DBG) ||(defined(BB_SOC)&&defined(CONFIG_ATE))
	{RTPRIV_IOCTL_BBP, IW_PRIV_TYPE_CHAR | 1024, IW_PRIV_TYPE_CHAR | 1024,
	 "bbp"},
	{RTPRIV_IOCTL_MAC, IW_PRIV_TYPE_CHAR | 1024, IW_PRIV_TYPE_CHAR | 1024,
	 "mac"},
#ifdef RTMP_RF_RW_SUPPORT
	{RTPRIV_IOCTL_RF, IW_PRIV_TYPE_CHAR | 1024, IW_PRIV_TYPE_CHAR | 1024,
	 "rf"},
#endif /* RTMP_RF_RW_SUPPORT */
#endif /* defined(DBG) ||(defined(BB_SOC)&&defined(CONFIG_ATE)) */
#ifdef WSC_AP_SUPPORT
	{RTPRIV_IOCTL_WSC_PROFILE, IW_PRIV_TYPE_CHAR | 1024,
	 IW_PRIV_TYPE_CHAR | 1024,
	 "get_wsc_profile"},
#endif /* WSC_AP_SUPPORT */
	{RTPRIV_IOCTL_QUERY_BATABLE, IW_PRIV_TYPE_CHAR | 1024,
	 IW_PRIV_TYPE_CHAR | 1024,
	 "get_ba_table"},
	{RTPRIV_IOCTL_STATISTICS, IW_PRIV_TYPE_CHAR | 1024,
	 IW_PRIV_TYPE_CHAR | 1024,
	 "stat"},
	{RTPRIV_IOCTL_RX_STATISTICS, IW_PRIV_TYPE_CHAR | 1024,
	 IW_PRIV_TYPE_CHAR | 1024,
	 "rx"}
};

static int zyUMAC_iw_ioctl_txpow_set(struct net_device *dev,
				    struct iw_request_info *info,
				    struct iw_param *rrq, char *extra)
{
	UCHAR band;
	CHANNEL_CTRL *pChCtrl;
	zyUMAC_wal_wdev_t wdev = __zyUMAC_wal_netdev_to_wdev(dev);
	PRTMP_ADAPTER pAd = RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev);
	int ret = -1;

	if (pAd) {
		pAd->MaxTxPwr = rrq->value;
		band = HcGetBandByWdev(wdev);
		pChCtrl = hc_get_channel_ctrl(pAd->hdev_ctrl, band);
		if (pChCtrl) {
			ret =
			    hc_set_ChCtrlChListStat(pChCtrl,
						    CH_LIST_STATE_NONE);
			BuildChannelList(pAd, wdev);
		}
	}
	return ret;
}

static int zyUMAC_iw_ioctl_freq_get(struct net_device *dev,
				   struct iw_request_info *info,
				   struct iw_freq *freq, char *extra)
{
	zyUMAC_wal_wdev_t wdev = __zyUMAC_wal_netdev_to_wdev(dev);

	freq->m = wdev->channel;
	freq->e = 0;
	freq->i = 0;

	return 0;
}

static int zyUMAC_iw_ioctl_freq_set(struct net_device *dev,
				   struct iw_request_info *info,
				   struct iw_freq *freq, char *extra)
{
	zyUMAC_wal_wdev_t wdev = __zyUMAC_wal_netdev_to_wdev(dev);
	int ret;
	//TODO: DFS

	if (freq->e > 0) {
		return -EINVAL;
	}
	//TODO: support frequency
	ret =
	    rtmp_set_channel(RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev), wdev,
			     freq->m);

	return 0;
}

static int zyUMAC_iw_ioctl_range_get(struct net_device *dev,
				    struct iw_request_info *info,
				    struct iw_point *data, char *extra)
{
	struct iw_range *range = (struct iw_range *)extra;

	memset(range, 0, sizeof(struct iw_range));
	range->we_version_compiled = WIRELESS_EXT;
	range->we_version_source = 14;
	range->max_qual.qual = 100;
	range->max_qual.level = 0;
	range->max_qual.noise = 0;

	return 0;
}

static const iw_handler zyUMAC_iw_handlers_stand[] = {
	NULL,			//SIOCSIWCOMMIT   0x8B00          /* Commit pending changes to driver */
	NULL,			//SIOCGIWNAME     0x8B01          /* get name == wireless protocol */
	NULL,			//SIOCSIWNWID     0x8B02          /* set network id (pre-802.11) */
	NULL,			//SIOCGIWNWID     0x8B03          /* get network id (the cell) */
	(iw_handler) zyUMAC_iw_ioctl_freq_set,	//SIOCSIWFREQ     0x8B04          /* set channel/frequency (Hz) */
	(iw_handler) zyUMAC_iw_ioctl_freq_get,	//SIOCGIWFREQ     0x8B05          /* get channel/frequency (Hz) */
	NULL,			//SIOCSIWMODE     0x8B06          /* set operation mode */
	NULL,			//SIOCGIWMODE     0x8B07          /* get operation mode */
	NULL,			//SIOCSIWSENS     0x8B08          /* set sensitivity (dBm) */
	NULL,			//SIOCGIWSENS     0x8B09          /* get sensitivity (dBm) */
	NULL,			//SIOCSIWRANGE    0x8B0A          /* Unused */
	(iw_handler) zyUMAC_iw_ioctl_range_get,	//SIOCGIWRANGE    0x8B0B          /* Get range of parameters */
	NULL,			//SIOCSIWPRIV     0x8B0C          /* Unused */
	NULL,			//SIOCGIWPRIV     0x8B0D          /* get private ioctl interface info */
	NULL,			//SIOCSIWSTATS    0x8B0E          /* Unused */
	NULL,			//SIOCGIWSTATS    0x8B0F          /* Get /proc/net/wireless stats */
	NULL,			//SIOCSIWSPY      0x8B10          /* set spy addresses */
	NULL,			//SIOCGIWSPY      0x8B11          /* get spy info (quality of link) */
	NULL,			//SIOCSIWTHRSPY   0x8B12          /* set spy threshold (spy event) */
	NULL,			//SIOCGIWTHRSPY   0x8B13          /* get spy threshold */
	NULL,			//SIOCSIWAP       0x8B14          /* set access point MAC addresses */
	NULL,			//SIOCGIWAP       0x8B15          /* get access point MAC addresses */
	NULL,			//                0x8B16
	NULL,			//SIOCGIWAPLIST   0x8B17          /* Deprecated in favor of scanning */
	NULL,			//SIOCSIWSCAN     0x8B18          /* trigger scanning (list cells) */
	NULL,			//SIOCGIWSCAN     0x8B19          /* get scanning results */
	NULL,			//SIOCSIWESSID    0x8B1A          /* set ESSID (network name) */
	NULL,			//SIOCGIWESSID    0x8B1B          /* get ESSID */
	NULL,			//SIOCSIWNICKN    0x8B1C          /* set node name/nickname */
	NULL,			//SIOCGIWNICKN    0x8B1D          /* get node name/nickname */
	NULL,			//                0x8B1E
	NULL,			//                0x8B1F
	NULL,			//SIOCSIWRATE     0x8B20          /* set default bit rate (bps) */
	NULL,			//SIOCGIWRATE     0x8B21          /* get default bit rate (bps) */
	NULL,			//SIOCSIWRTS      0x8B22          /* set RTS/CTS threshold (bytes) */
	NULL,			//SIOCGIWRTS      0x8B23          /* get RTS/CTS threshold (bytes) */
	NULL,			//SIOCSIWFRAG     0x8B24          /* set fragmentation thr (bytes) */
	NULL,			//SIOCGIWFRAG     0x8B25          /* get fragmentation thr (bytes) */
	(iw_handler) zyUMAC_iw_ioctl_txpow_set,	//SIOCSIWTXPOW    0x8B26          /* set transmit power (dBm) */
	NULL,			//SIOCGIWTXPOW    0x8B27          /* get transmit power (dBm) */
	NULL,			//SIOCSIWRETRY    0x8B28          /* set retry limits and lifetime */
	NULL,			//SIOCGIWRETRY    0x8B29          /* get retry limits and lifetime */
};

static int zyUMAC_ieee80211_setparam(struct net_device *dev,
				    struct iw_request_info *info,
				    void *w, char *extra)
{
	int ret = -1;

	if (_zyUMAC_ieee80211_setparam) {
		ret = _zyUMAC_ieee80211_setparam(dev, info, w, extra, NULL);
	}
	return 0;
}

static int zyUMAC_ieee80211_getparam(struct net_device *dev,
				    struct iw_request_info *info,
				    void *w, char *extra)
{
	int ret = -1;

	if (_zyUMAC_ieee80211_getparam) {
		ret = _zyUMAC_ieee80211_getparam(dev, info, w, extra, NULL);
	}

	return 0;
}

const struct iw_handler_def zyUMAC_iw_handler_dummy = {
#define N(a)    (sizeof (a) / sizeof (a[0]))
	.standard = (iw_handler *) zyUMAC_iw_handlers_stand,
	.num_standard = N(zyUMAC_iw_handlers_stand),
#ifdef CONFIG_WEXT_PRIV
	.private = NULL,
	.num_private = 0,
	.private_args = (struct iw_priv_args *)zyUMAC_ap_privtab,
	.num_private_args = N(zyUMAC_ap_privtab),
#endif	
	//.get_wireless_stats = rt28xx_get_wireless_stats,
#undef N
};

int zyUMAC_ra_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd)
{
	int rtn = -1;
	struct iwreq *iwr = (struct iwreq *)ifr;
	struct iw_request_info info = {.cmd = cmd,.flags = 0 };

	if (orig_ra_ioctl) {
		rtn = orig_ra_ioctl(dev, ifr, cmd);
	}

	switch (cmd) {
	/* adapt application wireless, convert bps to Kbps */
	case SIOCGIWRATE:
		iwr->u.bitrate.value /= 1000;
		break;
	case SIOCDEVPRIVATE:		/* 0x89F0 */
	case zyUMAC_IOCTL_STATS:		/* SIOCDEVPRIVATE+2*/
	case zyUMAC_IOCTL_STA_INFO:	/* SIOCDEVPRIVATE+6*/
	case SIOCGIWPRIV:		/* 0x8B0D */
	case SIOCSIWESSID:		/* 0x8B1A */
	case SIOCGIWESSID:		/* 0x8B1B */
	case SIOCIWFIRSTPRIV:		/* 0x8BE0 */
	case RTPRIV_IOCTL_SET:		/* SIOCIWFIRSTPRIV+0x02 */
	case RTPRIV_IOCTL_SHOW:		/* SIOCIWFIRSTPRIV+0x11 */
	case RTPRIV_IOCTL_STATISTICS:	/* SIOCIWFIRSTPRIV+0x09 */
		break;
	case zyUMAC_IOCTL_GETPARAM:	/* SIOCIWFIRSTPRIV+0x01 */
		rtn =
		    zyUMAC_ieee80211_getparam(dev, &info, &(iwr->u),
					     (char *)&(iwr->u));
		break;
	case zyUMAC_IOCTL_SETPARAM:	/* SIOCIWFIRSTPRIV+0x06 */
		rtn =
		    zyUMAC_ieee80211_setparam(dev, &info, &(iwr->u),
					     (char *)&(iwr->u));
		break;
	}

	return rtn;
}

void zyUMAC_wal_vap_ioctl_attach(struct net_device *dev)
{
	if (!dev) {
		zyUMAC_LOG(LOG_ERR, "netdev is null");
		return;
	}

	if (dev->netdev_ops && orig_ra_ioctl == NULL) {
		orig_ra_ioctl = dev->netdev_ops->ndo_do_ioctl;
	}

	/* overwrite original ioctl */
	((struct net_device_ops *)(dev->netdev_ops))->ndo_do_ioctl =
	    zyUMAC_ra_ioctl;

	/* append wireless handler */
	dev->wireless_handlers =
	    (struct iw_handler_def *)&zyUMAC_iw_handler_dummy;
}

void zyUMAC_wal_vap_ioctl_detach(struct net_device *dev)
{
	if (!dev) {
		zyUMAC_LOG(LOG_ERR, "netdev is null");
		return;
	}

	if (dev->netdev_ops && orig_ra_ioctl != NULL)
		((struct net_device_ops *)(dev->netdev_ops))->ndo_do_ioctl =
			orig_ra_ioctl;

	dev->wireless_handlers = NULL;
}

int __zyUMAC_wal_RTMP_AP_IoctlHandle(void *pAdSrc,
				    RTMP_IOCTL_INPUT_STRUCT * wrq,
				    int cmd,
				    unsigned short subcmd,
				    void *pData, unsigned long Data, int status)
{
	switch (cmd) {
	case CMD_RTPRIV_IOCTL_MAIN_OPEN:	//1st vap
		//CMD_RTPRIV_IOCTL_MAIN_OPEN is called, then CMD_RTPRIV_IOCTL_VIRTUAL_INF_UP
		//wdev = &pAd->ApCfg.MBSSID[MAIN_MBSSID].wdev;

		//zyUMAC_vap_create_completed(wdev);
		/* deprecated in driver v5.0 */
		/* TODO: consider cfg80211 may use again */
		break;
	case CMD_RTPRIV_IOCTL_MBSS_OPEN:	//2nd and later vap
		/* deprecated in driver v5.0 */
		break;
	case CMD_RTPRIV_IOCTL_MBSS_CLOSE:	//2nd and later vap
		break;
	case CMD_RTPRIV_IOCTL_MBSS_INIT:	//1st vap init
		break;
	case CMD_RTPRIV_IOCTL_MBSS_REMOVE:	//rmmod
		break;
	}

	return status;
}

int __zyUMAC_wal_RTMP_COM_IoctlHandle(void *pAdSrc,
				     RTMP_IOCTL_INPUT_STRUCT * wrq,
				     int cmd,
				     unsigned short subcmd,
				     void *pData,
				     unsigned long Data, int status)
{
	RT_CMD_INF_UP_DOWN *pInfConf = (RT_CMD_INF_UP_DOWN *) pData;
	//don't use get_wdev_by_ioctl_idx_and_iftype() here, BUG there!

	switch (cmd) {
	case CMD_RTPRIV_IOCTL_VIRTUAL_INF_UP:	//VIRTUAL_IF_UP
	case CMD_RTPRIV_IOCTL_VIRTUAL_INF_DOWN:	//VIRTUAL_IF_DOWN
		break;
	case CMD_RTPRIV_IOCTL_VIRTUAL_INF_INIT:
		zyUMAC_vap_init_completed(RTMP_OS_NETDEV_GET_WDEV
					 (pInfConf->operation_dev_p));
		break;
	case CMD_RTPRIV_IOCTL_VIRTUAL_INF_DEINIT:
		zyUMAC_vap_deinit_completed(RTMP_OS_NETDEV_GET_WDEV
					   (pInfConf->operation_dev_p));
		break;
	}
	return status;
}

int __zyUMAC_wal_wdev_mlme_request(zyUMAC_wal_wdev_t wdev,
				  zyUMAC_wal_req_mlme_t *request)
{
	INT Status = NDIS_STATUS_SUCCESS;
	PRTMP_ADAPTER pAd = NULL;

	if (!wdev || !request) {
		zyUMAC_LOG(LOG_ERR, "invalid arguments");
		return -1;
	}

	pAd = RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev);
	if (!pAd)
		return -1;

	switch (request->mlme_op) {
	case zyUMAC_WAL_MLME_REQ_DISASSOC:
		{
			char mac[17];

			/*Mac address format 01:02:03:04:05:06 length 17 */
			sprintf(mac, "%02x:%02x:%02x:%02x:%02x:%02x",
				request->mlme_macaddr[0],
				request->mlme_macaddr[1],
				request->mlme_macaddr[2],
				request->mlme_macaddr[3],
				request->mlme_macaddr[4],
				request->mlme_macaddr[5]);
			Set_DisConnectSta_Proc(pAd, mac);
			break;
		}

	default:
		break;
	}

	return Status;
}

int __zyUMAC_wal_wdev_ioctl_attach(zyUMAC_wal_wdev_t wdev,
				  zyUMAC_wal_ioctl_hook_table * ioctl_hook_table)
{
	struct net_device *dev = NULL;
	struct iw_handler_def *orig_vap_iw_handler_def = NULL;
#ifdef CONFIG_WEXT_PRIV
	struct iw_priv_args *orig_vap_iw_priv_args = NULL;
	int orig_vap_iw_priv_args_size = 0;
	int zyUMAC_vap_iw_priv_args_size = 0;
#endif

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null!");
		return -1;
	}

	dev = wdev->if_dev;
	if (!dev) {
		zyUMAC_LOG(LOG_ERR, "netdev is null!");
		return -1;
	}

	orig_vap_iw_handler_def =
	    (struct iw_handler_def *)dev->wireless_handlers;
	ASSERT(orig_vap_iw_handler_def != NULL);

	_zyUMAC_ieee80211_setparam = ioctl_hook_table->zyUMAC_param_set_handler;
	_zyUMAC_ieee80211_getparam = ioctl_hook_table->zyUMAC_param_get_handler;

#ifdef CONFIG_WEXT_PRIV
	if (!zyUMAC_vap_iw_priv_args && ioctl_hook_table->zyUMAC_params_size) {
		orig_vap_iw_priv_args =
		    (struct iw_priv_args *)orig_vap_iw_handler_def->
		    private_args;

		if (!orig_vap_iw_priv_args) {
			return -1;
		}

		orig_vap_iw_priv_args_size =
		    orig_vap_iw_handler_def->num_private_args;
		zyUMAC_vap_iw_priv_args_size =
		    orig_vap_iw_priv_args_size +
		    ioctl_hook_table->zyUMAC_params_size;
		zyUMAC_vap_iw_priv_args =
		    kmalloc(zyUMAC_vap_iw_priv_args_size *
			    sizeof(struct iw_priv_args), GFP_KERNEL);

		if (!zyUMAC_vap_iw_priv_args) {
			return -1;
		}

		memcpy(zyUMAC_vap_iw_priv_args,
		       orig_vap_iw_priv_args,
		       orig_vap_iw_priv_args_size *
		       sizeof(struct iw_priv_args));
		memcpy(zyUMAC_vap_iw_priv_args + orig_vap_iw_priv_args_size,
		       ioctl_hook_table->zyUMAC_params,
		       ioctl_hook_table->zyUMAC_params_size *
		       sizeof(struct iw_priv_args));

		orig_vap_iw_handler_def->private_args = zyUMAC_vap_iw_priv_args;
		orig_vap_iw_handler_def->num_private_args =
		    zyUMAC_vap_iw_priv_args_size;
	}
#endif

	return 0;
}

void __zyUMAC_wal_wdev_ioctl_detach(void)
{
	if (zyUMAC_vap_iw_priv_args) {
		kfree(zyUMAC_vap_iw_priv_args);
		zyUMAC_vap_iw_priv_args = NULL;
	}
}
