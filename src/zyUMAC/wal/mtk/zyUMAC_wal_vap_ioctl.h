#ifndef ZYXEL_UMAC_WAL_VAP_IOCTL_HEADER_H
#define ZYXEL_UMAC_WAL_VAP_IOCTL_HEADER_H

void zyUMAC_wal_vap_ioctl_attach(struct net_device *dev);
void zyUMAC_wal_vap_ioctl_detach(struct net_device *dev);

#endif
