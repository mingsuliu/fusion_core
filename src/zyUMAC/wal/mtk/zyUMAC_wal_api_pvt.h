#ifndef __zyUMAC_WAL_API_PVT_H__
#define __zyUMAC_WAL_API_PVT_H__

#include "hdev/hdev_basic.h"
#include "zyUMAC_log.h"

#define IEEE80211_ADDR_EQ(a1,a2)        (memcmp(a1, a2, 6) == 0)

#define MAX_WDEV_NUM 16

#define __zyUMAC_wal_wphy_setdata(dev, data)  dev->scn_zyUMAC_priv = data
#define __zyUMAC_wal_wphy_getdata(dev)        dev->scn_zyUMAC_priv

extern zyUMAC_wal_wphy_t __zyUMAC_wal_netdev_to_wphy(struct net_device *ndev);
extern INT Set_ACLAddEntry_Proc(PRTMP_ADAPTER pAd, RTMP_STRING *arg);
extern INT Set_DisConnectSta_Proc(PRTMP_ADAPTER pAd, RTMP_STRING *arg);

static inline int __zyUMAC_wal_get_ac(int mtk_ac)
{
	switch (mtk_ac) {
	case QID_AC_BK:
		return WME_AC_BK;
	case QID_AC_BE:
		return WME_AC_BE;
	case QID_AC_VI:
		return WME_AC_VI;
	case QID_AC_VO:
		return WME_AC_VO;
	default:
		return 0; /*avoid invalid array access*/
	}
}

static inline zyUMAC_wal_wdev_t __zyUMAC_wal_netdev_to_wdev(struct net_device
							  *ndev)
{
	return RTMP_OS_NETDEV_GET_WDEV(ndev);
}

static inline struct net_device *__zyUMAC_wal_wphy_to_netdev(zyUMAC_wal_wphy_t
							    wphy)
{
	return wphy->dev;	//return the virtual radio, not vap
}

static inline struct proc_dir_entry
    *__zyUMAC_wal_wphy_get_proc_dir(zyUMAC_wal_wphy_t wphy)
{
	return wphy->proc;
}

static inline int
__zyUMAC_wal_wphy_get_wdev_limit(zyUMAC_wal_wphy_t wphy)
{
	/* TODO: depend on driver and board config */
	return MAX_WDEV_NUM;
}

static inline  void
__zyUMAC_wal_wphy_update_radio_stats(zyUMAC_wal_wphy_t wphy,
				    zyUMAC_scn_priv_radio_stats_t *stats)
{
	PRTMP_ADAPTER pAd;
	struct hdev_ctrl *ctrl = NULL;
	RADIO_CTRL *radioCtrl = NULL;
	int band;
	int min_rssi, max_rssi;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	band = wphy->band_idx;
	pAd = wphy->pAd;
	ctrl = (struct hdev_ctrl *)pAd->hdev_ctrl;

	if (!ctrl)
		return;
	radioCtrl = ctrl->rdev[band].pRadioCtrl;

	if (!radioCtrl)
		return;

	stats->wal_stats.rtsGood =
		pAd->WlanCounters[band].RTSSuccessCount.u.LowPart;
	stats->wal_stats.rtsBad =
		pAd->WlanCounters[band].RTSFailureCount.u.LowPart;
	stats->wal_stats.ackRcvBad = 0;
	stats->wal_stats.fcsBad =
		pAd->WlanCounters[band].FCSErrorCount.u.LowPart;
	stats->wal_stats.pdev_cont_xretry =
		pAd->WlanCounters[band].RetryCount.u.LowPart;

	stats->wal_stats.tx_packets = radioCtrl->TxMgmtPacketCount.QuadPart +
				      radioCtrl->TxDataPacketCount.QuadPart;
	stats->wal_stats.tx_bytes = radioCtrl->TxDataPacketByte.QuadPart;
	stats->wal_stats.tx_num_data = radioCtrl->TxDataPacketCount.QuadPart;
	stats->wal_stats.tx_xretry = 0; /* Albert: TODO */
	stats->wal_stats.tx_beacon = radioCtrl->TxBeaconPacketCount.QuadPart;
	stats->wal_stats.tx_bawadv = 0;

	stats->wal_stats.rx_badcrypt =
		radioCtrl->RxDecryptionErrorCount.QuadPart;
	stats->wal_stats.rx_packets = radioCtrl->RxMgmtPacketCount.QuadPart +
				      radioCtrl->RxDataPacketCount.QuadPart;
	stats->wal_stats.rx_bytes = radioCtrl->RxDataPacketByte.QuadPart;
	stats->wal_stats.rx_phyerr = pAd->RalinkCounters.PhyErrCnt;
	stats->wal_stats.rx_badmic = radioCtrl->RxMICErrorCount.QuadPart;
	stats->wal_stats.rx_num_data = radioCtrl->RxDataPacketCount.QuadPart;
	stats->wal_stats.rx_data_bytes = radioCtrl->RxDataPacketByte.QuadPart;
	stats->wal_stats.rx_crcerr = radioCtrl->RxCRCErrorCount.QuadPart;

	stats->noise_floor = 0; /* Albert: TODO */

	max_rssi = RTMPMaxRssi(pAd, radioCtrl->LastDataPktRssi[0],
				radioCtrl->LastDataPktRssi[1],
				radioCtrl->LastDataPktRssi[2]);

	min_rssi = RTMPMinRssi(pAd, radioCtrl->LastDataPktRssi[0],
				radioCtrl->LastDataPktRssi[1],
				radioCtrl->LastDataPktRssi[2],
				radioCtrl->LastDataPktRssi[3]);

	if (min_rssi < stats->rx_rssi_lowest)
		stats->rx_rssi_lowest = min_rssi;

	if (max_rssi > stats->rx_rssi_highest)
		stats->rx_rssi_highest = max_rssi;
	
	stats->Last1TxFailCnt =
		radioCtrl->Last1TxFailCnt;
	stats->Last1TxCnt =
		radioCtrl->Last1TxCnt;
	stats->Last1SecPER =
		((100 * (stats->Last1TxFailCnt))/
				stats->Last1TxCnt);
}

static inline void
__zyUMAC_wal_wphy_update_channel_util(zyUMAC_wal_wphy_t wphy,
				     zyUMAC_scn_priv_radio_stats_t *stats,
				     unsigned int enable)
{
	PRTMP_ADAPTER pAd;
	int band;
	char buf[4];
	UINT32 cr;

	band = wphy->band_idx;
	pAd = (PRTMP_ADAPTER)wphy->pAd;

	HW_IO_READ32(pAd, RMAC_AIRTIME0, &cr);
	if (enable)
		HW_IO_WRITE32(pAd, RMAC_AIRTIME0, cr | RX_AIRTIME_EN);
	else
		HW_IO_WRITE32(pAd, RMAC_AIRTIME0, cr & ~RX_AIRTIME_EN);

	sprintf(buf, "%d", enable);
	Set_MibBucket_Proc(pAd, buf);

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return;
	}

	if (!stats) {
		zyUMAC_LOG(LOG_ERR, "zyUMAC_scn_priv_radio_stats_t is null");
		return;
	}
	stats->channel_stats.duration = ONE_SEC_2_US;
	stats->channel_stats.busy_time =
		pAd->OneSecMibBucket.ChannelBusyTime[band];
	stats->channel_stats.tx_time =
		pAd->OneSecMibBucket.MyTxAirtime[band];
	stats->channel_stats.rx_time =
		pAd->OneSecMibBucket.MyRxAirtime[band];
	stats->channel_stats.obss_time =
		pAd->OneSecMibBucket.OBSSAirtime[band];
	stats->channel_stats.edcca_time =
		pAd->OneSecMibBucket.EDCCAtime[band];

}

static inline void
__zyUMAC_wal_wdev_update_stats(zyUMAC_wal_wdev_t wdev,
			      zyUMAC_vap_priv_stats_t *stats)
{
	PRTMP_ADAPTER pAd = NULL;
	BSS_STRUCT *pMbss = NULL;
	struct wifi_dev *pdev = NULL;
	int i;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return;
	}

	if (!stats) {
		zyUMAC_LOG(LOG_ERR, "stats is null");
		return;
	}

	pAd = RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev);
	if (!pAd)
		return;

	for (i = 0 ; i < pAd->ApCfg.BssidNum; i++) {
		pdev = &pAd->ApCfg.MBSSID[i].wdev;
		if (pdev && (pdev->wdev_idx == wdev->wdev_idx)) {
			pMbss = &pAd->ApCfg.MBSSID[i];
			break;
		}
	}

	if (!pMbss)
		return;

	/*TODO*/
	stats->wal_stats.unicast_tx_discard =
			pMbss->stat_bss.TxPacketDroppedCount.QuadPart;
	stats->wal_stats.multicast_tx_discard =
			pMbss->stat_bss.TxPacketDroppedCount.QuadPart;

	stats->wal_stats.unicast_rx_fcserr = 0;
	stats->wal_stats.multicast_rx_fcserr = 0;
	stats->wal_stats.unicast_rx_tkipmic =
			pMbss->stat_bss.RxMICErrorCount.QuadPart;
	stats->wal_stats.multicast_rx_tkipmic =
			pMbss->stat_bss.RxMICErrorCount.QuadPart;
	
	stats->Last1TxFailCnt =
			pMbss->stat_bss.Last1TxFailCnt;
	stats->Last1TxCnt =
			pMbss->stat_bss.Last1TxCnt;
	stats->Last1SecPER =
			((100 * (stats->Last1TxFailCnt))/
			stats->Last1TxCnt);
}

static inline void
__zyUMAC_wal_wnode_get_stats(PMAC_TABLE_ENTRY pEntry,
			       EXT_EVENT_TX_STATISTIC_RESULT_T *tx_stats,
			       HTTRANSMIT_SETTING *last_tx_rates,
			       HTTRANSMIT_SETTING *last_rx_rates)
{
	/* ref: Show_Sta_Stat_Proc() */
	UINT32 raw_rx;
	UCHAR phy_mode_rx, rate_rx;

	if (!pEntry || !tx_stats || !last_tx_rates || !last_rx_rates) {
		zyUMAC_LOG(LOG_ERR, "invalid arguments");
		return;
	}

	raw_rx = pEntry->LastRxRate;
	phy_mode_rx = (raw_rx >> 13) & 0x7;
	rate_rx = raw_rx & 0x3F;

	MtCmdGetTxStatistic(pEntry->pAd,
			    GET_TX_STAT_ENTRY_TX_RATE,
			    0,
			    pEntry->wcid,
			    tx_stats);

	/* tx rate */
	last_tx_rates->field.MODE = tx_stats->rEntryTxRate.MODE;
	last_tx_rates->field.BW = tx_stats->rEntryTxRate.BW;
	last_tx_rates->field.ldpc = tx_stats->rEntryTxRate.ldpc ? 1 : 0;
	last_tx_rates->field.ShortGI =
		tx_stats->rEntryTxRate.ShortGI ? 1 : 0;
	last_tx_rates->field.STBC = tx_stats->rEntryTxRate.STBC;
	if (last_tx_rates->field.MODE == MODE_VHT)
		last_tx_rates->field.MCS =
			(((tx_stats->rEntryTxRate.VhtNss - 1) & 0x3) << 4)
			 + tx_stats->rEntryTxRate.MCS;
	else if (last_tx_rates->field.MODE == MODE_OFDM)
		last_tx_rates->field.MCS =
			getLegacyOFDMMCSIndex(
				tx_stats->rEntryTxRate.MCS) & 0x0000003F;
	else
		last_tx_rates->field.MCS = tx_stats->rEntryTxRate.MCS;

	/* rx rate */
	last_rx_rates->word = (USHORT)(pEntry->LastRxRate);
	if (phy_mode_rx == MODE_OFDM) {
		if (rate_rx == TMI_TX_RATE_OFDM_6M)
			last_rx_rates->field.MCS = 0;
		else if (rate_rx == TMI_TX_RATE_OFDM_9M)
			last_rx_rates->field.MCS = 1;
		else if (rate_rx == TMI_TX_RATE_OFDM_12M)
			last_rx_rates->field.MCS = 2;
		else if (rate_rx == TMI_TX_RATE_OFDM_18M)
			last_rx_rates->field.MCS = 3;
		else if (rate_rx == TMI_TX_RATE_OFDM_24M)
			last_rx_rates->field.MCS = 4;
		else if (rate_rx == TMI_TX_RATE_OFDM_36M)
			last_rx_rates->field.MCS = 5;
		else if (rate_rx == TMI_TX_RATE_OFDM_48M)
			last_rx_rates->field.MCS = 6;
		else if (rate_rx == TMI_TX_RATE_OFDM_54M)
			last_rx_rates->field.MCS = 7;
		else
			last_rx_rates->field.MCS = 0;
	} else if (phy_mode_rx == MODE_CCK) {
		if (rate_rx == TMI_TX_RATE_CCK_1M_LP)
			last_rx_rates->field.MCS = 0;
		else if (rate_rx == TMI_TX_RATE_CCK_2M_LP)
			last_rx_rates->field.MCS = 1;
		else if (rate_rx == TMI_TX_RATE_CCK_5M_LP)
			last_rx_rates->field.MCS = 2;
		else if (rate_rx == TMI_TX_RATE_CCK_11M_LP)
			last_rx_rates->field.MCS = 3;
		else if (rate_rx == TMI_TX_RATE_CCK_2M_SP)
			last_rx_rates->field.MCS = 1;
		else if (rate_rx == TMI_TX_RATE_CCK_5M_SP)
			last_rx_rates->field.MCS = 2;
		else if (rate_rx == TMI_TX_RATE_CCK_11M_SP)
			last_rx_rates->field.MCS = 3;
		else
			last_rx_rates->field.MCS = 0;
	}
}

static inline void
__zyUMAC_wal_wnode_update_stats(zyUMAC_wal_wnode_t ni,
			       struct zyUMAC_node_priv_stats *stats)
{
	EXT_EVENT_TX_STATISTIC_RESULT_T tx_stats;
	HTTRANSMIT_SETTING last_tx_rates, last_rx_rates;
	ULONG data_rate_tx, data_rate_rx;

	__zyUMAC_wal_wnode_get_stats(ni, &tx_stats,
		&last_tx_rates, &last_rx_rates);

	getRate(last_tx_rates, &data_rate_tx);
	getRate(last_rx_rates, &data_rate_rx);

	stats->wal_stats.ns_last_tx_rate = data_rate_tx * 1000; /*kbps*/
	stats->wal_stats.ns_last_rx_rate = data_rate_rx * 1000; /*kbps*/

	stats->wal_stats.ns_rx_dup = 0; /*TODO*/

	stats->an_framereceive[WME_AC_BK] =
		ni->RxDataPacketCountPerAC[QID_AC_BK].QuadPart;
	stats->an_framereceive[WME_AC_BE] =
		ni->RxDataPacketCountPerAC[QID_AC_BE].QuadPart;
	stats->an_framereceive[WME_AC_VI] =
		ni->RxDataPacketCountPerAC[QID_AC_VI].QuadPart;
	stats->an_framereceive[WME_AC_VO] =
		ni->RxDataPacketCountPerAC[QID_AC_VO].QuadPart;

	stats->an_frametransmit[WME_AC_BK] =
		ni->TxDataPacketCountPerAC[QID_AC_BK].QuadPart;
	stats->an_frametransmit[WME_AC_BE] =
		ni->TxDataPacketCountPerAC[QID_AC_BE].QuadPart;
	stats->an_frametransmit[WME_AC_VI] =
		ni->TxDataPacketCountPerAC[QID_AC_VI].QuadPart;
	stats->an_frametransmit[WME_AC_VO] =
		ni->TxDataPacketCountPerAC[QID_AC_VO].QuadPart;
	
	stats->Last1SecPER = ni->LastOneSecPER; 
		
}

static inline zyUMAC_wal_wdev_t
__zyUMAC_wal_wphy_get_next_wdev_byidx(zyUMAC_wal_wphy_t wphy, int idx)
{
	PRTMP_ADAPTER pAd;
	struct wifi_dev *wdev = NULL;
	int band, i;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return NULL;
	}

	band = wphy->band_idx;
	pAd = wphy->pAd;

	for (i = idx; i < pAd->ApCfg.BssidNum; i++) {
		if (!RtmpOSNetDevIsUp(pAd->ApCfg.MBSSID[i].wdev.if_dev))
			continue;
		if (band == HcGetBandByWdev(&pAd->ApCfg.MBSSID[i].wdev)) {
			wdev = &pAd->ApCfg.MBSSID[i].wdev;
			break;
		}
	}

	return wdev;
}

static inline zyUMAC_wal_wdev_t
__zyUMAC_wal_wphy_get_first_wdev(zyUMAC_wal_wphy_t wphy)
{
	return __zyUMAC_wal_wphy_get_next_wdev_byidx(wphy, MAIN_MBSSID);
}

static inline zyUMAC_wal_wdev_t
__zyUMAC_wal_wphy_get_next_wdev(zyUMAC_wal_wphy_t wphy, zyUMAC_wal_wdev_t wdev)
{
	return __zyUMAC_wal_wphy_get_next_wdev_byidx(wphy, wdev->func_idx + 1);
}

static inline zyUMAC_wal_wnode_t
__zyUMAC_wal_wphy_get_first_wnode(zyUMAC_wal_wphy_t wphy)
{
	PRTMP_ADAPTER pAd;
	zyUMAC_wal_wnode_t ni = NULL;
	int band, i;

	if (!wphy) {
		zyUMAC_LOG(LOG_ERR, "wphy is null");
		return NULL;
	}

	band = wphy->band_idx;
	pAd = wphy->pAd;

	for (i = 0; VALID_UCAST_ENTRY_WCID(pAd, i); i++) {
		PMAC_TABLE_ENTRY pEntry = &pAd->MacTab.Content[i];

		if (pEntry && IS_ENTRY_CLIENT(pEntry) &&
		    (pEntry->Sst == SST_ASSOC)) {
			if (HcGetBandByWdev(pEntry->wdev) == band) {
				ni = pEntry;
				break;
			}
		}
	}

	return ni;
}

static inline zyUMAC_wal_wnode_t
__zyUMAC_wal_wphy_get_next_wnode(zyUMAC_wal_wphy_t wphy, zyUMAC_wal_wnode_t node)
{
	zyUMAC_wal_wnode_t ni = NULL;
	PMAC_TABLE_ENTRY pEntry;
	int band;

	if (!wphy || !node) {
		zyUMAC_LOG(LOG_ERR, "wphy or wnode is null");
		return NULL;
	}

	band = wphy->band_idx;
	pEntry = (PMAC_TABLE_ENTRY)node;

	while ((pEntry = (pEntry->pNext))) {
		if (IS_ENTRY_CLIENT(pEntry) &&
		    (pEntry->Sst == SST_ASSOC)) {
			if (HcGetBandByWdev(pEntry->wdev) == band) {
				ni = pEntry;
				break;
			}
		}
	}

	return ni;
}

static inline void __zyUMAC_wal_wphy_iterate_wnodes(zyUMAC_wal_wphy_t wphy,
						   zyUMAC_wal_wnode_itrator cb,
						   void *arg)
{
	PRTMP_ADAPTER pAd;
	PMAC_TABLE_ENTRY pEntry;
	int band, i;

	if (!wphy || !cb) {
		zyUMAC_LOG(LOG_ERR, "wphy or cb is null");
		return;
	}

	band = wphy->band_idx;
	pAd = wphy->pAd;

	for (i = 0; VALID_UCAST_ENTRY_WCID(pAd, i); i++) {
		pEntry = &pAd->MacTab.Content[i];

		if (pEntry && IS_ENTRY_CLIENT(pEntry) &&
		    (pEntry->Sst == SST_ASSOC)) {
			if (HcGetBandByWdev(pEntry->wdev) == band)
				cb(pEntry, arg);
		}
	}
}

#define __zyUMAC_wal_wphy_get_id(wphy)    (wphy->band_idx)
extern zyUMAC_wal_wphy_t __zyUMAC_wal_wdev_get_wphy_by_id(int band);

#define __zyUMAC_wal_wdev_setdata(wdev, data)    wdev->iv_zyUMAC_vap_priv = data
#define __zyUMAC_wal_wdev_getdata(wdev)          wdev->iv_zyUMAC_vap_priv
extern zyUMAC_wal_wphy_t __zyUMAC_wal_wdev_get_wphy(zyUMAC_wal_wdev_t wdev);

static inline struct net_device *__zyUMAC_wal_wdev_to_netdev(zyUMAC_wal_wdev_t
							    wdev)
{
	return wdev->if_dev;
}

static inline int __zyUMAC_wal_wdev_acl_add(zyUMAC_wal_wdev_t wdev,
					   const char *macstr,
					   u_int8_t acl_list_id)
{
	PRTMP_ADAPTER pAd = NULL;

	if (!wdev) {
		zyUMAC_LOG(LOG_ERR, "wdev is null");
		return -1;
	}

	pAd = RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev);
	if (!pAd)
		return -1;

	return Set_ACLAddEntry_Proc(pAd, (RTMP_STRING *)macstr);
}

#define __zyUMAC_wal_wdev_is_ap_mode(wdev)       (wdev->wdev_type == WDEV_TYPE_AP)
#define __zyUMAC_wal_wdev_get_macaddr(wdev)      wdev->if_addr

static inline void
__zyUMAC_wal_wdev_get_ssid(zyUMAC_wal_wdev_t wdev,
					zyUMAC_wal_ssid_t *ssid)
{
	BSS_STRUCT *pMbss = NULL;
	unsigned int len = 0;

	if (!ssid || !wdev) {
		zyUMAC_LOG(LOG_ERR, "invalid arguments");
		return;
	}

	memset(ssid, 0, sizeof(zyUMAC_wal_ssid_t));

	if (wdev->func_dev) {
		pMbss = (BSS_STRUCT *)wdev->func_dev;
		len = (pMbss->SsidLen > MAX_LEN_OF_SSID)
			? MAX_LEN_OF_SSID : pMbss->SsidLen;
		memcpy(ssid->ssid, pMbss->Ssid, len);
		ssid->len = len;
	}
}

static inline int8_t __zyUMAC_wal_wdev_get_chanutil(zyUMAC_wal_wdev_t wdev)
{
	return 0;
	/* TODO: get_channel_utilization(PRTMP_ADAPTER pAd, u32 ifindex) */
}


#define __zyUMAC_wal_ssid_get_data(ssid)   ssid.ssid
#define __zyUMAC_wal_ssid_get_len(ssid)    ssid.len

static inline int __zyUMAC_wal_wdev_get_txpow(zyUMAC_wal_wdev_t wdev)
{
	PRTMP_ADAPTER pAd;
	UCHAR op_ht_bw = wlan_operate_get_ht_bw(wdev);
	pAd = RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev);
	return GetCuntryMaxTxPwr(pAd, wdev->PhyMode, wdev, op_ht_bw);
}

extern int __zyUMAC_wal_wdev_ioctl_attach(zyUMAC_wal_wdev_t wdev,
					 zyUMAC_wal_ioctl_hook_table *
					 ioctl_hook_table);
extern void __zyUMAC_wal_wdev_ioctl_detach(void);

static inline zyUMAC_wal_wnode_t __zyUMAC_wal_wnode_get(zyUMAC_wal_wdev_t wdev,
						      unsigned char *mac)
{
	PRTMP_ADAPTER pAd = RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev);
	MAC_TABLE_ENTRY *pEntry = MacTableLookup(pAd, mac);
	return pEntry;
}

#define __zyUMAC_wal_wnode_put(wnode)

static inline int __zyUMAC_wal_wdev_get_sta_count(zyUMAC_wal_wdev_t wdev)
{
	return MacTableAssocStaNumGet(RTMP_OS_NETDEV_GET_PRIV(wdev->if_dev));
}

static inline struct proc_dir_entry *__zyUMAC_wal_wdev_get_proc(zyUMAC_wal_wdev_t
							       wdev)
{
	if (!wdev->iv_proc) {
		wdev->iv_proc =
		    proc_mkdir(RTMP_OS_NETDEV_GET_DEVNAME(wdev->if_dev), NULL);
	}
	return wdev->iv_proc;
}

#define __zyUMAC_wal_wdev_get_chan(wdev)    (wdev->channel)

#define __zyUMAC_wal_wnode_setdata(wnode, data)      wnode->ni_zyUMAC_node_priv = data;
#define __zyUMAC_wal_wnode_getdata(wnode)            wnode->ni_zyUMAC_node_priv

#define __zyUMAC_wal_wnode_get_wdev(wnode)           wnode->wdev
#define __zyUMAC_wal_wnode_get_macaddr(wnode)        wnode->Addr
#define __zyUMAC_wal_wnode_get_bssid(wnode)          wnode->bssid
#define __zyUMAC_wal_wnode_get_assocuptime(wnode)    wnode->StaConnectTime
#define __zyUMAC_wal_wnode_get_associd(wnode)        wnode->Aid
#define __zyUMAC_wal_wnode_get_rssi(wnode) \
	(wnode->RssiSample.AvgRssi[0])	/* TODO: cal avg of chains */
#define __zyUMAC_wal_wnode_get_phymode(wnode)        0/*wnode->HTPhyMode*/

static inline int __zyUMAC_wal_wnode_get_maxrate(zyUMAC_wal_wnode_t wnode)
{
	PMAC_TABLE_ENTRY pEntry = (PMAC_TABLE_ENTRY)wnode;
	ULONG data_rate;

	getRate(pEntry->MaxHTPhyMode, &data_rate);

	return (int)data_rate;
}

static inline int __zyUMAC_wal_wnode_get_radioid(zyUMAC_wal_wnode_t wnode)
{
	/*FIXME: use
	 *__zyUMAC_wal_wdev_get_wphy
	 */
	return wnode->apidx;
}

#define __zyUMAC_wal_wnode_is_authed(wnode)         TRUE

#define __zyUMAC_wal_ieee80211_header_get_addr1(wh)   (wh->Addr1)
#define __zyUMAC_wal_ieee80211_header_get_addr2(wh)   (wh->Addr2)
#define __zyUMAC_wal_ieee80211_header_get_seq(wh)     (wh->Sequence)

extern int zyUMAC_wal_attach(RTMP_ADAPTER * pAd, int num_radios);
extern void zyUMAC_wal_detach(RTMP_ADAPTER * pAd);
//extern int zyUMAC_wal_mbss_attach(RTMP_ADAPTER *pAd);
extern int zyUMAC_wal_stats_attach(void *arg);
extern void zyUMAC_wal_stats_detach(void *arg);

extern int __zyUMAC_wal_wphy_ioctl_attach(zyUMAC_wal_wphy_t wphy,
					 zyUMAC_wal_ioctl_hook_table *
					 ioctl_hook_table);
extern void __zyUMAC_wal_wphy_ioctl_detach(zyUMAC_wal_wphy_t wphy);

extern int __zyUMAC_wal_wdev_attach(zyUMAC_wal_wphy_t wphy,
				   zyUMAC_wdev_hook_table * wdev_hooks);
extern int __zyUMAC_wal_wdev_mlme_request(zyUMAC_wal_wdev_t wdev,
					 zyUMAC_wal_req_mlme_t *request);

extern void __zyUMAC_wal_wphy_set_nodesurvey(zyUMAC_wal_wphy_t wphy);

#endif
