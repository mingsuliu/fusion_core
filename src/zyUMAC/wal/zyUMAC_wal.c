#include <linux/module.h>
#include "zyUMAC_wal_api.h"

zyUMAC_module_hooks_table *_zyUMAC_module_hooks;

int zyUMAC_wal_init(zyUMAC_module_hooks_table * module_hooks)
{
	zyUMAC_LOG(LOG_INFO, "Starting Zyxel Micro UMAC module");

	ASSERT(module_hooks != NULL);
	_zyUMAC_module_hooks = module_hooks;
	return 0;
}

void zyUMAC_wal_exit(void)
{
	zyUMAC_LOG(LOG_INFO, "Stopping Zyxel Micro UMAC module");
	//_zyUMAC_module_hooks = NULL;
}

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif
EXPORT_SYMBOL(zyUMAC_wal_init);
EXPORT_SYMBOL(zyUMAC_wal_exit);
