#ifndef ZYXEL_UMAC_COMPAT_HEADER_H
#define ZYXEL_UMAC_COMPAT_HEADER_H

typedef struct wmi_unified zyUMAC_wmi_handle_t;

#define zyUMAC_ieee80211_vap_find_node(vap, mac) \
	ieee80211_vap_find_node(vap, mac, WLAN_MISC_ID)

#define zyUMAC_ieee80211_find_node(vap, mac) \
	ieee80211_find_node(vap, mac, WLAN_MISC_ID)

#define zyUMAC_ieee80211_free_node(ni)    ieee80211_free_node(ni, WLAN_MISC_ID)

#define zyUMAC_dp_getrateindex(gi, mcs, nss, preamble, bw, rix)         \
	({                                                             \
		u_int16_t ratecode;                                    \
		dp_getrateindex(gi, mcs, nss, preamble, bw, &rix, &ratecode); \
	})

#endif
