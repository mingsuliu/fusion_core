#include "ol_if_athvar.h"
#include "wlan_osif_priv.h"

#include "zyUMAC_umac.h"

static int (*orig_mgtstart) (struct ieee80211com * ic, wbuf_t wbuf) = NULL;

int (*origional_wlan_receive_filter_80211) (os_if_t osif, wbuf_t wbuf,
					    u_int16_t type, u_int16_t subtype,
					    ieee80211_recv_status * rs) = NULL;

QDF_STATUS(*orig_mgmt_tx_completion_handler) (struct wlan_objmgr_pdev * pdev,
					      uint32_t desc_id, uint32_t status,
					      void *tx_compl_params);

int zyUMAC_tx_mgmt_wmi_send(struct ieee80211com *ic, wbuf_t wbuf)
{
	return orig_mgtstart(ic, wbuf);
}

/*
 * post-filter after ieee80211_recv_mgmt()
 */
int zyUMAC_wlan_receive_filter_80211(os_if_t osif, wbuf_t wbuf,
				    u_int16_t type, u_int16_t subtype,
				    ieee80211_recv_status * rs)
{
	return origional_wlan_receive_filter_80211(osif, wbuf, type, subtype,
						   rs);
}

QDF_STATUS zyUMAC_mgmt_tx_completion_handler(struct wlan_objmgr_pdev * pdev,
					    uint32_t desc_id, uint32_t status,
					    void *tx_compl_params)
{
	struct ol_ath_softc_net80211 *scn;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	struct ieee80211_frame *wh;
	int subtype;
	struct wlan_objmgr_psoc *psoc = wlan_pdev_get_psoc(pdev);
	struct wlan_lmac_if_mgmt_txrx_rx_ops *mgmt_rx_ops =
	    &psoc->soc_cb.rx_ops.mgmt_txrx_rx_ops;
	qdf_nbuf_t nbuf =
	    mgmt_rx_ops->mgmt_txrx_get_nbuf_from_desc_id(pdev, desc_id);
	scn =
	    (struct ol_ath_softc_net80211 *)pdev->pdev_nif.pdev_ospriv->
	    legacy_osif_priv;

	if (scn) {
		zyUMAC_scn_priv_radio_stats_t *stats;
		zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;

		if (zyUMAC_scn_priv) {
			stats = &zyUMAC_scn_priv->scn_radio_stats;
			stats->tx_mgmt++;
		}
	}

	wh = (struct ieee80211_frame *)qdf_nbuf_data(nbuf);
	subtype = wh->i_fc[0] & IEEE80211_FC0_SUBTYPE_MASK;

	if (zyUMAC_scn_priv) {
		if (subtype == IEEE80211_FC0_SUBTYPE_PROBE_RESP) {
			zyUMAC_scn_priv->scn_radio_stats.tx_probresp++;
		} else if (subtype == IEEE80211_FC0_SUBTYPE_ASSOC_RESP) {
			zyUMAC_scn_priv->scn_radio_stats.tx_assocresp++;
		} else if (subtype == IEEE80211_FC0_SUBTYPE_REASSOC_RESP) {
			zyUMAC_scn_priv->scn_radio_stats.tx_reassocresp++;
		}
	}

	return orig_mgmt_tx_completion_handler(pdev, desc_id, status,
					       tx_compl_params);
}

int zyUMAC_mgmt_attach(struct ol_ath_softc_net80211 *scn)
{
	struct ieee80211com *ic = &scn->sc_ic;
	struct wlan_objmgr_psoc *psoc = wlan_pdev_get_psoc(scn->sc_pdev);
	struct wlan_lmac_if_mgmt_txrx_rx_ops *mgmt_rx_ops =
	    &psoc->soc_cb.rx_ops.mgmt_txrx_rx_ops;

	ASSERT(ic != NULL);

	if (orig_mgtstart == NULL) {
		orig_mgtstart = ic->ic_mgtstart;
	}

	/* overwrite point in ol_ath_mgmt_attach() and ol_ath_target_start() */
	ic->ic_mgtstart = zyUMAC_tx_mgmt_wmi_send;

	if (orig_mgmt_tx_completion_handler == NULL) {
		orig_mgmt_tx_completion_handler =
		    mgmt_rx_ops->mgmt_tx_completion_handler;
	}
	mgmt_rx_ops->mgmt_tx_completion_handler =
	    zyUMAC_mgmt_tx_completion_handler;

	return 0;
}

EXPORT_SYMBOL(zyUMAC_mgmt_attach);
