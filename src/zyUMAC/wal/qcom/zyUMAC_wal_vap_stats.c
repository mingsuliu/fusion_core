#include <linux/module.h>

#include "ol_if_athvar.h"
#include <osif_private.h>
#ifndef UMAC_SUPPORT_CFG80211
#include <osdep_internal.h>
#endif
#include <wdi_event_api.h>
#include <ol_txrx_types.h>
#include <ol_if_stats.h>
#include <ath_netlink.h>

#include <htt_internal.h>

#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_stats.h"
#include "zyUMAC_wal_vap.h"
#ifdef UMAC_SUPPORT_CFG80211
#include "dp_types.h"
#include "target_type.h"
#include "cdp_txrx_ctrl.h"
#endif
#include "ar_internal.h"
#include "zyUMAC_compat.h"

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif

static int zyUMAC_vap_node_update_rssi(struct ieee80211_node *ni, int rssi,
				      struct ieee80211vap *vap)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	struct stats_ieee80211_rssi *is_rssi, *ns_rssi;

	if (!ni || !vap) {
		goto failed;
	}

	if ((zyUMAC_node_priv =
	     (zyUMAC_node_priv_t *) ni->ni_zyUMAC_node_priv) == NULL) {
		goto failed;
	}
	ns_rssi = &zyUMAC_node_priv->ns_stats.ns_rssi_counter;

	if ((zyUMAC_vap_priv =
	     (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv) == NULL) {
		goto failed;
	}
	is_rssi = &zyUMAC_vap_priv->is_stats.is_rssi_counter;

	//copied from ol_ath_vap_node_update_rssi()
	if (0 <= rssi) {
		is_rssi->rssi_abnormal++;
		ns_rssi->rssi_abnormal++;
	} else if (-60 <= rssi) {
		is_rssi->rssi_60_00_dBm++;
		ns_rssi->rssi_60_00_dBm++;
	} else if (-63 <= rssi) {
		is_rssi->rssi_63_60_dBm++;
		ns_rssi->rssi_63_60_dBm++;
	} else if (-66 <= rssi) {
		is_rssi->rssi_66_63_dBm++;
		ns_rssi->rssi_66_63_dBm++;
	} else if (-69 <= rssi) {
		is_rssi->rssi_69_66_dBm++;
		ns_rssi->rssi_69_66_dBm++;
	} else if (-72 <= rssi) {
		is_rssi->rssi_72_69_dBm++;
		ns_rssi->rssi_72_69_dBm++;
	} else if (-75 <= rssi) {
		is_rssi->rssi_75_72_dBm++;
		ns_rssi->rssi_75_72_dBm++;
	} else if (-78 <= rssi) {
		is_rssi->rssi_78_75_dBm++;
		ns_rssi->rssi_78_75_dBm++;
	} else if (-81 <= rssi) {
		is_rssi->rssi_81_78_dBm++;
		ns_rssi->rssi_81_78_dBm++;
	} else if (-84 <= rssi) {
		is_rssi->rssi_84_81_dBm++;
		ns_rssi->rssi_84_81_dBm++;
	} else if (-87 <= rssi) {
		is_rssi->rssi_87_84_dBm++;
		ns_rssi->rssi_87_84_dBm++;
	} else if (-90 <= rssi) {
		is_rssi->rssi_90_87_dBm++;
		ns_rssi->rssi_90_87_dBm++;
	} else if (-99 <= rssi) {
		is_rssi->rssi_99_90_dBm++;
		ns_rssi->rssi_99_90_dBm++;
	} else {
		is_rssi->rssi_abnormal++;
		ns_rssi->rssi_abnormal++;
	}

	return 0;

failed:
	return -1;
}

static int zyUMAC_vap_node_update_snr(struct ieee80211_node *ni, int snr,
				     struct ieee80211vap *vap)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	struct stats_ieee80211_snr *is_snr, *ns_snr;

	if (!ni || !vap) {
		goto failed;
	}

	if ((zyUMAC_node_priv =
	     (zyUMAC_node_priv_t *) ni->ni_zyUMAC_node_priv) == NULL) {
		goto failed;
	}
	ns_snr = &zyUMAC_node_priv->ns_stats.ns_snr_counter;

	if ((zyUMAC_vap_priv =
	     (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv) == NULL) {
		goto failed;
	}
	is_snr = &zyUMAC_vap_priv->is_stats.is_snr_counter;

	//copied from ol_ath_vap_node_update_rssi()
	if (snr < 0) {
		is_snr->snr_abnormal++;
		ns_snr->snr_abnormal++;
	} else if (snr < 9) {
		is_snr->snr_0_9_dB++;
		ns_snr->snr_0_9_dB++;
	} else if (snr < 12) {
		is_snr->snr_9_12_dB++;
		ns_snr->snr_9_12_dB++;
	} else if (snr < 15) {
		is_snr->snr_12_15_dB++;
		ns_snr->snr_12_15_dB++;
	} else if (snr < 18) {
		is_snr->snr_15_18_dB++;
		ns_snr->snr_15_18_dB++;
	} else if (snr < 21) {
		is_snr->snr_18_21_dB++;
		ns_snr->snr_18_21_dB++;
	} else if (snr < 24) {
		is_snr->snr_21_24_dB++;
		ns_snr->snr_21_24_dB++;
	} else if (snr < 27) {
		is_snr->snr_24_27_dB++;
		ns_snr->snr_24_27_dB++;
	} else if (snr < 30) {
		is_snr->snr_27_30_dB++;
		ns_snr->snr_27_30_dB++;
	} else if (snr < 33) {
		is_snr->snr_30_33_dB++;
		ns_snr->snr_30_33_dB++;
	} else if (snr < 100) {
		is_snr->snr_33_100_dB++;
		ns_snr->snr_33_100_dB++;
	} else {
		is_snr->snr_abnormal++;
		ns_snr->snr_abnormal++;
	}

	return 0;
failed:
	return -1;
}

int zyUMAC_stats_update_peer_stats(struct ieee80211_node *ni,
				  wmi_host_peer_stats * peer_stats)
{
	struct ol_ath_softc_net80211 *scn = NULL;
	struct ieee80211com *ic = NULL;
	struct ieee80211vap *vap = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;

	if (!ni || !peer_stats) {
		goto failed;
	}

	if ((vap = ni->ni_vap) == NULL) {
		goto failed;
	}

	if ((ic = vap->iv_ic) == NULL) {
		goto failed;
	}

	if ((scn = OL_ATH_SOFTC_NET80211(ic)) == NULL) {
		goto failed;
	}

	if ((zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv) == NULL) {
		goto failed;
	}

	if (ni->ni_zyUMAC_node_priv == NULL) {
		ni->ni_zyUMAC_node_priv =
		    OS_MALLOC(vap->iv_ic->ic_osdev, sizeof(zyUMAC_node_priv_t),
			      GFP_KERNEL);
		OS_MEMSET(ni->ni_zyUMAC_node_priv, 0, sizeof(zyUMAC_node_priv_t));
	}

	if (ni->ni_zyUMAC_node_priv == NULL) {
		// @TODO:
		zyUMAC_LOG(LOG_ERR, "cannot create node priv");
		goto failed;
	}

	if (ni->ni_rssi_min < zyUMAC_scn_priv->scn_radio_stats.rx_rssi_lowest) {
		zyUMAC_scn_priv->scn_radio_stats.rx_rssi_lowest =
		    ni->ni_rssi_min;
	}
	if (ni->ni_rssi_max > zyUMAC_scn_priv->scn_radio_stats.rx_rssi_highest) {
		zyUMAC_scn_priv->scn_radio_stats.rx_rssi_highest =
		    ni->ni_rssi_max;
	}

	zyUMAC_vap_node_update_rssi(ni,
				   peer_stats->peer_rssi +
				   DEFAULT_CHAN_REF_NOISE_FLOOR, vap);
	zyUMAC_vap_node_update_snr(ni, peer_stats->peer_rssi, vap);

	return 0;

failed:
	return -1;
}

EXPORT_SYMBOL(zyUMAC_stats_update_peer_stats);

void zyUMAC_stats_tx_complete_update_txrate(struct ol_ath_softc_net80211 *scn,
					   struct ieee80211vap *vap)
{
	u_int32_t preamble = 0, nss = 0, mcs = 0, sgi = 0, bw = 0;
	struct ieee80211com *ic;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	struct ieee80211_tx_status *ts = NULL;

	if (!scn || !vap) {
		return;
	}

	if ((zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv) == NULL) {
		return;
	}

	ic = &(scn->sc_ic);
	if (!ic) {
		return;
	}

	ts = scn->tx_status_buf;
	if (!ts) {
		return;
	}

	preamble = HTT_GET_HW_RATECODE_PREAM(ts->ppdu_rate);
	nss = HTT_GET_HW_RATECODE_NSS(ts->ppdu_rate);
	mcs = HTT_GET_HW_RATECODE_RATE(ts->ppdu_rate);
	bw = ts->ch_width;
	if (qdf_likely(vap)) {
		sgi = vap->iv_data_sgi;
	}

	switch (preamble) {
	case 0:		// CCK
		if (mcs > 3) {
			return;
		}
		break;
	case 1:		// OFDM
		if (mcs > 7) {
			return;
		}
		break;
	case 2:		// HT
		if (mcs > 7) {
			return;
		}
		if (bw == 0) {
			preamble = 12;	// HT20
		} else {
			preamble = 13;	// HT40
		}
		if ((nss == 1) || (nss == 3)) {
			mcs += 8;
		}

		break;
	case 3:		// VHT
		if (mcs > 9) {
			return;
		}
		if (bw == 0) {
			preamble = 14;	// VHT20
		} else if (bw == 1) {
			preamble = 15;	// VHT40
		} else if (bw == 2) {
			preamble = 16;	// VHT80
		} else if (bw == 3) {
			preamble = 17;	// VHT160
		}
		break;
	}

#if (SPFVER <= 9)
	kbps = ic->ic_whal_mcs_to_kbps(preamble, mcs, nss, sgi);
	if (!kbps) {
		return;
	}

	for (i = 0; i < OL_RATE_TABLE_SIZE; i++) {
		if (zyUMAC_scn_priv->scn_radio_stats.txratecnt[i].rateval == 0) {
			zyUMAC_scn_priv->scn_radio_stats.txratecnt[i].framecnt++;
			zyUMAC_scn_priv->scn_radio_stats.txratecnt[i].rateval =
			    kbps;
			break;
		} else if (zyUMAC_scn_priv->scn_radio_stats.txratecnt[i].
			   rateval == kbps) {
			zyUMAC_scn_priv->scn_radio_stats.txratecnt[i].framecnt++;
			break;
		}
	}
#else
	//TODO
#endif
}

void zyUMAC_stats_tx_complete_update(struct ol_ath_softc_net80211 *scn,
				    struct ieee80211vap *vap,
				    qdf_nbuf_t netbuf,
				    enum htt_tx_status status)
{
	struct ieee80211_node *ni = NULL;
	struct ethhdr *eh = (struct ethhdr *)qdf_nbuf_data(netbuf);
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	int is_bcast = 0;
	int is_mcast = 0;
	int byte_cnt = qdf_nbuf_len(netbuf) - sizeof(struct ethhdr);

	if (!scn || !vap) {
		return;
	}

	if ((zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv) == NULL) {
		return;
	}

	zyUMAC_stats_tx_complete_update_txrate(scn, vap);

	is_mcast = IEEE80211_IS_MULTICAST(eh->h_dest);
	is_bcast = IEEE80211_IS_BROADCAST(eh->h_dest);

	if (qdf_likely(vap)) {
		zyUMAC_scn_priv_radio_stats_t *stats =
		    &zyUMAC_scn_priv->scn_radio_stats;
		zyUMAC_vap_priv_t *zyUMAC_vap_priv =
		    (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
		zyUMAC_vap_priv_stats_t *vap_stats = &zyUMAC_vap_priv->is_stats;
		struct ieee80211com *ic = &scn->sc_ic;

		if (qdf_likely(ic)) {
			stats->tx_data_bytes += byte_cnt;
			if (is_mcast || is_bcast) {
				stats->tx_mcast++;
				stats->tx_mcast_bytes += byte_cnt;
			} else if (status == htt_tx_status_ok) {
				stats->tx_ucast++;
				stats->tx_ucast_bytes += byte_cnt;
			}
			if (byte_cnt > 1024) {
				stats->txmsduthan1024++;
			} else if (byte_cnt > 512) {
				stats->txmsdu512to1024++;
			} else if (byte_cnt > 128) {
				stats->txmsdu128to512++;
			} else {
				stats->txmsdu0to128++;
			}
		}

		vap_stats->is_tx_packets++;
		vap_stats->is_tx_bytes += byte_cnt;
		vap_stats->is_tx_data++;
		vap_stats->is_tx_data_bytes += byte_cnt;

		if (is_mcast || is_bcast) {
			if (is_bcast) {
				vap_stats->is_tx_bcast++;
				vap_stats->is_tx_bcast_bytes += byte_cnt;
			}
			return;
		}

		if (status == htt_tx_status_ok) {
			vap_stats->is_tx_unicast++;
			vap_stats->is_tx_unicast_bytes += byte_cnt;
		} else {
			vap_stats->is_tx_drop++;
			vap_stats->is_tx_drop_bytes += byte_cnt;
		}

		ni = zyUMAC_ieee80211_vap_find_node(vap, eh->h_dest);

		if (ni) {
			zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
			zyUMAC_node_priv_stats_t *node_stats;

			if ((zyUMAC_node_priv =
			     (zyUMAC_node_priv_t *) ni->ni_zyUMAC_node_priv) ==
			    NULL) {
				zyUMAC_ieee80211_free_node(ni);
				return;
			}
			node_stats = &zyUMAC_node_priv->ns_stats;

			if (status == htt_tx_status_ok) {
				node_stats->tx_good_bytes +=
				    byte_cnt;
				node_stats->tx_good_pkts++;
			} else {
				node_stats->tx_bad_bytes += byte_cnt;
				node_stats->tx_bad_pkts++;
			}

			if (!(ni->ni_flags & IEEE80211_NODE_AUTH)) {
				node_stats->tx_data_unauthed_pktes++;
				node_stats->tx_data_unauthed_bytes +=
				    byte_cnt;
			}

			zyUMAC_ieee80211_free_node(ni);
		}
	}
}

EXPORT_SYMBOL(zyUMAC_stats_tx_complete_update);

void zyUMAC_stats_rx_deliver_update(struct ol_ath_softc_net80211 *scn,
				   struct ieee80211vap *vap,
				   struct ol_txrx_peer_t *peer,
				   qdf_nbuf_t msdu,
				   htt_pdev_handle htt_pdev,
				   enum htt_rx_status status)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	zyUMAC_vap_priv_stats_t *vap_stats = NULL;
	zyUMAC_node_priv_stats_t *node_stats = NULL;
	struct ieee80211_node *ni = NULL;
	uint32_t data_length = 0;
	void *rx_desc;
	struct ethhdr *eh = NULL;
	int is_mcast;
	int is_bcast;

	if (!scn || !vap) {
		return;
	}

	zyUMAC_vap_priv = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	eh = (struct ethhdr *)qdf_nbuf_data(msdu);

	zyUMAC_vap_priv = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	if (zyUMAC_vap_priv) {
		vap_stats = &zyUMAC_vap_priv->is_stats;
	}

	if (!vap_stats) {
		return;
	}

	switch (status) {
	case HTT_RX_IND_MPDU_STATUS_OK:
		ni = zyUMAC_ieee80211_vap_find_node(vap, eh->h_source);

		if (!ni) {
			zyUMAC_LOG(LOG_ERR, "src %pM", eh->h_source);
			return;
		}

		zyUMAC_node_priv = (zyUMAC_node_priv_t *) ni->ni_zyUMAC_node_priv;
		if (zyUMAC_node_priv) {
			node_stats = &zyUMAC_node_priv->ns_stats;
			if (node_stats == NULL) {
				zyUMAC_ieee80211_free_node(ni);
				zyUMAC_LOG(LOG_ERR, "no node stats");
				return;
			}
		}

		while (msdu) {
			zyUMAC_scn_priv_t *zyUMAC_scn_priv =
			    (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
			zyUMAC_scn_priv_radio_stats_t *stats =
			    &zyUMAC_scn_priv->scn_radio_stats;

			rx_desc = htt_rx_msdu_desc_retrieve(htt_pdev, msdu);
			data_length =
			    htt_pdev->ar_rx_ops->
			    msdu_desc_msdu_length(rx_desc) -
			    sizeof(struct ethhdr);

			is_mcast = IEEE80211_IS_MULTICAST(eh->h_dest);
			is_bcast = IEEE80211_IS_BROADCAST(eh->h_dest);

			vap_stats->is_rx_packets++;
			vap_stats->is_rx_bytes += data_length;
			vap_stats->is_rx_data++;
			vap_stats->is_rx_data_bytes += data_length;
			if (is_mcast) {
				vap_stats->is_rx_nunicast++;
				vap_stats->is_rx_nunicast_bytes += data_length;
			} else if (is_bcast) {
				vap_stats->is_rx_bcast++;
				vap_stats->is_rx_bcast_bytes += data_length;
			} else {
				vap_stats->is_rx_unicast++;
				vap_stats->is_rx_unicast_bytes += data_length;
				stats->rx_ucast++;
			}

			if (data_length > 1024) {
				stats->rxmsduthan1024++;
			} else if (data_length > 512) {
				stats->rxmsdu512to1024++;
			} else if (data_length > 128) {
				stats->rxmsdu128to512++;
			} else {
				stats->rxmsdu0to128++;
			}

			node_stats->an_framereceive[WME_AC_BE]++;
			node_stats->an_bytesreceive[WME_AC_BE] += data_length;

			if (!(ni->ni_flags & IEEE80211_NODE_AUTH)) {
				node_stats->rx_data_unauthed_pktes++;
				node_stats->rx_data_unauthed_bytes +=
				    data_length;
			}
			msdu = qdf_nbuf_next(msdu);
		}
		zyUMAC_ieee80211_free_node(ni);

		break;
	case HTT_RX_IND_MPDU_STATUS_ERR_FCS:
		/* ignore data length for FCS err */
		if (zyUMAC_vap_priv) {
			zyUMAC_vap_priv->is_stats.is_rx_drop++;
		}
		break;
	case HTT_RX_IND_MPDU_STATUS_TKIP_MIC_ERR:
	case HTT_RX_IND_MPDU_STATUS_DECRYPT_ERR:
		if (zyUMAC_vap_priv) {
			zyUMAC_vap_priv->is_stats.is_rx_drop++;
			zyUMAC_vap_priv->is_stats.is_rx_drop_bytes +=
			    data_length;
		}
		break;
	default:
		break;
	}
}

EXPORT_SYMBOL(zyUMAC_stats_rx_deliver_update);

void zyUMAC_stats_update_radio_stats(struct ol_ath_softc_net80211 *scn)
{
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_scn_priv_radio_stats_t *stats;

	if (!scn) {
		return;
	}

	if ((zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv) == NULL) {
		return;
	}

	stats = &zyUMAC_scn_priv->scn_radio_stats;

	//update busy cycle
	do {
		stats->tx_cycle = scn->scn_dcs.chan_stats.tx_frame_count;
		stats->rx_cycle = scn->scn_dcs.chan_stats.my_bss_rx_cycle_count;
		stats->busy_cycle = scn->scn_dcs.chan_stats.rx_clear_count;
		stats->total_cycle = scn->scn_dcs.chan_stats.cycle_count;
	} while ((stats->tx_cycle != scn->scn_dcs.chan_stats.tx_frame_count)
		 || (stats->rx_cycle !=
		     scn->scn_dcs.chan_stats.my_bss_rx_cycle_count)
		 || (stats->busy_cycle !=
		     scn->scn_dcs.chan_stats.rx_clear_count)
		 || (stats->total_cycle !=
		     scn->scn_dcs.chan_stats.cycle_count));
}

EXPORT_SYMBOL(zyUMAC_stats_update_radio_stats);

void zyUMAC_stats_update_vap_stats(struct ol_ath_softc_net80211 *scn,
				  struct ieee80211vap *vap,
				  wmi_host_vdev_stats * vdev_stats)
{
//TODO
}

EXPORT_SYMBOL(zyUMAC_stats_update_vap_stats);
