#ifndef ZYXEL_UMAC_WAL_STATS_HEADER_H
#define ZYXEL_UMAC_WAL_STATS_HEADER_H

#include <ol_txrx_types.h>
#include "zyUMAC_types.h"

int zyUMAC_stats_update_peer_stats(struct ieee80211_node *ni,
				  wmi_host_peer_stats * peer_stats);

#endif
