/*
 * Zyxel  Micro UMAC module (zyUMAC).
 */

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,38)
#ifndef AUTOCONF_INCLUDED
#include <linux/config.h>
#endif
#endif
#include <linux/version.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/wireless.h>
#include <net/iw_handler.h>

#include <if_athioctl.h>
#include "ol_if_athvar.h"
#include "ol_ath_ucfg.h"
#include "osif_private.h"

#include "zyUMAC_wal_types.h"
#include "zyUMAC_wal_vap.h"
#include "zyUMAC_wal_mgmt_hook.h"
#include "zyUMAC_log.h"

static struct iw_priv_args *zyUMAC_ath_iw_priv_args = NULL;

static iw_handler orig_ath_iw_setparam = NULL;
static iw_handler orig_ath_iw_getparam = NULL;
static iw_handler_hook _zyUMAC_iw_setparam = NULL;
static iw_handler_hook _zyUMAC_iw_getparam = NULL;

static int (*orig_ath_ioctl) (struct net_device * dev, struct ifreq * ifr,
			      int cmd) = NULL;

static int zyUMAC_ath_iw_setparam(struct net_device *dev,
				 struct iw_request_info *info,
				 void *w, char *extra)
{
	int res = _zyUMAC_iw_setparam(dev, info, w, extra, orig_ath_iw_setparam);

	if (res != -1) {
		return 0;
	} else {
		return orig_ath_iw_setparam(dev, info, w, extra);
	}
}

static int zyUMAC_ath_iw_getparam(struct net_device *dev,
				 struct iw_request_info *info,
				 void *w, char *extra)
{
	int res = _zyUMAC_iw_getparam(dev, info, w, extra, orig_ath_iw_getparam);

	if (res != -1) {
		return 0;
	} else {
		return orig_ath_iw_getparam(dev, info, w, extra);
	}
}

static int zyUMAC_ath_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd)
{
	wlan_if_t vap;
	int rtn = orig_ath_ioctl(dev, ifr, cmd);

	if (!rtn) {
		//here dev is for wifiX
		//using this to find the last added vap
		//instead of using it as athX vap
		struct ol_ath_softc_net80211 *scn = ath_netdev_priv(dev);
		struct ieee80211com *ic = &scn->sc_ic;
		switch (cmd) {
		case SIOC80211IFCREATE:
			{
				vap = TAILQ_LAST(&ic->ic_vaps, ic_vaps_head);
				zyUMAC_vap_create_completed(vap);
				zyUMAC_mgmt_attach(ath_netdev_priv(dev));
			}
			break;

		default:
			break;
		}
	}
	return rtn;
}

/*
 * Replace all necessary ic function pointer here, call from __ol_ath_attach of qca_ol module
 */
int __zyUMAC_wal_wphy_ioctl_attach(struct ol_ath_softc_net80211 *scn,
				  zyUMAC_wal_ioctl_hook_table * ioctl_hook_table)
{
	iw_handler *private_handlers;
	struct net_device *dev;
	struct iw_handler_def *orig_ol_ath_iw_handler_def = NULL;
	int new_iw_priv_args_size = 0;
	osif_dev *osifp = NULL;

	ASSERT(scn != NULL);

#if UMAC_SUPPORT_WEXT
	ASSERT(scn->netdev != NULL);
	dev = scn->netdev;
#else
	ASSERT(scn->sc_osdev != NULL);
	ASSERT(scn->sc_osdev->netdev != NULL);
	dev = scn->sc_osdev->netdev;
#endif

	ASSERT(dev != NULL);

	_zyUMAC_iw_setparam = ioctl_hook_table->zyUMAC_param_set_handler;
	_zyUMAC_iw_getparam = ioctl_hook_table->zyUMAC_param_get_handler;

	orig_ol_ath_iw_handler_def =
	    (struct iw_handler_def *)dev->wireless_handlers;
	ASSERT(orig_ol_ath_iw_handler_def != NULL);

	//replace original iwpriv table with new one
	if (!zyUMAC_ath_iw_priv_args && ioctl_hook_table->zyUMAC_params_size) {
		struct iw_priv_args *orig_ath_iw_priv_args = NULL;
		orig_ath_iw_priv_args =
		    (struct iw_priv_args *)orig_ol_ath_iw_handler_def->
		    private_args;
		if (orig_ath_iw_priv_args == NULL) {
			zyUMAC_LOG(LOG_ERR, "orig_ath_iw_priv_args is null");
			return 0;
		}

		osifp = ath_netdev_priv(dev);
		new_iw_priv_args_size =
		    orig_ol_ath_iw_handler_def->num_private_args +
		    ioctl_hook_table->zyUMAC_params_size;
		zyUMAC_ath_iw_priv_args =
		    (struct iw_priv_args *)OS_MALLOC(osifp->os_handle,
						     new_iw_priv_args_size *
						     sizeof(struct
							    iw_priv_args),
						     GFP_KERNEL);
		if (!zyUMAC_ath_iw_priv_args) {
			zyUMAC_LOG(LOG_ERR, "zyUMAC_ath_iw_priv_args is null");
			return 0;
		}
		OS_MEMCPY(zyUMAC_ath_iw_priv_args, orig_ath_iw_priv_args,
			  orig_ol_ath_iw_handler_def->num_private_args *
			  sizeof(struct iw_priv_args));
		OS_MEMCPY(zyUMAC_ath_iw_priv_args +
			  orig_ol_ath_iw_handler_def->num_private_args,
			  ioctl_hook_table->zyUMAC_params,
			  ioctl_hook_table->zyUMAC_params_size *
			  sizeof(struct iw_priv_args));
		orig_ol_ath_iw_handler_def->private_args =
		    zyUMAC_ath_iw_priv_args;
		orig_ol_ath_iw_handler_def->num_private_args =
		    new_iw_priv_args_size;
	}

	private_handlers = (iw_handler *) orig_ol_ath_iw_handler_def->private;
	if (private_handlers == NULL) {
		zyUMAC_LOG(LOG_ERR, "private_handlers is null");
		return 0;
	}

	if (orig_ath_iw_setparam == NULL) {
		orig_ath_iw_setparam = private_handlers[0];
		private_handlers[0] = (iw_handler) zyUMAC_ath_iw_setparam;
	}

	if (orig_ath_iw_getparam == NULL) {
		orig_ath_iw_getparam = private_handlers[1];
		private_handlers[1] = (iw_handler) zyUMAC_ath_iw_getparam;
	}

	ASSERT(dev->netdev_ops);
	if (dev->netdev_ops && orig_ath_ioctl == NULL) {
		orig_ath_ioctl = dev->netdev_ops->ndo_do_ioctl;
	}
	((struct net_device_ops *)(dev->netdev_ops))->ndo_do_ioctl =
	    zyUMAC_ath_ioctl;

	return 0;
}

void __zyUMAC_wal_wphy_ioctl_detach(struct ol_ath_softc_net80211 *scn)
{
	if (zyUMAC_ath_iw_priv_args) {
		OS_FREE(zyUMAC_ath_iw_priv_args);
		zyUMAC_ath_iw_priv_args = NULL;
	}
}
