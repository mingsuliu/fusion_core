#ifndef ZYXEL_UMAC_WAL_MGMT_HOOKER_HEADER_H
#define ZYXEL_UMAC_WAL_MGMT_HOOKER_HEADER_H

int zyUMAC_mgmt_attach(struct ol_ath_softc_net80211 *scn);

int zyUMAC_wlan_receive_filter_80211(os_if_t osif, wbuf_t wbuf,
				    u_int16_t type, u_int16_t subtype,
				    ieee80211_recv_status * rs);
extern int (*origional_wlan_receive_filter_80211) (os_if_t osif, wbuf_t wbuf,
						   u_int16_t type,
						   u_int16_t subtype,
						   ieee80211_recv_status * rs);
QDF_STATUS zyUMAC_mgmt_tx_completion_handler(struct wlan_objmgr_pdev *pdev,
					    uint32_t desc_id, uint32_t status,
					    void *tx_compl_params);

#endif
