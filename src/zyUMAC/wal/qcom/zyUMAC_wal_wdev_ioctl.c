/*
 * Zyxel Micro UMAC module (zyUMAC).
 */

#include <linux/netdevice.h>
#include <linux/wireless.h>
#include <net/iw_handler.h>
#include "osif_net.h"

#include "zyUMAC_umac.h"
#include "zyUMAC_wal_types.h"
#include "zyUMAC_ieee80211.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_event.h"
#include "zyUMAC_compat.h"

#define MAX_APPS_NUM 10
#define MAX_APPS_NAME_LEN 20
#define HOSTAPD_PROCESS_NAME    "hostapd"

//static char apps_name[MAX_APPS_NUM][MAX_APPS_NAME_LEN] = {"hostapd"};
static iw_handler orig_ieee80211_setmlme = NULL;
static iw_handler orig_ieee80211_kickmac = NULL;
static iw_handler orig_ieee80211_setparam = NULL;
static iw_handler orig_ieee80211_getparam = NULL;

static iw_handler_hook _zyUMAC_ieee80211_setparam = NULL;
static iw_handler_hook _zyUMAC_ieee80211_getparam = NULL;
static struct iw_priv_args *zyUMAC_vap_iw_priv_args = NULL;

static int zyUMAC_ieee80211_setmlme(struct net_device *dev,
				   struct iw_request_info *info,
				   void *w, char *extra)
{
	struct ieee80211req_mlme *mlme = (struct ieee80211req_mlme *)extra;
	sender_info_t *p_sender_info = (sender_info_t *) w;
	uint16_t sender = 0;
	osif_dev *osifp = ath_netdev_priv(dev);
	wlan_if_t vap = osifp->os_if;
	//struct ieee80211com *ic = NULL;
	//struct ieee80211_app_ie_t optie;
	struct ieee80211_node *ni = NULL;
	int res;
	int reason = 0;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;

	res = orig_ieee80211_setmlme(dev, info, w, extra);
	if (res != 0) {
		goto fail;
	}

	if (osifp->os_opmode == IEEE80211_M_HOSTAP) {
		goto done;
	}

	if (p_sender_info && (p_sender_info->type == ZY_SENDER_TYPE)) {
		sender = p_sender_info->sender;
	} else
	    if (!strncmp
		(current->comm, HOSTAPD_PROCESS_NAME,
		 strlen(HOSTAPD_PROCESS_NAME))) {
		sender = zyUMAC_AP_HOSTAPD;
	}

	ni = zyUMAC_ieee80211_vap_find_node(vap, mlme->im_macaddr);
	if (ni == NULL) {
		goto done;
	}

	zyUMAC_node_priv = (zyUMAC_node_priv_t *) ni->ni_zyUMAC_node_priv;
	if (zyUMAC_node_priv != NULL) {
		zyUMAC_node_priv->wal_wnode_priv.mlme_cmd_sender = sender;
	}

	reason = (sender << 16) | mlme->im_reason;
	switch (mlme->im_op) {
	case IEEE80211_MLME_ASSOC:
		{
			zyUMAC_wal_event_mlme_assoc_ind_t event = {
				.wdev = vap,
				.macaddr = mlme->im_macaddr,
				.result = reason,
				.reassoc = false
			};

			zyUMAC_wal_event_publish
			    (zyUMAC_WAL_EVENT_MLME_ASSOC_INDICATION, &event);
			break;
		}

	case IEEE80211_MLME_REASSOC:
		{
			zyUMAC_wal_event_mlme_assoc_ind_t event = {
				.wdev = vap,
				.macaddr = mlme->im_macaddr,
				.result = reason,
				.reassoc = true
			};

			zyUMAC_wal_event_publish
			    (zyUMAC_WAL_EVENT_MLME_ASSOC_INDICATION, &event);
			break;
		}

	default:
		break;
	}

	zyUMAC_ieee80211_free_node(ni);

done:
	return 0;

fail:
	return res;
}

static int zyUMAC_ieee80211_ioctl_kickmac(struct net_device *dev,
					 struct iw_request_info *info, void *w,
					 char *extra)
{
	struct sockaddr *sa = (struct sockaddr *)extra;
	struct ieee80211req_mlme mlme;
	sender_info_t sender = { 0 };

	if (sa->sa_family != ARPHRD_ETHER) {
		return -EINVAL;
	}

	zyUMAC_LOG(LOG_INFO, "%p", w);

	/* Setup a MLME request for disassociation of the given MAC */
	mlme.im_op = IEEE80211_MLME_DISASSOC;
	mlme.im_reason = ZY_REASON_KICK_BY_IWPRIV;
	sender.type = ZY_SENDER_TYPE;
	sender.sender = zyUMAC_AP_IWPRIV_KICK_OUT;

	IEEE80211_ADDR_COPY(&(mlme.im_macaddr), sa->sa_data);

	/* Send the MLME request and return the result. */
	return zyUMAC_ieee80211_setmlme(dev, info, (void *)&sender,
				       (char *)&mlme);
}

static int zyUMAC_ieee80211_setparam(struct net_device *dev,
				    struct iw_request_info *info,
				    void *w, char *extra)
{
	int res =
	    _zyUMAC_ieee80211_setparam(dev, info, w, extra,
				      orig_ieee80211_setparam);

	if (res != -1) {
		return 0;
	} else {
		return orig_ieee80211_setparam(dev, info, w, extra);
	}
}

static int zyUMAC_ieee80211_getparam(struct net_device *dev,
				    struct iw_request_info *info,
				    void *w, char *extra)
{
	int res;

	res =
	    _zyUMAC_ieee80211_getparam(dev, info, w, extra,
				      orig_ieee80211_getparam);

	if (res != -1) {
		return 0;
	} else {
		return orig_ieee80211_getparam(dev, info, w, extra);
	}
}

int __zyUMAC_wal_wdev_mlme_request(zyUMAC_wal_wdev_t wdev,
				  zyUMAC_wal_req_mlme_t * request)
{
	struct ieee80211req_mlme mlme;
	struct net_device *dev = OSIF_TO_NETDEV(wdev->iv_ifp);

	mlme.im_op = request->mlme_op;
	mlme.im_reason = (request->mlme_sender << 16) | request->mlme_reason;
	IEEE80211_ADDR_COPY(&(mlme.im_macaddr), request->mlme_macaddr);

	/* Send the MLME request and return the result. */
	return zyUMAC_ieee80211_setmlme(dev, NULL, NULL, (char *)&mlme);
}

/*
 * Replace handlers for ieee80211_ioctl_vattach(dev)
 */
int __zyUMAC_wal_wdev_ioctl_attach(zyUMAC_wal_wdev_t vap,
				  zyUMAC_wal_ioctl_hook_table * ioctl_hook_table)
{
#define IW(idx) (idx - SIOCSIWCOMMIT)
	iw_handler *private_handlers = NULL, *standard_handlers = NULL;
	struct net_device *dev = NULL;
	struct iw_handler_def *orig_vap_iw_handler_def = NULL;
	int new_iw_priv_args_size = 0;
	osif_dev *osifp = NULL;
	ASSERT(vap != NULL);
	ASSERT(vap->iv_ifp != NULL);
	ASSERT(((osif_dev *) vap->iv_ifp)->netdev != NULL);

	dev = ((osif_dev *) vap->iv_ifp)->netdev;
	osifp = (osif_dev *) vap->iv_ifp;
	//replace original iwpriv table with new one
	orig_vap_iw_handler_def =
	    (struct iw_handler_def *)dev->wireless_handlers;
	ASSERT(orig_vap_iw_handler_def != NULL);

	_zyUMAC_ieee80211_setparam = ioctl_hook_table->zyUMAC_param_set_handler;
	_zyUMAC_ieee80211_getparam = ioctl_hook_table->zyUMAC_param_get_handler;

	if (!zyUMAC_vap_iw_priv_args && ioctl_hook_table->zyUMAC_params_size) {
		struct iw_priv_args *orig_vap_iw_priv_args = NULL;
		orig_vap_iw_priv_args =
		    (struct iw_priv_args *)orig_vap_iw_handler_def->
		    private_args;
		if (orig_vap_iw_priv_args == NULL) {
			zyUMAC_LOG(LOG_ERR, "orig_vap_iw_priv_args is null");
			return -1;
		}

		osifp = ath_netdev_priv(dev);
		new_iw_priv_args_size =
		    orig_vap_iw_handler_def->num_private_args +
		    ioctl_hook_table->zyUMAC_params_size;
		zyUMAC_vap_iw_priv_args =
		    (struct iw_priv_args *)OS_MALLOC(osifp->os_handle,
						     new_iw_priv_args_size *
						     sizeof(struct
							    iw_priv_args),
						     GFP_KERNEL);
		if (!zyUMAC_vap_iw_priv_args) {
			zyUMAC_LOG(LOG_ERR, "zyUMAC_vap_iw_priv_args is null");
			return -1;
		}
		OS_MEMCPY(zyUMAC_vap_iw_priv_args, orig_vap_iw_priv_args,
			  orig_vap_iw_handler_def->num_private_args *
			  sizeof(struct iw_priv_args));
		OS_MEMCPY(zyUMAC_vap_iw_priv_args +
			  orig_vap_iw_handler_def->num_private_args,
			  ioctl_hook_table->zyUMAC_params,
			  ioctl_hook_table->zyUMAC_params_size *
			  sizeof(struct iw_priv_args));
		orig_vap_iw_handler_def->private_args = zyUMAC_vap_iw_priv_args;
		orig_vap_iw_handler_def->num_private_args =
		    new_iw_priv_args_size;
	}

	standard_handlers = (iw_handler *) orig_vap_iw_handler_def->standard;
	private_handlers = (iw_handler *) orig_vap_iw_handler_def->private;

	if (!standard_handlers || !private_handlers)
		return -1;

    /*** private ***/
#ifdef ZY_LEAVE_REPORT_SUPPORT
	if (orig_ieee80211_setmlme == NULL) {	/* SIOCWFIRSTPRIV+6 */
		orig_ieee80211_setmlme = private_handlers[6];
		private_handlers[6] = (iw_handler) zyUMAC_ieee80211_setmlme;
	}

	if (orig_ieee80211_kickmac == NULL) {	/* SIOCWFIRSTPRIV+15 */
		orig_ieee80211_kickmac = private_handlers[15];
		private_handlers[15] =
		    (iw_handler) zyUMAC_ieee80211_ioctl_kickmac;
	}
#endif

	if (orig_ieee80211_setparam == NULL) {	/* SIOCWFIRSTPRIV+0 */
		orig_ieee80211_setparam = private_handlers[0];
		private_handlers[0] = (iw_handler) zyUMAC_ieee80211_setparam;
	}

	if (orig_ieee80211_getparam == NULL) {	/* SIOCWFIRSTPRIV+1 */
		orig_ieee80211_getparam = private_handlers[1];
		private_handlers[1] = (iw_handler) zyUMAC_ieee80211_getparam;
	}

	return 0;
#undef IW
}

//call only when driver unload/detach
void __zyUMAC_wal_wdev_ioctl_detach(void)
{
	if (zyUMAC_vap_iw_priv_args) {
		OS_FREE(zyUMAC_vap_iw_priv_args);
		zyUMAC_vap_iw_priv_args = NULL;
	}
}
