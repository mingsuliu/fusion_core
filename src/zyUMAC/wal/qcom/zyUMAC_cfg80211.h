#ifndef ZYXEL_UMAC_CFG80211_HEADER_H
#define ZYXEL_UMAC_CFG80211_HEADER_H

#if UMAC_SUPPORT_CFG80211
#include <ieee80211_cfg80211.h>
#endif

//#include "zyUMAC.h"
#include "zyUMAC_common_var.h"

int zyUMAC_cfg80211_radio_attach(struct device *dev, struct net_device *net_dev,
				struct ieee80211com *ic);

#endif
