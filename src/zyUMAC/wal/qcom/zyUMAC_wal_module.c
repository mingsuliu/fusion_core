#include <linux/module.h>
#include <ol_if_athvar.h>
#include <wdi_event_api.h>
#include <wmi_unified_priv.h>
#ifdef UMAC_SUPPORT_CFG80211
#include <init_deinit_lmac.h>
#include "cdp_txrx_ctrl.h"
#include "cdp_txrx_cmn_struct.h"
#include "dp_ratetable.h"
#endif
#include "ath_netlink.h"

#include "zyUMAC_umac.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_mgmt_hook.h"

extern int zyUMAC_wal_stats_attach(struct ol_ath_softc_net80211 *scn);
extern void zyUMAC_wal_stats_detach(struct ol_ath_softc_net80211 *scn);
#if UMAC_SUPPORT_CFG80211
extern struct ieee80211_node *zyUMAC_WAL_ol_ath_node_alloc(struct ieee80211vap *vap,
						      const u_int8_t * macaddr,
						      bool tmpnode, void *peer);
#else
extern struct ieee80211_node *zyUMAC_WAL_ol_ath_node_alloc(struct ieee80211vap *vap,
						      const u_int8_t * macaddr,
						      bool tmpnode);
#endif
extern void zyUMAC_WAL_ol_ath_node_free(struct ieee80211_node *ni);

extern zyUMAC_module_hooks_table *_zyUMAC_module_hooks;

int zyUMAC_wal_attach(zyUMAC_wal_wphy_t scn, int num_radios)
{
	struct ieee80211com *ic = &scn->sc_ic;
	zyUMAC_scn_priv_t *scn_priv = NULL;
	int ret;

	ASSERT(ic != NULL);
	qdf_print("Attaching Zyxel UMAC WAL");

	//ret = zyUMAC_umac_attach(scn);
	ret = _zyUMAC_module_hooks->module_hook_attach(scn);
	scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	if (!scn_priv) {
		qdf_print("scn_zyUMAC_priv is nul!!");
		return -1;
	}

	zyUMAC_mgmt_attach(scn);

	//node allocate & free
	scn_priv->orig_ic_node_alloc = ic->ic_node_alloc;
	ic->ic_node_alloc = zyUMAC_WAL_ol_ath_node_alloc;

	scn_priv->orig_ic_node_free = ic->ic_node_free;
	ic->ic_node_free = zyUMAC_WAL_ol_ath_node_free;

	zyUMAC_wal_stats_attach(scn);

	return ret;
}

void zyUMAC_wal_detach(zyUMAC_wal_wphy_t wphy)
{
	qdf_print("Detaching Zyxel UMAC WAL");
	zyUMAC_wal_stats_detach(wphy);
	_zyUMAC_module_hooks->module_hook_detach(wphy);
}

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif
EXPORT_SYMBOL(zyUMAC_wal_attach);
EXPORT_SYMBOL(zyUMAC_wal_detach);
