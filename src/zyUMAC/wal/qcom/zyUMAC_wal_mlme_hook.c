#include <string.h>
#include <ieee80211.h>
#include <ieee80211_defines.h>
#include "zyUMAC_api.h"
#include "zyUMAC_ieee80211.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_event.h"
#include "zyUMAC_wal_mlme_hook.h"

static int zyUMAC_wal_event_reason_converter(int result)
{
	int newreason;

	if (result > ACFG_PAD_TO_ZY_REASON &&
	    result <= (ACFG_PAD_TO_ZY_REASON +
		       ACFG_REASON_ASSOC_CHANWIDTH_MISMATCH)) {

		switch (result & 0xF) {
		case ACFG_REASON_ASSOC_RATE_MISMATCH:
		case ACFG_REASON_ASSOC_11AC_TKIP_USED:
			newreason = IEEE80211_STATUS_BASIC_RATE;
			break;
		case ACFG_REASON_ASSOC_PUREAC_NOVHT:
			newreason = IEEE80211_STATUS_NO_VHT;
			break;
		case ACFG_REASON_ASSOC_PUREN_NOHT:
			newreason = IEEE80211_STATUS_NO_HT;
			break;
		case ACFG_REASON_ASSOC_CHANWIDTH_MISMATCH:
			newreason = IEEE80211_STATUS_LACK_BANDWIDTH;
			break;
		case ACFG_REASON_ASSOC_CAP_MISMATCH:
			newreason = IEEE80211_STATUS_CAPINFO;
			break;
		default:
			break;
		}
	}
	return newreason;
}

/*
 * handler called when the AP vap comes up asynchronously.
 * (happens only when resource  manager is present).
 */
static void zyUMAC_create_infra_complete(os_handle_t osif,
					IEEE80211_STATUS status)
{
	return;
}

static void zyUMAC_auth_complete_ap(os_handle_t osif, u_int8_t *macaddr, IEEE80211_STATUS status)
{
	return;
}

static void
zyUMAC_assoc_complete_ap(os_handle_t osif, IEEE80211_STATUS status,
			u_int16_t aid, wbuf_t wbuf)
{
	return;
}

static void
zyUMAC_deauth_complete_ap(os_handle_t osif, u_int8_t * macaddr,
			 IEEE80211_STATUS status, u_int16_t associd,
			 u_int16_t reason)
{
	zyUMAC_wal_event_mlme_deauth_complete_t event = {
		.wdev = ((osif_dev *) osif)->os_if,
		.macaddr = macaddr,
		.status = reason
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_MLME_DEAUTH_COMPLETE, &event);
	return;
}

static void
zyUMAC_auth_indication_ap(os_handle_t osif, u_int8_t * macaddr, u_int16_t status)
{
	zyUMAC_wal_event_mlme_auth_ind_t event = {
		.wdev = ((osif_dev *) osif)->os_if,
		.macaddr = macaddr,
		.status = status
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_MLME_AUTH_INDICATION, &event);
	return;
}

static void
zyUMAC_assoc_indication_ap(os_handle_t osif, u_int8_t * macaddr,
			  u_int16_t result, wbuf_t wbuf,
			  wbuf_t resp_buf, bool reassoc)
{
	int reason = zyUMAC_wal_event_reason_converter(result);
	zyUMAC_wal_event_mlme_assoc_ind_t event = {
		.wdev = ((osif_dev *) osif)->os_if,
		.macaddr = macaddr,
		.result = reason,
		.reassoc = reassoc
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_MLME_ASSOC_INDICATION, &event);
	return;
}

static void zyUMAC_deauth_indication_ap(os_handle_t osif, u_int8_t * macaddr,
				       u_int16_t associd, u_int16_t reason)
{
	zyUMAC_wal_event_mlme_deauth_ind_t event = {
		.wdev = ((osif_dev *) osif)->os_if,
		.macaddr = macaddr,
		.associd = associd,
		.reason = reason
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_MLME_DEAUTH_INDICATION, &event);
	return;
}

static void zyUMAC_disassoc_indication_ap(os_handle_t osif, u_int8_t * macaddr,
					 u_int16_t associd, u_int32_t reason)
{
	zyUMAC_wal_event_mlme_disassoc_ind_t event = {
		.wdev = ((osif_dev *) osif)->os_if,
		.macaddr = macaddr,
		.associd = associd,
		.reason = reason
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_MLME_DISASSOC_INDICATION,
				&event);
	return;
}

static void zyUMAC_disassoc_complete_ap(os_handle_t osif, u_int8_t * macaddr,
				       u_int32_t reason,
				       IEEE80211_STATUS status)
{
	zyUMAC_wal_event_mlme_disassoc_complete_t event = {
		.wdev = ((osif_dev *) osif)->os_if,
		.macaddr = macaddr,
		.reason = reason,
		.status = status
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_MLME_DISASSOC_COMPLETE, &event);
	return;
}

static void zyUMAC_node_authorized_indication_ap(os_handle_t osif,
						u_int8_t * mac_addr)
{
	return;
}

/*
    indicator : leave_arg.sender = IEEE80211_AP_PROTO; leave_arg.frame_type = RECORD_TYPE_DROP; leave_arg.is_status = RECORD_STATUS;
*/

wlan_mlme_event_handler_table zyUMAC_ap_mlme_evt_handler = {
#ifdef UMAC_SUPPORT_CFG80211
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_JOIN_COMPLETE_SET_COUNTRY */
#endif
	zyUMAC_create_infra_complete,	/* IEEE80211_DELIVER_EVENT_MLME_JOIN_COMPLETE_INFRA */
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_JOIN_COMPLETE_ADHOC */
	zyUMAC_auth_complete_ap,	/* IEEE80211_DELIVER_EVENT_MLME_AUTH_COMPLETE */
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_ASSOC_REQ */
	zyUMAC_assoc_complete_ap,	/* IEEE80211_DELIVER_EVENT_MLME_ASSOC_COMPLETE */
	zyUMAC_assoc_complete_ap,	/* IEEE80211_DELIVER_EVENT_MLME_REASSOC_COMPLETE */
	zyUMAC_deauth_complete_ap,	/* IEEE80211_DELIVER_EVENT_MLME_DEAUTH_COMPLETE */
	zyUMAC_disassoc_complete_ap,	/* IEEE80211_DELIVER_EVENT_MLME_DISASSOC_COMPLETE */
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_TXCHANSWITCH_COMPLETE */
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_REPEATER_CAC_COMPLETE */
	zyUMAC_auth_indication_ap,	/* IEEE80211_DELIVER_EVENT_MLME_AUTH_INDICATION */
	zyUMAC_deauth_indication_ap,	/* IEEE80211_DELIVER_EVENT_MLME_DEAUTH_INDICATION */
	zyUMAC_assoc_indication_ap,	/* IEEE80211_DELIVER_EVENT_MLME_ASSOC_INDICATION */
	zyUMAC_assoc_indication_ap,	/* IEEE80211_DELIVER_EVENT_MLME_REASSOC_INDICATION */
	zyUMAC_disassoc_indication_ap,	/* IEEE80211_DELIVER_EVENT_MLME_DISASSOC_INDICATION */
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_IBSS_MERGE_START_INDICATION */
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_IBSS_MERGE_COMPLETE_INDICATION */
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_RADAR_DETECTED */
	zyUMAC_node_authorized_indication_ap,	/* IEEE80211_DELIVER_EVENT_MLME_NODE_AUTHORIZED_INDICATION */
	NULL,			/* IEEE80211_DELIVER_EVENT_MLME_UNPROTECTED_DEAUTH_INDICATION */
};

static void zyUMAC_osif_channel_changed(os_handle_t osif, wlan_chan_t chan)
{
	zyUMAC_wal_event_wdev_channel_change event = {
		.wdev = ((osif_dev *) osif)->os_if,
		.chan = chan
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_WDEV_CHANNEL_CHANGE, &event);
	return;
}

wlan_misc_event_handler_table zyUMAC_ap_misc_evt_handler = {
	zyUMAC_osif_channel_changed,	/* wlan_channel_change */
	NULL,			/* wlan_country_changed */
	NULL,			/* wlan_linkspeed */
	NULL,			/* wlan_michael_failure_indication */
	NULL,			/* wlan_replay_failure_indication */
	NULL,			/* wlan_beacon_miss_indication */
	NULL,			/* wlan_beacon_rssi_indication */
	NULL,			/* wlan_device_error_indication */
	NULL,			/* wlan_sta_clonemac_indication */
	NULL,			/* wlan_sta_scan_entry_update */
	NULL,			/* wlan_ap_stopped */
#if ATH_SUPPORT_WAPI
	NULL,			/* wlan_sta_rekey_indication */
#endif
#if ATH_SUPPORT_IBSS_NETLINK_NOTIFICATION
	NULL,			/* wlan_ibss_rssi_monitor */
#endif
#if UMAC_SUPPORT_RRM_MISC
	NULL,			/* wlan_channel_load */
	NULL,			/* wlan_nonerpcnt */
	NULL,			/* wlan_bgjoin */
	NULL,			/* wlan_cochannelap_cnt */
#endif
#if ATH_SUPPORT_HYFI_ENHANCEMENTS
	NULL,			/* wlan_buffull */
#endif
	NULL,			/* wlan_session_timeout */
#if ATH_SUPPORT_MGMT_TX_STATUS
	NULL,			/* wlan_mgmt_tx_status */
#endif
#if ATH_BAND_STEERING
	zyUMAC_osif_band_steering_event,	/* bsteering_event */
#endif
#if ATH_SSID_STEERING
	NULL,			/* ssid_event */
#endif
#if UMAC_SUPPORT_ACL
	NULL,			/* assocdeny_event */
#endif /* UMAC_SUPPORT_ACL */
#if ATH_TX_OVERFLOW_IND
	NULL,			/* wlan_tx_overflow */
#endif
	NULL,			/* wlan_ch_hop_channel_change */
	NULL,			/* wlan_recv_probereq */
#if DBDC_REPEATER_SUPPORT
	NULL,			/* stavap_connection */
#endif
#if ATH_SUPPORT_DFS && ATH_SUPPORT_STA_DFS
	NULL,			/* wlan_sta_cac_started */
#endif
	NULL,			/* chan_util_event */
	NULL,			/* wlan_keyset_done_indication */
	NULL,			/* wlan_blklst_sta_auth_indication */
#if ATH_PARAMETER_API
	NULL,			/* papi_event */
#endif
};
