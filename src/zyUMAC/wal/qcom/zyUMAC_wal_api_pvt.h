#ifndef __zyUMAC_WAL_API_PVT_H__
#define __zyUMAC_WAL_API_PVT_H__

#include "ol_if_athvar.h"
#include "osif_private.h"
#include "ieee80211_ioctl.h"
#include "ath_netlink.h"
#include "osif_net.h"
#include "acfg_event.h"

//#include "zyUMAC_umac.h"
#ifdef UMAC_SUPPORT_CFG80211
extern int
wlan_get_peer_dp_stats(struct ieee80211com *ic,
	struct wlan_objmgr_peer *peer,
	struct ieee80211_nodestats *ni_stats_user);
extern int
wlan_get_vdev_dp_stats(struct ieee80211vap *vap,
	struct ieee80211_stats *vap_stats_usr,
	struct ieee80211_mac_stats *ucast_stats_usr,
	struct ieee80211_mac_stats *mcast_stats_usr);
extern int
wlan_get_pdev_dp_stats(struct ieee80211com *ic,
	struct ol_ath_radiostats *scn_stats_user);
#endif

#define __zyUMAC_wal_wphy_setdata(dev, data)  dev->scn_zyUMAC_priv = data
#define __zyUMAC_wal_wphy_getdata(dev)        dev->scn_zyUMAC_priv

static inline zyUMAC_wal_wphy_t __zyUMAC_wal_netdev_to_wphy(struct net_device
							  *ndev)
{
	return ath_netdev_priv(ndev);
}

static inline struct net_device *__zyUMAC_wal_wphy_to_netdev(zyUMAC_wal_wphy_t
							    wphy)
{
#if UMAC_SUPPORT_WEXT
	ASSERT(wphy->netdev != NULL);
	return wphy->netdev;
#else
	ASSERT(wphy->sc_osdev != NULL);
	ASSERT(wphy->sc_osdev->netdev != NULL);
	return wphy->sc_osdev->netdev;
#endif
}

static inline struct proc_dir_entry
    *__zyUMAC_wal_wphy_get_proc_dir(zyUMAC_wal_wphy_t wphy)
{
	struct ieee80211com *ic = &wphy->sc_ic;

#ifdef UMAC_SUPPORT_CFG80211
	return ic->ic_proc;
#else
	return proc_mkdir(ic->ic_osdev->netdev->name, NULL);
#endif
}

static inline int
__zyUMAC_wal_wphy_get_wdev_limit(zyUMAC_wal_wphy_t wphy)
{
#ifdef UMAC_SUPPORT_CFG80211
	struct wlan_objmgr_psoc *psoc = wlan_pdev_get_psoc(wphy->sc_pdev);

	return wlan_psoc_get_max_vdev_count(psoc);
#else
	return wphy->max_vdevs;
#endif
}

static inline zyUMAC_wal_wdev_t
__zyUMAC_wal_wphy_get_first_wdev(zyUMAC_wal_wphy_t wphy)
{
	/* note: take care of lock_state */
	struct ieee80211com *ic = &wphy->sc_ic;

	return TAILQ_FIRST(&ic->ic_vaps);
}

static inline zyUMAC_wal_wdev_t
__zyUMAC_wal_wphy_get_next_wdev(zyUMAC_wal_wphy_t wphy, zyUMAC_wal_wdev_t wdev)
{
	return TAILQ_NEXT(wdev, iv_next);
}

static inline zyUMAC_wal_wnode_t
__zyUMAC_wal_wphy_get_first_wnode(zyUMAC_wal_wphy_t wphy)
{
	zyUMAC_wal_wnode_t ni = NULL;
	struct ieee80211com *ic = &wphy->sc_ic;

	ni = TAILQ_FIRST(&ic->ic_sta.nt_node);
	if (ni && ni->ni_flags & IEEE80211_NODE_AUTH)
		return ni;
	else
		return NULL;
}

static inline zyUMAC_wal_wnode_t
__zyUMAC_wal_wphy_get_next_wnode(zyUMAC_wal_wphy_t wphy, zyUMAC_wal_wnode_t node)
{
	zyUMAC_wal_wnode_t ni = NULL;

	ni = TAILQ_NEXT(node, ni_list);
	if (ni && ni->ni_flags & IEEE80211_NODE_AUTH)
		return ni;
	else
		return NULL;
}

static inline void
__zyUMAC_wal_wphy_get_radio_stats(zyUMAC_wal_wphy_t wphy,
				 struct ol_ath_radiostats *stats)
{
	struct ieee80211com *ic;

	if (!wphy || !stats)
		return;

	ic = &wphy->sc_ic;

#ifdef UMAC_SUPPORT_CFG80211
	if (ic) {
		if (!wlan_pdev_get_dp_handle(ic->ic_pdev_obj))
			return;

		wlan_get_pdev_dp_stats(ic, stats);
	}
#else
	memcpy(stats, &wphy->scn_stats, sizeof(struct ol_ath_radiostats));
#endif
}

static inline void
__zyUMAC_wal_wphy_update_radio_stats(zyUMAC_wal_wphy_t wphy,
				    zyUMAC_scn_priv_radio_stats_t *stats)
{
	struct ol_ath_radiostats scn_stats = { 0 };

	if (!stats)
		return;

	__zyUMAC_wal_wphy_get_radio_stats(wphy, &scn_stats);

	stats->wal_stats.rtsGood = scn_stats.rtsgood;
	stats->wal_stats.rtsBad = scn_stats.rtsbad;
	stats->wal_stats.ackRcvBad = scn_stats.ackrcvbad;
	stats->wal_stats.fcsBad = scn_stats.fcsbad;
	stats->wal_stats.tx_packets = scn_stats.tx_packets;
	stats->wal_stats.rx_badcrypt = scn_stats.rx_badcrypt;
	stats->wal_stats.rx_packets = scn_stats.rx_packets;
	stats->wal_stats.rx_bytes = scn_stats.rx_bytes;
	stats->wal_stats.rx_phyerr = scn_stats.rx_phyerr;
	stats->wal_stats.rx_badmic = scn_stats.rx_badmic;
	stats->wal_stats.rx_num_data = scn_stats.rx_num_data;
	stats->wal_stats.rx_data_bytes = scn_stats.rx_data_bytes;
	stats->wal_stats.tx_bytes = scn_stats.tx_bytes;
	stats->wal_stats.tx_num_data = scn_stats.tx_num_data;
	stats->wal_stats.tx_xretry = 0;
			/*TODO:scn_stats.ath_stats.tx.tx_xretry;*/
	stats->wal_stats.pdev_cont_xretry = 0;
			/*TODO:scn_stats.ath_stats.tx.pdev_cont_xretry;*/
	stats->wal_stats.tx_beacon = scn_stats.tx_beacon;
	stats->wal_stats.rx_crcerr = scn_stats.rx_crcerr;
	stats->wal_stats.tx_bawadv = scn_stats.tx_bawadv;
}

static inline void
__zyUMAC_wal_wphy_update_channel_util(zyUMAC_wal_wphy_t wphy,
				     zyUMAC_scn_priv_radio_stats_t *stats,
				     unsigned int enable)
{
	/*TODO*/
}

static inline void
__zyUMAC_wal_wphy_set_nodesurvey(zyUMAC_wal_wphy_t wphy)
{
	/*TODO*/
}

#define __zyUMAC_wal_wphy_get_id(wphy)    (wphy->radio_id)

static inline zyUMAC_wal_wphy_t __zyUMAC_wal_wdev_get_wphy_by_id(int band)
{
	struct net_device *dev;
	char name[IFNAMSIZ];

	snprintf(name, IFNAMSIZ, WPHY_NAME"%d", band);
	dev = dev_get_by_name(&init_net, name);

	return __zyUMAC_wal_netdev_to_wphy(dev);
}

#define __zyUMAC_wal_wdev_setdata(wdev, data)    wdev->iv_zyUMAC_vap_priv = data
#define __zyUMAC_wal_wdev_getdata(wdev)          wdev->iv_zyUMAC_vap_priv
#define __zyUMAC_wal_wdev_get_wphy(wdev)         OL_ATH_SOFTC_NET80211(wdev->iv_ic)

#define __zyUMAC_wal_wdev_is_ap_mode(wdev)       wdev->iv_opmode == IEEE80211_M_HOSTAP
#define __zyUMAC_wal_wdev_get_macaddr(wdev)      wdev->iv_myaddr

static inline void __zyUMAC_wal_wdev_get_ssid(zyUMAC_wal_wdev_t wdev,
					     zyUMAC_wal_ssid_t * ssid)
{
	wlan_get_desired_ssidlist(wdev, ssid, 1);
}

static inline int8_t __zyUMAC_wal_wdev_get_chanutil(zyUMAC_wal_wdev_t wdev)
{
	return wdev->chanutil_info.value;
}

static inline void
__zyUMAC_wal_wdev_get_stats(zyUMAC_wal_wdev_t wdev,
			   struct ieee80211_stats *vapstats,
			   struct ieee80211_mac_stats *unimac_stats,
			   struct ieee80211_mac_stats *multimac_stats)
{
	if (!wdev || !vapstats || !unimac_stats || !multimac_stats)
		return;

	memset(vapstats, 0, sizeof(struct ieee80211_stats));
	memset(unimac_stats, 0, sizeof(struct ieee80211_mac_stats));
	memset(multimac_stats, 0, sizeof(struct ieee80211_mac_stats));

#ifdef UMAC_SUPPORT_CFG80211
	if (vapstats) {
		wlan_get_vdev_dp_stats(wdev,
				       vapstats,
				       unimac_stats,
				       multimac_stats);
	}
#else
	memcpy(vapstats, (void *)&wdev->iv_stats,
		sizeof(struct ieee80211_stats));
	memcpy(unimac_stats, (void *)&wdev->iv_unicast_stats,
		sizeof(struct ieee80211_mac_stats));
	memcpy(multimac_stats, (void *)&wdev->iv_multicast_stats,
		sizeof(struct ieee80211_mac_stats));
#endif
}

static inline void
__zyUMAC_wal_wdev_update_stats(zyUMAC_wal_wdev_t wdev,
			      zyUMAC_vap_priv_stats_t *stats)
{
	struct ieee80211_stats *vapstats;
	struct ieee80211_mac_stats *unimac_stats;
	struct ieee80211_mac_stats *multimac_stats;

	if (!wdev || !stats)
		return;

	vapstats =
	    qdf_mem_malloc(sizeof(struct ieee80211_stats));
	unimac_stats =
	    qdf_mem_malloc(sizeof(struct ieee80211_mac_stats));
	multimac_stats =
	    qdf_mem_malloc(sizeof(struct ieee80211_mac_stats));

	if (vapstats && unimac_stats && multimac_stats) {
		__zyUMAC_wal_wdev_get_stats(wdev,
			vapstats, unimac_stats, multimac_stats);

		stats->wal_stats.unicast_tx_discard =
			unimac_stats->ims_tx_discard;
		stats->wal_stats.multicast_tx_discard =
			multimac_stats->ims_tx_discard;
		stats->wal_stats.unicast_rx_fcserr =
			unimac_stats->ims_rx_fcserr;
		stats->wal_stats.multicast_rx_fcserr =
			multimac_stats->ims_rx_fcserr;
		stats->wal_stats.unicast_rx_tkipmic =
			unimac_stats->ims_rx_tkipmic;
		stats->wal_stats.multicast_rx_tkipmic =
			multimac_stats->ims_rx_tkipmic;
	}

	if (multimac_stats)
		qdf_mem_free(multimac_stats);
	if (unimac_stats)
		qdf_mem_free(unimac_stats);
	if (vapstats)
		qdf_mem_free(vapstats);
}

static inline zyUMAC_wal_wnode_t
__zyUMAC_wal_wnode_get(zyUMAC_wal_wdev_t wdev, unsigned char *mac)
{
	return ieee80211_find_node(&wdev->iv_ic->ic_sta, mac, WLAN_MISC_ID);
}

static inline int __zyUMAC_wal_wdev_get_txpow(zyUMAC_wal_wdev_t wdev)
{
	return wdev->iv_ic->ic_txpowlimit / 2;
}

#define __zyUMAC_wal_wnode_put(wnode)    ieee80211_free_node(wnode, WLAN_MISC_ID)
#define __zyUMAC_wal_wdev_get_sta_count(wdev) \
					qdf_atomic_read(&wdev->iv_node_count)

#define __zyUMAC_wal_wdev_get_proc(wdev)    (wdev->iv_proc)

#define __zyUMAC_wal_wdev_get_chan(wdev)    (wdev->iv_bsschan->ic_ieee)

#define __zyUMAC_wal_wnode_setdata(wnode, data)    wnode->ni_zyUMAC_node_priv = data;
#define __zyUMAC_wal_wnode_getdata(wnode)          wnode->ni_zyUMAC_node_priv

#define __zyUMAC_wal_wnode_get_wdev(wnode)         wnode->ni_vap
#define __zyUMAC_wal_wnode_get_macaddr(wnode)      wnode->ni_macaddr
#define __zyUMAC_wal_wnode_get_bssid(wnode)        wnode->ni_bssid
#define __zyUMAC_wal_wnode_get_assocuptime(wnode)  wnode->ni_assocuptime
#define __zyUMAC_wal_wnode_get_associd(wnode)      wnode->ni_associd
#define __zyUMAC_wal_wnode_get_rssi(wnode) \
	(wnode->ni_rssi + DEFAULT_CHAN_REF_NOISE_FLOOR)
#define __zyUMAC_wal_wnode_get_phymode(wnode)      wnode->ni_phymode
#define __zyUMAC_wal_wnode_get_maxrate(wnode)      wnode->ni_maxrate

static inline int __zyUMAC_wal_wnode_get_radioid(zyUMAC_wal_wnode_t wnode)
{
	struct ieee80211com *ic = wnode->ni_ic;
	return ic->interface_id;
}

static inline void
__zyUMAC_wal_wnode_get_stats(zyUMAC_wal_wnode_t ni,
			    struct ieee80211_nodestats *stats)
{
#ifdef UMAC_SUPPORT_CFG80211
	struct ieee80211com *ic;
#endif

	if (!ni || !stats)
		return;

#ifdef UMAC_SUPPORT_CFG80211
	ic = ni->ni_ic;
	wlan_get_peer_dp_stats(ic, ni->peer_obj, stats);
#else
	memcpy(stats, &ni->ni_stats,
	       sizeof(struct ieee80211_nodestats));
#endif
}

#ifdef QCAWIFI3
static inline struct cdp_peer_stats *
__zyUMAC_wal_wnode_get_dp_stats(zyUMAC_wal_wnode_t ni)
{
	struct ieee80211com *ic;
	struct wlan_objmgr_psoc *psoc;
	struct cdp_peer *peer_dp_handle;
	ol_txrx_soc_handle soc_dp_handle;

	if (!ni)
		return NULL;

	ic = ni->ni_ic;

	psoc = wlan_pdev_get_psoc(ic->ic_pdev_obj);
	if (!psoc)
		return NULL;

	peer_dp_handle = wlan_peer_get_dp_handle(ni->peer_obj);
	if (!peer_dp_handle)
		return NULL;

	soc_dp_handle = wlan_psoc_get_dp_handle(psoc);
	if (!soc_dp_handle)
		return NULL;

	return cdp_host_get_peer_stats(soc_dp_handle, peer_dp_handle);
}
#endif

static inline void
__zyUMAC_wal_wnode_update_stats(zyUMAC_wal_wnode_t ni,
			       struct zyUMAC_node_priv_stats *stats)
{
	struct ieee80211_nodestats wal_stats = {0};
#ifdef QCAWIFI3
	struct cdp_peer_stats *peer_stats;
	int i;
#endif

	if (!stats)
		return;

	__zyUMAC_wal_wnode_get_stats(ni, &wal_stats);
	stats->wal_stats.ns_rx_dup = wal_stats.ns_rx_dup;
	stats->wal_stats.ns_last_tx_rate = wal_stats.ns_last_tx_rate;
	stats->wal_stats.ns_last_rx_rate = wal_stats.ns_last_rx_rate;

#ifdef QCAWIFI3
	peer_stats = __zyUMAC_wal_wnode_get_dp_stats(ni);

	if (!peer_stats)
		return;

	stats->wal_stats.ns_tx_success_num = peer_stats->tx.tx_success.num;
	stats->wal_stats.ns_tx_success_bytes = peer_stats->tx.tx_success.bytes;
	stats->wal_stats.ns_rx_success_num = peer_stats->rx.to_stack.num;
	stats->wal_stats.ns_rx_success_bytes = peer_stats->rx.to_stack.bytes;

	for (i = 0; i < WME_AC_MAX; i++) {
		stats->an_framereceive[i] =
			peer_stats->rx.to_stack_per_ac[i].num;
		stats->an_bytesreceive[i] =
			peer_stats->rx.to_stack_per_ac[i].bytes;
		stats->an_frametransmit[i] = peer_stats->tx.tx_per_ac[i].num;
		stats->an_bytestransmit[i] =
			peer_stats->tx.tx_per_ac[i].bytes;
	}

	stats->tx_good_pkts = peer_stats->tx.tx_success.num;
	stats->tx_good_bytes = peer_stats->tx.tx_success.bytes;
#endif
}

#define __zyUMAC_wal_wnode_is_authed(wnode)      ieee80211_node_is_authorized(wnode)

static inline struct net_device *__zyUMAC_wal_wdev_to_netdev(zyUMAC_wal_wdev_t
							    wdev)
{
	return OSIF_TO_NETDEV(wdev->iv_ifp);
}

static inline zyUMAC_wal_wdev_t __zyUMAC_wal_netdev_to_wdev(struct net_device
							  *ndev)
{
	osif_dev *osifp = ath_netdev_priv(ndev);
	return osifp->os_if;
}

static inline int __zyUMAC_wal_wdev_acl_add(zyUMAC_wal_wdev_t vap,
					   const char *macstr,
					   u_int8_t acl_list_id)
{
	u_int8_t mac[6];
	int i;
	const char *macptr = macstr;

	memset(mac, 0, 6);
	for (i = 0; i < 6; i++) {
		hex2bin(&mac[i], macptr, 1);
		macptr = macptr + 3;
	}

	return wlan_set_acl_add(vap, mac, acl_list_id);
}

#define __zyUMAC_wal_ssid_get_data(ssid)   ssid.ssid
#define __zyUMAC_wal_ssid_get_len(ssid)    ssid.len

#define __zyUMAC_wal_ieee80211_header_get_addr1(wh)   (wh->i_addr1)
#define __zyUMAC_wal_ieee80211_header_get_addr2(wh)   (wh->i_addr2)
#define __zyUMAC_wal_ieee80211_header_get_seq(wh) \
	(le16toh(*(u_int16_t *)wh->i_seq) >> IEEE80211_SEQ_SEQ_SHIFT)

extern int zyUMAC_wal_attach(zyUMAC_wal_wphy_t dev, int num_radios);
extern void zyUMAC_wal_detach(zyUMAC_wal_wphy_t dev);
extern int zyUMAC_wal_stats_attach(struct ol_ath_softc_net80211 *scn);
extern void zyUMAC_wal_stats_detach(struct ol_ath_softc_net80211 *scn);

extern int __zyUMAC_wal_wphy_ioctl_attach(struct ol_ath_softc_net80211 *scn,
					 zyUMAC_wal_ioctl_hook_table *
					 ioctl_hook_table);
extern void __zyUMAC_wal_wphy_ioctl_detach(struct ol_ath_softc_net80211 *scn);

extern int __zyUMAC_wal_wdev_attach(zyUMAC_wal_wphy_t wphy,
				   zyUMAC_wdev_hook_table * wdev_hooks);
extern void __zyUMAC_wal_wphy_iterate_wnodes(zyUMAC_wal_wphy_t wphy,
					    zyUMAC_wal_wnode_itrator cb,
					    void *arg);
extern int __zyUMAC_wal_wdev_mlme_request(zyUMAC_wal_wdev_t wdev,
					 zyUMAC_wal_req_mlme_t * request);

extern int __zyUMAC_wal_wdev_ioctl_attach(zyUMAC_wal_wdev_t vap,
				zyUMAC_wal_ioctl_hook_table *ioctl_hook_table);
extern void __zyUMAC_wal_wdev_ioctl_detach(void);

#endif
