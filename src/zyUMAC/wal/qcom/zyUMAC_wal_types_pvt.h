#ifndef __zyUMAC_WAL_TYPES_PVT_H__
#define __zyUMAC_WAL_TYPES_PVT_H__

#include <ol_if_athvar.h>
#include <osif_private.h>

#define __zyUMAC_WAL_MLME_REQ_ASSOC        IEEE80211_MLME_ASSOC	/* associate station */
#define __zyUMAC_WAL_MLME_REQ_DISASSOC     IEEE80211_MLME_DISASSOC	/* disassociate station */
#define __zyUMAC_WAL_MLME_REQ_DEAUTH       IEEE80211_MLME_DEAUTH	/* deauthenticate station */
#define __zyUMAC_WAL_MLME_REQ_REASSOC      IEEE80211_MLME_REASSOC	/* reassoc to station */

typedef struct ol_ath_softc_net80211 *__zyUMAC_wal_wphy_t;
typedef struct ieee80211vap *__zyUMAC_wal_wdev_t;
typedef ieee80211_recv_status *__zyUMAC_wal_rx_status_t;	//struct ieee80211_rx_status
typedef struct ieee80211_node *__zyUMAC_wal_wnode_t;
typedef ieee80211_ssid __zyUMAC_wal_ssid;

typedef struct ieee80211_stats __zyUMAC_ieee80211_stats_t;

typedef struct ieee80211_frame __zyUMAC_ieee80211_header_t;

struct zyUMAC_wal_priv {
	/* virtual ap create/delete */
	struct ieee80211vap *(*orig_ic_vap_create) (struct ieee80211com *,
						    int opmode,
						    int scan_priority_base,
						    int flags,
						    const u_int8_t
						    bssid[IEEE80211_ADDR_LEN],
						    const u_int8_t
						    mataddr[IEEE80211_ADDR_LEN],
						    void *osifp_handle);
	void (*orig_ic_vap_delete) (struct wlan_objmgr_vdev * vdev);
	void (*orig_ic_vap_free) (struct ieee80211vap *);
	int (*orig_ic_vap_alloc_macaddr) (struct ieee80211com * ic,
					  u_int8_t * bssid);
	int (*orig_ic_vap_free_macaddr) (struct ieee80211com * ic,
					 u_int8_t * bssid);
#ifdef UMAC_SUPPORT_CFG80211
	int (*orig_ic_cfg80211_radio_attach) (struct device * dev,
					      struct net_device * net_dev,
					      struct ieee80211com * ic);
	struct ieee80211_node *(*orig_ic_node_alloc) (struct ieee80211vap * vap,
						      const u_int8_t * macaddr,
						      bool tmpnode, void *peer);
#else
	struct ieee80211_node *(*orig_ic_node_alloc) (struct ieee80211vap * vap,
						      const u_int8_t * macaddr,
						      bool tmpnode);
#endif
	void (*orig_ic_node_free) (struct ieee80211_node * ni);

	int (*orig_ol_ath_enable_ap_stats) (struct ieee80211com * ic,
					    u_int8_t stats_cmd);
	int (*orig_ol_ath_update_stats_event_handler) (ol_scn_t sc,
						       u_int8_t * data,
						       u_int16_t datalen);
	int (*orig_ol_ath_bss_chan_info_event_handler) (ol_scn_t sc,
							u_int8_t * data,
							u_int32_t datalen);
	void (*orig_ic_ol_chan_stats_event) (struct ieee80211com * ic,
					     struct ieee80211_mib_cycle_cnts *
					     pstats,
					     struct ieee80211_mib_cycle_cnts *
					     nstats);

	wdi_event_subscribe stats_rx_subscriber;
	wdi_event_subscribe stats_tx_subscriber;
	wdi_event_subscribe stats_txdata_subscriber;
	wdi_event_subscribe stats_dp_subscriber;
};

#define orig_ic_vap_create          scn_wal_pri.orig_ic_vap_create
#define orig_ic_vap_delete          scn_wal_pri.orig_ic_vap_delete
#define orig_ic_vap_free            scn_wal_pri.orig_ic_vap_free
#define orig_ic_vap_alloc_macaddr   scn_wal_pri.orig_ic_vap_alloc_macaddr
#define orig_ic_vap_free_macaddr    scn_wal_pri.orig_ic_vap_free_macaddr
#define orig_ic_cfg80211_radio_attach   scn_wal_pri.orig_ic_cfg80211_radio_attach
#define orig_ic_node_alloc              scn_wal_pri.orig_ic_node_alloc
#define orig_ic_node_free               scn_wal_pri.orig_ic_node_free
#define orig_ol_ath_enable_ap_stats     scn_wal_pri.orig_ol_ath_enable_ap_stats
#define orig_ic_ol_chan_stats_event     scn_wal_pri.orig_ic_ol_chan_stats_event
#define orig_ol_ath_update_stats_event_handler      scn_wal_pri.orig_ol_ath_update_stats_event_handler
#define orig_ol_ath_bss_chan_info_event_handler      scn_wal_pri.orig_ol_ath_bss_chan_info_event_handler
#define stats_rx_subscriber             scn_wal_pri.stats_rx_subscriber
#define stats_tx_subscriber             scn_wal_pri.stats_tx_subscriber
#define stats_txdata_subscriber         scn_wal_pri.stats_txdata_subscriber
#define stats_dp_subscriber             scn_wal_pri.stats_dp_subscriber

struct zyUMAC_wal_vap_priv {
	wlan_event_handler_table vap_evtable;;
	wlan_event_handler_table *orig_vap_evtable;

	struct net_device_ops dev_ops;
	struct net_device_ops *orig_dev_ops;
};

#define vap_evtable                 wal_vap_priv.vap_evtable
#define orig_vap_evtable            wal_vap_priv.orig_vap_evtable
#define dev_ops                     wal_vap_priv.dev_ops
#define orig_dev_ops                wal_vap_priv.orig_dev_ops

struct zyUMAC_wal_wnode_priv {
	u_int16_t mlme_cmd_sender;
};

#endif
