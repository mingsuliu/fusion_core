#ifndef ZYXEL_UMAC_WAL_VAP_HEADER_H
#define ZYXEL_UMAC_WAL_VAP_HEADER_H

extern zyUMAC_wdev_hook_table *_wdev_hook_table;

void zyUMAC_vap_create_completed(struct ieee80211vap *vap);
void zyUMAC_vap_delete(struct wlan_objmgr_vdev *vdev);
void zyUMAC_vap_free(struct ieee80211vap *vap);
int zyUMAC_vap_alloc_macaddr(struct ieee80211com *ic, u_int8_t * bssid);
int zyUMAC_vap_free_macaddr(struct ieee80211com *ic, u_int8_t * bssid);

struct ieee80211vap *zyUMAC_ol_ath_vap_get(struct ol_ath_softc_net80211 *scn,
					  u_int8_t vdev_id);
#endif
