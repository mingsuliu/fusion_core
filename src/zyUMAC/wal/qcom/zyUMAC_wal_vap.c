#include "cdp_txrx_cmn.h"
#include "ol_txrx_types.h"
#ifdef UMAC_SUPPORT_CFG80211
#include "dp_types.h"
#include "target_type.h"
#endif

#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_vap.h"
#include "zyUMAC_wal_api_pvt.h"
#include "zyUMAC_wal_mlme_hook.h"
#include "zyUMAC_wal_mgmt_hook.h"
#include "zyUMAC_cfg80211.h"

static ol_txrx_rx_mon_fp orig_rx_mon_fp = NULL;

zyUMAC_wdev_hook_table *_wdev_hook_table = NULL;

static void zyUMAC_rx_monitor(os_if_t osif, wbuf_t wbuf,
			     ieee80211_recv_status * rs)
{
#if SK_LOCATION
	int found = 0;
	osif_dev *osifp = (osif_dev *) osif;
	struct ieee80211vap *vap = osifp->os_if;
	struct ieee80211com *ic = wlan_vap_get_devhandle(vap);
	struct sk_buff *skb = (struct sk_buff *)wbuf;
	if (vap->mac_entries) {
		int hash;
		struct ieee80211_qosframe_addr4 *qwh =
		    (struct ieee80211_qosframe_addr4 *)wbuf_header(skb);
		struct ieee80211_mac_filter_list *mac_en;

		hash = MON_MAC_HASH(qwh->i_addr1);
		LIST_FOREACH(mac_en, &vap->mac_filter_hash[hash], mac_entry) {
			if (mac_en->mac_addr != NULL) {
				if (IEEE80211_ADDR_EQ
				    (qwh->i_addr1, mac_en->mac_addr)) {
					found = 1;
				}
			}
		}
	}
	if (ic->mon_filter_osif_mac == 1) {
		/*If osif MAC addr based filter enabled, */
		/*only input pkts from specified MAC */
		if (qdf_unlikely(found == 1)) {
			location_sta_info_gather(vap->iv_ic, wbuf, rs);
		}
	} else {
		location_sta_info_gather(vap->iv_ic, wbuf, rs);
	}
#endif
	if (orig_rx_mon_fp) {
		orig_rx_mon_fp((struct ol_osif_vdev_t *)osif, wbuf, rs);
	}
}

#if 0
struct vap_id_map {
	struct ieee80211vap *vap;
	u_int8_t vdev_id;
};

void zyUMAC_ol_ath_vap_iter_id(void *arg, struct ieee80211vap *vap)
{
	struct vap_id_map *v_id_map = arg;

	if (vap->iv_unit == v_id_map->vdev_id) {
		v_id_map->vap = vap;
	}
}

struct ieee80211vap *zyUMAC_ol_ath_vap_get(struct ol_ath_softc_net80211 *scn,
					  u_int8_t vdev_id)
{
	struct vap_id_map v_id_map;

	v_id_map.vap = NULL;
	v_id_map.vdev_id = vdev_id;

	//  Get a vap given the vdev_id : iterate through the vap list and get the vap. The ic maintains a list of vaps.
	wlan_iterate_vap_list(&scn->sc_ic, zyUMAC_ol_ath_vap_iter_id, &v_id_map);
	return v_id_map.vap;
}
#endif

static void zyUMAC_osif_receive(os_if_t osif, wbuf_t wbuf,
			       u_int16_t type, u_int16_t subtype,
			       ieee80211_recv_status * rs)
{
	struct net_device *dev = OSIF_TO_NETDEV(osif);
	wlan_if_t vap = NETDEV_TO_VAP(dev);
	zyUMAC_vap_priv_t *zyUMAC_vap_priv;

	if (!_wdev_hook_table->wdev_hook_rx(vap, wbuf, type, subtype, rs)) {
		return;
	}

	/* deliver the data to network interface */
	zyUMAC_vap_priv = vap->iv_zyUMAC_vap_priv;
	if (zyUMAC_vap_priv == NULL) {
		goto done;
	}

	if (zyUMAC_vap_priv->orig_vap_evtable == NULL) {
		goto done;
	}

	zyUMAC_vap_priv->orig_vap_evtable->wlan_receive(osif, wbuf, type,
						       subtype, rs);
	return;

done:
	wbuf_free(wbuf);
	return;
}

static int wdev_hook_mgmt_output_filter_handler(wbuf_t wbuf)
{
	zyUMAC_wal_wnode_t ni;
	ieee80211_vap_complete_buf_handler handler;
	void *arg;
	struct ieee80211_frame *wh;
	int type, subtype;

	wh = (struct ieee80211_frame *)wbuf_header(wbuf);
	type = wh->i_fc[0] & IEEE80211_FC0_TYPE_MASK;
	subtype = wh->i_fc[0] & IEEE80211_FC0_SUBTYPE_MASK;

	if (_wdev_hook_table->wdev_hook_mgmt_output_filter == NULL)
		return 0;

	/* Since ni is not contained in proto type of the output_mgmt_filter,
	   but it might be set by using wbuf_set_complete_handler in SPF driver.
	   Thus, we need to retrieve ni by using wbuf_get_complete_handler */
	wbuf_get_complete_handler(wbuf, (void **)&handler, &arg);
	ni = (zyUMAC_wal_wnode_t) arg;

	return _wdev_hook_table->wdev_hook_mgmt_output_filter(ni, wbuf, subtype);
}

static int zyUMAC_hard_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	wlan_if_t vap = NETDEV_TO_VAP(dev);

	zyUMAC_vap_priv = vap->iv_zyUMAC_vap_priv;
	ASSERT(zyUMAC_vap_priv != NULL);
	ASSERT(zyUMAC_vap_priv->orig_dev_ops->ndo_start_xmit != NULL);

	if (_wdev_hook_table->wdev_hook_tx_data_inspec != NULL) {
		/* If the return value of wdev_hook_tx_data_inspec is not 0,
		   just free the buffer. */
		if (_wdev_hook_table->wdev_hook_tx_data_inspec(vap, skb) != 0) {
			wbuf_free(skb);
			return 0;
		}
	}

	return zyUMAC_vap_priv->orig_dev_ops->ndo_start_xmit(skb, dev);
}

void zyUMAC_vap_create_completed(struct ieee80211vap *vap)
{
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	os_if_t tmpOsif;

	ASSERT(vap != NULL);
	ASSERT(_wdev_hook_table != NULL);

	if (vap->iv_opmode != IEEE80211_M_HOSTAP) {
		return;
	}

	_wdev_hook_table->wdev_hook_create(vap);
	zyUMAC_vap_priv = vap->iv_zyUMAC_vap_priv;
	ASSERT(zyUMAC_vap_priv != NULL);

	//replace original event handler
	tmpOsif = wlan_vap_get_registered_handle(vap);
	wlan_vap_register_mlme_event_handlers(vap, (os_handle_t) tmpOsif,
					      &zyUMAC_ap_mlme_evt_handler);
	wlan_vap_register_misc_event_handlers(vap, (os_handle_t) tmpOsif,
					      &zyUMAC_ap_misc_evt_handler);

	OS_MEMCPY((void *)&zyUMAC_vap_priv->vap_evtable, (void *)vap->iv_evtable,
		  sizeof(wlan_event_handler_table));
	zyUMAC_vap_priv->vap_evtable.wlan_receive = zyUMAC_osif_receive;
	zyUMAC_vap_priv->orig_vap_evtable = vap->iv_evtable;
	wlan_vap_register_event_handlers(vap, &zyUMAC_vap_priv->vap_evtable);

	ieee80211vap_set_input_mgmt_filter(vap,
					   _wdev_hook_table->
					   wdev_hook_mgmt_input_filter);

	ieee80211vap_set_output_mgmt_filter_func(vap,
						 wdev_hook_mgmt_output_filter_handler);

	origional_wlan_receive_filter_80211 =
	    vap->iv_evtable->wlan_receive_filter_80211;
	vap->iv_evtable->wlan_receive_filter_80211 =
	    zyUMAC_wlan_receive_filter_80211;

	{
#ifdef UMAC_SUPPORT_CFG80211
		struct ieee80211com *ic = wlan_vap_get_devhandle(vap);
		ol_txrx_vdev_handle vdev_handle =
		    wlan_vdev_get_dp_handle(vap->vdev_obj);
		u_int32_t target_type = ic->ic_get_tgt_type(ic);

		if ((target_type == TARGET_TYPE_QCA8074) ||
		    (target_type == TARGET_TYPE_QCA8074V2) ||
		    (target_type == TARGET_TYPE_QCA6018) ||
		    (target_type == TARGET_TYPE_QCA6290)) {
			struct dp_vdev *vdev = (struct dp_vdev *)vdev_handle;

			if (!orig_rx_mon_fp) {
				orig_rx_mon_fp = vdev->osif_rx_mon;
			}

			if (orig_rx_mon_fp) {
				vdev->osif_rx_mon =
				    (ol_txrx_rx_mon_fp) zyUMAC_rx_monitor;
			}

			if (zyUMAC_vap_priv->dev_ops.ndo_start_xmit == NULL) {
				struct net_device *dev =
				    ((osif_dev *) vap->iv_ifp)->netdev;

				memcpy(&zyUMAC_vap_priv->dev_ops,
				       dev->netdev_ops,
				       sizeof(struct net_device_ops));
				zyUMAC_vap_priv->orig_dev_ops =
				    (struct net_device_ops *)dev->netdev_ops;
				zyUMAC_vap_priv->dev_ops.ndo_start_xmit =
				    zyUMAC_hard_start_xmit;
				dev->netdev_ops = &zyUMAC_vap_priv->dev_ops;
			}
		} else {
			struct ol_txrx_vdev_t *vdev =
			    (struct ol_txrx_vdev_t *)vdev_handle;
			if (!orig_rx_mon_fp) {
				orig_rx_mon_fp =
				    (ol_txrx_rx_mon_fp) vdev->osif_rx_mon;
			}

			if (orig_rx_mon_fp) {
				vdev->osif_rx_mon =
				    (ol_txrx_rx_mon_fp) zyUMAC_rx_monitor;
			}
		}
#else
		struct ol_txrx_vdev_t *vdev =
		    (struct ol_txrx_vdev_t *)vap->iv_txrx_handle;
		if (!orig_rx_mon_fp) {
			orig_rx_mon_fp = (ol_txrx_rx_mon_fp) vdev->osif_rx_mon;
		}

		if (orig_rx_mon_fp) {
			vdev->osif_rx_mon =
			    (ol_txrx_rx_mon_fp) zyUMAC_rx_monitor;
		}
#endif
	}
}


void zyUMAC_vap_delete(struct wlan_objmgr_vdev *vdev)
{
	struct ieee80211vap *vap =
	    vdev ? wlan_vdev_get_mlme_ext_obj(vdev) : NULL;
	struct ol_ath_softc_net80211 *scn = OL_ATH_SOFTC_NET80211(vap->iv_ic);
	zyUMAC_scn_priv_t *zyUMAC_scn_priv =
	    (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	struct net_device *dev = ((osif_dev *) vap->iv_ifp)->netdev;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = vap->iv_zyUMAC_vap_priv;

	if (zyUMAC_vap_priv->orig_dev_ops)
		dev->netdev_ops = zyUMAC_vap_priv->orig_dev_ops;

	_wdev_hook_table->wdev_hook_delete(vap);

	if (TAILQ_LAST(&vap->iv_ic->ic_vaps, ic_vaps_head) == NULL)
		zyUMAC_scn_priv->current_channel = 0;

	return zyUMAC_scn_priv->orig_ic_vap_delete(vdev);
}

void zyUMAC_vap_free(struct ieee80211vap *vap)
{
	struct ol_ath_softc_net80211 *scn = OL_ATH_SOFTC_NET80211(vap->iv_ic);
	zyUMAC_scn_priv_t *zyUMAC_scn_priv =
	    (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;

	return zyUMAC_scn_priv->orig_ic_vap_free(vap);
}

int zyUMAC_vap_alloc_macaddr(struct ieee80211com *ic, u_int8_t * bssid)
{
	struct ol_ath_softc_net80211 *scn = OL_ATH_SOFTC_NET80211(ic);
	zyUMAC_scn_priv_t *zyUMAC_scn_priv =
	    (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;

	return zyUMAC_scn_priv->orig_ic_vap_alloc_macaddr(ic, bssid);
}

int zyUMAC_vap_free_macaddr(struct ieee80211com *ic, u_int8_t * bssid)
{
	struct ol_ath_softc_net80211 *scn = OL_ATH_SOFTC_NET80211(ic);
	zyUMAC_scn_priv_t *zyUMAC_scn_priv =
	    (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;

	return zyUMAC_scn_priv->orig_ic_vap_free_macaddr(ic, bssid);
}

void zyUMAC_vap_recover_complete(struct ieee80211vap *vap)
{
	zyUMAC_vap_create_completed(vap);
}

int __zyUMAC_wal_wdev_attach(zyUMAC_wal_wphy_t scn,
			    zyUMAC_wdev_hook_table * wdev_hooks)
{
	struct ieee80211com *ic = &scn->sc_ic;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv =
	    (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;

	ASSERT(wdev_hooks != NULL);
	_wdev_hook_table = wdev_hooks;

	if (!zyUMAC_scn_priv->orig_ic_vap_delete) {
		zyUMAC_scn_priv->orig_ic_vap_delete = ic->ic_vap_delete;
		ic->ic_vap_delete = zyUMAC_vap_delete;
	}

	if (!zyUMAC_scn_priv->orig_ic_vap_free) {
		zyUMAC_scn_priv->orig_ic_vap_free = ic->ic_vap_free;
		ic->ic_vap_free = zyUMAC_vap_free;
	}
	/* Since callback ic_vap_recover_complete is added by our patch,
	   we don't need to save the original pointer */
	ic->ic_vap_recover_complete = zyUMAC_vap_recover_complete;

	if (!zyUMAC_scn_priv->orig_ic_vap_alloc_macaddr) {
		zyUMAC_scn_priv->orig_ic_vap_alloc_macaddr =
		    ic->ic_vap_alloc_macaddr;
		ic->ic_vap_alloc_macaddr = zyUMAC_vap_alloc_macaddr;
	}

	if (!zyUMAC_scn_priv->orig_ic_vap_free_macaddr) {
		zyUMAC_scn_priv->orig_ic_vap_free_macaddr =
		    ic->ic_vap_free_macaddr;
		ic->ic_vap_free_macaddr = zyUMAC_vap_free_macaddr;
	}
#if UMAC_SUPPORT_CFG80211
	if (!zyUMAC_scn_priv->orig_ic_cfg80211_radio_attach) {
		zyUMAC_scn_priv->orig_ic_cfg80211_radio_attach =
		    ic->ic_cfg80211_radio_attach;
		ic->ic_cfg80211_radio_attach = zyUMAC_cfg80211_radio_attach;
	}
#endif

	return 0;
}
