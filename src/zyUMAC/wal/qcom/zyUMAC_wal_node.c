#include <linux/module.h>
#include <ol_if_athvar.h>
#include <wdi_event_api.h>
#include <wmi_unified_priv.h>
#include "ath_netlink.h"

#include "zyUMAC_umac.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_mgmt_hook.h"

extern zyUMAC_wdev_hook_table *_wdev_hook_table;

#if UMAC_SUPPORT_CFG80211
struct ieee80211_node *zyUMAC_WAL_ol_ath_node_alloc(struct ieee80211vap *vap,
					       const u_int8_t * macaddr,
					       bool tmpnode, void *peer)
#else
struct ieee80211_node *zyUMAC_WAL_ol_ath_node_alloc(struct ieee80211vap *vap,
					       const u_int8_t * macaddr,
					       bool tmpnode)
#endif
{
	struct ieee80211_node *ni;
	struct ol_ath_softc_net80211 *scn = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;

	if (!vap) {
		qdf_print("vap is null");
		return NULL;
	}

	if ((scn = OL_ATH_SOFTC_NET80211(vap->iv_ic)) == NULL) {
		qdf_print("scn is null");
		return NULL;
	}

	if ((zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv) == NULL) {
		qdf_print("zyUMAC_scn_priv is null");
		return NULL;
	}

	if (zyUMAC_scn_priv->orig_ic_node_alloc == NULL) {
		qdf_print("warn: no original hook");
		return NULL;
	}
#if UMAC_SUPPORT_CFG80211
	ni = zyUMAC_scn_priv->orig_ic_node_alloc(vap, macaddr, tmpnode, peer);
#else
	ni = zyUMAC_scn_priv->orig_ic_node_alloc(vap, macaddr, tmpnode);
#endif

	if (ni == NULL) {
		goto err;
	}

	_wdev_hook_table->wdev_hook_node_create(ni);
	ASSERT(ni->ni_zyUMAC_node_priv != NULL);
	return ni;

err:
	return NULL;
}

EXPORT_SYMBOL(zyUMAC_WAL_ol_ath_node_alloc);

static void _node_free(struct ieee80211_node *ni)
{
	_wdev_hook_table->wdev_hook_node_delete(ni);
}

void zyUMAC_WAL_ol_ath_node_free(struct ieee80211_node *ni)
{
	struct ieee80211vap *vap = NULL;
	struct ol_ath_softc_net80211 *scn = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;

	if (!ni) {
		return;
	}

	vap = ni->ni_vap;
	if (!vap) {
		return;
	}

	if ((scn = OL_ATH_SOFTC_NET80211(vap->iv_ic)) == NULL) {
		qdf_print("scn is null");
		return;
	}

	if ((zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv) == NULL) {
		qdf_print("zyUMAC_scn_priv is null");
		return;
	}

	_node_free(ni);

	if (zyUMAC_scn_priv && zyUMAC_scn_priv->orig_ic_node_free) {
		zyUMAC_scn_priv->orig_ic_node_free(ni);
	}
}

EXPORT_SYMBOL(zyUMAC_WAL_ol_ath_node_free);

void __zyUMAC_wal_wphy_iterate_wnodes(zyUMAC_wal_wphy_t scn,
				     zyUMAC_wal_wnode_itrator cb,
				     void *arg)
{
	struct ieee80211_node *ni;
	struct ieee80211_node_table *nt;
	rwlock_state_t lock_state;
	struct ieee80211com *ic = &scn->sc_ic;
	int hash;
	OS_BEACON_DECLARE_AND_RESET_VAR(flags);

	nt = &ic->ic_sta;
	OS_BEACON_READ_LOCK(&nt->nt_nodelock, &lock_state, flags);
	for (hash = 0; hash < sizeof(nt->nt_hash) / sizeof(nt->nt_hash[0]);
	     hash++) {
		LIST_FOREACH(ni, &nt->nt_hash[hash], ni_hash) {
			if (!ni) {
				zyUMAC_LOG(LOG_ERR, "ni is NULL");
				continue;
			}

			if ((!(ni->ni_flags & IEEE80211_NODE_TEMP))
			    && (ni != ni->ni_bss_node)) {
				cb(ni, arg);
			}
		}
	}
	OS_BEACON_READ_UNLOCK(&nt->nt_nodelock, &lock_state, flags);
}
