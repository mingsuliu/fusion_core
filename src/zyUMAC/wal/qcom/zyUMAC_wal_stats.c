#include <linux/module.h>
#include <ol_if_athvar.h>
#include <wdi_event_api.h>
#include <wmi_unified_priv.h>
#ifdef UMAC_SUPPORT_CFG80211
#include <init_deinit_lmac.h>
#include "cdp_txrx_ctrl.h"
#include "cdp_txrx_cmn_struct.h"
#include "dp_ratetable.h"
#include "target_type.h"
#else
#include "bmi_msg.h"
#endif
#include "zyUMAC_umac.h"
#include "zyUMAC_common_var.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_wal_stats.h"
#include "zyUMAC_wal_event.h"
#include "zyUMAC_compat.h"

extern void zyUMAC_stats_tx_complete_update(struct ol_ath_softc_net80211 *scn,
					   struct ieee80211vap *vap,
					   qdf_nbuf_t netbuf,
					   enum htt_tx_status status);
extern void zyUMAC_stats_rx_deliver_update(struct ol_ath_softc_net80211 *scn,
					  struct ieee80211vap *vap,
					  struct ol_txrx_peer_t *peer,
					  qdf_nbuf_t msdu,
					  htt_pdev_handle htt_pdev,
					  enum htt_rx_status status);
extern void zyUMAC_stats_update_radio_stats(struct ol_ath_softc_net80211 *scn);
extern void zyUMAC_stats_update_vap_stats(struct ol_ath_softc_net80211 *scn,
					 struct ieee80211vap *vap,
					 wmi_host_vdev_stats * vdev_stats);

void zyUMAC_stats_get_all(void *pdev, enum WDI_EVENT event,
			 void *log_data, uint16_t peer_id,
			 enum htt_rx_status status);

static unsigned long long zyUMAC_walget_stats_sub(unsigned long long new,
					      unsigned long long old)
{
	if (new < old) {
		return ((unsigned int)(-1) - old) + new;
	}
	return new - old;
}

int zyUMAC_walstats_register_wdi_event(struct ol_ath_softc_net80211 *scn)
{
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	struct ol_txrx_pdev_t *pdev_txrx_handle;
#ifdef UMAC_SUPPORT_CFG80211
	ol_txrx_soc_handle soc_txrx_handle;
#ifdef QCAWIFI3
	struct ieee80211com *ic = &scn->sc_ic;
#endif
#endif

	if (!scn) {
		qdf_print("scn is null!!");
		return -1;
	}
#ifdef UMAC_SUPPORT_CFG80211
	if ((soc_txrx_handle =
	     wlan_psoc_get_dp_handle(scn->soc->psoc_obj)) == NULL) {
		qdf_print("can't find ol_txrx_soc_handle");
		return -1;
	}
	pdev_txrx_handle = wlan_pdev_get_dp_handle(scn->sc_pdev);
#else
	pdev_txrx_handle = scn->pdev_txrx_handle;
#endif

	if (!pdev_txrx_handle) {
		qdf_print("can't find pdev_txrx_handle");
		return -1;
	}

	if ((zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv) == NULL) {
		qdf_print("zyUMAC_scn_priv is null");
		return -1;
	}
#ifdef UMAC_SUPPORT_CFG80211

#ifndef QCAWIFI3
	if (zyUMAC_scn_priv->stats_tx_subscriber.context == NULL &&
	    zyUMAC_scn_priv->stats_tx_subscriber.callback == NULL) {
		zyUMAC_scn_priv->stats_tx_subscriber.context = pdev_txrx_handle;
		zyUMAC_scn_priv->stats_tx_subscriber.callback =
		    zyUMAC_stats_get_all;
		if (cdp_wdi_event_sub
		    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
		     &zyUMAC_scn_priv->stats_tx_subscriber,
		     WDI_EVENT_OFFLOAD_ALL)) {
			qdf_print("can't subcribe WDI_EVENT_OFFLOAD_ALL");
		}
	} else {
		qdf_print("WDI_EVENT_OFFLOAD_ALL already subscribed");
	}

	if (zyUMAC_scn_priv->stats_rx_subscriber.context == NULL &&
	    zyUMAC_scn_priv->stats_rx_subscriber.callback == NULL) {
		zyUMAC_scn_priv->stats_rx_subscriber.context = pdev_txrx_handle;
		zyUMAC_scn_priv->stats_rx_subscriber.callback =
		    zyUMAC_stats_get_all;
		if (cdp_wdi_event_sub
		    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
		     &zyUMAC_scn_priv->stats_rx_subscriber,
		     WDI_EVENT_RX_DESC_REMOTE)) {
			qdf_print("can't subcribe WDI_EVENT_RX_DESC_REMOTE");
		}
	} else {
		qdf_print("WDI_EVENT_RX_DESC_REMOTE already subscribed");
	}

	if (zyUMAC_scn_priv->stats_txdata_subscriber.context == NULL &&
	    zyUMAC_scn_priv->stats_txdata_subscriber.callback == NULL) {
		zyUMAC_scn_priv->stats_txdata_subscriber.context =
		    pdev_txrx_handle;
		zyUMAC_scn_priv->stats_txdata_subscriber.callback =
		    zyUMAC_stats_get_all;
		if (cdp_wdi_event_sub
		    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
		     &zyUMAC_scn_priv->stats_txdata_subscriber,
		     WDI_EVENT_TX_DATA)) {
			qdf_print("can't subcribe WDI_EVENT_TX_DATA");
		}
	} else {
		qdf_print("WDI_EVENT_TX_DATA already subscribed");
	}
#else
//remove data stats handler because rate count per packet requirement is gone
//leave code in case someday need it
#if 0
	//for WIFI 3.0 data stats
	if (zyUMAC_scn_priv->stats_tx_subscriber.context == NULL &&
	    zyUMAC_scn_priv->stats_tx_subscriber.callback == NULL) {
		zyUMAC_scn_priv->stats_tx_subscriber.context = scn->sc_pdev;;
		zyUMAC_scn_priv->stats_tx_subscriber.callback =
		    zyUMAC_stats_get_all;
		if (cdp_wdi_event_sub
		    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
		     &zyUMAC_scn_priv->stats_tx_subscriber,
		     WDI_EVENT_TX_PPDU_DESC)) {
			qdf_print("can't subcribe WDI_EVENT_TX_PPDU_DESC");
		}
	} else {
		qdf_print("WDI_EVENT_TX_PPDU_DESC already subscribed");
	}

	if (zyUMAC_scn_priv->stats_rx_subscriber.context == NULL &&
	    zyUMAC_scn_priv->stats_rx_subscriber.callback == NULL) {
		zyUMAC_scn_priv->stats_rx_subscriber.context = scn->sc_pdev;;
		zyUMAC_scn_priv->stats_rx_subscriber.callback =
		    zyUMAC_stats_get_all;
		if (cdp_wdi_event_sub
		    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
		     &zyUMAC_scn_priv->stats_rx_subscriber,
		     WDI_EVENT_RX_PPDU_DESC)) {
			qdf_print("can't subcribe WDI_EVENT_RX_PPDU_DESC");
		}
	} else {
		qdf_print("WDI_EVENT_RX_PPDU_DESC already subscribed");
	}
#endif
	//for wifi 3.0 peer update
	if (zyUMAC_scn_priv->stats_dp_subscriber.context == NULL &&
	    zyUMAC_scn_priv->stats_dp_subscriber.callback == NULL) {
		zyUMAC_scn_priv->stats_dp_subscriber.callback =
		    zyUMAC_stats_get_all;
		zyUMAC_scn_priv->stats_dp_subscriber.context = ic->ic_pdev_obj;
		if (cdp_wdi_event_sub
		    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
		     &zyUMAC_scn_priv->stats_dp_subscriber,
		     WDI_EVENT_UPDATE_DP_STATS)) {
			qdf_print("can't subcribe WDI_EVENT_UPDATE_DP_STATS");
		}
	} else {
		qdf_print("WDI_EVENT_UPDATE_DP_STATS already subscribed");
	}
#endif

#else
	if (zyUMAC_scn_priv->stats_tx_subscriber.callback == NULL) {
		if (wdi_event_sub(pdev_txrx_handle,
				  &zyUMAC_scn_priv->stats_tx_subscriber,
				  WDI_EVENT_OFFLOAD_ALL)) {
			qdf_print("can't subcribe WDI_EVENT_OFFLOAD_ALL");
		}
	} else {
		qdf_print("WDI_EVENT_OFFLOAD_ALL already subscribed");
	}

	if (zyUMAC_scn_priv->stats_rx_subscriber.callback == NULL) {
		if (wdi_event_sub(pdev_txrx_handle,
				  &zyUMAC_scn_priv->stats_rx_subscriber,
				  WDI_EVENT_RX_DESC_REMOTE)) {
			qdf_print("can't subcribe WDI_EVENT_RX_DESC_REMOTE");
		}
	} else {
		qdf_print("WDI_EVENT_RX_DESC_REMOTE already subscribed");
	}

	if (zyUMAC_scn_priv->stats_txdata_subscriber.callback == NULL) {
		if (wdi_event_sub(pdev_txrx_handle,
				  &zyUMAC_scn_priv->stats_txdata_subscriber,
				  WDI_EVENT_TX_DATA)) {
			qdf_print("can't subcribe WDI_EVENT_TX_DATA");
		}
	} else {
		qdf_print("WDI_EVENT_TX_DATA already subscribed");
	}
#endif

	return 0;
}

int zyUMAC_walstats_unregister_wdi_event(struct ol_ath_softc_net80211 *scn)
{
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	struct ieee80211com *ic = NULL;
	struct ol_txrx_pdev_t *pdev_txrx_handle;
#ifdef UMAC_SUPPORT_CFG80211
	ol_txrx_soc_handle soc_txrx_handle;
#endif

	if (!scn) {
		qdf_print("scn is null!!");
		return -1;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	if (zyUMAC_scn_priv == NULL || zyUMAC_scn_priv->scn_dev == NULL) {
		qdf_print("zyUMAC is not initialized");
		return -1;
	}

	ic = &zyUMAC_scn_priv->scn_dev->sc_ic;

#ifdef UMAC_SUPPORT_CFG80211
	if ((soc_txrx_handle =
	     wlan_psoc_get_dp_handle(scn->soc->psoc_obj)) == NULL) {
		qdf_print("can't find ol_txrx_soc_handle");
		return -1;
	}
	pdev_txrx_handle = wlan_pdev_get_dp_handle(ic->ic_pdev_obj);
#else
	pdev_txrx_handle = scn->pdev_txrx_handle;
#endif

	if (!pdev_txrx_handle) {
		qdf_print("can't find pdev_txrx_handle");
		return -1;
	}
#ifdef UMAC_SUPPORT_CFG80211

#ifndef QCAWIFI3
	if (cdp_wdi_event_unsub
	    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
	     &zyUMAC_scn_priv->stats_tx_subscriber, WDI_EVENT_OFFLOAD_ALL)) {
		qdf_print("fail to unsubcribe WDI_EVENT_OFFLOAD_ALL");
	}
	if (cdp_wdi_event_unsub
	    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
	     &zyUMAC_scn_priv->stats_rx_subscriber, WDI_EVENT_RX_DESC_REMOTE)) {
		qdf_print("fail to unsubcribe WDI_EVENT_RX_DESC_REMOTE");
	}
	if (cdp_wdi_event_unsub
	    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
	     &zyUMAC_scn_priv->stats_txdata_subscriber, WDI_EVENT_TX_DATA)) {
		qdf_print("fail to unsubcribe WDI_EVENT_TX_DATA");
	}
#else
#if 0
	if (cdp_wdi_event_unsub
	    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
	     &zyUMAC_scn_priv->stats_tx_subscriber, WDI_EVENT_TX_PPDU_DESC)) {
		qdf_print("fail to unsubcribe WDI_EVENT_TX_PPDU_DESC");
	}

	if (cdp_wdi_event_unsub
	    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
	     &zyUMAC_scn_priv->stats_rx_subscriber, WDI_EVENT_RX_PPDU_DESC)) {
		qdf_print("fail to unsubcribe WDI_EVENT_RX_PPDU_DESC");
	}
#endif
	if (cdp_wdi_event_unsub
	    (soc_txrx_handle, (struct cdp_pdev *)pdev_txrx_handle,
	     &zyUMAC_scn_priv->stats_dp_subscriber, WDI_EVENT_UPDATE_DP_STATS)) {
		qdf_print
		    ("%s %d cdp_wdi_event_unsub fail on WDI_EVENT_UPDATE_DP_STATS\n",
		     __func__, __LINE__);
	}
#endif

	zyUMAC_scn_priv->stats_tx_subscriber.context = NULL;
	zyUMAC_scn_priv->stats_rx_subscriber.context = NULL;
	zyUMAC_scn_priv->stats_txdata_subscriber.context = NULL;
	zyUMAC_scn_priv->stats_dp_subscriber.context = NULL;

#else
	if (wdi_event_unsub
	    (pdev_txrx_handle, &zyUMAC_scn_priv->stats_tx_subscriber,
	     WDI_EVENT_OFFLOAD_ALL)) {
		qdf_print("fail to unsubcribe WDI_EVENT_OFFLOAD_ALL");
	}
	if (wdi_event_unsub
	    (pdev_txrx_handle, &zyUMAC_scn_priv->stats_rx_subscriber,
	     WDI_EVENT_RX_DESC_REMOTE)) {
		qdf_print("fail to unsubcribe WDI_EVENT_RX_DESC_REMOTE");
	}
	if (wdi_event_unsub
	    (pdev_txrx_handle, &zyUMAC_scn_priv->stats_txdata_subscriber,
	     WDI_EVENT_TX_DATA)) {
		qdf_print("fail to unsubcribe WDI_EVENT_TX_DATA");
	}
#endif

	zyUMAC_scn_priv->stats_tx_subscriber.callback = NULL;
	zyUMAC_scn_priv->stats_rx_subscriber.callback = NULL;
	zyUMAC_scn_priv->stats_txdata_subscriber.callback = NULL;
	zyUMAC_scn_priv->stats_dp_subscriber.callback = NULL;

	return 0;
}

int zyUMAC_walstats_enable_ap_stats(struct ieee80211com *ic, u_int8_t stats_cmd)
{
	struct ol_ath_softc_net80211 *scn = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;

	if (!ic) {
		qdf_print("ic is null");
		return -1;
	}

	scn = OL_ATH_SOFTC_NET80211(ic);
	if (!scn) {
		qdf_print("scn is null");
		return -1;
	}

	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	if (zyUMAC_scn_priv == NULL) {
		qdf_print("zyUMAC_scn_priv is null");
		return -1;
	}

	if (stats_cmd == 1) {
		zyUMAC_walstats_register_wdi_event(scn);
	} else if (stats_cmd == 0) {
		zyUMAC_walstats_unregister_wdi_event(scn);
	} else {
		qdf_print("cmd %d isn't support", stats_cmd);
		return -1;
	}

	if (zyUMAC_scn_priv->orig_ol_ath_enable_ap_stats) {
		return zyUMAC_scn_priv->orig_ol_ath_enable_ap_stats(ic,
								   stats_cmd);
	} else {
		qdf_print("warn: no original hook");
		return -1;
	}
}

int zyUMAC_walstats_update_stats_event_handler(ol_scn_t sc, u_int8_t * data,
					   u_int16_t datalen)
{
	struct ol_ath_softc_net80211 *scn = NULL;
	wmi_unified_t wmi_handle = NULL;
	wmi_host_stats_event stats_param = { 0 };
	int ret = -1;
	zyUMAC_scn_priv_t *scn_priv;

#if UMAC_SUPPORT_CFG80211
	ol_ath_soc_softc_t *soc = (ol_ath_soc_softc_t *) sc;
	struct wlan_objmgr_psoc *psoc = soc->psoc_obj;
	struct wlan_objmgr_pdev *pdev = NULL;

	pdev =
	    wlan_objmgr_get_pdev_by_id(psoc, PDEV_UNIT(stats_param.pdev_id),
				       WLAN_MLME_SB_ID);
	if (pdev) {
		scn = lmac_get_pdev_feature_ptr(pdev);
		wlan_objmgr_pdev_release_ref(pdev, WLAN_MLME_SB_ID);
	} else {
		qdf_print("can't find pdev");
	}
#else
	scn = (struct ol_ath_softc_net80211 *)sc;
#endif

	scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;

	if (scn_priv->orig_ol_ath_update_stats_event_handler) {
		ret =
		    scn_priv->orig_ol_ath_update_stats_event_handler(sc, data,
								     datalen);
	} else {
		qdf_print("warn: no original hook");
	}

#if UMAC_SUPPORT_CFG80211
	wmi_handle = lmac_get_wmi_unified_hdl(psoc);
#else
	wmi_handle = scn->wmi_handle;
#endif

	if (!wmi_handle) {
		qdf_print("can't find wmi_handle");
		return -1;
	}

	OS_MEMZERO(&stats_param, sizeof(wmi_host_stats_event));
	if (wmi_extract_stats_param(wmi_handle, data, &stats_param) !=
	    QDF_STATUS_SUCCESS) {
		qdf_print("Failed to extract stats");
		return -1;
	}
	//update radio
	zyUMAC_stats_update_radio_stats(scn);

	//update vap
	if (stats_param.num_vdev_stats > 0) {
		int i;
		for (i = 0; i < stats_param.num_vdev_stats; i++) {
			struct ieee80211vap *vap = NULL;
			wmi_host_vdev_stats vdev_stats;

			if (wmi_extract_vdev_stats
			    (wmi_handle, data, i,
			     &vdev_stats) != QDF_STATUS_SUCCESS) {
				continue;
			}
			vap = ol_ath_vap_get(scn, vdev_stats.vdev_id);
			if (vap) {
				zyUMAC_stats_update_vap_stats(scn, vap,
							     &vdev_stats);
#ifdef UMAC_SUPPORT_CFG80211
				ol_ath_release_vap(vap);
#endif
			}
		}
	}
	//update peers
	if (stats_param.num_peer_stats > 0) {
		wmi_host_peer_stats st_peer_stats;
		wmi_host_peer_stats *peer_stats = &st_peer_stats;
		u_int8_t c_macaddr[IEEE80211_ADDR_LEN];
		struct ieee80211_node *ni;
		struct ieee80211com *ic;
		int i;

		if (scn && (ic = &scn->sc_ic)) {
			for (i = 0; i < stats_param.num_peer_stats; i++) {
				if (wmi_extract_peer_stats
				    (wmi_handle, data, i,
				     peer_stats) != QDF_STATUS_SUCCESS) {
					continue;
				}
				WMI_HOST_MAC_ADDR_TO_CHAR_ARRAY(&peer_stats->
								peer_macaddr,
								c_macaddr);
				ni = zyUMAC_ieee80211_find_node(&ic->ic_sta,
							 c_macaddr);
				if (ni) {
					zyUMAC_stats_update_peer_stats(ni,
								      peer_stats);
					zyUMAC_ieee80211_free_node(ni);
				}
			}
		}
	}

	return ret;
}

int zyUMAC_walwmi_unified_get_event_handler_ix(wmi_unified_t wmi_handle,
					   uint32_t event_id)
{
	uint32_t idx = 0;
	int32_t invalid_idx = -1;
	int32_t max_event_idx;
#if UMAC_SUPPORT_CFG80211
	struct wmi_soc *soc = NULL;
#endif

	if (!wmi_handle) {
		qdf_print("wmi_handle is null");
		return -1;
	}
#if UMAC_SUPPORT_CFG80211
	soc = wmi_handle->soc;
	max_event_idx = soc->max_event_idx;
#else
	max_event_idx = qdf_atomic_read(&wmi_handle->max_event_idx);
#endif

	for (idx = 0; (idx < max_event_idx &&
		       idx < WMI_UNIFIED_MAX_EVENT); ++idx) {
		if (wmi_handle->event_id[idx] == event_id &&
		    wmi_handle->event_handler[idx] != NULL) {
			return idx;
		}
	}

	return invalid_idx;
}

int zyUMAC_stats_process_rx(struct ol_txrx_pdev_t *txrx_pdev, qdf_nbuf_t msdu,
			   uint16_t peer_id, enum htt_rx_status status)
{
#ifdef UMAC_SUPPORT_CFG80211
	struct ol_ath_softc_net80211 *scn =
	    (struct ol_ath_softc_net80211 *)txrx_pdev->scnctx;
#else
	struct ol_ath_softc_net80211 *scn =
	    (struct ol_ath_softc_net80211 *)txrx_pdev->ctrl_pdev;
#endif
	struct ol_txrx_peer_t *peer;
	struct ieee80211vap *vap;

	if (scn == NULL) {
		qdf_print("scn is null");
		goto err;
	}

	peer = (peer_id == HTT_INVALID_PEER) ?
	    NULL : txrx_pdev->peer_id_to_obj_map[peer_id];

	if (peer && peer->vdev) {
		vap = ol_ath_vap_get(scn, peer->vdev->vdev_id);

		if (!vap) {
			qdf_print("vap is null");
			goto err;
		}

		zyUMAC_stats_rx_deliver_update(scn, vap, peer, msdu,
					      txrx_pdev->htt_pdev, status);

#ifdef UMAC_SUPPORT_CFG80211
		ol_ath_release_vap(vap);
#endif
		return 0;
	}

err:
	return -1;
}

int zyUMAC_stats_process_tx(struct ol_txrx_pdev_t *txrx_pdev, void *data,
			   uint16_t peer_id, enum htt_tx_status status)
{
	return 0;
}

int zyUMAC_stats_process_txdata(struct ol_txrx_pdev_t *txrx_pdev, void *data,
			       uint16_t peer_id, enum htt_tx_status status)
{
#ifdef UMAC_SUPPORT_CFG80211
	struct ol_ath_softc_net80211 *scn =
	    (struct ol_ath_softc_net80211 *)txrx_pdev->scnctx;
#else
	struct ol_ath_softc_net80211 *scn =
	    (struct ol_ath_softc_net80211 *)txrx_pdev->ctrl_pdev;
#endif
	struct ol_txrx_peer_t *peer;
	struct ieee80211vap *vap;

	peer = (peer_id == HTT_INVALID_PEER) ?
	    NULL : txrx_pdev->peer_id_to_obj_map[peer_id];

	if (peer && peer->vdev) {
		if (!scn) {
			qdf_print("scn is null");
			goto err;
		}
		vap = ol_ath_vap_get(scn, peer->vdev->vdev_id);

		if (!vap) {
			qdf_print("vap is null");
			goto err;
		}

		zyUMAC_stats_tx_complete_update(scn, vap, (qdf_nbuf_t) data,
					       status);
#ifdef UMAC_SUPPORT_CFG80211
		ol_ath_release_vap(vap);
#endif
		return 0;
	}

err:
	return -1;
}

#ifdef QCAWIFI3

void zyUMAC_handle_rx_ppdu_stats(struct ol_ath_softc_net80211 *scn,
				qdf_nbuf_t nbuf)
{
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	int ridx = -1;
	u_int32_t rate = 0;
	struct cdp_rx_indication_ppdu *cdp_rx_ppdu = NULL;
	if (!scn || !nbuf) {
		qdf_warn("scn or nbuf is NULL for pdev");
		return;
	}

	cdp_rx_ppdu = (struct cdp_rx_indication_ppdu *)nbuf->data;
	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	if (!zyUMAC_scn_priv) {
		qdf_warn("zyUMAC_scn_priv is NULL");
		return;
	}
	rate =
	    zyUMAC_dp_getrateindex(cdp_rx_ppdu->u.gi, cdp_rx_ppdu->u.mcs,
			    cdp_rx_ppdu->u.nss, cdp_rx_ppdu->u.preamble,
			    cdp_rx_ppdu->u.bw, ridx);

#if 0
	qdf_print("mac_addr             :%pM ", cdp_rx_ppdu->mac_addr);
	qdf_print("rssi :               :%d ", cdp_rx_ppdu->rssi);
	qdf_print("ppdu_length          :%d ", cdp_rx_ppdu->length);
	qdf_print("rate                 :%d ", rate);
#endif

	if (!rate) {
		return;
	}
	if (ridx < 0 || ridx >= OL_RATE_TABLE_SIZE) {
		qdf_warn("rate index out of range");
		return;
	}
	if (zyUMAC_scn_priv->scn_radio_stats.rxratecnt[ridx].rateval == 0) {
		zyUMAC_scn_priv->scn_radio_stats.rxratecnt[ridx].framecnt++;
		zyUMAC_scn_priv->scn_radio_stats.rxratecnt[ridx].rateval = rate;
		zyUMAC_scn_priv->scn_radio_stats.rxratecnt[ridx].bytecnt +=
		    cdp_rx_ppdu->length;
	} else if (zyUMAC_scn_priv->scn_radio_stats.rxratecnt[ridx].rateval ==
		   rate) {
		zyUMAC_scn_priv->scn_radio_stats.rxratecnt[ridx].framecnt++;
		zyUMAC_scn_priv->scn_radio_stats.rxratecnt[ridx].bytecnt +=
		    cdp_rx_ppdu->length;
	}
}

void zyUMAC_handle_tx_ppdu_stats(struct ol_ath_softc_net80211 *scn,
				qdf_nbuf_t nbuf)
{
	int i, ridx = -1;
	struct cdp_tx_completion_ppdu *ppdu_desc;
	struct cdp_tx_completion_ppdu_user *ppdu_user_desc;
	u_int32_t rate = 0;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	if (!scn || !nbuf) {
		qdf_warn("scn or nbuf is NULL for pdev");
		return;
	}
	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	if (!zyUMAC_scn_priv) {
		qdf_warn("zyUMAC_scn_priv is NULL");
		return;
	}
	ppdu_desc = (struct cdp_tx_completion_ppdu *)qdf_nbuf_data(nbuf);

	for (i = 0; i < ppdu_desc->num_users; i++) {
		ppdu_user_desc = &ppdu_desc->user[i];
		if ((ppdu_user_desc->mac_addr[0] == 0) &&
		    (ppdu_user_desc->mac_addr[1] == 0) &&
		    (ppdu_user_desc->mac_addr[2] == 0) &&
		    (ppdu_user_desc->mac_addr[3] == 0) &&
		    (ppdu_user_desc->mac_addr[4] == 0) &&
		    (ppdu_user_desc->mac_addr[5] == 0)) {
			continue;
		}
	/**
         * TID > 8 is set only for multicast data and management/ctrl packets
         * Stats print are required only for multicast data packets
         */
		if ((ppdu_user_desc->tid) > 8 && !ppdu_user_desc->is_mcast)
			continue;
#if 0
		qdf_print("    User     : %d", i);
		qdf_print("    mac_addr        : %pM ",
			  ppdu_user_desc->mac_addr);
		qdf_print("    peer_id         : %d ", ppdu_user_desc->peer_id);
		qdf_print("    success_bytes   : %u ",
			  ppdu_user_desc->success_bytes);
		qdf_print("    retry_bytes     : %d ",
			  ppdu_user_desc->retry_bytes);
		qdf_print("    bw              : %d ", ppdu_user_desc->bw);
		qdf_print("    nss             : %d ",
			  (ppdu_user_desc->nss + 1));
		qdf_print("    mcs             : %d ", ppdu_user_desc->mcs);
		qdf_print("    preamble        : %d ",
			  ppdu_user_desc->preamble);
		qdf_print("    gi              : %d ", ppdu_user_desc->gi);
		qdf_print("    tx_rate         : %d ",
			  dp_getrateindex(ppdu_user_desc->gi,
					  ppdu_user_desc->mcs,
					  ppdu_user_desc->nss,
					  ppdu_user_desc->preamble,
					  ppdu_user_desc->bw));
		qdf_print("    tx_ratekbps     : %d ",
			  ppdu_user_desc->tx_ratekbps);
#endif
		rate =
		    zyUMAC_dp_getrateindex(ppdu_user_desc->gi,
				    ppdu_user_desc->mcs,
				    ppdu_user_desc->nss,
				    ppdu_user_desc->preamble,
				    ppdu_user_desc->bw, ridx);
		if (!rate) {
			return;
		}
		if (ridx < 0 || ridx >= OL_RATE_TABLE_SIZE) {
			qdf_warn("rate index out of range");
			return;
		}
		if (zyUMAC_scn_priv->scn_radio_stats.txratecnt[ridx].rateval ==
		    0) {
			zyUMAC_scn_priv->scn_radio_stats.txratecnt[ridx].
			    framecnt++;
			zyUMAC_scn_priv->scn_radio_stats.txratecnt[ridx].
			    rateval = rate;
			zyUMAC_scn_priv->scn_radio_stats.txratecnt[ridx].
			    bytecnt += ppdu_user_desc->success_bytes;
		} else if (zyUMAC_scn_priv->scn_radio_stats.txratecnt[ridx].
			   rateval == rate) {
			zyUMAC_scn_priv->scn_radio_stats.txratecnt[ridx].
			    framecnt++;
			zyUMAC_scn_priv->scn_radio_stats.txratecnt[ridx].
			    bytecnt += ppdu_user_desc->success_bytes;
		}
	}
}

void zyUMAC_process_ppdu_stats(struct ol_ath_softc_net80211 *scn,
			      struct ieee80211com *ic, void *data, uint8_t dir)
{
	qdf_nbuf_t nbuf = (qdf_nbuf_t) data;

	struct cdp_tx_completion_ppdu *cdp_tx_ppdu;
	struct cdp_rx_indication_ppdu *cdp_rx_ppdu;

	if (!nbuf)
		return;

	if (dir == 0) {
		cdp_tx_ppdu =
		    (struct cdp_tx_completion_ppdu *)qdf_nbuf_data(nbuf);
		if (!cdp_tx_ppdu
		    || cdp_tx_ppdu->frame_type != CDP_PPDU_FTYPE_DATA)
			goto exit;
	} else {
		cdp_rx_ppdu = (struct cdp_rx_indication_ppdu *)nbuf->data;

		if (!cdp_rx_ppdu || cdp_rx_ppdu->peer_id == HTT_INVALID_PEER)
			goto exit;
	}
#if  ATH_DATA_TX_INFO_EN
	if (dir == 1)
		zyUMAC_handle_rx_ppdu_stats(scn, nbuf);
	else
		zyUMAC_handle_tx_ppdu_stats(scn, nbuf);
#endif
exit:
	return;
}

static void zyUMAC_peer_rssi_crossing_notify(zyUMAC_wal_wdev_t wdev,
					    u_int8_t * macaddr,
					    u_int8_t peer_rssi)
{
	zyUMAC_wal_event_wdev_sta_rssi_crossing event = {
		.wdev = wdev,
		.macaddr = macaddr,
		.rssi = peer_rssi,
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_WDEV_STA_RSSI_CROSSING, &event);
	return;
}

static void zyUMAC_peer_rate_crossing_notify(zyUMAC_wal_wdev_t wdev,
					    u_int8_t * macaddr,
					    u_int32_t last_tx_rate)
{
	zyUMAC_wal_event_wdev_sta_rate_crossing event = {
		.wdev = wdev,
		.macaddr = macaddr,
		.last_tx_rate = last_tx_rate
	};

	zyUMAC_wal_event_publish(zyUMAC_WAL_EVENT_WDEV_STA_RATE_CROSSING, &event);
	return;
}

//should be zyUMAC_stat.c, but some offload function used, move function here
int zyUMAC_stats_v30_update_peer_stats(void *pdev, void *stats, uint16_t peer_id)
{
	struct ieee80211vap *vap = NULL;
	struct ieee80211com *ic = NULL;
	struct ieee80211_node *ni;
	uint16_t vdev_id = CDP_INVALID_VDEV_ID;
	uint8_t peer_mac[6];
	struct ol_ath_softc_net80211 *scn;
	struct wlan_objmgr_pdev *pdev_obj = (struct wlan_objmgr_pdev *)pdev;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	zyUMAC_vap_priv_t *zyUMAC_vap_priv = NULL;
	zyUMAC_node_priv_t *zyUMAC_node_priv = NULL;
	struct cdp_peer_stats *peer_stats = NULL;

	if (wlan_objmgr_pdev_try_get_ref(pdev, WLAN_OSIF_ID) !=
	    QDF_STATUS_SUCCESS)
		return A_ERROR;

	scn =
	    (struct ol_ath_softc_net80211 *)lmac_get_pdev_feature_ptr(pdev_obj);
	if (!scn) {
		qdf_warn("scn is NULL for pdev:%pK", pdev_obj);
		goto stats_done;
	}
	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;

	if (peer_id == HTT_INVALID_PEER)
		goto stats_done;

	vdev_id =
	    cdp_get_peer_mac_addr_frm_id(wlan_psoc_get_dp_handle
					 (scn->soc->psoc_obj), peer_id,
					 peer_mac);
	if (vdev_id == CDP_INVALID_VDEV_ID)
		goto stats_done;

	vap = ol_ath_vap_get(scn, vdev_id);

	if (!vap) {
		goto stats_done;
	}
	ni = zyUMAC_ieee80211_vap_find_node(vap, peer_mac);
	if (!ni) {
		goto stats_done;
	}

	if (ni->ni_zyUMAC_node_priv == NULL) {
		// @TODO:
		zyUMAC_LOG(LOG_ERR, "cannot create node priv");
		goto stats_done;
	}

	ic = vap->iv_ic;

	zyUMAC_vap_priv = (zyUMAC_vap_priv_t *) vap->iv_zyUMAC_vap_priv;
	zyUMAC_node_priv = ni->ni_zyUMAC_node_priv;
	peer_stats = (struct cdp_peer_stats *)stats;

	if (ni != ni->ni_bss_node && vap->iv_opmode == IEEE80211_M_HOSTAP) {
		//check rssi/rate is good enough or kick it to BL if necessary
		/* compare the user provided rssi with peer rssi received */
		if (ni->ni_associd) {
			int a_rssi = ni->ni_rssi + DEFAULT_CHAN_REF_NOISE_FLOOR;
			zyUMAC_node_priv_stats_t *ns_stats =
				&zyUMAC_node_priv->ns_stats;
			u_int32_t last_rx_pkts =
				ns_stats->rx_to_stack_last_pkts;
			u_int32_t last_rx_eapol_pkts =
				ns_stats->rx_eapol_pkts;
			int is_eapol =
				(peer_stats->rx.rx_eapol != last_rx_eapol_pkts);

			/*
			 * Since UPDATE_PEER_STATS will be called both Tx/Rx
			 * event, we need to add the last_rx_pkts protection,
			 * otherwise, we might calculate ns_rate_fail_cnt
			 * more than once.
			 */
			if (last_rx_pkts != peer_stats->rx.to_stack.num) {
				if (!is_eapol && peer_stats->rx.last_rx_rate &&
				    peer_stats->rx.last_rx_rate <
				    zyUMAC_vap_priv->frgrate) {
					ns_stats->ns_rate_fail_cnt++;
				} else {
					ns_stats->ns_rate_fail_cnt = 0;
				}
			}

			//deauth if rate or rssi fail
			if (zyUMAC_vap_priv->frgrcnt
			    && zyUMAC_node_priv->ns_stats.ns_rate_fail_cnt >=
			    zyUMAC_vap_priv->frgrcnt) {
				zyUMAC_peer_rate_crossing_notify(vap, peer_mac,
								peer_stats->tx.
								last_tx_rate);
			}

			if (zyUMAC_vap_priv->frgkickoutpower && ni->ni_rssi &&
			(zyUMAC_vap_priv->frgkickoutpower > a_rssi)) {
				zyUMAC_peer_rssi_crossing_notify(vap, peer_mac,
								ni->ni_rssi);
			}
		}
		//zyUMAC_stats_v30_update_peer_stats() should be called per packet but keep the flexible
		if ((zyUMAC_scn_priv->ic_thrpktlen == 0) ||
		    ((zyUMAC_walget_stats_sub
		      (peer_stats->tx.tx_success.num,
		       zyUMAC_node_priv->ns_stats.
		       tx_success_last_pkts) *
		      zyUMAC_scn_priv->ic_thrpktlen) <
		     zyUMAC_walget_stats_sub(peer_stats->tx.tx_success.bytes,
					 zyUMAC_node_priv->ns_stats.
					 tx_success_last_bytes))) {
			zyUMAC_node_priv->ns_stats.
			    tx_data_throughput_pkts +=
			    zyUMAC_walget_stats_sub(peer_stats->tx.tx_success.num,
						zyUMAC_node_priv->ns_stats.
						tx_success_last_pkts);
			zyUMAC_node_priv->ns_stats.
			    tx_data_throughput_bytes +=
			    zyUMAC_walget_stats_sub(peer_stats->tx.tx_success.bytes,
						zyUMAC_node_priv->ns_stats.
						tx_success_last_bytes);
		}

		if ((zyUMAC_scn_priv->ic_thrpktlen == 0) ||
		    ((zyUMAC_walget_stats_sub
		      (peer_stats->rx.to_stack.num,
		       zyUMAC_node_priv->ns_stats.
		       rx_to_stack_last_pkts) *
		      zyUMAC_scn_priv->ic_thrpktlen) <
		     zyUMAC_walget_stats_sub(peer_stats->rx.to_stack.bytes,
					 zyUMAC_node_priv->ns_stats.
					 rx_to_stack_last_bytes))) {
			zyUMAC_node_priv->ns_stats.
			    rx_data_throughput_pkts +=
			    zyUMAC_walget_stats_sub(peer_stats->rx.to_stack.num,
						zyUMAC_node_priv->ns_stats.
						rx_to_stack_last_pkts);
			zyUMAC_node_priv->ns_stats.
			    rx_data_throughput_bytes +=
			    zyUMAC_walget_stats_sub(peer_stats->rx.to_stack.bytes,
						zyUMAC_node_priv->ns_stats.
						rx_to_stack_last_bytes);
		}
	}

	zyUMAC_node_priv->ns_stats.tx_success_last_pkts =
	    peer_stats->tx.tx_success.num;
	zyUMAC_node_priv->ns_stats.tx_success_last_bytes =
	    peer_stats->tx.tx_success.bytes;
	zyUMAC_node_priv->ns_stats.rx_to_stack_last_pkts =
	    peer_stats->rx.to_stack.num;
	zyUMAC_node_priv->ns_stats.rx_to_stack_last_bytes =
	    peer_stats->rx.to_stack.bytes;
	zyUMAC_node_priv->ns_stats.rx_eapol_pkts =
	    peer_stats->rx.rx_eapol;

	zyUMAC_ieee80211_free_node(ni);

	if (vap)
		ol_ath_release_vap(vap);
	wlan_objmgr_pdev_release_ref(pdev, WLAN_OSIF_ID);

	return 0;

stats_done:
	if (ni)
		zyUMAC_ieee80211_free_node(ni);

	if (vap)
		ol_ath_release_vap(vap);
	wlan_objmgr_pdev_release_ref(pdev, WLAN_OSIF_ID);

	return A_ERROR;
}

#endif

void zyUMAC_stats_get_all(void *pdev, enum WDI_EVENT event,
			 void *log_data, uint16_t peer_id, uint32_t status)
{

#ifdef QCAWIFI3
	struct wlan_objmgr_pdev *pdev_obj = (struct wlan_objmgr_pdev *)pdev;
	struct ieee80211com *ic = wlan_pdev_get_mlme_ext_obj(pdev_obj);
	struct ol_ath_softc_net80211 *scn = OL_ATH_SOFTC_NET80211(ic);
#endif

	if (!pdev) {
		return;
	}

	switch (event) {
	case WDI_EVENT_RX_DESC_REMOTE:
		/* process_rx_stats */
		zyUMAC_stats_process_rx(pdev, log_data, peer_id, status);
		break;

	case WDI_EVENT_TX_STATUS:
	case WDI_EVENT_OFFLOAD_ALL:
		/* process_tx_stats */
		zyUMAC_stats_process_tx(pdev, log_data, peer_id, status);
		break;

	case WDI_EVENT_TX_DATA:
		zyUMAC_stats_process_txdata(pdev, log_data, peer_id, status);
		break;
#ifdef QCAWIFI3
	case WDI_EVENT_UPDATE_DP_STATS:
		if (status == UPDATE_PEER_STATS) {
			zyUMAC_stats_v30_update_peer_stats(pdev, log_data,
							  peer_id);
		}
		break;
	case WDI_EVENT_TX_PPDU_DESC:

		zyUMAC_process_ppdu_stats(scn, ic, log_data, 0);
		break;
	case WDI_EVENT_RX_PPDU_DESC:
		zyUMAC_process_ppdu_stats(scn, ic, log_data, 1);
		break;
#endif
	default:
		break;
	}
	return;
}

#ifdef SKS_STATS_SUPPORT
static int
zyUMAC_walstats_bss_chan_info_event_handler(ol_scn_t sc, u_int8_t * data,
					u_int32_t datalen)
{
	wmi_host_pdev_bss_chan_info_event bss_chan_info;
	wmi_host_pdev_bss_chan_info_event *chan_stats = &bss_chan_info;
	struct ol_ath_softc_net80211 *scn;
	zyUMAC_scn_priv_t *scn_priv;
	int status = 0;

	uint32_t target_type;

	struct hw_params {
		char name[10];
		uint64_t channel_counters_freq;
	};

	static const struct hw_params hw_params_list[] = {
		{
		 .name = "ar9888",
		 .channel_counters_freq = 88000,
		 },
		{
		 .name = "ipq4019",
		 .channel_counters_freq = 125000,
		 },
		{
		 .name = "qca9984",
		 .channel_counters_freq = 150000,
		 },
		{
		 .name = "qca9888",
		 .channel_counters_freq = 150000,
		 },
		{
		 .name = "ar900b",
		 .channel_counters_freq = 150000,
		 },
	};

	const struct hw_params *target;

#if UMAC_SUPPORT_CFG80211
	zyUMAC_wmi_handle_t *wmi_handle;
	struct wlan_objmgr_pdev *pdev;
	ol_ath_soc_softc_t *soc = (ol_ath_soc_softc_t *) sc;

	wmi_handle = lmac_get_wmi_hdl(soc->psoc_obj);

	if (wmi_extract_bss_chan_info_event(wmi_handle, data, &bss_chan_info) !=
	    QDF_STATUS_SUCCESS) {
		qdf_print("Failed to extract bss chan info");
	}
	target_type = lmac_get_tgt_type(soc->psoc_obj);
#else
	scn = (struct ol_ath_softc_net80211 *)sc;
	target_type = scn->target_type;
#endif

	if (target_type == TARGET_TYPE_AR9888)
		target = &hw_params_list[0];
	else if (target_type == TARGET_TYPE_IPQ4019)
		target = &hw_params_list[1];
	else if (target_type == TARGET_TYPE_QCA9984)
		target = &hw_params_list[2];
	else if (target_type == TARGET_TYPE_QCA9888)
		target = &hw_params_list[3];
	else {
		qdf_print("Error: not enabled for target type");
		return 0;
	}

#if UMAC_SUPPORT_CFG80211
	pdev =
	    wlan_objmgr_get_pdev_by_id(soc->psoc_obj,
				       PDEV_UNIT(bss_chan_info.pdev_id),
				       WLAN_MLME_SB_ID);
	if (pdev == NULL) {
		qdf_print("%s: pdev object (id: %d) is NULL ", __func__,
			  PDEV_UNIT(bss_chan_info.pdev_id));
		return -1;
	}

	scn = lmac_get_pdev_feature_ptr(pdev);
	if (scn == NULL) {
		qdf_print("%s: scn (id: %d) is NULL ", __func__,
			  PDEV_UNIT(bss_chan_info.pdev_id));
		status = -1;
		goto done;
	}
#endif

	scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;

	if (scn_priv == NULL) {
		qdf_print("scn_priv is NULL\n");
		status = -1;
		goto done;
	}

	if (scn_priv->orig_ol_ath_bss_chan_info_event_handler != NULL) {
		status =
		    scn_priv->orig_ol_ath_bss_chan_info_event_handler(sc, data,
								      datalen);

		if (status != 0) {
			goto done;
		}
	} else {
		qdf_print("orig_ol_ath_bss_chan_info_event_handler is NULL\n");
	}

	if (chan_stats) {
		uint64_t tx_cycle_count =
		    (uint64_t) chan_stats->
		    tx_cycle_count_high << 32 | chan_stats->tx_cycle_count_low;
		uint64_t rx_cycle_count =
		    (uint64_t) chan_stats->
		    rx_cycle_count_high << 32 | chan_stats->rx_cycle_count_low;
		uint64_t rx_bss_cycle_count =
		    (uint64_t) chan_stats->
		    rx_bss_cycle_count_high << 32 | chan_stats->
		    rx_bss_cycle_count_low;
		uint64_t rx_outside_bss_cycle_count =
		    rx_cycle_count - rx_bss_cycle_count;
		uint64_t cycle_count =
		    (uint64_t) chan_stats->cycle_count_high << 32 | chan_stats->
		    cycle_count_low;
		uint64_t tx_duration =
		    div_u64(tx_cycle_count, target->channel_counters_freq);
		uint64_t rx_duration =
		    div_u64(rx_cycle_count, target->channel_counters_freq);
		uint64_t rx_in_bss_duration = div_u64(rx_bss_cycle_count,
						      target->
						      channel_counters_freq);
		uint64_t rx_outside_bss_dur =
		    div_u64(rx_outside_bss_cycle_count,
			    target->channel_counters_freq);
		uint64_t total_duration =
		    div_u64(cycle_count, target->channel_counters_freq);

		scn_priv->txrx_fw_stats.ic_tx_duration =
		    (u_int32_t) div_u64(tx_duration * 100, total_duration);
		scn_priv->txrx_fw_stats.ic_rx_duration =
		    (u_int32_t) div_u64(rx_duration * 100, total_duration);
		scn_priv->txrx_fw_stats.ic_rx_in_bss_duration =
		    (u_int32_t) div_u64(rx_in_bss_duration * 100,
					total_duration);
		scn_priv->txrx_fw_stats.ic_rx_ou_bss_duration =
		    (u_int32_t) div_u64(rx_outside_bss_dur * 100,
					total_duration);
	}

done:
#if UMAC_SUPPORT_CFG80211
	wlan_objmgr_pdev_release_ref(pdev, WLAN_MLME_SB_ID);
#endif
	return status;
}
#endif

/*
 * Hook ol_ath_stats_attach() in __ol_ath_attach() or ol_ath_soc_start()
 */
int zyUMAC_wal_stats_attach(struct ol_ath_softc_net80211 *scn)
{
	struct ieee80211com *ic = NULL;
	zyUMAC_scn_priv_t *scn_priv = NULL;
#ifndef QCAWIFI3
	wmi_unified_t wmi_handle;
	int handler_id = -1;
	uint32_t evt_id;
#endif

	if (!scn) {
		qdf_print("scn is null");
		return -1;
	}

	if ((ic = &scn->sc_ic) == NULL) {
		qdf_print("ic is null");
		return -1;
	}

	scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	if (scn_priv == NULL) {
		qdf_print("zyUMAC_scn_priv is null");
		return -1;
	}
#ifndef QCAWIFI3
	//wmi
#ifdef UMAC_SUPPORT_CFG80211
	wmi_handle = lmac_get_wmi_unified_hdl(scn->soc->psoc_obj);
#else
	wmi_handle = scn->wmi_handle;
#endif

	if (wmi_handle) {
		qdf_print("Hook wmi_update_stats_event_id %d event",
			  wmi_update_stats_event_id);
		evt_id = wmi_handle->wmi_events[wmi_update_stats_event_id];
		handler_id =
		    zyUMAC_walwmi_unified_get_event_handler_ix(wmi_handle, evt_id);

		if (handler_id != -1 &&
		    !scn_priv->orig_ol_ath_update_stats_event_handler &&
		    zyUMAC_walstats_update_stats_event_handler !=
		    (void *)(wmi_handle->event_handler[handler_id])) {
			scn_priv->orig_ol_ath_update_stats_event_handler =
			    (void *)(wmi_handle->event_handler[handler_id]);
			wmi_unified_unregister_event_handler(wmi_handle,
							     wmi_update_stats_event_id);
			wmi_unified_register_event_handler(wmi_handle,
							   wmi_update_stats_event_id,
							   (wmi_unified_event_handler)
							   zyUMAC_walstats_update_stats_event_handler,
							   WMI_RX_UMAC_CTX);
		} else {
			qdf_print("Failed to hook %d event",
				  wmi_update_stats_event_id);
		}
#ifdef SKS_STATS_SUPPORT
		qdf_print("Hook wmi_pdev_bss_chan_info_event_id %d event",
			  wmi_pdev_bss_chan_info_event_id);
		evt_id =
		    wmi_handle->wmi_events[wmi_pdev_bss_chan_info_event_id];
		handler_id =
		    zyUMAC_walwmi_unified_get_event_handler_ix(wmi_handle, evt_id);

		if (handler_id != -1
		    && !scn_priv->orig_ol_ath_bss_chan_info_event_handler) {
			scn_priv->orig_ol_ath_bss_chan_info_event_handler =
			    (void *)(wmi_handle->event_handler[handler_id]);
			wmi_unified_unregister_event_handler(wmi_handle,
							     wmi_pdev_bss_chan_info_event_id);
			wmi_unified_register_event_handler(wmi_handle,
							   wmi_pdev_bss_chan_info_event_id,
							   (wmi_unified_event_handler)
							   zyUMAC_walstats_bss_chan_info_event_handler,
							   WMI_RX_UMAC_CTX);
		} else {
			qdf_print("Failed to hook %d event",
				  wmi_pdev_bss_chan_info_event_id);
		}
#endif
	} else {
		qdf_print("Can't find wmi_handle!!");
	}
#else /* #ifndef QCAWIFI3 */
#endif

	if (ic->ic_ath_enable_ap_stats == zyUMAC_walstats_enable_ap_stats) {
		qdf_print("zyUMAC_walstats_enable_ap_stats already hooked");
		return 0;
	}

	scn_priv->orig_ol_ath_enable_ap_stats = ic->ic_ath_enable_ap_stats;
	ic->ic_ath_enable_ap_stats = zyUMAC_walstats_enable_ap_stats;

	return 0;
}

void zyUMAC_wal_stats_detach(struct ol_ath_softc_net80211 *scn)
{
	struct ieee80211com *ic = NULL;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;

	if (!scn) {
		qdf_print("scn is null");
		return;
	}

	if ((ic = &scn->sc_ic) == NULL) {
		qdf_print("ic is null");
		return;
	}

	if ((zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv) == NULL) {
		qdf_print("zyUMAC_scn_priv is null");
		return;
	}

	if (ic->ic_ath_enable_ap_stats == zyUMAC_walstats_enable_ap_stats) {
		ic->ic_ath_enable_ap_stats =
		    zyUMAC_scn_priv->orig_ol_ath_enable_ap_stats;
	} else {
		qdf_print("zyUMAC_walstats_enable_ap_stats already unhooked");
	}

	zyUMAC_scn_priv->orig_ol_ath_enable_ap_stats = NULL;
}

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif
EXPORT_SYMBOL(zyUMAC_wal_stats_attach);
EXPORT_SYMBOL(zyUMAC_wal_stats_detach);
