#include "ol_if_athvar.h"
#include "zyUMAC_cfg80211.h"

#if UMAC_SUPPORT_CFG80211
#include <../net/wireless/core.h>
#include "ieee80211_ucfg.h"

#include "zyUMAC_wal_vap.h"
#include "zyUMAC_wal_api.h"
#include "zyUMAC_compat.h"

static struct wireless_dev *(*orig_cfg80211_add_virtual_intf) (struct wiphy *
							       wiphy,
							       const char *name,
							       unsigned char
							       name_assign_type,
							       enum
							       nl80211_iftype
							       type,
							       u_int32_t *
							       flags,
							       struct vif_params
							       * params);
static int (*orig_cfg80211_del_virtual_intf) (struct wiphy * wiphy,
					      struct wireless_dev * wdev);

struct wireless_dev *zyUMAC_cfg80211_add_virtual_intf(struct wiphy *wiphy,
						     const char *name,
						     unsigned char
						     name_assign_type,
						     enum nl80211_iftype type,
						     u_int32_t * flags,
						     struct vif_params *params)
{
	struct wireless_dev *rtnPtr;
	struct cfg80211_context *cfg_ctx = NULL;
	struct ieee80211com *ic;
	struct ol_ath_softc_net80211 *scn = NULL;

	wlan_if_t vap;
	zyUMAC_scn_priv_t *zyUMAC_scn_priv = NULL;
	cfg_ctx = (struct cfg80211_context *)wiphy_priv(wiphy);
	ic = cfg_ctx->ic;
	scn = (struct ol_ath_softc_net80211 *)ic;
	zyUMAC_scn_priv = (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	rtnPtr =
	    orig_cfg80211_add_virtual_intf(wiphy, name, name_assign_type, type,
					   flags, params);

	if (rtnPtr) {
		vap = TAILQ_LAST(&ic->ic_vaps, ic_vaps_head);
		zyUMAC_vap_create_completed(vap);
	}

	return rtnPtr;
}

int zyUMAC_del_virtual_intf(struct wiphy *wiphy, struct wireless_dev *wdev)
{
	int retval = 0;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3, 6, 0))
	struct net_device *dev = wdev->netdev;
#endif
	struct cfg80211_context *cfg_ctx = NULL;
	struct ieee80211com *ic;
	cfg_ctx = (struct cfg80211_context *)wiphy_priv(wiphy);
	ic = cfg_ctx->ic;

	/*
	 * can not delete wifiX interface
	 * when recovery_in_progress is set VAP destroy is already done.
	 */
	if ((ic->ic_wdev.netdev == dev) || ic->recovery_in_progress) {
		return -1;
	}

	retval = osif_ioctl_delete_vap(dev);
	return retval;
}

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3, 16, 0))
int zyUMAC_cfg80211_get_station(struct wiphy *wiphy,
			       struct net_device *dev, const uint8_t * mac,
			       struct station_info *sinfo)
#else
int zyUMAC_cfg80211_get_station(struct wiphy *wiphy,
			       struct net_device *dev, uint8_t * mac,
			       struct station_info *sinfo)
#endif
{
	struct ieee80211_node *ni;
	osif_dev *osifp = ath_netdev_priv(dev);
	wlan_if_t vap = osifp->os_if;

	ni = zyUMAC_ieee80211_vap_find_node(vap, mac);
	if (ni) {
		struct stainforeq req;
		struct ieee80211req_sta_info *si_data;

		zyUMAC_node_priv_t *zyUMAC_node_priv;

		req.space = 0;
		req.vap = vap;
		get_sta_space(&req, ni);
		if (req.space == 0) {
			zyUMAC_ieee80211_free_node(ni);
			return 0;
		}
		si_data = (void *)qdf_mem_malloc(req.space);
		if (si_data == NULL) {
			zyUMAC_ieee80211_free_node(ni);
			return -ENOMEM;
		}

		zyUMAC_node_priv = ni->ni_zyUMAC_node_priv;

		memset(si_data, 0, sizeof(req.space));
		req.si = si_data;
		get_sta_info(&req, ni);
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3, 16, 0))
		sinfo->inactive_time = si_data->isi_inact * 1000;
		sinfo->filled |= BIT(NL80211_STA_INFO_INACTIVE_TIME);

		zyUMAC_wal_wnode_update_stats(ni, &zyUMAC_node_priv->ns_stats);

		sinfo->rx_bytes =
		    zyUMAC_node_priv->ns_stats.wal_stats.ns_rx_success_bytes;
		sinfo->tx_bytes =
		    zyUMAC_node_priv->ns_stats.wal_stats.ns_tx_success_bytes;
		sinfo->rx_packets =
		    zyUMAC_node_priv->ns_stats.wal_stats.ns_rx_success_num;
		sinfo->tx_packets =
		    zyUMAC_node_priv->ns_stats.wal_stats.ns_tx_success_num;

		sinfo->filled |= (BIT(NL80211_STA_INFO_RX_BYTES64) |
				  BIT(NL80211_STA_INFO_TX_BYTES64) |
				  BIT(NL80211_STA_INFO_RX_PACKETS) |
				  BIT(NL80211_STA_INFO_TX_PACKETS));
#endif

		qdf_mem_free(si_data);
		zyUMAC_ieee80211_free_node(ni);
	} else {
		/* Due to in some cases, the query of callback get_station will be after we free ni,
		   but we still need to report statistics of the left station in this case.
		   Use the hook to check if the platform needs to handle this case. 
		 */
		if (_wdev_hook_table->wdev_hook_get_left_station_stats != NULL) {
			zyUMAC_wal_station_txrx_stats_t stats = { 0 };

			if (_wdev_hook_table->
			    wdev_hook_get_left_station_stats(vap, &stats,
							     mac) == 0) {
				sinfo->rx_bytes = stats.rx_bytes;
				sinfo->tx_bytes = stats.tx_bytes;
				sinfo->rx_packets = stats.rx_packets;
				sinfo->tx_packets = stats.tx_packets;

				sinfo->filled |=
				    (BIT(NL80211_STA_INFO_RX_BYTES64) |
				     BIT(NL80211_STA_INFO_TX_BYTES64) |
				     BIT(NL80211_STA_INFO_RX_PACKETS) |
				     BIT(NL80211_STA_INFO_TX_PACKETS));
			}
		}
	}

	return 0;
}

int zyUMAC_cfg80211_radio_attach(struct device *dev, struct net_device *net_dev,
				struct ieee80211com *ic)
{
	struct ol_ath_softc_net80211 *scn = OL_ATH_SOFTC_NET80211(ic);
	zyUMAC_scn_priv_t *zyUMAC_scn_priv =
	    (zyUMAC_scn_priv_t *) scn->scn_zyUMAC_priv;
	int status =
	    zyUMAC_scn_priv->orig_ic_cfg80211_radio_attach(dev, net_dev, ic);

	if (!status) {
		struct wiphy *wiphy = ic->ic_wiphy;
		struct cfg80211_registered_device *rdev = wiphy_to_rdev(wiphy);
		struct cfg80211_ops *wlan_cfg80211_ops =
		    (struct cfg80211_ops *)rdev->ops;

//Add hook for add/delete on CFG80211
		if (!orig_cfg80211_add_virtual_intf) {
			orig_cfg80211_add_virtual_intf =
			    wlan_cfg80211_ops->add_virtual_intf;
		}
		wlan_cfg80211_ops->add_virtual_intf =
		    zyUMAC_cfg80211_add_virtual_intf;

		if (!orig_cfg80211_del_virtual_intf) {
			orig_cfg80211_del_virtual_intf =
			    wlan_cfg80211_ops->del_virtual_intf;
		}
		wlan_cfg80211_ops->del_virtual_intf = zyUMAC_del_virtual_intf;

		/* Since in QCA SPF driver, get_station callback doesn't do anything,
		   thus we don't need to save the original callback function */
		wlan_cfg80211_ops->get_station = zyUMAC_cfg80211_get_station;
	}
	return status;
}

#else

int zyUMAC_cfg80211_radio_attach(struct device *dev, struct net_device *net_dev,
				struct ieee80211com *ic)
{
	return 0;
}

#endif
