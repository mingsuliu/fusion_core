#ifndef ZYXEL_LOG_HEADER_H
#define ZYXEL_LOG_HEADER_H

enum {
	LOG_CRIT = 2,
	LOG_ERR = 3,
	LOG_WARN = 4,
	LOG_INFO = 6,
	LOG_DBG = 7
};

void zyUMAC_log_output(unsigned int mask, char *format, ...);

#define zyUMAC_LOG(mask, s, ...)	\
	zyUMAC_log_output(mask, "%s(%d): " s, __func__, __LINE__, ##__VA_ARGS__)

#endif
