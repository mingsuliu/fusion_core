#include <linux/printk.h>
#include <linux/string.h>
#include "zyUMAC_log.h"

void zyUMAC_log_output(unsigned int mask, char *format, ...)
{
	va_list args;
	struct va_format vaf;
	char verbose_fmt[] = KERN_DEFAULT "zyUMAC> %pV\n";

	va_start(args, format);
	vaf.fmt = format;
	vaf.va = &args;

	switch (mask) {
	case LOG_CRIT:
		memcpy(verbose_fmt, KERN_CRIT, sizeof(KERN_CRIT) - 1);
		dump_stack();
	case LOG_ERR:
		memcpy(verbose_fmt, KERN_ERR, sizeof(KERN_ERR) - 1);
		break;
	case LOG_WARN:
		memcpy(verbose_fmt, KERN_WARNING, sizeof(KERN_WARNING) - 1);
		break;
	case LOG_INFO:
		memcpy(verbose_fmt, KERN_INFO, sizeof(KERN_INFO) - 1);
		break;
	case LOG_DBG:
		memcpy(verbose_fmt, KERN_DEBUG, sizeof(KERN_DEBUG) - 1);
		break;
	default:
		break;
	}

	printk(verbose_fmt, &vaf);
	va_end(args);
}
EXPORT_SYMBOL(zyUMAC_log_output);
