#ifndef __zyUMAC_WAL_EVENT_H__
#define __zyUMAC_WAL_EVENT_H__

#include "zyUMAC_wal_types.h"

enum {
	zyUMAC_WAL_EVENT_MLME_DEAUTH_COMPLETE,
	zyUMAC_WAL_EVENT_MLME_DISASSOC_COMPLETE,
	zyUMAC_WAL_EVENT_MLME_AUTH_INDICATION,
	zyUMAC_WAL_EVENT_MLME_DEAUTH_INDICATION,
	zyUMAC_WAL_EVENT_MLME_ASSOC_INDICATION,
	zyUMAC_WAL_EVENT_MLME_REASSOC_INDICATION,
	zyUMAC_WAL_EVENT_MLME_DISASSOC_INDICATION,

	zyUMAC_WAL_EVENT_WDEV_CHANNEL_CHANGE,
	zyUMAC_WAL_EVENT_WDEV_STA_RSSI_CROSSING,
	zyUMAC_WAL_EVENT_WDEV_STA_RATE_CROSSING,

	zyUMAC_WAL_EVENT_MAX
};

typedef struct _zyUMAC_wal_event_mlme_deauth_complete {
	zyUMAC_wal_wdev_t wdev;
	u_int8_t *macaddr;
	int status;
} zyUMAC_wal_event_mlme_deauth_complete_t;

typedef struct _zyUMAC_wal_event_mlme_disassoc_complete {
	zyUMAC_wal_wdev_t wdev;
	u_int8_t *macaddr;
	u_int32_t reason;
	int status;
} zyUMAC_wal_event_mlme_disassoc_complete_t;

typedef struct _zyUMAC_wal_event_mlme_auth_ind {
	zyUMAC_wal_wdev_t wdev;
	u_int8_t *macaddr;
	u_int16_t status;
} zyUMAC_wal_event_mlme_auth_ind_t;

typedef struct _zyUMAC_wal_event_mlme_deauth_ind {
	zyUMAC_wal_wdev_t wdev;
	u_int8_t *macaddr;
	u_int16_t associd;
	u_int16_t reason;
} zyUMAC_wal_event_mlme_deauth_ind_t;

typedef struct _zyUMAC_wal_event_mlme_assoc_ind {
	zyUMAC_wal_wdev_t wdev;
	u_int8_t *macaddr;
	u_int16_t result;
	bool reassoc;
} zyUMAC_wal_event_mlme_assoc_ind_t;

typedef struct _zyUMAC_wal_event_mlme_disassoc_ind {
	zyUMAC_wal_wdev_t wdev;
	u_int8_t *macaddr;
	u_int16_t associd;
	u_int16_t reason;
} zyUMAC_wal_event_mlme_disassoc_ind_t;

typedef struct _zyUMAC_wal_event_wdev_channel_change {
	zyUMAC_wal_wdev_t wdev;
	wlan_chan_t chan;
} zyUMAC_wal_event_wdev_channel_change;

typedef struct _zyUMAC_wal_event_wdev_sta_rssi_crossing {
	zyUMAC_wal_wdev_t wdev;
	u_int8_t *macaddr;
	u_int8_t rssi;
} zyUMAC_wal_event_wdev_sta_rssi_crossing;

typedef struct _zyUMAC_wal_event_wdev_sta_rate_crossing {
	zyUMAC_wal_wdev_t wdev;
	u_int8_t *macaddr;
	u_int32_t last_tx_rate;
} zyUMAC_wal_event_wdev_sta_rate_crossing;

#endif
