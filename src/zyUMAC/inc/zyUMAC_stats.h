#ifndef ZYXEL_UMAC_STATS_HEADER_H
#define ZYXEL_UMAC_STATS_HEADER_H

#include "zyUMAC_types.h"

/* Radio MPDU Statistics */
typedef struct {
	ULONGLONG txMpduCnt;
	ULONGLONG txDiscardMpduCnt;
	ULONGLONG txRetryMpduCnt;
	ULONGLONG txPayloadBytes;
	ULONGLONG rxPayloadBytes;
} RADIO_MPDU_STATS_T;

/* BSS Statistics */
typedef struct {
	char WlanID[6];
	ULONGLONG DropFrameCnt;
	ULONGLONG DropFrameBytes;
	ULONGLONG TxFrameCnt;
	ULONGLONG TxUcastFrameCnt;
	ULONGLONG TxBcastFrameCnt;
	ULONGLONG TxAccBcastFrameCnt;
	ULONGLONG TxFrameBytes;

	ULONGLONG TxUcastFrameBytes;
	ULONGLONG TxBcastFrameByte;
	ULONGLONG RxFrameCnt;
	ULONGLONG RxUcastFrameCnt;
	ULONGLONG RxBcastFrameCnt;
	ULONGLONG RxFrameBytes;
	ULONGLONG RxUcastFrameBytes;
	ULONGLONG RxBcastFrameBytes;
	ULONGLONG RxTotalFrameCnt;
	ULONGLONG RxTotalFrameBytes;
	ULONGLONG TxTotalFrameCnt;
	ULONGLONG TxTotalFrameBytes;

} BSS_STATS_T;

/* Station Statistics */
typedef struct {
	char IfName[32];
	unsigned char MacAddr[6];
	ULONGLONG RxFrameCnt[4];
	ULONGLONG TxFrameCnt[4];
	ULONGLONG DropFrameCnt[4];
	ULONGLONG RxFrameBytes[4];
	ULONGLONG TxFrameBytes[4];
	ULONGLONG DropFrameBytes[4];

	ULONGLONG Snr;
	INT32 Rssi;
	ULONGLONG RxRate;
	ULONGLONG TxRate;
	ULONGLONG DataRate;
	u_int32_t PER;
	char wireless_mode;
} STATION_STATS_T;

struct zyUMAC_wal_radio_stats {
	u_int32_t rtsGood;
	u_int32_t rtsBad;
	u_int32_t ackRcvBad;
	u_int32_t fcsBad;
	u_int32_t pdev_cont_xretry;
	u_int32_t rx_badcrypt;
	u_int32_t rx_packets;
	u_int64_t rx_bytes;
	u_int32_t rx_phyerr;
	u_int32_t rx_badmic;
	u_int32_t rx_num_data;
	u_int32_t rx_data_bytes;
	u_int32_t rx_crcerr;
	u_int64_t tx_bytes;
	u_int32_t tx_num_data;
	u_int32_t tx_xretry;
	u_int32_t tx_packets;
	u_int64_t tx_beacon;
	u_int32_t tx_bawadv;
};

struct zyUMAC_channel_stats {
	u_int32_t duration;
	u_int32_t busy_time;
	u_int32_t tx_time;
	u_int32_t rx_time;
	u_int32_t obss_time;
	u_int32_t edcca_time;
};

typedef struct zyUMAC_scn_priv_radio_stats {
	struct zyUMAC_wal_radio_stats wal_stats;
	struct zyUMAC_channel_stats channel_stats;

	/*radio counter*/
	u_int32_t noise_floor;

	u_int32_t rx_rssi_highest;
	u_int32_t rx_rssi_lowest;

	u_int64_t tx_cycle;
	u_int64_t rx_cycle;

	u_int64_t busy_cycle;
	u_int64_t total_cycle;

	u_int32_t Last1SecPER;
	u_int32_t Last1TxFailCnt;
	u_int32_t Last1TxCnt;

	/*rate counter*/
	struct {
		u_int32_t rateval;
		u_int64_t framecnt;
		u_int64_t bytecnt;
	} rxratecnt[OL_RATE_TABLE_SIZE];

	struct {
		u_int32_t rateval;
		u_int64_t framecnt;
		u_int64_t bytecnt;
	} txratecnt[OL_RATE_TABLE_SIZE];

	/*msdu counter*/
	u_int64_t txmsdu0to128;
	u_int64_t rxmsdu0to128;
	u_int64_t txmsdu128to512;
	u_int64_t rxmsdu128to512;
	u_int64_t txmsdu512to1024;
	u_int64_t rxmsdu512to1024;
	u_int64_t txmsduthan1024;
	u_int64_t rxmsduthan1024;

	/*rx data counter*/
	u_int64_t rx_ucast;
	u_int64_t rx_mcast;
	u_int64_t rx_discard;
	u_int64_t rx_frag;

	/*rx data length*/
	u_int64_t rx_ucast_bytes;
	u_int64_t rx_mcast_bytes;
	u_int64_t rx_data_bytes;
	u_int64_t rx_discard_bytes;

	/*rx mgmt counter*/
	u_int64_t rx_assreq;
	u_int64_t rx_disassoc;
	u_int64_t rx_auth;
	u_int64_t rx_deauth;
	u_int64_t rx_reassocreq;
	u_int64_t rx_probreq;

	/*tx data counter*/
	u_int64_t tx_ucast;
	u_int64_t tx_mcast;
	u_int64_t tx_frag;

	/*tx data length*/
	u_int64_t tx_ucast_bytes;
	u_int64_t tx_mcast_bytes;
	u_int64_t tx_data_bytes;

	/*tx mgmt counter*/
	u_int64_t tx_mgmt;
	u_int64_t tx_probresp;
	u_int64_t tx_assocresp;
	u_int64_t tx_reassocresp;
} zyUMAC_scn_priv_radio_stats_t;

struct zyUMAC_wal_vap_stats {
	u_int64_t unicast_tx_discard;
	u_int64_t unicast_rx_fcserr;
	u_int64_t unicast_rx_tkipmic;
	u_int64_t multicast_tx_discard;
	u_int64_t multicast_rx_fcserr;
	u_int64_t multicast_rx_tkipmic;
};

struct stats_ieee80211_snr {
	u_int64_t snr_abnormal;	/* snr >= 100dB || snr < 0dB */
	u_int64_t snr_0_9_dB;	/* 0dB <= snr < 9dB */
	u_int64_t snr_9_12_dB;	/* 9dB <= snr < 12dB */
	u_int64_t snr_12_15_dB;	/* 12dB <= snr < 15dB */
	u_int64_t snr_15_18_dB;	/* 15dB <= snr < 18dB */
	u_int64_t snr_18_21_dB;	/* 18dB <= snr < 21dB */
	u_int64_t snr_21_24_dB;	/* 21dB <= snr < 24dB */
	u_int64_t snr_24_27_dB;	/* 24dB <= snr < 27dB */
	u_int64_t snr_27_30_dB;	/* 27dB <= snr < 30dB */
	u_int64_t snr_30_33_dB;	/* 30dB <= snr < 33dB */
	u_int64_t snr_33_100_dB;	/* 33dB <= snr < 100dB */
};

struct stats_ieee80211_rssi {
	u_int64_t rssi_abnormal;	/* rssi >= 0dBm || rssi < 99dBm */
	u_int64_t rssi_60_00_dBm;	/* -60dBm <= rssi < -00dBm */
	u_int64_t rssi_63_60_dBm;	/* -63dBm <= rssi < -60dBm */
	u_int64_t rssi_66_63_dBm;	/* -66dBm <= rssi < -63dBm */
	u_int64_t rssi_69_66_dBm;	/* -69dBm <= rssi < -66dBm */
	u_int64_t rssi_72_69_dBm;	/* -72dBm <= rssi < -69dBm */
	u_int64_t rssi_75_72_dBm;	/* -75dBm <= rssi < -72dBm */
	u_int64_t rssi_78_75_dBm;	/* -78dBm <= rssi < -75dBm */
	u_int64_t rssi_81_78_dBm;	/* -81dBm <= rssi < -78dBm */
	u_int64_t rssi_84_81_dBm;	/* -84dBm <= rssi < -81dBm */
	u_int64_t rssi_87_84_dBm;	/* -87dBm <= rssi < -84dBm */
	u_int64_t rssi_90_87_dBm;	/* -90dBm <= rssi < -87dBm */
	u_int64_t rssi_99_90_dBm;	/* -99dBm <= rssi < -90dBm */
};

typedef struct zyUMAC_vap_priv_stats {
	struct zyUMAC_wal_vap_stats wal_stats;

	u_int32_t is_rx_assoc;	/* total association request */
	u_int32_t is_rx_assoc_fail;	/* total failed association request */
	u_int32_t is_rx_reassoc;	/* total association request */
	u_int32_t is_rx_reassoc_ok;	/* ok association request */
	u_int64_t is_rx_probreq;

	u_int32_t is_rx_packets;	/* total packets received       */
	u_int32_t is_rx_nunicast;	/* multicast packets received   */
	u_int32_t is_rx_unicast;	/* unicast packets received   */
	u_int32_t is_rx_bcast;	/* bcast packets received   */
	u_int32_t is_rx_mgmt;	/*rx mgmt frame */
	u_int32_t is_rx_data;	/*rx data frame */
	u_int32_t is_rx_drop;	/*rx drop frame */

	u_int64_t is_rx_bytes;	/* total bytes received         */
	u_int64_t is_rx_nunicast_bytes;	/*rx multicast bytes */
	u_int64_t is_rx_unicast_bytes;	/*rx unicast bytes */
	u_int64_t is_rx_bcast_bytes;	/*rx bcast bytes */
	u_int64_t is_rx_mgmt_bytes;	/*rx mgmt bytes */
	u_int64_t is_rx_data_bytes;	/*rx data bytes */
	u_int64_t is_rx_drop_bytes;	/*rx drop bytes */

	u_int32_t is_tx_assoc_resp;
	u_int32_t is_tx_assoc_resp_no_resource;
	u_int32_t is_tx_assoc_resp_wrong_rate;
	u_int32_t is_tx_assoc_resp_unspecified;

	u_int32_t is_tx_packets;	/* total packets transmitted    */
	u_int32_t is_tx_unicast;	/* unicast packets sent   */
	u_int32_t is_tx_bcast;	/* bcast packets sent   */
	u_int32_t is_tx_mgmt;	/*tx mgmt frame */
	u_int32_t is_tx_data;	/*tx data frame */
	u_int32_t is_tx_drop;	/*tx drop frame */

	u_int64_t is_tx_bytes;	/* total bytes transmitted      */
	u_int64_t is_tx_unicast_bytes;	/*tx unicast bytes */
	u_int64_t is_tx_bcast_bytes;	/*tx bcast bytes */
	u_int64_t is_tx_mgmt_bytes;	/*tx mgmt bytes */
	u_int64_t is_tx_data_bytes;	/*tx data bytes */
	u_int64_t is_tx_drop_bytes;	/*tx drop bytes */

	u_int32_t Last1SecPER;
	u_int32_t Last1TxFailCnt;
	u_int32_t Last1TxCnt;

	struct stats_ieee80211_snr is_snr_counter;
	struct stats_ieee80211_rssi is_rssi_counter;
} zyUMAC_vap_priv_stats_t;

struct zyUMAC_wal_node_stats {
	u_int32_t ns_rx_dup;
	u_int32_t ns_last_rx_rate;
	u_int32_t ns_last_tx_rate;
	u_int32_t ns_tx_success_num;
	u_int64_t ns_tx_success_bytes;
	u_int32_t ns_rx_success_num;
	u_int64_t ns_rx_success_bytes;
};

typedef struct zyUMAC_node_priv_stats {
	struct zyUMAC_wal_node_stats wal_stats;

	unsigned long long an_framereceive[WME_NUM_AC];
	unsigned long long an_frametransmit[WME_NUM_AC];
	unsigned long long an_framediscard[WME_NUM_AC];
	unsigned long long an_bytesdiscard[WME_NUM_AC];
	unsigned long long an_bytesreceive[WME_NUM_AC];
	unsigned long long an_bytestransmit[WME_NUM_AC];

	struct stats_ieee80211_snr ns_snr_counter;
	struct stats_ieee80211_rssi ns_rssi_counter;

	/*tx: ap-to-sta*/
	u_int64_t tx_good_bytes;
	u_int64_t tx_bad_bytes;
	u_int64_t tx_good_pkts;
	u_int64_t tx_bad_pkts;
	u_int64_t tx_data_unauthed_bytes;
	u_int64_t tx_data_unauthed_pktes;
	u_int64_t tx_data_throughput_bytes;
	u_int32_t tx_data_throughput_pkts;
	u_int64_t tx_success_last_bytes;
	u_int32_t tx_success_last_pkts;

	u_int32_t Last1SecPER;
	u_int32_t Last1TxFailCnt;
	u_int32_t Last1TxCnt;

	/*rx: sta-to-ap*/
	u_int64_t rx_data_unauthed_bytes;
	u_int64_t rx_data_unauthed_pktes;
	u_int64_t rx_data_throughput_bytes;
	u_int32_t rx_data_throughput_pkts;
	u_int64_t rx_to_stack_last_bytes;
	u_int32_t rx_to_stack_last_pkts;

	u_int32_t rx_eapol_pkts;

	/*timestamp*/
	systick_t auth_uptime;	/* auth up time */
	systick_t assoc_req_uptime;
	systick_t assoc_resp_uptime;

	/*other*/
	u_int16_t ns_rate_fail_cnt;
} zyUMAC_node_priv_stats_t;
#endif
