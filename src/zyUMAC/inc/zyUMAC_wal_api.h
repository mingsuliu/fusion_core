#ifndef __zyUMAC_WAL_API_H__
#define __zyUMAC_WAL_API_H__

#include <linux/notifier.h>

#include "zyUMAC_umac_pvt.h"
#include "zyUMAC_wal_types.h"
#include "zyUMAC_wal_api_pvt.h"
#include "zyUMAC_stats.h"
#include "zyUMAC_log.h"

#define zyUMAC_wal_wphy_setdata(dev, data)    __zyUMAC_wal_wphy_setdata(dev, data)
#define zyUMAC_wal_wphy_getdata(dev)          __zyUMAC_wal_wphy_getdata(dev)
#define zyUMAC_wal_wphy_get_id(dev)           __zyUMAC_wal_wphy_get_id(dev)
#define zyUMAC_wal_wdev_get_wphy_by_id(id)    __zyUMAC_wal_wdev_get_wphy_by_id(id)

static inline zyUMAC_wal_wphy_t zyUMAC_wal_netdev_to_wphy(struct net_device *ndev)
{
	return __zyUMAC_wal_netdev_to_wphy(ndev);
}

static inline struct net_device *zyUMAC_wal_wphy_to_netdev(zyUMAC_wal_wphy_t wphy)
{
	return __zyUMAC_wal_wphy_to_netdev(wphy);
}

static inline int zyUMAC_wal_wphy_ioctl_attach(zyUMAC_wal_wphy_t wphy,
					      zyUMAC_wal_ioctl_hook_table *
					      ioctl_hook_table)
{
	return __zyUMAC_wal_wphy_ioctl_attach(wphy, ioctl_hook_table);
}

static inline void zyUMAC_wal_wphy_ioctl_detach(zyUMAC_wal_wphy_t wphy)
{
	__zyUMAC_wal_wphy_ioctl_detach(wphy);
}

static inline struct proc_dir_entry
    *zyUMAC_wal_wphy_get_proc_dir(zyUMAC_wal_wphy_t wphy)
{
	return __zyUMAC_wal_wphy_get_proc_dir(wphy);
}

static inline int
zyUMAC_wal_wphy_get_wdev_limit(zyUMAC_wal_wphy_t wphy)
{
	return __zyUMAC_wal_wphy_get_wdev_limit(wphy);
}

static inline void
zyUMAC_wal_wphy_update_radio_stats(zyUMAC_wal_wphy_t wphy,
				  zyUMAC_scn_priv_radio_stats_t *stats)
{
	return __zyUMAC_wal_wphy_update_radio_stats(wphy, stats);
}

static inline void
zyUMAC_wal_wphy_update_channel_util(zyUMAC_wal_wphy_t wphy,
				   zyUMAC_scn_priv_radio_stats_t *stats,
				   unsigned int enable)
{
	__zyUMAC_wal_wphy_update_channel_util(wphy, stats, enable);
}

static inline void
zyUMAC_wal_wphy_set_nodesurvey(zyUMAC_wal_wphy_t wphy)
{
	__zyUMAC_wal_wphy_set_nodesurvey(wphy);
}

static inline void zyUMAC_wal_wphy_iterate_wnodes(zyUMAC_wal_wphy_t wphy,
						 zyUMAC_wal_wnode_itrator cb,
						 void *arg)
{
	__zyUMAC_wal_wphy_iterate_wnodes(wphy, cb, arg);
}

static inline zyUMAC_wal_wdev_t
zyUMAC_wal_wphy_get_first_wdev(zyUMAC_wal_wphy_t wphy)
{
	return __zyUMAC_wal_wphy_get_first_wdev(wphy);
}

static inline zyUMAC_wal_wdev_t
zyUMAC_wal_wphy_get_next_wdev(zyUMAC_wal_wphy_t wphy, zyUMAC_wal_wdev_t wdev)
{
	return __zyUMAC_wal_wphy_get_next_wdev(wphy, wdev);
}

static inline zyUMAC_wal_wnode_t
zyUMAC_wal_wphy_get_first_wnode(zyUMAC_wal_wphy_t wphy)
{
	return __zyUMAC_wal_wphy_get_first_wnode(wphy);
}

static inline zyUMAC_wal_wnode_t
zyUMAC_wal_wphy_get_next_wnode(zyUMAC_wal_wphy_t wphy, zyUMAC_wal_wnode_t wnode)
{
	return __zyUMAC_wal_wphy_get_next_wnode(wphy, wnode);
}

static inline int zyUMAC_wal_wdev_attach(zyUMAC_wal_wphy_t wphy,
					zyUMAC_wdev_hook_table * wdev_hooks)
{
	return __zyUMAC_wal_wdev_attach(wphy, wdev_hooks);
}

#define zyUMAC_wal_wdev_setdata(wdev, data)    __zyUMAC_wal_wdev_setdata(wdev, data)
#define zyUMAC_wal_wdev_getdata(wdev)          __zyUMAC_wal_wdev_getdata(wdev)
#define zyUMAC_wal_wdev_get_wphy(wdev)          __zyUMAC_wal_wdev_get_wphy(wdev)

static inline struct net_device *zyUMAC_wal_wdev_to_netdev(zyUMAC_wal_wdev_t wdev)
{
	return __zyUMAC_wal_wdev_to_netdev(wdev);
}

static inline zyUMAC_wal_wdev_t zyUMAC_wal_netdev_to_wdev(struct net_device *ndev)
{
	return __zyUMAC_wal_netdev_to_wdev(ndev);
}

static inline int zyUMAC_wal_wdev_ioctl_attach(zyUMAC_wal_wdev_t wdev,
					      zyUMAC_wal_ioctl_hook_table *
					      ioctl_hook_table)
{
	return __zyUMAC_wal_wdev_ioctl_attach(wdev, ioctl_hook_table);
}

static inline void zyUMAC_wal_wdev_ioctl_detach(void)
{
	__zyUMAC_wal_wdev_ioctl_detach();
}

#define zyUMAC_wal_wdev_is_ap_mode(wdev)     __zyUMAC_wal_wdev_is_ap_mode(wdev)

#define zyUMAC_wal_wdev_get_macaddr(wdev)    __zyUMAC_wal_wdev_get_macaddr(wdev)

static inline void zyUMAC_wal_wdev_get_ssid(zyUMAC_wal_wdev_t wdev,
					   zyUMAC_wal_ssid_t * ssid)
{
	__zyUMAC_wal_wdev_get_ssid(wdev, ssid);
}

#define zyUMAC_wal_wdev_get_chanutil(wdev) __zyUMAC_wal_wdev_get_chanutil(wdev)

static inline void
zyUMAC_wal_wdev_update_stats(zyUMAC_wal_wdev_t wdev,
			      zyUMAC_vap_priv_stats_t *stats)
{
	return __zyUMAC_wal_wdev_update_stats(wdev, stats);
}

static inline int zyUMAC_wal_wdev_acl_add(zyUMAC_wal_wdev_t vap,
					 const char *macstr,
					 u_int8_t acl_list_id)
{
	return __zyUMAC_wal_wdev_acl_add(vap, macstr, acl_list_id);
}

static inline int zyUMAC_wal_wdev_mlme_request(zyUMAC_wal_wdev_t wdev,
					      zyUMAC_wal_req_mlme_t * request)
{
	return __zyUMAC_wal_wdev_mlme_request(wdev, request);
}

static inline int zyUMAC_wal_wdev_get_txpow(zyUMAC_wal_wdev_t vap)
{
	return __zyUMAC_wal_wdev_get_txpow(vap);
}

#define zyUMAC_wal_wnode_get(wdev, mac) __zyUMAC_wal_wnode_get(wdev, mac)
#define zyUMAC_wal_wnode_put(wnode) __zyUMAC_wal_wnode_put(wnode)

#define zyUMAC_wal_wdev_get_sta_count(wdev) __zyUMAC_wal_wdev_get_sta_count(wdev)
#define zyUMAC_wal_wdev_get_proc(wdev) __zyUMAC_wal_wdev_get_proc(wdev)
#define zyUMAC_wal_wdev_get_chan(wdev) __zyUMAC_wal_wdev_get_chan(wdev)

#define zyUMAC_wal_wnode_setdata(wnode, data)    __zyUMAC_wal_wnode_setdata(wnode, data)
#define zyUMAC_wal_wnode_getdata(wnode)          __zyUMAC_wal_wnode_getdata(wnode)
#define zyUMAC_wal_wnode_get_wdev(wnode)         __zyUMAC_wal_wnode_get_wdev(wnode)
#define zyUMAC_wal_wnode_get_macaddr(wnode)      __zyUMAC_wal_wnode_get_macaddr(wnode)
#define zyUMAC_wal_wnode_get_bssid(wnode)        __zyUMAC_wal_wnode_get_bssid(wnode)
#define zyUMAC_wal_wnode_get_assocuptime(wnode)  __zyUMAC_wal_wnode_get_assocuptime(wnode)
#define zyUMAC_wal_wnode_get_associd(wnode)      __zyUMAC_wal_wnode_get_associd(wnode)
#define zyUMAC_wal_wnode_get_radioid(wnode)      __zyUMAC_wal_wnode_get_radioid(wnode)
#define zyUMAC_wal_wnode_get_rssi(wnode) \
			__zyUMAC_wal_wnode_get_rssi(wnode)
#define zyUMAC_wal_wnode_get_phymode(wnode) \
			__zyUMAC_wal_wnode_get_phymode(wnode)
#define zyUMAC_wal_wnode_get_maxrate(wnode) \
			__zyUMAC_wal_wnode_get_maxrate(wnode)

#define zyUMAC_wal_wnode_update_stats(ni, stats) \
			__zyUMAC_wal_wnode_update_stats(ni, stats)

#define zyUMAC_wal_wnode_is_authed(wnode)        __zyUMAC_wal_wnode_is_authed(wnode)

#define zyUMAC_wal_ssid_get_data(ssid)   __zyUMAC_wal_ssid_get_data(ssid)
#define zyUMAC_wal_ssid_get_len(ssid)    __zyUMAC_wal_ssid_get_len(ssid)

#define zyUMAC_wal_ieee80211_header_get_addr1(wh)	\
			__zyUMAC_wal_ieee80211_header_get_addr1(wh)
#define zyUMAC_wal_ieee80211_header_get_addr2(wh)	\
			__zyUMAC_wal_ieee80211_header_get_addr2(wh)
#define zyUMAC_wal_ieee80211_header_get_seq(wh)	\
			__zyUMAC_wal_ieee80211_header_get_seq(wh)

int zyUMAC_wal_init(zyUMAC_module_hooks_table *module_hooks);
void zyUMAC_wal_exit(void);

int zyUMAC_wal_event_register_listener(struct notifier_block *nb);
int zyUMAC_wal_event_unregister_listener(struct notifier_block *nb);
int zyUMAC_wal_event_publish(unsigned long type, void *event);

#endif
