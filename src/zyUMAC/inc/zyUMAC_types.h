#ifndef __ZYXEL_UMAC_TYPES_HEAD__
#define __ZYXEL_UMAC_TYPES_HEAD__

typedef int INT32;
typedef long long A_INT64;
typedef unsigned long long A_UINT64;
typedef unsigned long long ULONGLONG;

#ifndef QCAWIFI3
#if (NUM_SPATIAL_STREAM > 3)
#define OL_RATE_TABLE_SIZE 236
#elif (NUM_SPATIAL_STREAM == 3)
#define OL_RATE_TABLE_SIZE 180
#elif (NUM_SPATIAL_STREAM == 2)
#define OL_RATE_TABLE_SIZE 124
#else /* mandatory support */
#define OL_RATE_TABLE_SIZE 68
#endif
#define STA_INFO_POOL_SIZE          128
#else
#define OL_RATE_TABLE_SIZE          748
#define STA_INFO_POOL_SIZE          512
#endif

#ifndef WME_NUM_AC
#define WME_NUM_AC  4
#endif

#ifndef WME_AC_BE
#define WME_AC_BE	0
#endif
#ifndef WME_AC_BK
#define WME_AC_BK	1
#endif
#ifndef WME_AC_VI
#define WME_AC_VI	2
#endif
#ifndef WME_AC_VO
#define WME_AC_VO	3
#endif

typedef enum {
	zyUMAC_STATUS_SUCCESS,
	zyUMAC_STATUS_E_RESOURCES,
	zyUMAC_STATUS_E_INVAL,
	zyUMAC_STATUS_E_FAULT,
	zyUMAC_STATUS_E_BUSY,
	zyUMAC_STATUS_E_CANCELED,
	zyUMAC_STATUS_E_ABORTED,
	zyUMAC_STATUS_E_NOSUPPORT,
	zyUMAC_STATUS_E_EMPTY,
	zyUMAC_STATUS_E_EXISTS,
	zyUMAC_STATUS_E_TIMEOUT,
	zyUMAC_STATUS_E_FAILURE,
	zyUMAC_STATUS_E_NOMEM,
} zyUMAC_STATUS;

#define zyUMAC_NODE_SURVEY_FILTER(_filter) (1 << _filter)
enum zyUMAC_survey_filter {
	SURVEY_FILTER_BEACON,
	SURVEY_FILTER_ASSOC_REQ,
	SURVEY_FILTER_AUTH,
	SURVEY_FILTER_PROBE_REQ,
	SURVEY_FILTER_DUMP = 7,
};

#endif
