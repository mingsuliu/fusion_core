#ifndef __ZYXEL_UMAC_LIST_HEAD__
#define __ZYXEL_UMAC_LIST_HEAD__

#include <linux/list.h>
#include "zyUMAC_types.h"
#include "zyUMAC_log.h"

typedef struct list_head zyUMAC_list_node_t;

typedef struct _zyUMAC_list {
	zyUMAC_list_node_t anchor;
	uint32_t count;
	uint32_t max_size;
} zyUMAC_list_t;

#define zyUMAC_list_entry(ptr, type, member) \
	list_entry(ptr, type, member)

#define zyUMAC_list_for_each(pos, list) \
	list_for_each(pos, &(list.anchor))

static inline void zyUMAC_list_create(zyUMAC_list_t *list, uint32_t max_size)
{
	INIT_LIST_HEAD(&list->anchor);
	list->count = 0;
	list->max_size = max_size;
}

static inline void zyUMAC_list_deprecate_check(zyUMAC_list_t *list)
{
	if (list->count != 0) {
		zyUMAC_LOG(LOG_ERR, "list length not equal to zero");
		dump_stack();
	}
}

static inline uint32_t zyUMAC_list_size(zyUMAC_list_t *list)
{
	return list->count;
}

static inline uint32_t zyUMAC_list_max_size(zyUMAC_list_t *list)
{
	return list->max_size;
}

static inline zyUMAC_STATUS
zyUMAC_list_insert_back(zyUMAC_list_t *list, zyUMAC_list_node_t *node)
{
	list_add_tail(node, &list->anchor);
	list->count++;

	return zyUMAC_STATUS_SUCCESS;
}

static inline zyUMAC_STATUS
zyUMAC_list_remove_node(zyUMAC_list_t *list, zyUMAC_list_node_t *node_to_remove)
{
	if (list_empty(&list->anchor))
		return zyUMAC_STATUS_E_EMPTY;

	list_del(node_to_remove);

	list->count--;

	return zyUMAC_STATUS_SUCCESS;
}

#endif
