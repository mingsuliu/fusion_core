#ifndef __ZYXEL_UMAC_HEAD__
#define __ZYXEL_UMAC_HEAD__

#include "zyUMAC_umac_pvt.h"

typedef struct __zyUMAC_scn_priv zyUMAC_scn_priv_t;
typedef struct __zyUMAC_vap_priv zyUMAC_vap_priv_t;
typedef struct __zyUMAC_node_priv zyUMAC_node_priv_t;

enum record_type {
	RECORD_REASON = 0,
	RECORD_STATUS = 1
};

enum record_frame_type {
	zyUMAC_RECORD_TYPE_DEAUTH = 1,
	zyUMAC_RECORD_TYPE_DISASSOC = 2,
	zyUMAC_RECORD_TYPE_AUTH = 3,
	zyUMAC_RECORD_TYPE_ASSOC_RESP = 4,
	zyUMAC_RECORD_TYPE_DROP = 6,
};

typedef enum kickoff_type {
	zyUMAC_AC_USER_LOGOUT = 1,
	zyUMAC_IDLE_TIMEOUT = 4,
	zyUMAC_SESSION_TIMEOUT = 5,
	zyUMAC_ADMIN_RESET = 6,
	zyUMAC_USER_INFT_TRUSET = 10,
	zyUMAC_USER_ERROR = 17,
	zyUMAC_MASTER_REQUEST = 19,
	zyUMAC_RADIUS_DM = 20,
	zyUMAC_USERMISS_IP_CHANGE = 21,
	zyUMAC_GET_AAA_PROFILE_FAIL = 22,
	zyUMAC_GET_USER_ROLE_FAIL = 23,
	zyUMAC_BSS_DEL = 30,
	zyUMAC_REDUNDANCY_DEL = 31,
	zyUMAC_ROAMING_DEL = 32,
	zyUMAC_AP_MOTIFY = 50,
} kickoff_type_e;

typedef enum sta_leave_type {
	zyUMAC_AP_PROTO = 0,
	zyUMAC_STA_PROTO = 1,
	zyUMAC_AC_PROTO = 2,
	zyUMAC_AP_WEAK_RSSI = 3,
	zyUMAC_AP_DOS_ATTACK = 4,
	zyUMAC_AP_BAlANCE_STEERED = 5,
	zyUMAC_AP_LOAD_BALANCE = 6,
	zyUMAC_AP_ACL = 7,
	zyUMAC_AP_VAP_MAX_STA = 8,
	zyUMAC_AP_DP_DEL = 9,
	zyUMAC_AP_TIMEOUT = 10,
	zyUMAC_AP_KICK_OUT = 11,
	zyUMAC_AP_ROUGE_AP = 12,
	zyUMAC_AP_HOSTAPD = 13,
	zyUMAC_AP_MAC_AUTH = 14,
	zyUMAC_AP_NODE_LEAVE = 15,
	zyUMAC_AP_WIM = 16,
	zyUMAC_AP_DOT11V = 17,
	zyUMAC_AP_RADIO_MAX_STA = 18,
	zyUMAC_AP_IWPRIV_KICK_OUT = 100,
} sta_leave_type_e;
#endif
