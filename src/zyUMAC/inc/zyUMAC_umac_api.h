#ifndef __zyUMAC_UMAC_API_HEAD__
#define __zyUMAC_UMAC_API_HEAD__

#include "zyUMAC_wal_types.h"

int zyUMAC_umac_init(void);
void zyUMAC_umac_exit(void);
int zyUMAC_umac_attach(zyUMAC_wal_wphy_t wphy);
void zyUMAC_umac_detach(zyUMAC_wal_wphy_t wphy);

#endif
