#ifndef __zyUMAC_API_HEAD__
#define __zyUMAC_API_HEAD__

#define ACFG_PAD_TO_ZY_REASON      0xACF0

int zyUMAC_init(void);
void zyUMAC_exit(void);

#endif
