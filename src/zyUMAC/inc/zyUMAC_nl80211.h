#ifndef __zyUMAC_NL80211_H__
#define __zyUMAC_NL80211_H__

#include <linux/wireless.h>

/* max GENL_NAMSIZ (16) bytes: */
#define ZYXEL_UMAC_GENL_FAM_NAME "zyUMAC"

/* max GENL_NAMSIZ (16) bytes: */
#define ZYXEL_UMAC_GENL_MC_GRP_NAME    "zyUMAC-mcast-grp"
#define zyUMAC_MCGRP_MLME_NAME           "mlme"
#define zyUMAC_MCGRP_STA_INFO_NAME       "sta-info"
#define ZYXEL_UMAC_GENL_VERSION 1

enum {
	zyUMAC_C_UNSPEC,
	zyUMAC_C_ECHO,
	zyUMAC_E_DOS,
	zyUMAC_E_CHANNEL_CHANGED,
	zyUMAC_E_TXPOWER_CHANGED,
	zyUMAC_E_TRAFFIC_REPORT,
	zyUMAC_E_MLME_FAIL,
	zyUMAC_E_ACCESS_FAIL,
	zyUMAC_E_STATS_VAP_DELETE,
	zyUMAC_E_STA_LOCATION_INFO,
	zyUMAC_E_BAND_STEER_NEW_STA = 15,	//ZY_MSG_TYPE_BAND_STEER_NEW_STA

	__zyUMAC_C_MAX
};

enum {
	zyUMAC_A_UNSPEC,
	zyUMAC_A_ECHO,
	zyUMAC_A_DOS,
	zyUMAC_A_CHANNEL_CHANGED,
	zyUMAC_A_TXPOWER_CHANGED,
	zyUMAC_A_TRAFFIC_REPORT,
	zyUMAC_A_MLME_FAIL,
	zyUMAC_A_ACCESS_FAIL,
	zyUMAC_A_STATS_VAP_DELETE,
	zyUMAC_A_STA_LOCATION_INFO,
	zyUMAC_A_BAND_STEER_NEW_STA = 15,	//ZY_MSG_TYPE_BAND_STEER_NEW_STA

	__zyUMAC_A_MAX,
};
#define zyUMAC_A_MAX (__zyUMAC_A_MAX - 1)

/* multicast groups */
enum zyUMAC_multicast_groups {
	zyUMAC_MCGRP_MLME,
	zyUMAC_MCGRP_STA_INFO
};

typedef struct channel_changed {
	char device[16];
	char cause[64];
	struct iw_freq pre;
	struct iw_freq cur;
	struct iw_range range;
} channel_changed_t;

typedef struct channel_changed_msg {
	unsigned short subtype;
	unsigned short len;
	channel_changed_t data;
} channel_changed_msg_t;

#endif
