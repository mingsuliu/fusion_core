#ifndef ZYXEL_UMAC_DP_EVENTS_HEADER_H
#define ZYXEL_UMAC_DP_EVENTS_HEADER_H

enum {
	zyUMAC_DP_EVENT_ADD_STA,
	zyUMAC_DP_EVENT_DEL_STA,
	zyUMAC_DP_EVENT_SYNC_STA,
	zyUMAC_DP_EVENT_STA_AUTHED,
	zyUMAC_DP_EVENT_TXPWR_RPT,
	zyUMAC_DP_EVENT_CH_TXPWR_RPT,
	zyUMAC_DP_EVENT_TRAFFIC_RPT,
	zyUMAC_DP_EVENT_DOS_RPT,
	zyUMAC_DP_EVENT_NBR_RPT,
	zyUMAC_DP_EVENT_MAX = 0xFF
};

typedef struct _zyUMAC_dp_add_sta {
	u_int8_t *mac;
	u_int8_t *bssid;
	void *arg;
	size_t len;
	unsigned long auth_rx_time;
	unsigned long assoc_req_time;
} zyUMAC_dp_add_sta_t;

typedef struct _zyUMAC_dp_del_sta {
	u_int8_t *mac;
	u_int8_t *bssid;
	int report;
	void *arg;
	size_t len;
} zyUMAC_dp_del_sta_t;

typedef struct _zyUMAC_dp_sync_sta {
	u_int8_t *mac;
	u_int8_t *bssid;
	u_int16_t aid;
	u_int8_t subtype;
	unsigned long time;
} zyUMAC_dp_sync_sta_t;

typedef struct _zyUMAC_dp_sta_authenticated {
	u_int8_t *mac;
	u_int8_t *bssid;
	u_int8_t wpa;
	u_int8_t pairwise;
	u_int8_t macauth;
	char *username;
} zyUMAC_dp_sta_authenticated_t;

typedef struct _zyUMAC_dp_txpower_report {
	u_int8_t *type;
	u_int8_t radio_id;
	u_int8_t txpower;
} zyUMAC_dp_txpower_report_t;

typedef struct _zyUMAC_dp_channel_and_txpower_report {
	u_int8_t radio_id;
	char *info;
	unsigned long length;
} zyUMAC_dp_channel_and_txpower_report_t;

typedef struct _zyUMAC_dp_dos_report {
	u_int8_t *report;
	int size;
} zyUMAC_dp_dos_report_t;

typedef struct _zyUMAC_dp_neighbour_report {
	unsigned char *neighbour_info;
	unsigned char *ifname;
	unsigned short length;
} zyUMAC_dp_neighbour_report_t;

#endif
