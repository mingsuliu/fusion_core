#ifndef __zyUMAC_WAL_TYPES_H__
#define __zyUMAC_WAL_TYPES_H__

#include <net/iw_handler.h>
#include <zyUMAC_wal_types_pvt.h>

#define zyUMAC_IEEE80211_ADDR_LEN    MAC_ADDR_LEN
#define WPHY_NAME	"wifi"	/*reused the name of qca's driver*/

typedef __zyUMAC_wal_wphy_t zyUMAC_wal_wphy_t;
typedef __zyUMAC_wal_wdev_t zyUMAC_wal_wdev_t;
typedef __zyUMAC_wal_wnode_t zyUMAC_wal_wnode_t;

typedef __zyUMAC_wal_rx_status_t zyUMAC_wal_rx_status_t;
typedef __zyUMAC_wal_ssid zyUMAC_wal_ssid_t;

typedef void (*zyUMAC_wal_wnode_itrator) (zyUMAC_wal_wnode_t wnode, void *arg);

typedef __zyUMAC_ieee80211_header_t zyUMAC_ieee80211_header_t;

#define zyUMAC_WAL_MLME_REQ_ASSOC        __zyUMAC_WAL_MLME_REQ_ASSOC
#define zyUMAC_WAL_MLME_REQ_DISASSOC     __zyUMAC_WAL_MLME_REQ_DISASSOC
#define zyUMAC_WAL_MLME_REQ_DEAUTH       __zyUMAC_WAL_MLME_REQ_DEAUTH
#define zyUMAC_WAL_MLME_REQ_REASSOC      __zyUMAC_WAL_MLME_REQ_REASSOC

typedef struct _zyUMAC_wal_req_mlme_t {
	u_int8_t mlme_op;
	u_int16_t mlme_reason;
	u_int16_t mlme_sender;
	u_int8_t mlme_macaddr[zyUMAC_IEEE80211_ADDR_LEN];
} zyUMAC_wal_req_mlme_t;

typedef struct _zyUMAC_wal_station_txrx_stats_t {
	u_int64_t rx_bytes;
	u_int64_t tx_bytes;
	u_int32_t rx_packets;
	u_int32_t tx_packets;
} zyUMAC_wal_station_txrx_stats_t;

typedef struct _zyUMAC_module_hook_table {
	int (*module_hook_attach) (zyUMAC_wal_wphy_t wphy);
	void (*module_hook_detach) (zyUMAC_wal_wphy_t wphy);
} zyUMAC_module_hooks_table;

typedef struct _zyUMAC_wdev_hook_table {
	int (*wdev_hook_create) (zyUMAC_wal_wdev_t wdev);

	void (*wdev_hook_delete) (zyUMAC_wal_wdev_t wdev);

	int (*wdev_hook_mgmt_input_filter) (zyUMAC_wal_wnode_t ni,
					    struct sk_buff * skb, int subtype,
					    zyUMAC_wal_rx_status_t rs);

	int (*wdev_hook_mgmt_output_filter) (zyUMAC_wal_wnode_t ni,
					     struct sk_buff * skb, int subtype);

	int (*wdev_hook_rx) (zyUMAC_wal_wdev_t wdev, struct sk_buff * skb,
			     u_int16_t type, u_int16_t subtype,
			     zyUMAC_wal_rx_status_t rs);

	int (*wdev_hook_node_create) (zyUMAC_wal_wnode_t wnode);

	void (*wdev_hook_node_delete) (zyUMAC_wal_wnode_t wnode);

	int (*wdev_hook_tx_data_inspec) (zyUMAC_wal_wdev_t wdev,
					 struct sk_buff * skb);

	int (*wdev_hook_get_left_station_stats) (zyUMAC_wal_wdev_t wdev,
						 zyUMAC_wal_station_txrx_stats_t
						 * stats, const u_int8_t * mac);

} zyUMAC_wdev_hook_table;

typedef int (*iw_handler_hook) (struct net_device * dev,
				struct iw_request_info * info,
				union iwreq_data * wrqu, char *extra,
				iw_handler orig_handler);

typedef struct _zyUMAC_wal_ioctl_hook_table {
	const struct iw_priv_args *zyUMAC_params;
	int zyUMAC_params_size;
	iw_handler_hook zyUMAC_param_get_handler;
	iw_handler_hook zyUMAC_param_set_handler;
} zyUMAC_wal_ioctl_hook_table;

#endif
