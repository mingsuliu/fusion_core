#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define RADIO_BASIC_STATISTICS_FILE "radio_basic_statistics"
#define RADIO_SIGNAL_SAMPLE_FILE	"radio_signal_sample"
#define RADIO_MPDU_STATISTICS_FILE  "radio_mpdu_statistics"
#define RADIO_RXTX_STATISTICS_FILE  "radio_rxtx_statistics"
#define RADIO_BASIC_STATISTICS_FILE	"radio_basic_statistics"
#define BSS_STATISTICS_FILE         "bss_statistics"
#define STATION_STATISTICS_FILE     "station_statistics"

int g_radio_id = 0;

#define GET_ABSOLUTE_PATH(filepath, filename)	\
	do{			\
		sprintf(filepath, "/proc/wifi%d/%s", g_radio_id, filename); \
	}while(0)

#define SHOW_HELP_MSG	\
	do{			\
		printf("usage: <appname> -r <radio id> -s <statistics>\n"		\
		"<statistics>:\n"			\
		"'radio_basic_statistics'\n"    \
		"'radio_singal_sample'\n"		\
		"'radio_mpdu_statistics'\n"		\
		"'radio_rxtx_statistics'\n"		\
		"'radio_basic_statistics'\n"	\
		"'bss_statistics'\n"			\
		"'station_statistics'\n\n"		\
		"-h		show help info and exit.\n\n");			\
		return 0;	\
	}while(0)

#define SHOW_USAGE_MSG		\
	do{		\
		printf("Error: unsupported option !\n\n"	\
			"usage:\n"		\
			"<appname> \nor\n"		\
			"<appname> -r <radio id> -s <statistics>\n"		\
			"try '<appneme> -h' to get more help info.\n\n");		\
		return 0;	\
	}while(0)


int get_Radio_Stats_By_File(char *stat_info)
{	
	char filepath[256] = {0};
	FILE *fp = NULL;
	char buf[1024] = {0};
	
	GET_ABSOLUTE_PATH(filepath, stat_info);

	fp = fopen(filepath, "r");
	if (!fp) {
		printf("unsupported statistics option %s, get help '-h'", stat_info);
		return -1;
	}	

	while (fgets(buf, sizeof(buf), fp)) {
		printf("%s", buf);
	}	

	return 0;
}

int main(int argc, char *argv[])
{
	char *stat_info = NULL;
	int ret = 0, optret = 0;
	
	opterr = 0;  /* not show 'getopt' error message.*/

	while((optret = getopt(argc, argv, "hr:s:d")) != -1) {
		switch (optret) {
			case 'h':
				SHOW_HELP_MSG;
				break;
			case 'r':
				g_radio_id = atoi(optarg);
				break;
			case 's':
				stat_info = optarg;
				break;
			default :
				SHOW_USAGE_MSG;
		}
	}
	
	ret = get_Radio_Stats_By_File(stat_info);

	return ret;
}
